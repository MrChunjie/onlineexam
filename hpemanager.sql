/*
MySQL Data Transfer
Source Host: 115.159.63.20
Source Database: xuanti
Target Host: 115.159.63.20
Target Database: xuanti
Date: 2017/10/20 11:22:22
*/

SET FOREIGN_KEY_CHECKS=0;
SET SQL_Mode="NO_AUTO_VALUE_ON_ZERO";
-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `pwd` varchar(64) NOT NULL,
  `type` int(11) NOT NULL COMMENT ' 1企合2教务',
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bigquestion
-- ----------------------------
DROP TABLE IF EXISTS `bigquestion`;
CREATE TABLE `bigquestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(9) DEFAULT '1' COMMENT '1、简答题；2、编程题；',
  `courseId` int(11) NOT NULL,
  `title` varchar(2000) DEFAULT NULL,
  `contentAns` varchar(4000) DEFAULT NULL,
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for class_history
-- ----------------------------
DROP TABLE IF EXISTS `class_history`;
CREATE TABLE `class_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `s_name` varchar(30) NOT NULL,
  `spare` varchar(255) DEFAULT NULL,
  `history_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1359 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for class_num
-- ----------------------------
DROP TABLE IF EXISTS `class_num`;
CREATE TABLE `class_num` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL COMMENT ' 班级id',
  `c_num` int(11) NOT NULL COMMENT ' 班级人数',
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT ' 变更时间',
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for class_teacher
-- ----------------------------
DROP TABLE IF EXISTS `class_teacher`;
CREATE TABLE `class_teacher` (
  `class_id` int(11) NOT NULL,
  `t_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `avg` double DEFAULT NULL COMMENT ' 平均成绩',
  `hours` int(11) DEFAULT NULL COMMENT ' 课时量',
  `satisfaction` double DEFAULT NULL COMMENT ' 满意度',
  `spare` varchar(255) DEFAULT NULL COMMENT ' 备用',
  PRIMARY KEY (`class_id`,`t_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for class_type
-- ----------------------------
DROP TABLE IF EXISTS `class_type`;
CREATE TABLE `class_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for direction
-- ----------------------------
DROP TABLE IF EXISTS `direction`;
CREATE TABLE `direction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for enter_post
-- ----------------------------
DROP TABLE IF EXISTS `enter_post`;
CREATE TABLE `enter_post` (
  `enter_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`enter_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for description
-- ----------------------------
DROP TABLE IF EXISTS `description`;
CREATE TABLE `description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dimension` int(10) NOT NULL,
  `score` int(10) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `description` (`dimension`,`score`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for enterprise
-- ----------------------------
DROP TABLE IF EXISTS `enterprise`;
CREATE TABLE `enterprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL COMMENT '企业名',
  `text` varchar(10000) DEFAULT NULL COMMENT '招聘正文',
  `type` int(1) DEFAULT NULL COMMENT '企业面向方向，0全部，1开发，2测试',
  `s_time` timestamp NULL DEFAULT NULL COMMENT '报名起始时间',
  `e_time` timestamp NULL DEFAULT NULL COMMENT '报名结束时间',
  `category` int(1) DEFAULT NULL COMMENT ' 企业类别0招聘1定制',
  `spare` varchar(255) DEFAULT NULL,
  `spare_1` varchar(255) DEFAULT NULL,
  `position` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for exam
-- ----------------------------
DROP TABLE IF EXISTS `exam`;
CREATE TABLE `exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `courseid` int(11) DEFAULT NULL,
  `starttime` timestamp NULL DEFAULT NULL,
  `endtime` timestamp NULL DEFAULT NULL,
  `questions` varchar(1000) DEFAULT NULL,
  `teacherid` int(11) DEFAULT NULL,
  `classids` varchar(200) DEFAULT NULL,
  `testtime` int(11) DEFAULT NULL,
  `score` double(10,0) unsigned zerofill DEFAULT NULL,
  `score1` double(10,0) unsigned zerofill DEFAULT NULL,
  `score2` double(10,0) unsigned zerofill DEFAULT NULL,
  `scores` double(10,0) unsigned zerofill DEFAULT NULL,
  `spare` varchar(255) DEFAULT NULL,
  `spare_1` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for grade
-- ----------------------------
DROP TABLE IF EXISTS `grade`;
CREATE TABLE `grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `grade` int(5) NOT NULL,
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `group_id` int(11) NOT NULL DEFAULT '1',
  `s_id` int(11) NOT NULL,
  `score_1` double NOT NULL DEFAULT '0',
  `score_2` double NOT NULL DEFAULT '0',
  `score_all` double NOT NULL DEFAULT '0',
  `rank` int(11) DEFAULT NULL,
  `spare` varchar(255) DEFAULT '0',
  `exam_ids` varchar(255) DEFAULT NULL,
  `group_time` timestamp NULL DEFAULT NULL,
  `direction` int(3) DEFAULT NULL,
  `spare_1` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`group_id`,`s_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for position
-- ----------------------------
DROP TABLE IF EXISTS `position`;
CREATE TABLE `position` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `province` varchar(20) DEFAULT '',
  `city` varchar(20) DEFAULT '',
  `longitude` varchar(10) DEFAULT '',
  `latitude` varchar(10) DEFAULT '',
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3180 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT ' 岗位id',
  `name` varchar(30) NOT NULL COMMENT ' 岗位名称',
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` int(11) NOT NULL,
  `queType` int(11) NOT NULL COMMENT '1:单选题； ',
  `queTitle` varchar(2000) NOT NULL,
  `choiceA` varchar(2000) NOT NULL,
  `choiceB` varchar(2000) NOT NULL,
  `choiceC` varchar(2000) NOT NULL,
  `choiceD` varchar(2000) NOT NULL,
  `ans` varchar(50) NOT NULL,
  `queExist` int(11) NOT NULL,
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for s_class
-- ----------------------------
DROP TABLE IF EXISTS `s_class`;
CREATE TABLE `s_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT ' 开发：1,测试：2定制：3',
  `state` int(11) DEFAULT NULL COMMENT ' 在读：1，已毕业：2',
  `create_time` date NOT NULL,
  `other` varchar(60) DEFAULT NULL COMMENT ' 备注',
  `spare` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for s_grade
-- ----------------------------
DROP TABLE IF EXISTS `s_grade`;
CREATE TABLE `s_grade` (
  `grade_id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `value` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`grade_id`,`s_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score` (
  `s_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `time` varchar(100) DEFAULT NULL,
  `score_1` double NOT NULL DEFAULT '0',
  `score_2` double DEFAULT '0',
  `score_3` double DEFAULT '0',
  `score_b` double DEFAULT '0',
  `score_m` double NOT NULL DEFAULT '0',
  `score_all` double NOT NULL DEFAULT '0',
  `wrongqueid` varchar(2000) DEFAULT NULL,
  `wrongans` varchar(2000) DEFAULT NULL,
  `contenttitl` varchar(2000) DEFAULT NULL,
  `contentans` varchar(2000) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `createdate` timestamp NULL DEFAULT NULL,
  `bigqueids` varchar(2000) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `class_rank` int(11) DEFAULT NULL,
  `spare` varchar(255) DEFAULT NULL,
  `spare_1` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`s_id`,`exam_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `max_sign` int(1) NOT NULL,
  `register_state` int(1) DEFAULT NULL,
  PRIMARY KEY (`max_sign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sign
-- ----------------------------
DROP TABLE IF EXISTS `sign`;
CREATE TABLE `sign` (
  `enter_id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `choice` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `type_b` int(11) DEFAULT NULL COMMENT ' 笔试情况',
  `type_m` int(11) DEFAULT NULL COMMENT ' 面试情况',
  `money` varchar(30) DEFAULT NULL COMMENT ' 薪资范围',
  `state` int(11) DEFAULT NULL COMMENT ' 录取状态0：未录取1：已录取',
  `spare` varchar(255) DEFAULT NULL,
  `position` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`enter_id`,`s_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `s_name` varchar(30) NOT NULL,
  `s_pwd` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for studentinfo
-- ----------------------------
DROP TABLE IF EXISTS `studentinfo`;
CREATE TABLE `studentinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` varchar(30) DEFAULT NULL COMMENT ' 原学校学号',
  `new_id` int(11) DEFAULT NULL COMMENT ' 慧与学号',
  `name` varchar(30) NOT NULL COMMENT '姓名',
  `tel` varchar(30) NOT NULL COMMENT '电话',
  `number` varchar(60) NOT NULL COMMENT ' 身份证号',
  `sex` varchar(10) DEFAULT NULL,
  `nation` varchar(10) DEFAULT NULL COMMENT ' 民族',
  `home` varchar(200) DEFAULT NULL COMMENT ' 家庭住址',
  `parent` varchar(200) DEFAULT NULL COMMENT ' 父母姓名及联系方式',
  `dorm` varchar(200) DEFAULT NULL COMMENT ' 宿舍号',
  `school` varchar(60) NOT NULL COMMENT '学校',
  `college` varchar(60) NOT NULL COMMENT '学院',
  `major` varchar(60) NOT NULL COMMENT '专业',
  `school_class` varchar(30) NOT NULL COMMENT '班级',
  `job` varchar(30) DEFAULT NULL COMMENT ' 职务',
  `counselor` varchar(200) DEFAULT NULL COMMENT ' 辅导员姓名及电话',
  `english` varchar(60) DEFAULT NULL COMMENT ' 外语水平',
  `skill` varchar(1000) DEFAULT NULL COMMENT '个人特长及获奖情况',
  `stu_order` varchar(60) DEFAULT NULL COMMENT ' 目前意向',
  `restudyname` varchar(200) DEFAULT NULL COMMENT ' 重修课名称',
  `restudynum` int(11) DEFAULT NULL COMMENT ' 重修课数量',
  `restudytime` varchar(200) DEFAULT NULL COMMENT ' 预计补考时间',
  `state` varchar(10) NOT NULL DEFAULT '' COMMENT '状态',
  `direction` varchar(10) DEFAULT NULL COMMENT '方向',
  `spare` varchar(255) DEFAULT NULL,
  `spare_1` varchar(255) DEFAULT NULL,
  `position` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `pwd` varchar(64) NOT NULL,
  `class_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Procedure structure for proc_adder
-- ----------------------------
DROP PROCEDURE IF EXISTS `proc_adder`;
DELIMITER ;;
CREATE PROCEDURE `proc_adder`(in studentId int,in theBeginDay VARCHAR(20),in theEndDay VARCHAR(20))
BEGIN

SET @str_tmp = '';
SET @EE = '';
SELECT
	@EE := CONCAT(
		@EE,
		'MAX(CASE exam_id WHEN \'',
		id,
		'\' THEN concat(score_b,\',\',score_m,\',\',cast(score_all as decimal(6,2))) ELSE \'\' END ) AS ',
		'\'',
		`name`,
		'\'',
		','
	) AS aa INTO @str_tmp
FROM
	(
		SELECT DISTINCT
			e.id,e.name
		FROM
			score s
		LEFT JOIN studentinfo si ON s.s_id = si.id
		LEFT JOIN exam e ON s.exam_id = e.id
		WHERE
			s.createdate > theBeginDay
			and s.createdate < theEndDay
		ORDER BY
			e.id
	) A
ORDER BY
	length(aa) DESC
LIMIT 1;


SET @QQ = CONCAT(
	'SELECT ',
	ifnull(
	LEFT (
		@str_tmp,
		char_length(@str_tmp) - 1
	),'1'),
	' FROM score s ',
	' where s.createdate < \'',theEndDay,'\' and s.createdate > \'',theBeginDay,'\' and s_id =',studentId,
  ' GROUP BY s_id '
);

PREPARE stmt
FROM
	@QQ;

EXECUTE stmt;

DEALLOCATE PREPARE stmt;

END;;
DELIMITER ;

-- ----------------------------
-- Records
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin1', '7C4A8D09CA3762AF61E59520943DC26494F8941B', '1', '0');
INSERT INTO `admin` VALUES ('2', 'admin2', '7C4A8D09CA3762AF61E59520943DC26494F8941B', '2', '0');
INSERT INTO `class_num` VALUES ('1', '0', '0', '2017-10-19 09:53:50', null);
INSERT INTO `description` VALUES ('1', '1', '60', '你的沟通能力有待提高哦', null);
INSERT INTO `description` VALUES ('2', '1', '61', '你的沟通能力有待提高哦', null);
INSERT INTO `description` VALUES ('3', '1', '62', '你的沟通能力有待提高哦', null);
INSERT INTO `description` VALUES ('4', '1', '63', '你的沟通能力有待提高哦', null);
INSERT INTO `description` VALUES ('5', '1', '64', '你的沟通能力有待提高哦', null);
INSERT INTO `description` VALUES ('6', '1', '65', '你的沟通能力有待提高哦', null);
INSERT INTO `description` VALUES ('7', '1', '66', '你的沟通能力有待提高哦', null);
INSERT INTO `description` VALUES ('8', '1', '67', '你的沟通能力有待提高哦', null);
INSERT INTO `description` VALUES ('9', '1', '68', '你的沟通能力有待提高哦', null);
INSERT INTO `description` VALUES ('10', '1', '69', '你的沟通能力有待提高哦', null);
INSERT INTO `description` VALUES ('11', '1', '70', '你的沟通能力有望超越大多数人', null);
INSERT INTO `description` VALUES ('12', '1', '71', '你的沟通能力有望超越大多数人', null);
INSERT INTO `description` VALUES ('13', '1', '72', '你的沟通能力有望超越大多数人', null);
INSERT INTO `description` VALUES ('14', '1', '73', '你的沟通能力有望超越大多数人', null);
INSERT INTO `description` VALUES ('15', '1', '74', '你的沟通能力有望超越大多数人', null);
INSERT INTO `description` VALUES ('16', '1', '75', '你的沟通能力有望超越大多数人', null);
INSERT INTO `description` VALUES ('17', '1', '76', '你的沟通能力有望超越大多数人', null);
INSERT INTO `description` VALUES ('18', '1', '77', '你的沟通能力有望超越大多数人', null);
INSERT INTO `description` VALUES ('19', '1', '78', '你的沟通能力有望超越大多数人', null);
INSERT INTO `description` VALUES ('20', '1', '79', '你的沟通能力有望超越大多数人', null);
INSERT INTO `description` VALUES ('21', '1', '80', '你的沟通能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('22', '1', '81', '你的沟通能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('23', '1', '82', '你的沟通能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('24', '1', '83', '你的沟通能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('25', '1', '84', '你的沟通能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('26', '1', '85', '你的沟通能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('27', '1', '86', '你的沟通能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('28', '1', '87', '你的沟通能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('29', '1', '88', '你的沟通能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('30', '1', '89', '你的沟通能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('31', '1', '90', '你的沟通能力令众人倾服', null);
INSERT INTO `description` VALUES ('32', '1', '91', '你的沟通能力令众人倾服', null);
INSERT INTO `description` VALUES ('33', '1', '92', '你的沟通能力令众人倾服', null);
INSERT INTO `description` VALUES ('34', '1', '93', '你的沟通能力令众人倾服', null);
INSERT INTO `description` VALUES ('35', '1', '94', '你的沟通能力令众人倾服', null);
INSERT INTO `description` VALUES ('36', '1', '95', '你的沟通能力令众人倾服', null);
INSERT INTO `description` VALUES ('37', '1', '96', '你的沟通能力令众人倾服', null);
INSERT INTO `description` VALUES ('38', '1', '97', '你的沟通能力令众人倾服', null);
INSERT INTO `description` VALUES ('39', '1', '98', '你的沟通能力令众人倾服', null);
INSERT INTO `description` VALUES ('40', '1', '99', '你的沟通能力令众人倾服', null);
INSERT INTO `description` VALUES ('41', '1', '100', '你的沟通能力令众人倾服', null);
INSERT INTO `description` VALUES ('42', '2', '60', '你的学习能力有待提高哦', null);
INSERT INTO `description` VALUES ('43', '2', '61', '你的学习能力有待提高哦', null);
INSERT INTO `description` VALUES ('44', '2', '62', '你的学习能力有待提高哦', null);
INSERT INTO `description` VALUES ('45', '2', '63', '你的学习能力有待提高哦', null);
INSERT INTO `description` VALUES ('46', '2', '64', '你的学习能力有待提高哦', null);
INSERT INTO `description` VALUES ('47', '2', '65', '你的学习能力有待提高哦', null);
INSERT INTO `description` VALUES ('48', '2', '66', '你的学习能力有待提高哦', null);
INSERT INTO `description` VALUES ('49', '2', '67', '你的学习能力有待提高哦', null);
INSERT INTO `description` VALUES ('50', '2', '68', '你的学习能力有待提高哦', null);
INSERT INTO `description` VALUES ('51', '2', '69', '你的学习能力有待提高哦', null);
INSERT INTO `description` VALUES ('52', '2', '70', '你的学习成绩还可以更棒', null);
INSERT INTO `description` VALUES ('53', '2', '71', '你的学习成绩还可以更棒', null);
INSERT INTO `description` VALUES ('54', '2', '72', '你的学习成绩还可以更棒', null);
INSERT INTO `description` VALUES ('55', '2', '73', '你的学习成绩还可以更棒', null);
INSERT INTO `description` VALUES ('56', '2', '74', '你的学习成绩还可以更棒', null);
INSERT INTO `description` VALUES ('57', '2', '75', '你的学习成绩还可以更棒', null);
INSERT INTO `description` VALUES ('58', '2', '76', '你的学习成绩还可以更棒', null);
INSERT INTO `description` VALUES ('59', '2', '77', '你的学习成绩还可以更棒', null);
INSERT INTO `description` VALUES ('60', '2', '78', '你的学习成绩还可以更棒', null);
INSERT INTO `description` VALUES ('61', '2', '79', '你的学习成绩还可以更棒', null);
INSERT INTO `description` VALUES ('62', '2', '80', '你的学习成绩已经超越大多数人了', null);
INSERT INTO `description` VALUES ('63', '2', '81', '你的学习成绩已经超越大多数人了', null);
INSERT INTO `description` VALUES ('64', '2', '82', '你的学习成绩已经超越大多数人了', null);
INSERT INTO `description` VALUES ('65', '2', '83', '你的学习成绩已经超越大多数人了', null);
INSERT INTO `description` VALUES ('66', '2', '84', '你的学习成绩已经超越大多数人了', null);
INSERT INTO `description` VALUES ('67', '2', '85', '你的学习成绩已经超越大多数人了', null);
INSERT INTO `description` VALUES ('68', '2', '86', '你的学习成绩已经超越大多数人了', null);
INSERT INTO `description` VALUES ('69', '2', '87', '你的学习成绩已经超越大多数人了', null);
INSERT INTO `description` VALUES ('70', '2', '88', '你的学习成绩已经超越大多数人了', null);
INSERT INTO `description` VALUES ('71', '2', '89', '你的学习成绩已经超越大多数人了', null);
INSERT INTO `description` VALUES ('72', '2', '90', '你的学习成绩登峰造极', null);
INSERT INTO `description` VALUES ('73', '2', '91', '你的学习成绩登峰造极', null);
INSERT INTO `description` VALUES ('74', '2', '92', '你的学习成绩登峰造极', null);
INSERT INTO `description` VALUES ('75', '2', '93', '你的学习成绩登峰造极', null);
INSERT INTO `description` VALUES ('76', '2', '94', '你的学习成绩登峰造极', null);
INSERT INTO `description` VALUES ('77', '2', '95', '你的学习成绩登峰造极', null);
INSERT INTO `description` VALUES ('78', '2', '96', '你的学习成绩登峰造极', null);
INSERT INTO `description` VALUES ('79', '2', '97', '你的学习成绩登峰造极', null);
INSERT INTO `description` VALUES ('80', '2', '98', '你的学习成绩登峰造极', null);
INSERT INTO `description` VALUES ('81', '2', '99', '你的学习成绩登峰造极', null);
INSERT INTO `description` VALUES ('82', '2', '100', '你的学习成绩登峰造极', null);
INSERT INTO `description` VALUES ('83', '3', '60', '你的实践能力有待提高哦', null);
INSERT INTO `description` VALUES ('84', '3', '61', '你的实践能力有待提高哦', null);
INSERT INTO `description` VALUES ('85', '3', '62', '你的实践能力有待提高哦', null);
INSERT INTO `description` VALUES ('86', '3', '63', '你的实践能力有待提高哦', null);
INSERT INTO `description` VALUES ('87', '3', '64', '你的实践能力有待提高哦', null);
INSERT INTO `description` VALUES ('88', '3', '65', '你的实践能力有待提高哦', null);
INSERT INTO `description` VALUES ('89', '3', '66', '你的实践能力有待提高哦', null);
INSERT INTO `description` VALUES ('90', '3', '67', '你的实践能力有待提高哦', null);
INSERT INTO `description` VALUES ('91', '3', '68', '你的实践能力有待提高哦', null);
INSERT INTO `description` VALUES ('92', '3', '69', '你的实践能力有待提高哦', null);
INSERT INTO `description` VALUES ('93', '3', '70', '你的实践能力还可以更棒', null);
INSERT INTO `description` VALUES ('94', '3', '71', '你的实践能力还可以更棒', null);
INSERT INTO `description` VALUES ('95', '3', '72', '你的实践能力还可以更棒', null);
INSERT INTO `description` VALUES ('96', '3', '73', '你的实践能力还可以更棒', null);
INSERT INTO `description` VALUES ('97', '3', '74', '你的实践能力还可以更棒', null);
INSERT INTO `description` VALUES ('98', '3', '75', '你的实践能力还可以更棒', null);
INSERT INTO `description` VALUES ('99', '3', '76', '你的实践能力还可以更棒', null);
INSERT INTO `description` VALUES ('100', '3', '77', '你的实践能力还可以更棒', null);
INSERT INTO `description` VALUES ('101', '3', '78', '你的实践能力还可以更棒', null);
INSERT INTO `description` VALUES ('102', '3', '79', '你的实践能力还可以更棒', null);
INSERT INTO `description` VALUES ('103', '3', '80', '你的实践能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('104', '3', '81', '你的实践能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('105', '3', '82', '你的实践能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('106', '3', '83', '你的实践能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('107', '3', '84', '你的实践能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('108', '3', '85', '你的实践能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('109', '3', '86', '你的实践能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('110', '3', '87', '你的实践能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('111', '3', '88', '你的实践能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('112', '3', '89', '你的实践能力已经超越大多数人了', null);
INSERT INTO `description` VALUES ('113', '3', '90', '你的实践能力足以胜任任何工作', null);
INSERT INTO `description` VALUES ('114', '3', '91', '你的实践能力足以胜任任何工作', null);
INSERT INTO `description` VALUES ('115', '3', '92', '你的实践能力足以胜任任何工作', null);
INSERT INTO `description` VALUES ('116', '3', '93', '你的实践能力足以胜任任何工作', null);
INSERT INTO `description` VALUES ('117', '3', '94', '你的实践能力足以胜任任何工作', null);
INSERT INTO `description` VALUES ('118', '3', '95', '你的实践能力足以胜任任何工作', null);
INSERT INTO `description` VALUES ('119', '3', '96', '你的实践能力足以胜任任何工作', null);
INSERT INTO `description` VALUES ('120', '3', '97', '你的实践能力足以胜任任何工作', null);
INSERT INTO `description` VALUES ('121', '3', '98', '你的实践能力足以胜任任何工作', null);
INSERT INTO `description` VALUES ('122', '3', '99', '你的实践能力足以胜任任何工作', null);
INSERT INTO `description` VALUES ('123', '3', '100', '你的实践能力足以胜任任何工作', null);
INSERT INTO `description` VALUES ('124', '4', '60', '你的提升有点慢哦', null);
INSERT INTO `description` VALUES ('125', '4', '61', '你的提升有点慢哦', null);
INSERT INTO `description` VALUES ('126', '4', '62', '你的提升有点慢哦', null);
INSERT INTO `description` VALUES ('127', '4', '63', '你的提升有点慢哦', null);
INSERT INTO `description` VALUES ('128', '4', '64', '你的提升有点慢哦', null);
INSERT INTO `description` VALUES ('129', '4', '65', '你的提升有点慢哦', null);
INSERT INTO `description` VALUES ('130', '4', '66', '你的提升有点慢哦', null);
INSERT INTO `description` VALUES ('131', '4', '67', '你的提升有点慢哦', null);
INSERT INTO `description` VALUES ('132', '4', '68', '你的提升有点慢哦', null);
INSERT INTO `description` VALUES ('133', '4', '69', '你的提升有点慢哦', null);
INSERT INTO `description` VALUES ('134', '4', '70', '你的提升空间还很大', null);
INSERT INTO `description` VALUES ('135', '4', '71', '你的提升空间还很大', null);
INSERT INTO `description` VALUES ('136', '4', '72', '你的提升空间还很大', null);
INSERT INTO `description` VALUES ('137', '4', '73', '你的提升空间还很大', null);
INSERT INTO `description` VALUES ('138', '4', '74', '你的提升空间还很大', null);
INSERT INTO `description` VALUES ('139', '4', '75', '你的提升空间还很大', null);
INSERT INTO `description` VALUES ('140', '4', '76', '你的提升空间还很大', null);
INSERT INTO `description` VALUES ('141', '4', '77', '你的提升空间还很大', null);
INSERT INTO `description` VALUES ('142', '4', '78', '你的提升空间还很大', null);
INSERT INTO `description` VALUES ('143', '4', '79', '你的提升空间还很大', null);
INSERT INTO `description` VALUES ('144', '4', '80', '你的进步已经超越大多数人了', null);
INSERT INTO `description` VALUES ('145', '4', '81', '你的进步已经超越大多数人了', null);
INSERT INTO `description` VALUES ('146', '4', '82', '你的进步已经超越大多数人了', null);
INSERT INTO `description` VALUES ('147', '4', '83', '你的进步已经超越大多数人了', null);
INSERT INTO `description` VALUES ('148', '4', '84', '你的进步已经超越大多数人了', null);
INSERT INTO `description` VALUES ('149', '4', '85', '你的进步已经超越大多数人了', null);
INSERT INTO `description` VALUES ('150', '4', '86', '你的进步已经超越大多数人了', null);
INSERT INTO `description` VALUES ('151', '4', '87', '你的进步已经超越大多数人了', null);
INSERT INTO `description` VALUES ('152', '4', '88', '你的进步已经超越大多数人了', null);
INSERT INTO `description` VALUES ('153', '4', '89', '你的进步已经超越大多数人了', null);
INSERT INTO `description` VALUES ('154', '4', '90', '你的进步超越所有人的想象', null);
INSERT INTO `description` VALUES ('155', '4', '91', '你的进步超越所有人的想象', null);
INSERT INTO `description` VALUES ('156', '4', '92', '你的进步超越所有人的想象', null);
INSERT INTO `description` VALUES ('157', '4', '93', '你的进步超越所有人的想象', null);
INSERT INTO `description` VALUES ('158', '4', '94', '你的进步超越所有人的想象', null);
INSERT INTO `description` VALUES ('159', '4', '95', '你的进步超越所有人的想象', null);
INSERT INTO `description` VALUES ('160', '4', '96', '你的进步超越所有人的想象', null);
INSERT INTO `description` VALUES ('161', '4', '97', '你的进步超越所有人的想象', null);
INSERT INTO `description` VALUES ('162', '4', '98', '你的进步超越所有人的想象', null);
INSERT INTO `description` VALUES ('163', '4', '99', '你的进步超越所有人的想象', null);
INSERT INTO `description` VALUES ('164', '4', '100', '你的进步超越所有人的想象', null);
INSERT INTO `description` VALUES ('165', '5', '60', '你的技能丰富度有点低哦', null);
INSERT INTO `description` VALUES ('166', '5', '61', '你的技能丰富度有点低哦', null);
INSERT INTO `description` VALUES ('167', '5', '62', '你的技能丰富度有点低哦', null);
INSERT INTO `description` VALUES ('168', '5', '63', '你的技能丰富度有点低哦', null);
INSERT INTO `description` VALUES ('169', '5', '64', '你的技能丰富度有点低哦', null);
INSERT INTO `description` VALUES ('170', '5', '65', '你的技能丰富度有点低哦', null);
INSERT INTO `description` VALUES ('171', '5', '66', '你的技能丰富度有点低哦', null);
INSERT INTO `description` VALUES ('172', '5', '67', '你的技能丰富度有点低哦', null);
INSERT INTO `description` VALUES ('173', '5', '68', '你的技能丰富度有点低哦', null);
INSERT INTO `description` VALUES ('174', '5', '69', '你的技能丰富度有点低哦', null);
INSERT INTO `description` VALUES ('175', '5', '70', '你的技能丰富度还能比现在更好', null);
INSERT INTO `description` VALUES ('176', '5', '71', '你的技能丰富度还能比现在更好', null);
INSERT INTO `description` VALUES ('177', '5', '72', '你的技能丰富度还能比现在更好', null);
INSERT INTO `description` VALUES ('178', '5', '73', '你的技能丰富度还能比现在更好', null);
INSERT INTO `description` VALUES ('179', '5', '74', '你的技能丰富度还能比现在更好', null);
INSERT INTO `description` VALUES ('180', '5', '75', '你的技能丰富度还能比现在更好', null);
INSERT INTO `description` VALUES ('181', '5', '76', '你的技能丰富度还能比现在更好', null);
INSERT INTO `description` VALUES ('182', '5', '77', '你的技能丰富度还能比现在更好', null);
INSERT INTO `description` VALUES ('183', '5', '78', '你的技能丰富度还能比现在更好', null);
INSERT INTO `description` VALUES ('184', '5', '79', '你的技能丰富度还能比现在更好', null);
INSERT INTO `description` VALUES ('185', '5', '80', '你的技能丰富度已经超越大多数人了', null);
INSERT INTO `description` VALUES ('186', '5', '81', '你的技能丰富度已经超越大多数人了', null);
INSERT INTO `description` VALUES ('187', '5', '82', '你的技能丰富度已经超越大多数人了', null);
INSERT INTO `description` VALUES ('188', '5', '83', '你的技能丰富度已经超越大多数人了', null);
INSERT INTO `description` VALUES ('189', '5', '84', '你的技能丰富度已经超越大多数人了', null);
INSERT INTO `description` VALUES ('190', '5', '85', '你的技能丰富度已经超越大多数人了', null);
INSERT INTO `description` VALUES ('191', '5', '86', '你的技能丰富度已经超越大多数人了', null);
INSERT INTO `description` VALUES ('192', '5', '87', '你的技能丰富度已经超越大多数人了', null);
INSERT INTO `description` VALUES ('193', '5', '88', '你的技能丰富度已经超越大多数人了', null);
INSERT INTO `description` VALUES ('194', '5', '89', '你的技能丰富度已经超越大多数人了', null);
INSERT INTO `description` VALUES ('195', '5', '90', '你的技能丰富度已经达到顶峰了', null);
INSERT INTO `description` VALUES ('196', '5', '91', '你的技能丰富度已经达到顶峰了', null);
INSERT INTO `description` VALUES ('197', '5', '92', '你的技能丰富度已经达到顶峰了', null);
INSERT INTO `description` VALUES ('198', '5', '93', '你的技能丰富度已经达到顶峰了', null);
INSERT INTO `description` VALUES ('199', '5', '94', '你的技能丰富度已经达到顶峰了', null);
INSERT INTO `description` VALUES ('200', '5', '95', '你的技能丰富度已经达到顶峰了', null);
INSERT INTO `description` VALUES ('201', '5', '96', '你的技能丰富度已经达到顶峰了', null);
INSERT INTO `description` VALUES ('202', '5', '97', '你的技能丰富度已经达到顶峰了', null);
INSERT INTO `description` VALUES ('203', '5', '98', '你的技能丰富度已经达到顶峰了', null);
INSERT INTO `description` VALUES ('204', '5', '99', '你的技能丰富度已经达到顶峰了', null);
INSERT INTO `description` VALUES ('205', '5', '100', '你的技能丰富度已经达到顶峰了', null);
INSERT INTO `direction` VALUES ('-1', '短训', null);
INSERT INTO `direction` VALUES ('1', '开发', null);
INSERT INTO `direction` VALUES ('2', '测试', null);
INSERT INTO `direction` VALUES ('3', '运维', null);
INSERT INTO `direction` VALUES ('4', '未分班', null);
INSERT INTO `direction` VALUES ('0', '定制', null);
INSERT INTO `position` VALUES ('1', '北京市', '北京市', '116.40', '39.90', null);
INSERT INTO `position` VALUES ('21', '天津市', '天津市', '117.20', '39.12', null);
INSERT INTO `position` VALUES ('41', '河北省', '石家庄市', '114.52', '38.05', null);
INSERT INTO `position` VALUES ('70', '河北省', '唐山市', '118.20', '39.63', null);
INSERT INTO `position` VALUES ('85', '河北省', '秦皇岛市', '119.60', '39.93', null);
INSERT INTO `position` VALUES ('93', '河北省', '邯郸市', '114.48', '36.62', null);
INSERT INTO `position` VALUES ('113', '河北省', '邢台市', '114.48', '37.07', null);
INSERT INTO `position` VALUES ('137', '河北省', '保定市', '115.47', '38.87', null);
INSERT INTO `position` VALUES ('163', '河北省', '张家口市', '114.88', '40.82', null);
INSERT INTO `position` VALUES ('185', '河北省', '承德市', '117.93', '40.97', null);
INSERT INTO `position` VALUES ('197', '河北省', '沧州市', '116.83', '38.30', null);
INSERT INTO `position` VALUES ('215', '河北省', '廊坊市', '116.70', '39.52', null);
INSERT INTO `position` VALUES ('226', '河北省', '衡水市', '115.68', '37.73', null);
INSERT INTO `position` VALUES ('238', '山西省', '太原市', '112.55', '37.87', null);
INSERT INTO `position` VALUES ('249', '山西省', '大同市', '113.30', '40.08', null);
INSERT INTO `position` VALUES ('265', '山西省', '阳泉市', '113.57', '37.85', null);
INSERT INTO `position` VALUES ('276', '山西省', '长治市', '113.12', '36.20', null);
INSERT INTO `position` VALUES ('294', '山西省', '晋城市', '112.83', '35.50', null);
INSERT INTO `position` VALUES ('304', '山西省', '朔州市', '112.43', '39.33', null);
INSERT INTO `position` VALUES ('310', '山西省', '晋中市', '112.75', '37.68', null);
INSERT INTO `position` VALUES ('322', '山西省', '运城市', '110.98', '35.02', null);
INSERT INTO `position` VALUES ('335', '山西省', '忻州市', '112.73', '38.42', null);
INSERT INTO `position` VALUES ('349', '山西省', '临汾市', '111.52', '36.08', null);
INSERT INTO `position` VALUES ('367', '山西省', '吕梁市', '111.13', '37.52', null);
INSERT INTO `position` VALUES ('381', '内蒙古自治区', '呼和浩特市', '111.73', '40.83', null);
INSERT INTO `position` VALUES ('391', '内蒙古自治区', '包头市', '109.83', '40.65', null);
INSERT INTO `position` VALUES ('400', '内蒙古自治区', '乌海市', '106.82', '39.67', null);
INSERT INTO `position` VALUES ('404', '内蒙古自治区', '赤峰市', '118.92', '42.27', null);
INSERT INTO `position` VALUES ('417', '内蒙古自治区', '通辽市', '122.27', '43.62', null);
INSERT INTO `position` VALUES ('426', '内蒙古自治区', '鄂尔多斯市', '109.80', '39.62', null);
INSERT INTO `position` VALUES ('435', '内蒙古自治区', '呼伦贝尔市', '119.77', '49.22', null);
INSERT INTO `position` VALUES ('448', '内蒙古自治区', '巴彦淖尔市', '107.42', '40.75', null);
INSERT INTO `position` VALUES ('456', '内蒙古自治区', '乌兰察布市', '113.12', '40.98', null);
INSERT INTO `position` VALUES ('492', '辽宁省', '沈阳市', '123.43', '41.80', null);
INSERT INTO `position` VALUES ('507', '辽宁省', '大连市', '121.62', '38.92', null);
INSERT INTO `position` VALUES ('518', '辽宁省', '鞍山市', '122.98', '41.10', null);
INSERT INTO `position` VALUES ('527', '辽宁省', '抚顺市', '123.98', '41.88', null);
INSERT INTO `position` VALUES ('535', '辽宁省', '本溪市', '123.77', '41.30', null);
INSERT INTO `position` VALUES ('542', '辽宁省', '丹东市', '124.38', '40.13', null);
INSERT INTO `position` VALUES ('549', '辽宁省', '锦州市', '121.13', '41.10', null);
INSERT INTO `position` VALUES ('556', '辽宁省', '营口市', '122.23', '40.67', null);
INSERT INTO `position` VALUES ('563', '辽宁省', '阜新市', '121.67', '42.02', null);
INSERT INTO `position` VALUES ('570', '辽宁省', '辽阳市', '123.17', '41.27', null);
INSERT INTO `position` VALUES ('578', '辽宁省', '盘锦市', '122.07', '41.12', null);
INSERT INTO `position` VALUES ('583', '辽宁省', '铁岭市', '123.83', '42.28', null);
INSERT INTO `position` VALUES ('591', '辽宁省', '朝阳市', '120.45', '41.57', null);
INSERT INTO `position` VALUES ('598', '辽宁省', '葫芦岛市', '120.83', '40.72', null);
INSERT INTO `position` VALUES ('605', '吉林省', '长春市', '125.32', '43.90', null);
INSERT INTO `position` VALUES ('616', '吉林省', '吉林市', '126.55', '43.83', null);
INSERT INTO `position` VALUES ('626', '吉林省', '四平市', '124.35', '43.17', null);
INSERT INTO `position` VALUES ('633', '吉林省', '辽源市', '125.13', '42.88', null);
INSERT INTO `position` VALUES ('638', '吉林省', '通化市', '125.93', '41.73', null);
INSERT INTO `position` VALUES ('646', '吉林省', '白山市', '126.42', '41.93', null);
INSERT INTO `position` VALUES ('652', '吉林省', '松原市', '124.82', '45.13', null);
INSERT INTO `position` VALUES ('657', '吉林省', '白城市', '122.83', '45.62', null);
INSERT INTO `position` VALUES ('672', '黑龙江省', '哈尔滨市', '126.53', '45.80', null);
INSERT INTO `position` VALUES ('690', '黑龙江省', '齐齐哈尔市', '123.95', '47.33', null);
INSERT INTO `position` VALUES ('705', '黑龙江省', '鸡西市', '130.97', '45.30', null);
INSERT INTO `position` VALUES ('715', '黑龙江省', '鹤岗市', '130.27', '47.33', null);
INSERT INTO `position` VALUES ('725', '黑龙江省', '双鸭山市', '131.15', '46.63', null);
INSERT INTO `position` VALUES ('734', '黑龙江省', '大庆市', '125.03', '46.58', null);
INSERT INTO `position` VALUES ('744', '黑龙江省', '伊春市', '128.90', '47.73', null);
INSERT INTO `position` VALUES ('761', '黑龙江省', '佳木斯市', '130.37', '46.82', null);
INSERT INTO `position` VALUES ('773', '黑龙江省', '七台河市', '130.95', '45.78', null);
INSERT INTO `position` VALUES ('778', '黑龙江省', '牡丹江市', '129.60', '44.58', null);
INSERT INTO `position` VALUES ('789', '黑龙江省', '黑河市', '127.48', '50.25', null);
INSERT INTO `position` VALUES ('795', '黑龙江省', '绥化市', '126.98', '46.63', null);
INSERT INTO `position` VALUES ('810', '上海市', '上海市', '121.47', '31.23', null);
INSERT INTO `position` VALUES ('830', '江苏省', '南京市', '118.78', '32.07', null);
INSERT INTO `position` VALUES ('845', '江苏省', '无锡市', '120.30', '31.57', null);
INSERT INTO `position` VALUES ('854', '江苏省', '徐州市', '117.18', '34.27', null);
INSERT INTO `position` VALUES ('867', '江苏省', '常州市', '119.95', '31.78', null);
INSERT INTO `position` VALUES ('875', '江苏省', '苏州市', '120.58', '31.30', null);
INSERT INTO `position` VALUES ('887', '江苏省', '南通市', '120.88', '31.98', null);
INSERT INTO `position` VALUES ('896', '江苏省', '连云港市', '119.22', '34.60', null);
INSERT INTO `position` VALUES ('904', '江苏省', '淮安市', '119.02', '33.62', null);
INSERT INTO `position` VALUES ('913', '江苏省', '盐城市', '120.15', '33.35', null);
INSERT INTO `position` VALUES ('923', '江苏省', '扬州市', '119.40', '32.40', null);
INSERT INTO `position` VALUES ('931', '江苏省', '镇江市', '119.45', '32.20', null);
INSERT INTO `position` VALUES ('938', '江苏省', '泰州市', '119.92', '32.45', null);
INSERT INTO `position` VALUES ('943', '江苏省', '宿迁市', '118.28', '33.97', null);
INSERT INTO `position` VALUES ('949', '浙江省', '杭州市', '120.15', '30.28', null);
INSERT INTO `position` VALUES ('963', '浙江省', '宁波市', '121.55', '29.88', null);
INSERT INTO `position` VALUES ('975', '浙江省', '温州市', '120.70', '28.00', null);
INSERT INTO `position` VALUES ('986', '浙江省', '嘉兴市', '120.75', '30.75', null);
INSERT INTO `position` VALUES ('993', '浙江省', '湖州市', '120.08', '30.90', null);
INSERT INTO `position` VALUES ('999', '浙江省', '绍兴市', '120.57', '30.00', null);
INSERT INTO `position` VALUES ('1006', '浙江省', '金华市', '119.65', '29.08', null);
INSERT INTO `position` VALUES ('1016', '浙江省', '衢州市', '118.87', '28.93', null);
INSERT INTO `position` VALUES ('1023', '浙江省', '舟山市', '122.20', '30.00', null);
INSERT INTO `position` VALUES ('1028', '浙江省', '台州市', '121.43', '28.68', null);
INSERT INTO `position` VALUES ('1038', '浙江省', '丽水市', '119.92', '28.45', null);
INSERT INTO `position` VALUES ('1048', '安徽省', '合肥市', '117.25', '31.83', null);
INSERT INTO `position` VALUES ('1056', '安徽省', '芜湖市', '118.38', '31.33', null);
INSERT INTO `position` VALUES ('1062', '安徽省', '蚌埠市', '117.38', '32.92', null);
INSERT INTO `position` VALUES ('1070', '安徽省', '淮南市', '117.00', '32.63', null);
INSERT INTO `position` VALUES ('1077', '安徽省', '马鞍山市', '118.50', '31.70', null);
INSERT INTO `position` VALUES ('1082', '安徽省', '淮北市', '116.80', '33.95', null);
INSERT INTO `position` VALUES ('1087', '安徽省', '铜陵市', '117.82', '30.93', null);
INSERT INTO `position` VALUES ('1092', '安徽省', '安庆市', '117.05', '30.53', null);
INSERT INTO `position` VALUES ('1104', '安徽省', '黄山市', '118.33', '29.72', null);
INSERT INTO `position` VALUES ('1112', '安徽省', '滁州市', '118.32', '32.30', null);
INSERT INTO `position` VALUES ('1121', '安徽省', '阜阳市', '115.82', '32.90', null);
INSERT INTO `position` VALUES ('1129', '安徽省', '宿州市', '116.98', '33.63', null);
INSERT INTO `position` VALUES ('1135', '安徽省', '巢湖市', '117.87', '31.60', null);
INSERT INTO `position` VALUES ('1141', '安徽省', '六安市', '116.50', '31.77', null);
INSERT INTO `position` VALUES ('1149', '安徽省', '亳州市', '115.78', '33.85', null);
INSERT INTO `position` VALUES ('1154', '安徽省', '池州市', '117.48', '30.67', null);
INSERT INTO `position` VALUES ('1159', '安徽省', '宣城市', '118.75', '30.95', null);
INSERT INTO `position` VALUES ('1167', '福建省', '福州市', '119.30', '26.08', null);
INSERT INTO `position` VALUES ('1181', '福建省', '厦门市', '118.08', '24.48', null);
INSERT INTO `position` VALUES ('1188', '福建省', '莆田市', '119.00', '25.43', null);
INSERT INTO `position` VALUES ('1194', '福建省', '三明市', '117.62', '26.27', null);
INSERT INTO `position` VALUES ('1207', '福建省', '泉州市', '118.67', '24.88', null);
INSERT INTO `position` VALUES ('1220', '福建省', '漳州市', '117.65', '24.52', null);
INSERT INTO `position` VALUES ('1232', '福建省', '南平市', '118.17', '26.65', null);
INSERT INTO `position` VALUES ('1243', '福建省', '龙岩市', '117.03', '25.10', null);
INSERT INTO `position` VALUES ('1251', '福建省', '宁德市', '119.52', '26.67', null);
INSERT INTO `position` VALUES ('1261', '江西省', '南昌市', '115.85', '28.68', null);
INSERT INTO `position` VALUES ('1271', '江西省', '景德镇市', '117.17', '29.27', null);
INSERT INTO `position` VALUES ('1276', '江西省', '萍乡市', '113.85', '27.63', null);
INSERT INTO `position` VALUES ('1282', '江西省', '九江市', '116.00', '29.70', null);
INSERT INTO `position` VALUES ('1295', '江西省', '新余市', '114.92', '27.82', null);
INSERT INTO `position` VALUES ('1298', '江西省', '鹰潭市', '117.07', '28.27', null);
INSERT INTO `position` VALUES ('1302', '江西省', '赣州市', '114.93', '25.83', null);
INSERT INTO `position` VALUES ('1321', '江西省', '吉安市', '114.98', '27.12', null);
INSERT INTO `position` VALUES ('1335', '江西省', '宜春市', '114.38', '27.80', null);
INSERT INTO `position` VALUES ('1346', '江西省', '抚州市', '116.35', '28.00', null);
INSERT INTO `position` VALUES ('1358', '江西省', '上饶市', '117.97', '28.45', null);
INSERT INTO `position` VALUES ('1371', '山东省', '济南市', '116.98', '36.67', null);
INSERT INTO `position` VALUES ('1384', '山东省', '青岛市', '120.38', '36.07', null);
INSERT INTO `position` VALUES ('1397', '山东省', '淄博市', '118.05', '36.82', null);
INSERT INTO `position` VALUES ('1405', '山东省', '枣庄市', '117.32', '34.82', null);
INSERT INTO `position` VALUES ('1414', '山东省', '东营市', '118.67', '37.43', null);
INSERT INTO `position` VALUES ('1420', '山东省', '烟台市', '121.43', '37.45', null);
INSERT INTO `position` VALUES ('1433', '山东省', '潍坊市', '119.15', '36.70', null);
INSERT INTO `position` VALUES ('1446', '山东省', '济宁市', '116.58', '35.42', null);
INSERT INTO `position` VALUES ('1461', '山东省', '泰安市', '117.08', '36.20', null);
INSERT INTO `position` VALUES ('1468', '山东省', '威海市', '122.12', '37.52', null);
INSERT INTO `position` VALUES ('1473', '山东省', '日照市', '119.52', '35.42', null);
INSERT INTO `position` VALUES ('1478', '山东省', '莱芜市', '117.67', '36.22', null);
INSERT INTO `position` VALUES ('1481', '山东省', '临沂市', '118.35', '35.05', null);
INSERT INTO `position` VALUES ('1494', '山东省', '德州市', '116.30', '37.45', null);
INSERT INTO `position` VALUES ('1506', '山东省', '聊城市', '115.98', '36.45', null);
INSERT INTO `position` VALUES ('1515', '山东省', '滨州市', '117.97', '37.38', null);
INSERT INTO `position` VALUES ('1523', '山东省', '菏泽市', '115.43', '35.25', null);
INSERT INTO `position` VALUES ('1532', '河南省', '郑州市', '113.62', '34.75', null);
INSERT INTO `position` VALUES ('1545', '河南省', '开封市', '114.30', '34.80', null);
INSERT INTO `position` VALUES ('1554', '河南省', '洛阳市', '112.45', '34.62', null);
INSERT INTO `position` VALUES ('1569', '河南省', '平顶山市', '113.18', '33.77', null);
INSERT INTO `position` VALUES ('1580', '河南省', '安阳市', '114.38', '36.10', null);
INSERT INTO `position` VALUES ('1590', '河南省', '鹤壁市', '114.28', '35.75', null);
INSERT INTO `position` VALUES ('1596', '河南省', '新乡市', '113.90', '35.30', null);
INSERT INTO `position` VALUES ('1609', '河南省', '焦作市', '113.25', '35.22', null);
INSERT INTO `position` VALUES ('1621', '河南省', '濮阳市', '115.03', '35.77', null);
INSERT INTO `position` VALUES ('1628', '河南省', '许昌市', '113.85', '34.03', null);
INSERT INTO `position` VALUES ('1635', '河南省', '漯河市', '114.02', '33.58', null);
INSERT INTO `position` VALUES ('1640', '河南省', '三门峡市', '111.20', '34.78', null);
INSERT INTO `position` VALUES ('1647', '河南省', '南阳市', '112.52', '33.00', null);
INSERT INTO `position` VALUES ('1661', '河南省', '商丘市', '115.65', '34.45', null);
INSERT INTO `position` VALUES ('1671', '河南省', '信阳市', '114.07', '32.13', null);
INSERT INTO `position` VALUES ('1682', '河南省', '周口市', '114.65', '33.62', null);
INSERT INTO `position` VALUES ('1692', '河南省', '驻马店市', '114.02', '32.98', null);
INSERT INTO `position` VALUES ('1703', '湖北省', '武汉市', '114.30', '30.60', null);
INSERT INTO `position` VALUES ('1717', '湖北省', '黄石市', '115.03', '30.20', null);
INSERT INTO `position` VALUES ('1724', '湖北省', '十堰市', '110.78', '32.65', null);
INSERT INTO `position` VALUES ('1733', '湖北省', '宜昌市', '111.28', '30.70', null);
INSERT INTO `position` VALUES ('1747', '湖北省', '襄樊市', '112.15', '32.02', null);
INSERT INTO `position` VALUES ('1757', '湖北省', '鄂州市', '114.88', '30.40', null);
INSERT INTO `position` VALUES ('1761', '湖北省', '荆门市', '112.20', '31.03', null);
INSERT INTO `position` VALUES ('1767', '湖北省', '孝感市', '113.92', '30.93', null);
INSERT INTO `position` VALUES ('1775', '湖北省', '荆州市', '112.23', '30.33', null);
INSERT INTO `position` VALUES ('1784', '湖北省', '黄冈市', '114.87', '30.45', null);
INSERT INTO `position` VALUES ('1795', '湖北省', '咸宁市', '114.32', '29.85', null);
INSERT INTO `position` VALUES ('1802', '湖北省', '随州市', '113.37', '31.72', null);
INSERT INTO `position` VALUES ('1814', '湖北省', '仙桃市', '113.45', '30.37', null);
INSERT INTO `position` VALUES ('1818', '湖南省', '长沙市', '112.93', '28.23', null);
INSERT INTO `position` VALUES ('1828', '湖南省', '株洲市', '113.13', '27.83', null);
INSERT INTO `position` VALUES ('1838', '湖南省', '湘潭市', '112.93', '27.83', null);
INSERT INTO `position` VALUES ('1844', '湖南省', '衡阳市', '112.57', '26.90', null);
INSERT INTO `position` VALUES ('1857', '湖南省', '邵阳市', '111.47', '27.25', null);
INSERT INTO `position` VALUES ('1870', '湖南省', '岳阳市', '113.12', '29.37', null);
INSERT INTO `position` VALUES ('1880', '湖南省', '常德市', '111.68', '29.05', null);
INSERT INTO `position` VALUES ('1890', '湖南省', '张家界市', '110.47', '29.13', null);
INSERT INTO `position` VALUES ('1895', '湖南省', '益阳市', '112.32', '28.60', null);
INSERT INTO `position` VALUES ('1902', '湖南省', '郴州市', '113.02', '25.78', null);
INSERT INTO `position` VALUES ('1914', '湖南省', '永州市', '111.62', '26.43', null);
INSERT INTO `position` VALUES ('1925', '湖南省', '怀化市', '110.00', '27.57', null);
INSERT INTO `position` VALUES ('1938', '湖南省', '娄底市', '112.00', '27.73', null);
INSERT INTO `position` VALUES ('1953', '广东省', '广州市', '113.27', '23.13', null);
INSERT INTO `position` VALUES ('1964', '广东省', '韶关市', '113.60', '24.82', null);
INSERT INTO `position` VALUES ('1975', '广东省', '深圳市', '114.05', '22.55', null);
INSERT INTO `position` VALUES ('1982', '广东省', '珠海市', '113.57', '22.27', null);
INSERT INTO `position` VALUES ('1986', '广东省', '汕头市', '116.68', '23.35', null);
INSERT INTO `position` VALUES ('1993', '广东省', '佛山市', '113.12', '23.02', null);
INSERT INTO `position` VALUES ('1998', '广东省', '江门市', '113.08', '22.58', null);
INSERT INTO `position` VALUES ('2004', '广东省', '湛江市', '110.35', '21.27', null);
INSERT INTO `position` VALUES ('2014', '广东省', '茂名市', '110.92', '21.67', null);
INSERT INTO `position` VALUES ('2021', '广东省', '肇庆市', '112.47', '23.05', null);
INSERT INTO `position` VALUES ('2030', '广东省', '惠州市', '114.42', '23.12', null);
INSERT INTO `position` VALUES ('2036', '广东省', '梅州市', '116.12', '24.28', null);
INSERT INTO `position` VALUES ('2045', '广东省', '汕尾市', '115.37', '22.78', null);
INSERT INTO `position` VALUES ('2049', '广东省', '河源市', '114.70', '23.73', null);
INSERT INTO `position` VALUES ('2056', '广东省', '阳江市', '111.98', '21.87', null);
INSERT INTO `position` VALUES ('2061', '广东省', '清远市', '113.03', '23.70', null);
INSERT INTO `position` VALUES ('2070', '广东省', '东莞市', '113.75', '23.05', null);
INSERT INTO `position` VALUES ('2071', '广东省', '中山市', '113.38', '22.52', null);
INSERT INTO `position` VALUES ('2072', '广东省', '潮州市', '116.62', '23.67', null);
INSERT INTO `position` VALUES ('2076', '广东省', '揭阳市', '116.37', '23.55', null);
INSERT INTO `position` VALUES ('2081', '广东省', '云浮市', '112.03', '22.92', null);
INSERT INTO `position` VALUES ('2087', '广西壮族自治区', '南宁市', '108.37', '22.82', null);
INSERT INTO `position` VALUES ('2099', '广西壮族自治区', '柳州市', '109.42', '24.33', null);
INSERT INTO `position` VALUES ('2107', '广西壮族自治区', '桂林市', '110.28', '25.28', null);
INSERT INTO `position` VALUES ('2119', '广西壮族自治区', '梧州市', '111.27', '23.48', null);
INSERT INTO `position` VALUES ('2124', '广西壮族自治区', '北海市', '109.12', '21.48', null);
INSERT INTO `position` VALUES ('2127', '广西壮族自治区', '防城港市', '108.35', '21.70', null);
INSERT INTO `position` VALUES ('2132', '广西壮族自治区', '钦州市', '108.62', '21.95', null);
INSERT INTO `position` VALUES ('2136', '广西壮族自治区', '贵港市', '109.60', '23.10', null);
INSERT INTO `position` VALUES ('2140', '广西壮族自治区', '玉林市', '110.17', '22.63', null);
INSERT INTO `position` VALUES ('2146', '广西壮族自治区', '百色市', '106.62', '23.90', null);
INSERT INTO `position` VALUES ('2158', '广西壮族自治区', '贺州市', '111.55', '24.42', null);
INSERT INTO `position` VALUES ('2162', '广西壮族自治区', '河池市', '108.07', '24.70', null);
INSERT INTO `position` VALUES ('2174', '广西壮族自治区', '来宾市', '109.23', '23.73', null);
INSERT INTO `position` VALUES ('2180', '广西壮族自治区', '崇左市', '107.37', '22.40', null);
INSERT INTO `position` VALUES ('2187', '海南省', '海口市', '110.32', '20.03', null);
INSERT INTO `position` VALUES ('2192', '海南省', '三亚市', '109.50', '18.25', null);
INSERT INTO `position` VALUES ('2193', '海南省', '五指山市', '109.52', '18.78', null);
INSERT INTO `position` VALUES ('2209', '重庆市', '重庆市', '106.55', '29.57', null);
INSERT INTO `position` VALUES ('2246', '四川省', '成都市', '104.07', '30.67', null);
INSERT INTO `position` VALUES ('2266', '四川省', '自贡市', '104.78', '29.35', null);
INSERT INTO `position` VALUES ('2273', '四川省', '攀枝花市', '101.72', '26.58', null);
INSERT INTO `position` VALUES ('2279', '四川省', '泸州市', '105.43', '28.87', null);
INSERT INTO `position` VALUES ('2287', '四川省', '德阳市', '104.38', '31.13', null);
INSERT INTO `position` VALUES ('2294', '四川省', '绵阳市', '104.73', '31.47', null);
INSERT INTO `position` VALUES ('2304', '四川省', '广元市', '105.83', '32.43', null);
INSERT INTO `position` VALUES ('2313', '四川省', '遂宁市', '105.57', '30.52', null);
INSERT INTO `position` VALUES ('2319', '四川省', '内江市', '105.05', '29.58', null);
INSERT INTO `position` VALUES ('2326', '四川省', '乐山市', '103.77', '29.57', null);
INSERT INTO `position` VALUES ('2339', '四川省', '南充市', '106.08', '30.78', null);
INSERT INTO `position` VALUES ('2349', '四川省', '眉山市', '103.83', '30.05', null);
INSERT INTO `position` VALUES ('2356', '四川省', '宜宾市', '104.62', '28.77', null);
INSERT INTO `position` VALUES ('2367', '四川省', '广安市', '106.63', '30.47', null);
INSERT INTO `position` VALUES ('2372', '四川省', '达州市', '107.50', '31.22', null);
INSERT INTO `position` VALUES ('2380', '四川省', '雅安市', '103.00', '29.98', null);
INSERT INTO `position` VALUES ('2389', '四川省', '巴中市', '106.77', '31.85', null);
INSERT INTO `position` VALUES ('2394', '四川省', '资阳市', '104.65', '30.12', null);
INSERT INTO `position` VALUES ('2450', '贵州省', '贵阳市', '106.63', '26.65', null);
INSERT INTO `position` VALUES ('2460', '贵州省', '六盘水市', '104.83', '26.60', null);
INSERT INTO `position` VALUES ('2465', '贵州省', '遵义市', '106.92', '27.73', null);
INSERT INTO `position` VALUES ('2480', '贵州省', '安顺市', '105.95', '26.25', null);
INSERT INTO `position` VALUES ('2498', '贵州省', '兴义市', '104.90', '25.08', null);
INSERT INTO `position` VALUES ('2544', '云南省', '昆明市', '102.72', '25.05', null);
INSERT INTO `position` VALUES ('2559', '云南省', '曲靖市', '103.80', '25.50', null);
INSERT INTO `position` VALUES ('2569', '云南省', '玉溪市', '102.55', '24.35', null);
INSERT INTO `position` VALUES ('2577', '云南省', '保山市', '99.17', '25.12', null);
INSERT INTO `position` VALUES ('2583', '云南省', '昭通市', '103.72', '27.33', null);
INSERT INTO `position` VALUES ('2595', '云南省', '丽江市', '100.23', '26.88', null);
INSERT INTO `position` VALUES ('2607', '云南省', '临沧市', '100.08', '23.88', null);
INSERT INTO `position` VALUES ('2680', '西藏自治区', '拉萨市', '91.13', '29.65', null);
INSERT INTO `position` VALUES ('2760', '陕西省', '西安市', '108.93', '34.27', null);
INSERT INTO `position` VALUES ('2774', '陕西省', '铜川市', '108.93', '34.90', null);
INSERT INTO `position` VALUES ('2779', '陕西省', '宝鸡市', '107.13', '34.37', null);
INSERT INTO `position` VALUES ('2792', '陕西省', '咸阳市', '108.70', '34.33', null);
INSERT INTO `position` VALUES ('2807', '陕西省', '渭南市', '109.50', '34.50', null);
INSERT INTO `position` VALUES ('2819', '陕西省', '延安市', '109.48', '36.60', null);
INSERT INTO `position` VALUES ('2832', '陕西省', '汉中市', '107.02', '33.07', null);
INSERT INTO `position` VALUES ('2844', '陕西省', '榆林市', '109.73', '38.28', null);
INSERT INTO `position` VALUES ('2857', '陕西省', '安康市', '109.02', '32.68', null);
INSERT INTO `position` VALUES ('2868', '陕西省', '商洛市', '109.93', '33.87', null);
INSERT INTO `position` VALUES ('2876', '甘肃省', '兰州市', '103.82', '36.07', null);
INSERT INTO `position` VALUES ('2883', '甘肃省', '嘉峪关市', '98.27', '39.80', null);
INSERT INTO `position` VALUES ('2884', '甘肃省', '金昌市', '102.18', '38.50', null);
INSERT INTO `position` VALUES ('2887', '甘肃省', '白银市', '104.18', '36.55', null);
INSERT INTO `position` VALUES ('2893', '甘肃省', '天水市', '105.72', '34.58', null);
INSERT INTO `position` VALUES ('2899', '甘肃省', '武威市', '102.63', '37.93', null);
INSERT INTO `position` VALUES ('2904', '甘肃省', '张掖市', '100.45', '38.93', null);
INSERT INTO `position` VALUES ('2911', '甘肃省', '平凉市', '106.67', '35.55', null);
INSERT INTO `position` VALUES ('2919', '甘肃省', '酒泉市', '98.52', '39.75', null);
INSERT INTO `position` VALUES ('2926', '甘肃省', '庆阳市', '107.63', '35.73', null);
INSERT INTO `position` VALUES ('2935', '甘肃省', '定西市', '104.62', '35.58', null);
INSERT INTO `position` VALUES ('2943', '甘肃省', '陇南市', '104.92', '33.40', null);
INSERT INTO `position` VALUES ('2970', '青海省', '西宁市', '101.78', '36.62', null);
INSERT INTO `position` VALUES ('3021', '宁夏回族自治区', '银川市', '106.28', '38.47', null);
INSERT INTO `position` VALUES ('3028', '宁夏回族自治区', '石嘴山市', '106.38', '39.02', null);
INSERT INTO `position` VALUES ('3032', '宁夏回族自治区', '吴忠市', '106.20', '37.98', null);
INSERT INTO `position` VALUES ('3037', '宁夏回族自治区', '固原市', '106.28', '36.00', null);
INSERT INTO `position` VALUES ('3043', '宁夏回族自治区', '中卫市', '105.18', '37.52', null);
INSERT INTO `position` VALUES ('3047', '新疆维吾尔', '乌鲁木齐市', '87.62', '43.82', null);
INSERT INTO `position` VALUES ('3056', '新疆维吾尔', '克拉玛依市', '84.87', '45.60', null);
INSERT INTO `position` VALUES ('3101', '新疆维吾尔', '阿图什市', '76.17', '39.72', null);
INSERT INTO `position` VALUES ('3153', '新疆维吾尔', '石河子市', '86.03', '44.30', null);
INSERT INTO `s_class` VALUES ('0', '入学默认班级', '4', '1', '2017-09-27', 'asdasd', null);
INSERT INTO `setting` VALUES ('3', '1');

-- ----------------------------
-- Trigger structure for insert_group_score_all
-- ----------------------------
DELIMITER ;;
CREATE TRIGGER `insert_group_score_all` BEFORE INSERT ON `group` FOR EACH ROW set new.score_all=new.score_1+new.score_2;;
DELIMITER ;

-- ----------------------------
-- Trigger structure for update_group_score_all
-- ----------------------------
DELIMITER ;;
CREATE TRIGGER `update_group_score_all` BEFORE UPDATE ON `group` FOR EACH ROW set new.score_all=new.score_1+new.score_2;;
DELIMITER ;

-- ----------------------------
-- Trigger structure for insert_score_b
-- ----------------------------
DELIMITER ;;
CREATE TRIGGER `insert_score_b` BEFORE INSERT ON `score` FOR EACH ROW set new.score_b=new.score_1+new.score_2+new.score_3,new.score_all=new.score_b*0.6+new.score_m*0.4;;
DELIMITER ;

-- ----------------------------
-- Trigger structure for update_score_b
-- ----------------------------
DELIMITER ;;
CREATE TRIGGER `update_score_b` BEFORE UPDATE ON `score` FOR EACH ROW set new.score_b=new.score_1+new.score_2+new.score_3,new.score_all=new.score_b*0.6+new.score_m*0.4;;
DELIMITER ;

package com.hpe.interceptor;

import com.hpe.common.Const;
import com.hpe.pojo.Admin;
import com.hpe.pojo.Student;
import com.hpe.pojo.Teacher;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author Fujt
 * @description
 * @date 18:48 2017/9/26
 */
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {

        //获取实际请求
        String url = httpServletRequest.getRequestURI().substring(httpServletRequest.getContextPath().length());

        //请求用户类别前缀
        String prefix = url.substring(0, url.indexOf("/", url.indexOf("/") + 1));

        HttpSession session = httpServletRequest.getSession();

        //根据用户类型区分 判断用户是否登录
        if ("/student".equals(prefix)) {
            Student student = ((Student) session.getAttribute(Const.CURRENT_STUDENT));
            if (student == null) {
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/index.do");
            }else {
                return true;
            }
        } else if ("/edu".equals(prefix) || "/ep".equals(prefix)) {
            Admin admin = ((Admin) session.getAttribute(Const.CURRENT_ADMIN));
            if (admin == null) {
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/index.do");
            }else {
                //判断admin是edu还是ep -> edu没有权限访问ep的内容
                if ("/edu".equals(prefix)) {
                    if (admin.getType() != Const.AdminType.EDUCATION) {
                        httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/index.do");
                    }else {
                        return true;
                    }
                }else {
                    if (admin.getType() != Const.AdminType.ENTERPRISE) {
                        httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/index.do");
                    }else {
                        return true;
                    }
                }
                return false;
            }
        }else if ("/teacher".equals(prefix)){
            Teacher teacher = ((Teacher) session.getAttribute(Const.CURRENT_TEACHER));
            if (teacher == null) {
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/index.do");
            }else {
                return true;
            }
        }

        return false;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}

package com.hpe.controller.teacher;

import com.github.pagehelper.PageInfo;
import com.hpe.common.Const;
import com.hpe.pojo.BigQuestion;
import com.hpe.pojo.Teacher;
import com.hpe.service.BigQuestionService;
import com.hpe.util.ResponseCode;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * @Author:XuShengJie
 * @Description:老师模块大题Controller
 * @Date:
 */
@Controller
@RequestMapping("/teacher/bigquestion")
public class TeacherBigQuestionController {

    @Autowired
    BigQuestionService bigQuestionsService;

    /**
     * @Author:XuShengJie
     * @Description:显示所有编程题和简答题,默认显示第一页,一页显示5条记录
     * @Date:17:11 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET,value = "/show_all_bigquestions.do")
    @ResponseBody
    public ServerResponse<PageInfo> showAllBigQuestions(@RequestParam(name = "pageNum" , defaultValue = "1") int pageNum,
                                                        @RequestParam(name = "pageSize",defaultValue = "5") int pageSize,
                                                        @RequestParam(name = "bqquestionTitle",defaultValue = "")String bqquestionTitle,
                                                        @RequestParam(name = "bqcourseName",defaultValue = "")String bqcourseName,
                                                        HttpSession session){
        //获取session中的teacher
        Teacher loginTeacher = (Teacher)session.getAttribute(Const.CURRENT_TEACHER);
        if(loginTeacher==null) {
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return bigQuestionsService.showAllBigQuestions(bqquestionTitle,bqcourseName,pageNum,pageSize);
    }

    /**
     * @Author:XuShengJie
     * @Description:修改和添加答题页面用于填写信息
     * @Param:需要id
     * @Date:19:35 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST,value = "/edit_bigquestion.do")
    @ResponseBody
    public ServerResponse<Map<String,Object>> editBigQuestion(BigQuestion bigQuestion){
        return  bigQuestionsService.editBigQuestion(bigQuestion);
    }

    /**
     * @Author:XuShengJie
     * @Description:添加或者修改大题
     * @Date:19:35 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST,value = "/update_bigquestion.do")
    @ResponseBody
    public ServerResponse updateBigQuestion(BigQuestion bigQuestion){
        return bigQuestionsService.updateBigQuestion(bigQuestion);
    }

    /**
     * @Author:XuShengJie
     * @Description:删除选中的大题
     * @Date:19:38 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST,value = "/delete_bigquestion.do")
    @ResponseBody
    public ServerResponse deleteBigQuestion(BigQuestion bigQuestion){
      return  bigQuestionsService.deleteBigQuestion(bigQuestion);
    }
    
    /**
     * @Author:XuShengJie
     * @Description:导入大题
     * @Date:19:40 2017/10/25
     */
    @RequestMapping(method = RequestMethod.POST,value = "/load_in_bigquestion.do")
    @ResponseBody
    public ServerResponse loadinExalForBq(MultipartFile file,
                                          @RequestParam(name = "courseId") Integer courseId,
                                          @RequestParam(name = "queType") Integer queType) throws Exception {
        if (file == null) {
            return ServerResponse.createByErrorMsg("未获取到文件, 请重新上传");
        }
        //文件名
        String name = file.getOriginalFilename();
        //后缀
        String suffix = name.substring(name.indexOf("."));

        if (!".xls".equals(suffix) && !".xlsx".equals(suffix)) {
            return ServerResponse.createByErrorMsg("请选择.xls或者.xlsx后缀的文件");
        }

        InputStream inputStream = file.getInputStream();
        if (inputStream == null) {
            return ServerResponse.createByErrorMsg("输入流为空");
        }
        return bigQuestionsService.loadinExcelForBq(inputStream,courseId,queType);
    }

    /**
     * @Author:XuShengJie
     * @Description:导出大题表表头
     * @Date:9:01 2017/10/26
     */
        @GetMapping("/get_bquestion_title_excel.do")
    @ResponseBody
    public void getBQuestionTitleExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        bigQuestionsService.outputSelectBQuestionExcelTitle(request,response);
    }
}

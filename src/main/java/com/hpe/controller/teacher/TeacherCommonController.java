package com.hpe.controller.teacher;

import com.hpe.common.Const;
import com.hpe.pojo.Teacher;
import com.hpe.service.TeacherService;
import com.hpe.util.ResponseCode;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Created by chunjie on 2017/9/27.
 */
@Controller
@RequestMapping("/teacher/common")
public class TeacherCommonController {

    @Autowired
    TeacherService teacherService;

    /**
     * @Author:XuShengJie
     * @Description:登录
     * @Date:21:59 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST,value = "/login.do")
    @ResponseBody
    public ServerResponse<Teacher> login(HttpSession session, String username , String password){
        Teacher teacher = new Teacher();
        teacher.setName(username);
        teacher.setPwd(password);
        ServerResponse<Teacher> serverResponse =teacherService.login(teacher);
        if(serverResponse.isSuccess()) {
            session.setAttribute(Const.CURRENT_TEACHER, serverResponse.getData());
        }
        return serverResponse;
    }
    /**
     * @description 修改密码
     * @author Fujt
     * @date 2017/9/26 15:04
     */
    @RequestMapping(method = RequestMethod.POST,value = "/change_pwd.do")
    @ResponseBody
    public ServerResponse<Teacher> updatePwd(HttpSession session,Teacher teacher){
        //获取session中的teacher
        Teacher loginTeacher = (Teacher)session.getAttribute(Const.CURRENT_TEACHER);
        if(loginTeacher==null) {
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        //获取新的密码
        loginTeacher.setPwd(teacher.getPwd());
        //调用service方法
        ServerResponse serverResponse = teacherService.updatePwd(loginTeacher);
        session.setAttribute(Const.CURRENT_TEACHER,loginTeacher);
        return serverResponse;
    }
}

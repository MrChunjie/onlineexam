package com.hpe.controller.teacher;

import com.github.pagehelper.PageInfo;
import com.hpe.common.Const;
import com.hpe.pojo.ClassTeacher;
import com.hpe.pojo.Course;
import com.hpe.pojo.Score;
import com.hpe.pojo.Teacher;
import com.hpe.service.ScoreService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/teacher/eva")
public class TeacherEvaController {

    @Autowired
    private ScoreService paperService;

    @RequestMapping(method = RequestMethod.POST, value = "/show_avg.do")
    @ResponseBody
    public ServerResponse<PageInfo> showAvg(HttpSession session, @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(name = "pageSize", defaultValue = "3") int pageSize,
                                                @RequestParam(name = "stuClassId", defaultValue = "-1") int stuClassId) {
        // 搜索条件
        Teacher t = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        ServerResponse<PageInfo> response = paperService.findAvg(stuClassId, t.getId(),pageNum, pageSize);
        return response;
    }
    @RequestMapping(method = RequestMethod.POST, value = "/show_avg_detail.do")
    @ResponseBody
    public ServerResponse<PageInfo> showAvg(HttpSession session, @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                            @RequestParam(name = "pageSize", defaultValue = "3") int pageSize) {
        // 搜索条件
        Score score=(Score)session.getAttribute("score");
        ServerResponse<PageInfo> response = paperService.findEvaDetail(score.getClassId(), score.getExamId(),pageNum, pageSize);
        return response;
    }

    /**
    * @Author:TangWenhao
    * @Description: 输出流到客户端
    * @Date:9:13 2017/10/17
    */
    @GetMapping("/avg_output_excel.do")
    public void outputExcel(HttpServletRequest req, HttpServletResponse resp,
                            int classId,int examId) throws Exception {
        paperService.outputExcelByEva(req, resp, classId, examId);
    }
}

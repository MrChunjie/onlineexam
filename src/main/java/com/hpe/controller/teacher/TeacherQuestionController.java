package com.hpe.controller.teacher;

import com.github.pagehelper.PageInfo;
import com.hpe.common.Const;
import com.hpe.pojo.BigQuestion;
import com.hpe.pojo.Questions;
import com.hpe.pojo.Teacher;
import com.hpe.service.BigQuestionService;
import com.hpe.service.QuestionService;
import com.hpe.util.ResponseCode;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/teacher/question")
public class TeacherQuestionController {

    @Autowired
    QuestionService questionService;

    /**
     * @Author:XuShengJie
     * @Description:添加选择题题库和修改一条题跳转的页面(修改的时候传id过来)
     * @Date:20:30 2017/9/25
     */
    @RequestMapping(method = RequestMethod.POST,value = "/edit_questions.do")
    @ResponseBody
    public ServerResponse<Map<String,Object>> editQuestions(Questions questions){
        return  questionService.editQuestions(questions);
    }

    /**
     * @Author:XuShengJie
     * @Description:添加和修改选择题,(前台读的时候需要保留id属性)
     * @Date:22:45 2017/9/25
     */
    @RequestMapping(method = RequestMethod.POST,value = "/update_questions.do")
    @ResponseBody
    public ServerResponse updateQuestions(Questions question){
       return  questionService.updateQuestion(question);
    }

    /**
     * @Author:XuShengJie
     * @Description:单选题的删除
     * @Date:9:24 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST,value = "/delete_questions.do")
    @ResponseBody
    public ServerResponse<Questions> deleteQuestions(Questions question){
        return questionService.deleteQuestion(question);
    }

    /**
     * @Author:XuShengJie
     * @Description:导入选择题题库
     * @Date:16:15 2017/10/23
     */
    @RequestMapping("/upload_question.do")
    @ResponseBody
    public ServerResponse uploadExcel(MultipartFile file) throws Exception {
        if (file == null) {
            return ServerResponse.createByErrorMsg("未获取到文件, 请重新上传");
        }
        //文件名
        String name = file.getOriginalFilename();
        //后缀
        String suffix = name.substring(name.indexOf("."));

        if (!".xls".equals(suffix) && !".xlsx".equals(suffix)) {
            return ServerResponse.createByErrorMsg("请选择.xls或者.xlsx后缀的文件");
        }

        InputStream inputStream = file.getInputStream();
        if (inputStream == null) {
            return ServerResponse.createByErrorMsg("输入流为空");
        }
        return questionService.uploadQuestion(inputStream);
    }

    /**
     * @Author:XuShengJie
     * @Description:显示所有单选题,默认显示第一页,一页显示5条记录
     * @Date:9:53 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET,value = "/show_all_questions.do")
    @ResponseBody
    public ServerResponse<PageInfo> showAllQuestions(@RequestParam(name = "pageNum" , defaultValue = "1") int pageNum,
                                                     @RequestParam(name = "pageSize",defaultValue = "5") int pageSize,
                                                     @RequestParam(name = "questionTitle" , defaultValue = "")String questionTitle,
                                                     @RequestParam(name = "courseName" , defaultValue = "")String courseName,
                                                     HttpSession session){
        //获取session中的teacher
        Teacher loginTeacher = (Teacher)session.getAttribute(Const.CURRENT_TEACHER);
        if(loginTeacher==null) {
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        return questionService.showAllQuestions(courseName,questionTitle,pageNum, pageSize);
    }


    /**
     * @Author:XuShengJie
     * @Description:导入选择题
     * @Date:17:43 2017/10/25
     */
    @RequestMapping(method = RequestMethod.POST,value = "/load_in_question.do")
    @ResponseBody
    public ServerResponse loadinExcel(MultipartFile file,
                                      @RequestParam(name = "courseId") Integer courseId) throws Exception {
        if (file == null) {
            return ServerResponse.createByErrorMsg("未获取到文件, 请重新上传");
        }
        //文件名
        String name = file.getOriginalFilename();
        //后缀
        String suffix = name.substring(name.indexOf("."));

        if (!".xls".equals(suffix) && !".xlsx".equals(suffix)) {
            return ServerResponse.createByErrorMsg("请选择.xls或者.xlsx后缀的文件");
        }

        InputStream inputStream = file.getInputStream();
        if (inputStream == null) {
            return ServerResponse.createByErrorMsg("输入流为空");
        }

        return questionService.loadinExcel(inputStream, courseId);
    }
    /**
     * @Author:XuShengJie
     * @Description:导出选择题表头
     * @Date:9:04 2017/10/26
     */
    @GetMapping("/get_question_title_excel.do")
    @ResponseBody
    public void getQuestionTitleExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
       questionService.outputSelectQuestionExcelTitle(request,response);
    }
}

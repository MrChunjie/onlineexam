package com.hpe.controller.teacher;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Grade;
import com.hpe.service.GradeService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/teacher/grade")
public class TeacherGradeController {

    @Autowired
    GradeService gradeService;

    /**
     * @Author:Liuli
     * @Description:获取全部技能
     * @Date:19:31 2017/9/25
     */
    @GetMapping(value ="/get_grade.do")
    @ResponseBody
    public Map getGrade(){
        return gradeService.getGrade();
    }

    /**
     * @Author:HouPeng
     * @Description:获取所有技能
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/all_grade.do")
    @ResponseBody
    public ServerResponse<PageInfo<Grade>> getAllGrade(Integer pageNum, Integer pageSize) {
        //获取所有技能
        ServerResponse<PageInfo<Grade>> serverResponse = gradeService.getAllGrade(pageNum, pageSize);
        return serverResponse;

    }

    /**
     * @Author:HouPeng
     * @Description:修改技能
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST, value = "/update_grade.do")
    @ResponseBody
    public ServerResponse update(Grade grade) {
        ServerResponse result = gradeService.updateGrade(grade);
        return result;
    }
    /**
     * @Author:HouPeng
     * @Description:根据id 获取数据 传递数据到前台
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/toupdate_grade.do")
    @ResponseBody
    public ServerResponse<Grade> toupdate(Integer gradeId) {
        Grade grade = gradeService.getGradeById(gradeId);
        return ServerResponse.createBySuccess(grade);
    }

    /**
     * @Author:HouPeng
     * @Description:删除
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/delete_grade.do")
    @ResponseBody
    public ServerResponse delete(Integer gradeId) {
        ServerResponse result= gradeService.deleteById(gradeId);
        return result;
    }
    /**
     *@Author:HouPeng
     *@Description:增加技能
     *@Date:16:26 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST, value = "/add_grade.do")
    @ResponseBody
    public ServerResponse add(Grade grade) {
        ServerResponse result= gradeService.addGrade(grade);
        return result;
    }
}

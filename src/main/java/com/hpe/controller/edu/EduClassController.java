package com.hpe.controller.edu;

import com.github.pagehelper.PageInfo;
import com.hpe.common.Const;
import com.hpe.pojo.Admin;
import com.hpe.pojo.SClass;
import com.hpe.service.ClassHistoryService;
import com.hpe.service.ClassNumService;
import com.hpe.service.SClassService;
import com.hpe.util.ResponseCode;
import com.hpe.util.ServerResponse;
import com.hpe.vo.ClassStuVo;
import com.hpe.vo.DivideClassVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/edu/class")
public class EduClassController {

    @Autowired
    private SClassService sClassService;

    @Autowired
    private ClassNumService classNumService;

    @Autowired
    private ClassHistoryService classHistoryService;

    /**
     * @Author:CuiCaijin
     * @Description: 添加班级
     * @Date:10:15 2017/9/26
     */
    @PostMapping("/add_class.do")
    @ResponseBody
    public ServerResponse<String> addClass(HttpSession session, SClass sClass){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        //判断管理员是否登录
        if(admin == null){
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());
        }
        //判断管理员是否有权限对此进行操作
        if(admin.getType() == Const.AdminType.ENTERPRISE){
            return ServerResponse.createByErrorMsg("您没有权限进行此操作");
        }
        return  sClassService.saveClass(sClass);
    }

    /**
     * @Author:CuiCaijin
     * @Description: 分页查询班级
     * @Date:14:32 2017/9/26
     */
    @GetMapping("/class_list.do")
    @ResponseBody
    public ServerResponse<PageInfo> getClassList(HttpSession session, @RequestParam(name = "pageNum",defaultValue = "1")int pageNum,
                                                 @RequestParam(name = "pageSize",defaultValue = "3")int pageSize){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        //判断管理员是否登录
        if(admin == null){
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());
        }
        //判断管理员是否有权限对此进行操作
        if(admin.getType() == Const.AdminType.ENTERPRISE){
            return ServerResponse.createByErrorMsg("您没有权限进行此操作");
        }
        return sClassService.getClassesByState(Const.ClassState.ONSCHOOL,pageNum, pageSize);
    }
    /**
     * @Author:XuShengJie
     * @Description:获取班级的成绩信息
     * @Date:15:19 2017/11/12
     */
    @GetMapping("/score_class_list.do")
    @ResponseBody
    public ServerResponse<PageInfo> getScoreForClassList(HttpSession session, @RequestParam(name = "pageNum",defaultValue = "1")int pageNum,
                                                 @RequestParam(name = "pageSize",defaultValue = "3")int pageSize,
                                                         @RequestParam(name = "className",defaultValue = "")String className,
                                                         @RequestParam(name = "teacherName",defaultValue = "")String teacherName,
                                                         @RequestParam(name = "courseName",defaultValue = "")String courseName){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        //判断管理员是否登录
        if(admin == null){
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());
        }
        //判断管理员是否有权限对此进行操作
        if(admin.getType() == Const.AdminType.ENTERPRISE){
            return ServerResponse.createByErrorMsg("您没有权限进行此操作");
        }
            return sClassService.getAllClassScore(className,teacherName,courseName,pageNum,pageSize);
    }
    /**
     * @Author:CuiCaijin
     * @Description:分页查询离校班级
     * @Date:14:32 2017/9/26
     */
    @GetMapping("/level_class_list.do")
    @ResponseBody
    public ServerResponse<PageInfo> getLevelClassList(HttpSession session, @RequestParam(name = "pageNum",defaultValue = "1")int pageNum,
                                                 @RequestParam(name = "pageSize",defaultValue = "3")int pageSize){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        //判断管理员是否登录
        if(admin == null){
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());
        }
        //判断管理员是否有权限对此进行操作
        if(admin.getType() == Const.AdminType.ENTERPRISE){
            return ServerResponse.createByErrorMsg("您没有权限进行此操作");
        }
        return sClassService.getClassesByState(Const.ClassState.LEVEL,pageNum, pageSize);
    }

    @PostMapping("/update_class_other.do")
    @ResponseBody
    public ServerResponse<String> updateClassContent(HttpSession session,SClass sClass){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        //判断管理员是否登录
        if(admin == null){
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());
        }
        //判断管理员是否有权限对此进行操作
        if(admin.getType() == Const.AdminType.ENTERPRISE){
            return ServerResponse.createByErrorMsg("您没有权限进行此操作");
        }
        return sClassService.updateClassOther(sClass);
    }

    /**
    * @Author:CuiCaijin
    * @Description:更新班级的状态
    * @Date:14:06 2017/10/16
    */
    @PostMapping("/update_class_state.do")
    @ResponseBody
    public ServerResponse<String> updateClassState(HttpSession session,SClass sClass){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        //判断管理员是否登录
        if(admin == null){
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());
        }
        //判断管理员是否有权限对此进行操作
        if(admin.getType() == Const.AdminType.ENTERPRISE){
            return ServerResponse.createByErrorMsg("您没有权限进行此操作");
        }
        return sClassService.updateClassState(sClass);
    }

    /**
     * @description 根据id获取班级
     * @author Fujt
     * @date 2017/10/10 18:59
     * @param id 要获取的班级id
     */
    @GetMapping("/get_class.do")
    @ResponseBody
    public ServerResponse<SClass> getByPrimaryKey(int id){
        SClass sClass = sClassService.selectByPrimaryKey(id);
        if (sClass == null) {
            return ServerResponse.createByErrorMsg("未找到");
        }
        return ServerResponse.createBySuccess(sClass);
    }

    /**
    * @Author:CuiCaijin
    * @Description:获得所有的班级列表
    * @Date:14:06 2017/10/16
    */
    @GetMapping("/get_all_class.do")
    @ResponseBody
    public ServerResponse<List<SClass>> getClasses(int type){
        return sClassService.getClasses(type);
    }


    /**
     * @Author:CuiCaijin
     * @Description:获得所有的班级列表
     * @Date:14:06 2017/10/16
     */
    @GetMapping("/all_class.do")
    @ResponseBody
    public ServerResponse<List<SClass>> getClasses(){
        return sClassService.getClasses();
    }

    /**
    * @Author:Liuli
    * @Description:根据状态获取班级id
    * @Date:14:48 2017/10/12
    */
    @PostMapping("/find_by_type.do")
    @ResponseBody
    public ServerResponse<List<Integer>> getClassesByType(int type){
        List<SClass> sClasses=sClassService.selectByType(type);
        List<Integer> list=new ArrayList<Integer>();
        for(SClass sClass:sClasses){
            list.add(sClass.getId());
        }
        return ServerResponse.createBySuccess(list);
    }

    /**
    * @Author:CuiCaijin
    * @Description:跳转到分班页面，并将groupId传过去
    * @Date:14:04 2017/10/16
    */
    @GetMapping("/edu-class-divide.do")
    public ModelAndView classDivide(HttpSession session,int groupId){
        ModelAndView modelAndView = new ModelAndView("edu-class-divide");
        if(groupId == 0){
            modelAndView.addObject("error","无法获得groupId");
        }
        modelAndView.addObject("groupId",groupId);
        return modelAndView;
    }

    /**
    * @Author:CuiCaijin
    * @Description:根据方向获得班级
    * @Date:14:42 2017/10/16
    */
    @GetMapping("/list_by_type.do")
    @ResponseBody
    public ServerResponse<List<SClass>> getClassIdAndNameByType(int direction){
        return sClassService.getSClassListByType(direction);
    }

    @GetMapping("/divide_class_scurve.do")
    @ResponseBody
    public ServerResponse<String> getDivideResult(String classIds,int groupId){
        return sClassService.divideClassBySCurve(classIds,groupId);
    }


    @GetMapping("/divide_class_three.do")
    @ResponseBody
    public ServerResponse<String> getDivideResult(DivideClassVo divideClassVo) {
        return sClassService.divideClassByThree(divideClassVo);
    }

    /**
    * @Author:Liuli
    * @Description:跳转至班级详情页
    * @Date:8:48 2017/10/18
    */
    @RequestMapping("/get_class_details.do")
    public ModelAndView getClassDetails(int id){
        ModelAndView modelAndView=new ModelAndView("edu-class-details");
        if(id == 0){
            modelAndView.addObject("error","无法获得classId");
        }
        int num=classNumService.getClassNum(id);
        modelAndView.addObject("classId",id);
        modelAndView.addObject("classNum",num);
        return modelAndView;
    }
    /**
     *@Author:HouPeng
     *@Description:获取班级所有学生
     *@Date:14:53 2017/10/17
     */
    @RequestMapping("/get_class_students.do")
    @ResponseBody
    public ServerResponse<PageInfo<ClassStuVo>> getClassStudentByClassId(Integer classId,
                                                                         @RequestParam(name = "pageNum",defaultValue = "1") Integer pageNum,
                                                                         @RequestParam(name="pageSize",defaultValue = "10") Integer pageSize){
        ServerResponse<PageInfo<ClassStuVo>> result = sClassService.getAllStuVoByClassId(classId,pageNum,pageSize);
        return result;
    }

    /**
    * @Author:CuiCaijin
    * @Description:获得某个时间点的学生
    * @Date:9:57 2017/11/14
    */
    @GetMapping("/get_class_history_id.do")
    @ResponseBody
    public ServerResponse<PageInfo> getClassStudentByClassIdAndDate(String date,Integer classId,
                                                                    @RequestParam(name = "pageNum",defaultValue = "1") Integer pageNum,
                                                                    @RequestParam(name="pageSize",defaultValue = "10") Integer pageSize){
        return classHistoryService.getStuVoByClassIdAndHistory(pageNum,pageSize,classId,date);
    }

    @GetMapping("/get_class_history_name.do")
    @ResponseBody
    public ServerResponse<PageInfo> getClassStudentByClassIdAndDateAndName(String date,Integer classId,String name,
                                                                    @RequestParam(name = "pageNum",defaultValue = "1") Integer pageNum,
                                                                    @RequestParam(name="pageSize",defaultValue = "10") Integer pageSize){
        return classHistoryService.getStuVoByClassIdAndHistoryAndName(pageNum,pageSize,classId,date,name);
    }

    /**
    * @Author:Liuli
    * @Description:获取班级所有历史人数
    * @Date:8:48 2017/10/18
    */
    @GetMapping("/class_all_number.do")
    @ResponseBody
    public Map classAllNumber(int classid){
        return classNumService.getAllClassNum(classid);
    }

    /**
    * @Author:Liuli
    * @Description:获取班级学生所有历史班级
    * @Date:20:18 2017/10/23
    */
    @GetMapping("/class_all_stu_class.do")
    @ResponseBody
    public Map classAllStuClss(int classid){
        return classHistoryService.getAllStuHistory(classid);
    }
}

package com.hpe.controller.edu;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Teacher;
import com.hpe.service.TeacherService;
import com.hpe.util.ServerResponse;
import com.hpe.vo.TeacherInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/edu/tea" )
public class EduTeaController {

    @Autowired
    TeacherService teacherService;

    /**
     * @Author:HouPeng
     * @Description:获取所有老师
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/all_teacher.do")
    @ResponseBody
    public ServerResponse<PageInfo<TeacherInfo>> getAllTeacher(Integer pageNum, Integer pageSize) {
        //获取所有老师
        ServerResponse<PageInfo<TeacherInfo>> serverResponse = teacherService.getAllTeacher(pageNum, pageSize);

        return serverResponse;

    }

    /**
     * @Author:HouPeng
     * @Description:修改老师
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST, value = "/update_teacher.do")
    @ResponseBody
    public ServerResponse update(Teacher teacher) {
        ServerResponse result = teacherService.updateTeacher(teacher);
        return result;
    }
    /**
     * @Author:HouPeng
     * @Description:根据id 获取数据 传递数据到前台
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/toupdate_teacher.do")
    @ResponseBody
    public ServerResponse<Teacher> toupdate(Integer teacherId) {
        ServerResponse<Teacher> teacher =  teacherService.getTeacherById(teacherId);
        return teacher;
    }

    /**
     * @Author:HouPeng
     * @Description:删除
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/delete_teacher.do")
    @ResponseBody
    public ServerResponse delete(Integer teacherId) {
        ServerResponse result= teacherService.deleteById(teacherId);
        return result;
    }
    /**
     *@Author:HouPeng
     *@Description:增加老师
     *@Date:16:26 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST, value = "/add_teacher.do")
    @ResponseBody
    public ServerResponse add(Teacher teacher) {
        ServerResponse result= teacherService.addTeacher(teacher);
        return result;
    }

    /**
    * @Author:Liuli
    * @Description:获取所有老师
    * @Date:19:21 2017/10/12
    */
    @RequestMapping("/get_all_teacher.do")
    @ResponseBody
    public ServerResponse<List<TeacherInfo>> getAllTeacher(){
        List<TeacherInfo> teacherInfos=teacherService.getAllTeacher();
        return ServerResponse.createBySuccess(teacherInfos);
    }

    /**
     * @description 重置老师密码 123456
     * @author Fujt
     * @date 2017/10/26 11:43
     */
    @PostMapping("/reset_pwd.do")
    @ResponseBody
    public ServerResponse resetPwd(String id){
        return teacherService.resetPwd(id);
    }
}

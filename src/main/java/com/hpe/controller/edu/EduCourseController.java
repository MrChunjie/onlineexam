package com.hpe.controller.edu;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Course;
import com.hpe.service.CourseService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/edu/course")
public class EduCourseController {

    @Autowired
    CourseService courseService;
    /**
     * @Author:HouPeng
     * @Description:获取所有课程
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/all_course.do")
    @ResponseBody
    public ServerResponse<PageInfo<Course>> getAllCourse(Integer pageNum, Integer pageSize) {
        //获取所有课程
        ServerResponse<PageInfo<Course>> serverResponse = courseService.getAllCourse(pageNum, pageSize);
        return serverResponse;
    }

    /**
     * @Author:HouPeng
     * @Description:修改课程
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST, value = "/update_course.do")
    @ResponseBody
    public ServerResponse update(Course course) {
        ServerResponse result = courseService.updateCourse(course);
        return result;
    }
    /**
     * @Author:HouPeng
     * @Description:根据id 获取数据 传递数据到前台
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/toupdate_course.do")
    @ResponseBody
    public ServerResponse<Course> toupdate(Integer courseId) {
        Course course =  courseService.getCourseById(courseId);
        return ServerResponse.createBySuccess(course);
    }

    /**
     * @Author:HouPeng
     * @Description:删除
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/delete_course.do")
    @ResponseBody
    public ServerResponse delete(Integer courseId) {
        ServerResponse result= courseService.deleteById(courseId);
        return result;
    }
    /**
     *@Author:HouPeng
     *@Description:增加课程
     *@Date:16:26 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST, value = "/add_course.do")
    @ResponseBody
    public ServerResponse add(Course course) {
        ServerResponse result= courseService.addCourse(course);
        return result;
    }

    /**
    * @Author:Liuli
    * @Description:获取所有课程
    * @Date:19:39 2017/10/12
    */
    @RequestMapping("/get_all_course.do")
    @ResponseBody
    public ServerResponse<List<Course>> getAllCourse(){
        List<Course> courses=courseService.findAllCourse();
        return ServerResponse.createBySuccess(courses);
    }
}

package com.hpe.controller.edu;

import com.github.pagehelper.PageInfo;
import com.hpe.common.Const;
import com.hpe.dao.ClassTeacherMapper;
import com.hpe.pojo.*;
import com.hpe.service.*;
import com.hpe.util.ServerResponse;
import com.hpe.vo.ClassDirectionVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/edu/score")
public class EduScoreController {

    @Autowired
    private ScoreService paperService;

    @Autowired
    private ExamService examService;

    @Autowired
    private ClassTeacherService classTeacherService;

    @Autowired
    private SClassService sClassService;


    @Autowired
    CourseService courseService;
    /**
     * @Author:TangWenhao
     * @Description：查询课程平均成绩
     * @Date:10:59 2017/10/14
     */
    @RequestMapping(method = RequestMethod.POST, value = "/show_avg.do")
    @ResponseBody
    public ServerResponse<PageInfo> showAvg(@RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                            @RequestParam(name = "pageSize", defaultValue = "3") int pageSize) {
        // 搜索条件
        ServerResponse<PageInfo> response = examService.findCourseAvgByEdu(pageNum, pageSize);
        return response;
    }

    /**
     * @Author:TangWenhao
     * @Description: 成绩评估班级详细成绩
     * @Date:11:00 2017/10/14
     */
    @RequestMapping(method = RequestMethod.POST, value = "/show_avg_detail.do")
    @ResponseBody
    public ServerResponse<PageInfo> showAvgDetail(HttpSession session,
                                                  @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                                  @RequestParam(name = "pageSize", defaultValue = "3") int pageSize) {
        // todo 搜索条件
        SClass sClass =(SClass) session.getAttribute("sClass");
        //获取班级的ids
        int classId=sClass.getId();
        //查询某一班级-课程-试卷对应的班级平均成绩
        ServerResponse<PageInfo> response = examService.findAllExamByClassId(classId,pageNum, pageSize);
        return response;
    }

    /**
     * @Author:XuShengJie
     * @Description:求班级对应课程的平均成绩,每场考试的平均成绩
     * @Date:10:10 2017/10/15
     */
    @RequestMapping(method = RequestMethod.POST, value = "/get_class_avg_by_course.do")
    @ResponseBody
    public ServerResponse getClassAvgByCourse(@RequestParam(name = "classId") int classId,
                                              @RequestParam(name = "courseId") int courseId) {
        if(classId == -3 && courseId ==-3){
            //汇总当前没注销的所有班级的所有课程的成绩
            List<ClassDirectionVo> allClass = sClassService.getCurrentClasss();
            for(int i=0;i<allClass.size();i++){
                int cid = allClass.get(i).getId();
                List<Integer> courseIds = classTeacherService.getCoursesByClassId(cid);
                for(int j=0;j<courseIds.size();j++){
                    examService.getExamAvg(cid, courseIds.get(j));
                    classTeacherService.getClassAvgByCourse(cid, courseIds.get(j));
                }
            }
        }

        //汇总单个课程
        if(courseId != -1) {
            examService.getExamAvg(classId, courseId);
            classTeacherService.getClassAvgByCourse(classId, courseId);
        }else{
            //汇总一个班级的所有课程
            List<Integer> courseIds = classTeacherService.getCoursesByClassId(classId);
            for(int i=0;i<courseIds.size();i++){
                examService.getExamAvg(classId, courseIds.get(i));
                classTeacherService.getClassAvgByCourse(classId, courseIds.get(i));
            }
        }
        return ServerResponse.createBySuccess("汇总成功");
    }
    
    /**
     * @Author:XuShengJie
     * @Description:根据选择的班级获取他的所有开课
     * @Date:15:59 2017/10/27
     */
    @RequestMapping(method = RequestMethod.POST,value = "/get_course.do")
    @ResponseBody
    public ServerResponse getCourseByClassId(@RequestParam(name = "classId") Integer classId){
        List result = classTeacherService.getCourseByClassId(classId);

        return ServerResponse.createBySuccess(result);
    }

    /**
    * @Author:TangWenhao
    * @Description: 所有学生成绩管理
    * @Date:10:46 2017/11/13
    */
    @RequestMapping(method = RequestMethod.POST, value = "/stu_detail.do")
    @ResponseBody
    public ServerResponse<PageInfo> showAvg(@RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                            @RequestParam(name = "pageSize", defaultValue = "3") int pageSize,
                                            @RequestParam(name = "stuName", defaultValue = "") String stuName,
                                            @RequestParam(name = "classOther", defaultValue = "") String classOther) {
        // 搜索条件
        ServerResponse<PageInfo> response = paperService.findStuScore(stuName,classOther,pageNum, pageSize);
        return response;
    }
}
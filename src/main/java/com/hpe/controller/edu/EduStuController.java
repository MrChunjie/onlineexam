package com.hpe.controller.edu;

import com.github.pagehelper.PageInfo;
import com.hpe.common.Const;
import com.hpe.pojo.Admin;
import com.hpe.pojo.ClassHistory;
import com.hpe.pojo.Direction;
import com.hpe.pojo.StudentInfo;
import com.hpe.service.*;
import com.hpe.util.ResponseCode;
import com.hpe.util.ServerResponse;
import com.hpe.vo.StudentInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/edu/stu")
public class EduStuController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentInfoService studentInfoService;

    @Autowired
    private ClassHistoryService classHistoryService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private DirectionService directionService;

    @Autowired
    private ScoreService scoreService;

    /**
     * @Author:CuiCaijin
     * @Description: 查询所有学生列表
     * @Date:9:22 2017/9/28
     */
    @PostMapping("/list.do")
    @ResponseBody
    public ServerResponse<PageInfo> getStudentList(HttpSession session, @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                                   @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        //判断管理员是否登录
        if (admin == null) {
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        //判断管理员是否有权限对此进行操作
        if (admin.getType() == Const.AdminType.ENTERPRISE) {
            return ServerResponse.createByErrorMsg("您没有权限进行此操作");
        }
        return studentInfoService.getStudentList(pageNum, pageSize);
    }

    /**
     * @description 获取所有的方向
     * @author Fujt
     * @date 2017/9/30 14:51
     */
    @GetMapping(value = "/get_all_direction.do")
    @ResponseBody
    public ServerResponse<List<Direction>> getAllDirection(){
        return directionService.getAllDirections();
    }



    @GetMapping("/level_list.do")
    @ResponseBody
    public ServerResponse<PageInfo> getNotOnlineStudentList(HttpSession session, @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                                            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        //判断管理员是否登录
        if (admin == null) {
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        //判断管理员是否有权限对此进行操作
        if (admin.getType() == Const.AdminType.ENTERPRISE) {
            return ServerResponse.createByErrorMsg("您没有权限进行此操作");
        }
        return studentInfoService.getNotOnlineStudentList(pageNum, pageSize);
    }

    /**
     * @Author:CuiCaijin
     * @Description: 修改学生
     * @Date:9:23 2017/9/28
     */
    @PostMapping("/update.do")
    @ResponseBody
    public ServerResponse<StudentInfo> updateStudent(HttpSession session, StudentInfo studentInfo) {
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        //判断管理员是否登录
        if (admin == null) {
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        //判断管理员是否有权限对此进行操作
        if (admin.getType() == Const.AdminType.ENTERPRISE) {
            return ServerResponse.createByErrorMsg("您没有权限进行此操作");
        }
        return studentInfoService.updateByPrimaryKeySelective(studentInfo);
    }


    /**
     * @Author:CuiCaijin
     * @Description:获取部分信息
     * @Date:21:53 2017/10/8
     */
    @GetMapping("/part.do")
    @ResponseBody
    public ServerResponse<StudentInfo> getStudentPartById(HttpSession session, int id) {
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        //判断管理员是否登录
        if (admin == null) {
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        //判断管理员是否有权限对此进行操作
        if (admin.getType() == Const.AdminType.ENTERPRISE) {
            return ServerResponse.createByErrorMsg("您没有权限进行此操作");
        }
        return studentInfoService.getStudentPartInfo(id);
    }

    /**
     * @Author:CuiCaijin
     * @Description:模糊查询
     * @Date:8:48 2017/10/9
     */
    @PostMapping("/search.do")
    @ResponseBody
    public ServerResponse<PageInfo> getStudentInfoByCondition(HttpSession session, String spare, String name, String school,
                                                              @RequestParam(name = "direction", defaultValue = "-1") int direction,
                                                              @RequestParam(name = "pageSize", defaultValue = "8") int pageSize,
                                                              @RequestParam(name = "pageNum", defaultValue = "1") int pageNum) {
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        //判断管理员是否登录
        if (admin == null) {
            return ServerResponse.createByErrorCodeMsg(ResponseCode.NEED_LOGIN.getCode(), ResponseCode.NEED_LOGIN.getDesc());
        }
        //判断管理员是否有权限对此进行操作
        if (admin.getType() == Const.AdminType.ENTERPRISE) {
            return ServerResponse.createByErrorMsg("您没有权限进行此操作");
        }
        return studentInfoService.getStudentInfoByCondition(pageNum, pageSize, school, direction, spare, name);
    }

    /**
     * @Author:CuiCaijin
     * @Description:展示学生详细信息
     * @Date:11:39 2017/10/9
     */
    @GetMapping("/detail.do")
    public ModelAndView getDetail(HttpSession session, int id) {
        ModelAndView view = new ModelAndView("edu-stu-detail");
        ServerResponse<StudentInfoVo> studentInfo = studentInfoService.getDetailById(id);
        view.addObject("student", studentInfo.getData());
        return view;
    }

    /**
     * @Author:CuiCaijin
     * @Description:毕业当前在读的学生
     * @Date:14:23 2017/10/9
     */
    @GetMapping("/graduation_all.do")
    @ResponseBody
    public ServerResponse<String> graduationStudent() {
        return studentInfoService.graduationStudent();
    }

    /**
     * @Author:Liuli
     * @Description:获取更新模态框内容并更新班级历史
     * @Date:16:54 2017/10/12
     */
    @PostMapping("/update_part.do")
    @ResponseBody
    public ServerResponse<String> updateStudentPart(int id, @RequestParam(name = "state", defaultValue = "-1") int state,
                                                    @RequestParam(name = "newId", defaultValue = "-1") int newId,
                                                    @RequestParam(name = "direction", defaultValue = "-1") int direction, String spare) {

        return studentInfoService.updateStudentPartInfomation(id, state, newId, direction, spare);
    }

    /**
     * @description 重置学生密码
     * @author Fujt
     * @date 2017/10/26 14:00
     */
    @PostMapping("/reset_pwd.do")
    @ResponseBody
    public ServerResponse resetPwdByPk(String id) {
        return studentService.resetPwd(id);
    }

    /**
    * @Author:cuicaijin
    * @Description:下载学生详情excel
    * @Date:23:29 2017/10/27
    */
    @GetMapping("/get_excel.do")
    @ResponseBody
    public void outputExcel(HttpServletRequest request, HttpServletResponse response,String begin,String end) throws IOException {
        groupService.outputStudentInfoAndGradeExcel(request,response,begin,end);
    }

    /**
    * @Author:Liuli
    * @Description:获取某段时间考卷数，不为0即可
    * @Date:23:45 2017/10/27
    */
    @PostMapping("/get_score_count.do")
    @ResponseBody
    public ServerResponse<Integer> getScoreCount(String begin,String end){
        int result = scoreService.countScores(begin, end);
        if (result <= 0) {
            return ServerResponse.createByError();
        }

        return ServerResponse.createBySuccess();
    }

    @GetMapping("/get_info_excel_title.do")
    @ResponseBody
    public void getInfoExcelTitle(HttpServletRequest request,HttpServletResponse response) throws IOException {
        studentInfoService.outputStudentRegisterExcelTitle(request, response);
    }

    @PostMapping("/upload_student_info_excel.do")
    @ResponseBody
    public ServerResponse uploadExcel(HttpServletRequest req) throws Exception {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) req;
        //根据参数名获取到文件
        MultipartFile file = multipartRequest.getFile("file");
        if (file == null) {
            return ServerResponse.createByErrorMsg("未获取到文件, 请重新上传");
        }
        //文件名
        String name = file.getOriginalFilename();
        //后缀
        String suffix = name.substring(name.indexOf("."));

        if (!".xls".equals(suffix) && !".xlsx".equals(suffix)) {
            return ServerResponse.createByErrorMsg("请选择.xls或者.xlsx后缀的文件");
        }

        InputStream inputStream = file.getInputStream();
        if (inputStream == null) {
            return ServerResponse.createByErrorMsg("输入流为空");
        }

        return studentInfoService.registerStudentFromExcel(inputStream);

    }

}

package com.hpe.controller.edu;


import com.hpe.service.SettingService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/edu/setting")
public class EduSettingController {

    @Autowired
    private SettingService settingService;


    /**
     * @description 获取注册状态
     * @author Fujt
     * @date 2017/10/23 11:44
     */
    @GetMapping("/get_register_state.do")
    @ResponseBody
    public ServerResponse getRegisterState(){
        return settingService.getRegisterState();
    }

    /**
     * @description 更新注册状态
     * @author Fujt
     * @date 2017/10/23 11:46
     */
    @PostMapping("/update_register_state.do")
    @ResponseBody
    public ServerResponse updateRegisterState(int registerState){
        return settingService.updateRegisterState(registerState);
    }

}

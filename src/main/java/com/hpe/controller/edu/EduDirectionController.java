package com.hpe.controller.edu;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Direction;
import com.hpe.pojo.SClass;
import com.hpe.service.DirectionService;
import com.hpe.service.SClassService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by dell on 2017/9/28.
 */
@Controller
@RequestMapping(value = "/edu/direction")
public class EduDirectionController {

    @Autowired
    DirectionService directionService;

    @Autowired
    SClassService sClassService;

    /**
     * @Author:Liuli
     * @Description:获取方向列表
     * @Date:16:20 2017/9/28
     */
    @GetMapping(value = "/get_direction.do")
    @ResponseBody
    public ServerResponse<PageInfo> getDirection(HttpSession session, @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                                 @RequestParam(name = "pageSize", defaultValue = "3") int pageSize) {
        return directionService.getDirections(pageNum, pageSize);
    }

    /**
     * @Author:Liuli
     * @Description:更新方向名称
     * @Date:16:20 2017/9/28
     */
    @PostMapping(value = "/update_direction_name.do")
    @ResponseBody
    public ServerResponse<String> updateDirectionName(Direction direction) {
        return directionService.updateDirectionName(direction);
    }

    /**
     * @Author:Liuli
     * @Description:增加方向
     * @Date:16:20 2017/9/28
     */
    @PostMapping(value = "/add_direction.do")
    @ResponseBody
    public ServerResponse<String> addDirection(Direction direction) {
        return directionService.addDirection(direction);
    }


    @GetMapping("/get_one_direction.do")
    @ResponseBody
    public ServerResponse<Direction> getDirectionById(int id) {
        return directionService.getByPrimaryKey(id);
    }

    /**
     * @Author:HouPeng
     * @Description:获取所有方向
     * @Date:11:28 2017/10/16
     */
    @GetMapping("/get_all_directions.do")
    @ResponseBody
    public ServerResponse<List<Direction>> getAllDirections() {
        return directionService.getAllDirections();
    }

    /**
     * @description 根据id删除
     * @author Fujt
     * @date 2017/10/20 11:38
     */
    @PostMapping("/delete_by_pk.do")
    @ResponseBody
    public ServerResponse deleteByPk(Integer id) {
        return directionService.deleteByPrimaryKey(id);
    }

    @GetMapping("/get_name.do")
    @ResponseBody
    public ServerResponse<String> getDirectionNam(int id) {
        return directionService.getDirectionNameById(id);
    }

    /**
     * @Author:Liuli
     * @Description:设置该方向可选并设置关联班级
     * @Date:10:50 2017/11/15
     */
    @PostMapping("/open_direction.do")
    @ResponseBody
    public ServerResponse<String> openDirection(@RequestParam(name = "directionId") int directionid,
                                                @RequestParam(name = "classId") int classid) {
        Direction direction = directionService.getDirection(directionid);
        int dresult = directionService.openDirection(direction);
        SClass sClass = sClassService.getSClass(classid);
        int cresult = sClassService.addClassDirection(sClass);
        if (dresult == 0) {
            return ServerResponse.createByErrorMsg("方向不存在");
        } else if (cresult == 0) {
            return ServerResponse.createByErrorMsg("班级不存在");
        } else {
            return ServerResponse.createBySuccessMsg("添加成功");
        }
    }

    /**
     * @Author:Liuli
     * @Description:移除方向可选
     * @Date:11:01 2017/11/15
     */
    @PostMapping("/close_direction.do")
    @ResponseBody
    public ServerResponse<String> openDirection(@RequestParam(name = "directionId") int directionid) {
        Direction direction = directionService.getDirection(directionid);
        int dresult = directionService.closeDirection(direction);
        SClass sClass = sClassService.getDirectionClass(directionid);
        int cresult = sClassService.deleteClassDirection(sClass);
        if (dresult == 0) {
            return ServerResponse.createByErrorMsg("方向不存在");
        } else if (cresult == 0) {
            return ServerResponse.createByErrorMsg("班级不存在");
        } else {
            return ServerResponse.createBySuccessMsg("移除成功");
        }
    }


    /**
     * @description 获得方向关联班级的名称
     * @author Fujt
     * @date 2017/11/15 19:23
     */
    @PostMapping("/get_conn_class.do")
    @ResponseBody
    public ServerResponse<String> getConnectClassName(int directionId){
        SClass sClass = sClassService.getDirectionClass(directionId);
        if (sClass == null) {
            ServerResponse.createByErrorMsg("班级不存在");
        }
        return ServerResponse.createBySuccessMsg(sClass.getName());
    }

}

package com.hpe.controller.edu;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.ClassTeacher;
import com.hpe.service.ClassTeacherService;
import com.hpe.util.ServerResponse;
import com.hpe.vo.ClassTeacherVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by chunjie on 2017/10/12.
 */
@Controller
@RequestMapping("/edu/class_teacher")
public class EduClassTeacherController {

    @Autowired
    ClassTeacherService classTeacherService;

    /**
     * @Author:Liuli
     * @Description:分页显示所有关系
     * @Date:16:45 2017/10/12
     */
    @RequestMapping("/get_all_class_teacher.do")
    @ResponseBody
    public ServerResponse<PageInfo> getAllClassTeacher(@RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                                       @RequestParam(name = "pageSize", defaultValue = "3") int pageSize) {
        return classTeacherService.getAllClassTeacher(pageNum, pageSize);
    }

    /**
     * @Author:Liuli
     * @Description:根据老师分页获取关系
     * @Date:16:45 2017/10/12
     */
    @RequestMapping("/get_class_teacher_by_teacher.do")
    @ResponseBody
    public ServerResponse<List<ClassTeacherVo>> getClassTeacherByTeacher(@RequestParam(name = "teacher_id") int teacher_id) {
        return classTeacherService.getClassTeacherByTeacherId(teacher_id);
    }

    /**
     * @Author:Liuli
     * @Description:根据班级分页获取关系
     * @Date:16:49 2017/10/12
     */
    @RequestMapping("/get_class_teacher_by_class.do")
    @ResponseBody
    public ServerResponse<List<ClassTeacherVo>> getClassTeacherByClass(@RequestParam(name = "class_id") int class_id) {
        return classTeacherService.getClassTeacherByClassId(class_id);
    }

    /**
     * @Author:Liuli
     * @Description:根据课程分页获取班级
     * @Date:16:51 2017/10/12
     */
    @RequestMapping("/get_class_teacher_by_course.do")
    @ResponseBody
    public ServerResponse<List<ClassTeacherVo>> getClassTeacherByCourse(@RequestParam(name = "course_id") int course_id) {
        return classTeacherService.getClassTeacherByCourseId(course_id);
    }

    /**
     * @Author:Liuli
     * @Description:修改关系
     * @Date:16:55 2017/10/12
     */
    @RequestMapping("/update_class_teacher.do")
    @ResponseBody
    public ServerResponse<String> updateClassTeacher(ClassTeacher classTeacher) {
        return classTeacherService.changeClassTeacher(classTeacher);
    }

    /**
     * @Author:Liuli
     * @Description:增加关系
     * @Date:17:02 2017/10/12
     */
    @PostMapping("/add_class_teacher.do")
    @ResponseBody
    public ServerResponse<String> addClassTeacher(ClassTeacher classTeacher) {
        return classTeacherService.addClassTeacher(classTeacher);
    }


    /**
     * @description 根据条件查询，条件可为空
     * @author Fujt
     * @date 2017/10/18 16:14
     */
    @PostMapping("/search_by_condition.do")
    @ResponseBody
    public ServerResponse<PageInfo> searchByCondition(int pageSize, int pageNum, String courseId, String teacherId, String classId) {
        return classTeacherService.findByConditions(pageSize, pageNum, courseId, teacherId, classId);
    }


    @PostMapping("/delete_by_pk.do")
    @ResponseBody
    public ServerResponse deleteByPrimaryKey(ClassTeacher classTeacher){
        return classTeacherService.deleteByPrimaryKey(classTeacher);
    }
}

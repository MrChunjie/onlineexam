package com.hpe.controller.edu;

import com.github.pagehelper.PageInfo;
import com.hpe.dao.CourseMapper;
import com.hpe.dao.SClassMapper;
import com.hpe.pojo.Course;
import com.hpe.pojo.Exam;
import com.hpe.pojo.SClass;
import com.hpe.service.BigQuestionService;
import com.hpe.service.ExamService;
import com.hpe.service.QuestionService;
import com.hpe.util.ServerResponse;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fujt
 * @description
 * @date 18:392017/10/23
 */
@Controller
@RequestMapping("/edu/paper")
public class EduPaperController {

    @Autowired
    private ExamService examService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private BigQuestionService bigQuestionService;

    @Autowired
    SClassMapper sClassMapper;

    @Autowired
    CourseMapper courseMapper;

    /**
     * @description 获得所有的考试
     * @author Fujt
     * @date 2017/10/23 19:16
     */
    @GetMapping("/get_all_exam.do")
    @ResponseBody
    public ServerResponse<PageInfo> getAllExam(int pageNum, int pageSize){
        return examService.findAllExam(pageNum, pageSize);
    }

    @GetMapping("/export_pdf.do")
    @ResponseBody
    public ServerResponse exportPdf(HttpServletRequest request, HttpServletResponse response, int examId) throws IOException, DocumentException {
        return examService.exportPdf(request, response, examId);
    }

    /**
     * @Author:XuShengJie
     * @Description:查看一张试卷的详细情况
     * @Date:17:26 2017/10/16
     */
    @RequestMapping(method = RequestMethod.GET,value = "/find_this_exam.do")
    public ModelAndView findThisExam(@RequestParam("examId") int examId, ModelAndView modelAndView){
        //根据试卷id获取试卷信息
        ServerResponse<Exam> response =  examService.selectByPrimaryKey(Integer.valueOf(examId));
        Exam test=response.getData();

        String ques = test.getQuestions(); // 题目主键的字符串 2,3,4,5;12,1,3;123,123,1===> [2,3,4,5]
        //单选题
        String[] typesQue = ques.split(";");
        // 根据test试卷 questions , 查询单选题。
        List queList = questionService.findByIds(typesQue[0].split(","));
        //  查询  简答题 或 编程题。
        List que1List  = new ArrayList();
        if(typesQue.length >= 2 && typesQue[1].length() >= 1 ){
            que1List = bigQuestionService.findBigQuesByIds(typesQue[1].split(","),1);
        }
        List que2List  = new ArrayList();//编程题。
        if(typesQue.length >= 3 &&  typesQue[2].length() >= 1 ){
            que2List = bigQuestionService.findBigQuesByIds(typesQue[2].split(","),2);
        }
        //获取科目信息
        Course course = courseMapper.selectByPrimaryKey(test.getCourseid());
        //获取班级信息
        List<SClass> sClasses = new ArrayList<>();
        sClasses = sClassMapper.getListForIds(test.getClassids().split(","));

        //填装信息
        modelAndView.addObject("examinfo",test);
        modelAndView.addObject("queList",queList);
        modelAndView.addObject("queListSize", queList == null?0:queList.size());
        modelAndView.addObject("queList1", que1List);
        modelAndView.addObject("queListSize1", que1List == null?0: que1List.size());
        modelAndView.addObject("queList2", que2List);
        modelAndView.addObject("queListSize2",  que2List == null?0:que2List.size());
        modelAndView.addObject("course",course);
        modelAndView.addObject("sclass",sClasses);
        modelAndView.setViewName("/t-history-exam");
        return modelAndView;
    }


}

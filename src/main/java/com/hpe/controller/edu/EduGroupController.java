package com.hpe.controller.edu;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Direction;
import com.hpe.pojo.Exam;
import com.hpe.pojo.Group;
import com.hpe.service.DirectionService;
import com.hpe.service.ExamService;
import com.hpe.service.GroupService;
import com.hpe.util.ServerResponse;
import com.hpe.vo.UpdateClassVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.List;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/edu/group")
public class EduGroupController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private ExamService examService;

    @Autowired
    private DirectionService directionService;


    @GetMapping("/get_divide_result.do")
    @ResponseBody
    public ServerResponse<Boolean> ifClassDivide(){
        return groupService.ifClassDivide();
    }

    @GetMapping("/group_stu_count.do")
    @ResponseBody
    public ServerResponse<Integer> getStudentCount(){
        return groupService.showStudentCount();
    }


    /**
     * @description 获取开发方向或者测试方向的考试
     * @author Fujt
     * @date 2017/10/12 15:37
     * @param classIds 开发方向或者测试方向的班级ids
     */
    @PostMapping("/get_exam.do")
    @ResponseBody
    public ServerResponse<List<Exam>> getExamByClassIds(String classIds){
        return examService.getExamByClassIds(classIds);
    }


    /**
     * @description 查询开发班（测试班）若干次次考试的成绩总和并排序
     * @author Fujt
     * @date 2017/10/13 14:54
     * @param type 开发或者测试 id
     * @param exams 考试的ids
     */
    @PostMapping("/summary_and_group.do")
    @ResponseBody
    public ServerResponse<List<Group>> getScoreByExam(@RequestParam(name = "type") String type,
                                                      @RequestParam(name = "exams") String exams){
        return groupService.findAndInsertGroup(type, exams);
    }


    /**
     * @description 获得所有的分组
     * @author Fujt
     * @date 2017/10/13 19:36
     */
    @GetMapping("/get_all_group.do")
    @ResponseBody
    public ServerResponse<PageInfo> getAllGroup(int pageNum, int pageSize){
        return groupService.showAllGroupVo(pageNum, pageSize);
    }

    /**
     * @description 根据主键获取Direction
     * @author Fujt
     * @date 2017/10/13 20:08
     */
    @PostMapping("/get_direction_name.do")
    @ResponseBody
    public ServerResponse<Direction> getDirectionByPrimaryKey(int id){
        return directionService.getByPrimaryKey(id);
    }

    /**
     * @description 根据exam ids 获取 exam
     * @author Fujt
     * @date 2017/10/13 21:37
     * @param ids exam ids
     */
    @PostMapping("/get_exam_name.do")
    @ResponseBody
    public ServerResponse<List<Exam>> getExamByIds(String ids){
        return examService.getExamByIds(ids.split(","));
    }


    /**
     * @description 输出流到客户端下载
     * @author Fujt
     * @date 2017/10/15 11:11
     */
    @GetMapping("/output_excel.do")
    public void outputExcel(HttpServletRequest req, HttpServletResponse resp, int id) throws Exception {
        groupService.outputExcelByGroupId(req, resp, id);
    }

    @PostMapping("/upload_excel.do")
    @ResponseBody
    public ServerResponse uploadExcel(HttpServletRequest req) throws Exception {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) req;
        //根据参数名获取到文件
        MultipartFile file = multipartRequest.getFile("file");
        if (file == null) {
            return ServerResponse.createByErrorMsg("未获取到文件, 请重新上传");
        }
        //文件名
        String name = file.getOriginalFilename();
        //后缀
        String suffix = name.substring(name.indexOf("."));

        if (!".xls".equals(suffix) && !".xlsx".equals(suffix)) {
            return ServerResponse.createByErrorMsg("请选择.xls或者.xlsx后缀的文件");
        }

        InputStream inputStream = file.getInputStream();
        if (inputStream == null) {
            return ServerResponse.createByErrorMsg("输入流为空");
        }

        return groupService.inputExcel(inputStream);

    }

    /**
    * @Author:CuiCaijin
    * @Description:根据groupId获得方向
    * @Date:10:53 2017/10/18
    */
    @GetMapping("/get_direction.do")
    @ResponseBody
    public ServerResponse<Integer> getDirection(int groupId){
        return groupService.getDirectionByGroupId(groupId);
    }

    /**
    * @Author:CuiCaijin
    * @Description:根据groupId获得人数
    * @Date:10:53 2017/10/18
    */
    @GetMapping("/get_count.do")
    @ResponseBody
    public ServerResponse<Integer> getCountByGroupId(int groupId){
        return groupService.getStudentCountByGroupId(groupId);
    }

    /**
    * @Author:CuiCaijin
    * @Description:输出分班结果excel
    * @Date:10:54 2017/10/18
    */
    @GetMapping("/output_divide_excel.do")
    @ResponseBody
    public void outputDivideClassResultExcel(HttpServletRequest req, HttpServletResponse resp, int id) throws Exception {
        groupService.outputDivideCLassResultExcelByGroupId(req,resp,id);
    }

    @GetMapping("/get_class_count.do")
    @ResponseBody
    public ServerResponse getClassCount(UpdateClassVo classVo){
        return groupService.getClassCountByGroupId(classVo);
    }

    @PostMapping("/update_class_state.do")
    @ResponseBody
    public ServerResponse<String> updateClassState(UpdateClassVo classVo){
        return groupService.updateClassStateByDate(classVo);
    }
}

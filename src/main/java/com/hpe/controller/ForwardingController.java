package com.hpe.controller;

import com.hpe.common.Const;
import com.hpe.pojo.*;
import com.hpe.vo.UsualScores;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

/**
 * @Author:Liuli
 * @Description:跳转控制器
 * @Date:21:12 2017/9/24
 */
@Controller
public class ForwardingController {

    /**
     * @Author:Liuli
     * @Description:跳转至完善信息页面
     * @Date:21:11 2017/9/24
     */
    @RequestMapping(value = "/stu-complete.do")
    public String stuComplete(HttpSession session) {
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        if(student == null){
            return "redirect:login.jsp";
        }
        return "stu-complete";
    }

    /**
     * @Author:Liuli
     * @Description:跳转至修改信息页面
     * @Date:21:11 2017/9/24
     */
    @RequestMapping(value = "/stu-changepwd.do")
    public String stuChangepwd(HttpSession session) {
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        if(student == null){
            return "redirect:login.jsp";
        }
        return "stu-changepwd";
    }

    /**
     * @Author:Liuli
     * @Description:跳转至进行考试页面
     * @Date:21:11 2017/9/24
     */
    @RequestMapping(value = "/stu-exam.do")
    public String stuExam(HttpSession session) {
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        if(student == null){
            return "redirect:login.jsp";
        }
        return "stu-exam";
    }

    /**
     * @Author:Liuli
     * @Description:跳转至历史成绩页面
     * @Date:21:12 2017/9/24
     */
    @RequestMapping(value = "/stu-ehistory.do")
    public String stuEhistory(HttpSession session) {
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        if(student == null){
            return "redirect:login.jsp";
        }
        return "stu-ehistory";
    }

    /**
    * @Author:TangWenhao
    * @Description: 跳转至历史成绩详细信息页面
    * @Date:14:11 2017/10/16
    */
    @RequestMapping(value = "/stu-ehistory-detail.do")
    public String stuEhistoryDetail(HttpSession session,
                                    @RequestParam(name = "stuId",defaultValue = "-1") int stuId,
                                    @RequestParam(name = "examId",defaultValue = "-1") int examId) {
        Score score=new Score();
        score.setsId(stuId);
        score.setExamId(examId);
        session.setAttribute("score",score);
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        if(student == null){
            return "redirect:login.jsp";
        }
        return "stu-ehistory-detail";
    }

    /**
     * @Author:Liuli
     * @Description:跳转至查看班级页面
     * @Date:21:12 2017/9/24
     */
    @RequestMapping(value = "/stu-class.do")
    public String stuClass(HttpSession session) {
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        if(student == null){
            return "redirect:login.jsp";
        }
        return "stu-class";
    }

    /**
     * @Author:Liuli
     * @Description:跳转至完善技能页面
     * @Date:21:12 2017/9/24
     */
    @RequestMapping(value = "/stu-grade.do")
    public String stuGrade(HttpSession session) {
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        if(student == null){
            return "redirect:login.jsp";
        }
        return "stu-grade";
    }

    /**
     * @Author:Liuli
     * @Description:跳转至企业报名页面
     * @Date:21:12 2017/9/24
     */
    @RequestMapping(value = "/stu-sign.do")
    public String stuSign(HttpSession session) {
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        if(student == null){
            return "redirect:login.jsp";
        }
        return "stu-sign";
    }

    /**
     * @Author:Liuli
     * @Description:跳转至报名结果页面
     * @Date:21:12 2017/9/24
     */
    @RequestMapping(value = "/stu-result.do")
    public String stuResult(HttpSession session) {
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        if(student == null){
            return "redirect:login.jsp";
        }
        return "stu-result";
    }

    /**
     * @Author:Liuli
     * @Description:跳转至查看生涯页面
     * @Date:21:12 2017/9/24
     */
    @RequestMapping(value = "/stu-career.do")
    public String stuCareer(HttpSession session) {
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        if(student == null){
            return "redirect:login.jsp";
        }
        return "stu-career";
    }

    /**
    * @Author:Liuli
    * @Description:企合跳转至增加页面
    * @Date:23:19 2017/9/25
    */
    @RequestMapping(value = "/ep-addep.do")
    public String epAddep(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "ep-addep";
    }

    /**
    * @Author:Liuli
    * @Description:企合跳转岗位管理
    * @Date:11:39 2017/10/9
    */
    @RequestMapping(value = "/ep-post.do")
    public String epPost(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "ep-post";
    }

    /**
    * @Author:Liuli
    * @Description:企合跳转至上传文件页面
    * @Date:23:21 2017/9/25
    */
    @RequestMapping(value = "/ep-input.do")
    public String epInput(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "ep-input";
    }

    /**
    * @Author:Liuli
    * @Description:企合跳转至下载文件页面
    * @Date:23:22 2017/9/25
    */
    @RequestMapping(value = "/ep-out.do")
    public String epOut(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "ep-out";
    }

    /**
    * @Author:Liuli
    * @Description:教务跳转班级管理
    * @Date:23:25 2017/9/25
    */
    @RequestMapping(value = "/edu-class.do")
    public String eduClass(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-class";
    }

    /**
    * @Author:Liuli
    * @Description:教务跳转课程管理
    * @Date:23:26 2017/9/25
    */
    @RequestMapping(value = "/edu-course.do")
    public String eduCourse(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-course";
    }

    /**
    * @Author:Liuli
    * @Description:教务跳转方向管理
    * @Date:10:05 2017/9/26
    */
    @RequestMapping(value = "/edu-direction.do")
    public String eduDirection(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-direction";
    }

    /**
    * @Author:Liuli
    * @Description:教务跳转分班管理
    * @Date:23:27 2017/9/25
    */
    @RequestMapping(value = "/edu-group.do")
    public String eduGroup(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-group";
    }

    /**
    * @Author:Liuli
    * @Description:教务跳转成绩管理
    * @Date:23:28 2017/9/25
    */
    @RequestMapping(value = "/edu-score.do")
    public String eduScore(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-score";
    }

    /**
    * @Author:TangWenhao
    * @Description: 教务端学生成绩管理
    * @Date:11:11 2017/11/13
    */
    @RequestMapping(value = "/edu-score-stu.do")
    public String eduScoreStu(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-score-stu";
    }

    @RequestMapping(value = "/edu-score-class.do")
    public String eduScoreClass(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-score-class";
    }
    /**
    * @Author:Liuli
    * @Description:教务跳转学生管理
    * @Date:23:28 2017/9/25
    */
    @RequestMapping(value = "/edu-stu.do")
    public String eduStu(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-stu";
    }

    /**
    * @Author:Liuli
    * @Description:教务跳转老师管理
    * @Date:23:30 2017/9/25
    */
    @RequestMapping(value = "/edu-tea.do")
    public String eduTea(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-tea";
    }



    /**
    * @Author:Liuli
    * @Description:老师跳转批改试卷
    * @Date:23:31 2017/9/25
    */
    @RequestMapping(value = "/t-correct.do")
    public String tCorrect(HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        if(teacher == null){
            return "redirect:login.jsp";
        }
        return "t-correct";
    }

    /**
    * @Author:TangWenhao
    * @Description:学生平时成绩列表
    * @Date:9:53 2017/10/12
    */
    @RequestMapping(value = "/t-usual.do")
    public String tUsual(HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        if(teacher == null){
     return "redirect:login.jsp";
     }
     return "t-usual";
     }

     /**
     * @Author:TangWenhao
     * @Description:展开打分页面
     * @Date:9:53 2017/10/12
     */
    @RequestMapping(value = "/t-usual-update.do")
    public String tUsualUpdate(HttpSession session,
                               @RequestParam(name = "sId",defaultValue = "-1") int sId,
                               @RequestParam(name = "examId",defaultValue = "-1") int examId,
                               @RequestParam(name = "classId",defaultValue = "-1") int classId){
        UsualScores usualScores=new UsualScores();
        usualScores.setsId(sId);
        usualScores.setExamId(examId);
        usualScores.setClassId(classId);
        usualScores.setTestName("firstTime");
        session.setAttribute("usualScores",usualScores);
        Teacher teacher = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        if(teacher == null){
            return "redirect:login.jsp";
        }
        return "t-usual-update";
    }

    /**
    * @Author:TangWenhao
    * @Description:课程成绩评估的班级详情
    * @Date:11:24 2017/10/14
    */
    @RequestMapping(value = "/edu-score-forward.do")
    public String eduScoreForward(HttpSession session,
                                  @RequestParam(name = "classId",defaultValue = "-1") int classId){
        SClass sClass=new SClass();
        sClass.setId(classId);
        session.setAttribute("sClass",sClass);
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-score-detail";
    }

    /**
    * @Author:Liuli
    * @Description:老师跳转成绩评估
    * @Date:23:32 2017/9/25
    */
    @RequestMapping(value = "/t-eva.do")
    public String tEva(HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        if(teacher == null){
            return "redirect:login.jsp";
        }
        return "t-eva";
    }

    /**
    * @Author:TangWenhao
    * @Description: 教师端成绩评估详细信息
    * @Date:11:15 2017/10/16
    */
    @RequestMapping(value = "/t-eva-detail.do")
    public String tEvaDetail(HttpSession session,
                             @RequestParam(name = "classId") int classId,
                             @RequestParam(name = "examId") int examId){
        Score score=new Score();
        score.setExamId(examId);
        score.setClassId(classId);
       /* classTeacher.setClassId(classId);
        classTeacher.setCourseId(examId);
        session.setAttribute("classTeacher",classTeacher);*/
       session.setAttribute("score",score);
        Teacher teacher = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        if(teacher == null){
            return "redirect:login.jsp";
        }
        return "t-eva-detail";
    }

    /**
     * @Author:XuShengJie
     * @Description:老师跳转流水阅卷
     * @Date:16:05 2017/10/10
     */
    @RequestMapping(value = "/t-lscorrect.do")
    public String tLSCorrect(HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        if(teacher == null){
            return "redirect:login.jsp";
        }
        return "t-lscorrect";
    }

    /**
     * @Author:XuShengJie
     * @Description:流水阅卷打分页面
     * @Date:10:25 2017/10/12
     */
    @RequestMapping(value = "/t-lscorrectgf.do")
    public String tLSCorrectGf(HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        if(teacher == null){
            return "redirect:login.jsp";
        }
        return "t-lscorrectgf";
    }
    /**
    * @Author:Liuli
    * @Description:老师跳转组卷
    * @Date:23:33 2017/9/25
    */
    @RequestMapping(value = "/t-exam.do")
    public String tExam(HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        if(teacher == null){
            return "redirect:login.jsp";
        }
        return "t-exam";
    }

    /**
    * @Author:Liuli
    * @Description:老师跳转题库管理
    * @Date:23:34 2017/9/25
    */
    @RequestMapping(value = "/t-question.do")
    public String tQuestion(HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        if(teacher == null){
            return "redirect:login.jsp";
        }
        return "t-question";
    }

    /**
     *@Author:HouPeng
     *@Description:老师跳转技能管理
     *@Date:11:53 2017/10/9
     */
    @RequestMapping(value = "/t-grade.do")
    public String tGrade(HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        if(teacher == null){
            return "redirect:login.jsp";
        }
        return "t-grade";
    }

    /**
    * @Author:Liuli
    * @Description:教务跳转教师课程分配
    * @Date:11:56 2017/10/11
    */
    @RequestMapping(value = "/edu-class-teacher.do")
    private String eduClassTeacher(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-class-teacher";
    }
    /**
     *@Author:HouPeng
     *@Description:教务跳转全局设置
     *@Date:16:56 2017/10/20
     */
    @RequestMapping(value = "/edu-setting.do")
    private String eduSetting(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-setting";
    }

   /**
    * @description 企合跳转全局设置
    * @author Fujt
    * @date 2017/11/7 17:00
    */
    @RequestMapping(value = "/ep-setting.do")
    private String epSetting(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "ep-setting";
    }

    /**
     * @description 跳转到试卷导出界面
     * @author Fujt
     * @date 2017/10/23 17:13
     */
    @GetMapping("/edu-paper.do")
    public String eduPaper(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        return "edu-paper";
    }

}

package com.hpe.controller;

import com.hpe.common.Const;
import com.hpe.pojo.Admin;
import com.hpe.pojo.Student;
import com.hpe.pojo.Teacher;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

/**
 * Created by chunjie on 2017/9/24.
 */
@Controller
public class IndexController {

    /**
    * @Author:CuiCaijin
    * @Description: 返回登录页
    * @Date:21:17 2017/9/25
    */
    @GetMapping("/index.do")
    public String index(){
        return "redirect:login.jsp";
    }

    /**
    * @Author:CuiCaijin
    * @Description: 进入学生的主页
    * @Date:21:18 2017/9/25
    */
    @GetMapping("/stu_index.do")
    public String stuIndex(HttpSession session){
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        if(student == null){
            return "redirect:login.jsp";
        }
        return "stu-index";
    }

    /**
    * @Author:CuiCaijin
    * @Description: 老师登录跳转
    * @Date:15:12 2017/9/25
    */
    @GetMapping("/teacher_index.do")
    public String teacherIndex(HttpSession session){
        Teacher teacher = (Teacher) session.getAttribute(Const.CURRENT_TEACHER);
        if(teacher == null){
            return "redirect:login.jsp";
        }
        return "t-index";
    }

    /**
    * @Author:CuiCaijin
    * @Description: 管理员登录跳转
    * @Date:15:12 2017/9/25
    */
    @GetMapping("/admin_index.do")
    public String adminIndex(HttpSession session){
        Admin admin = (Admin) session.getAttribute(Const.CURRENT_ADMIN);
        if(admin == null){
            return "redirect:login.jsp";
        }
        //根据管理员的类型进行跳转
        if(admin.getType() == Const.AdminType.ENTERPRISE){
            return "ep-index";
        }
        return "edu-index";
    }


    @GetMapping("/register.do")
    public String register(){
        return "register";
    }


    @GetMapping("/logout.do")
    public String logout(HttpSession httpSession){
        httpSession.invalidate();
        return "redirect:login.jsp";
    }
}

package com.hpe.controller.student;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hpe.common.Const;
import com.hpe.pojo.ClassHistory;
import com.hpe.pojo.SClass;
import com.hpe.pojo.Student;
import com.hpe.service.ClassHistoryService;
import com.hpe.service.SClassService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/student/class")
public class StudentClassController {

    @Autowired
    ClassHistoryService classHistoryService;

    @Autowired
    private SClassService sClassService;

    /**
    * @Author:Liuli
    * @Description:获取学生所有历史班级(分页）
    * @Date:16:41 2017/10/12
    */
    @GetMapping("/class_history.do")
    @ResponseBody
    public ServerResponse<PageInfo> classHistory(HttpSession session,@RequestParam(name = "pageNum",defaultValue = "1")int pageNum,
                                                           @RequestParam(name = "pageSize",defaultValue = "3")int pageSize){
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        int id=student.getId();
        PageHelper.startPage(pageNum,pageSize);
        List<ClassHistory> classHistories=classHistoryService.getHistoryByStudentId(id);
        PageInfo pageInfo=new PageInfo(classHistories);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
    * @Author:Liuli
    * @Description:获取学生所有历史班级(echarts)
    * @Date:16:20 2017/10/16
    */
    @GetMapping("/stu_class.do")
    @ResponseBody
    public Map stuClass(HttpSession session){
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        int id=student.getId();
        return classHistoryService.getHistoryByStuId(id);
    }

    /**
     * @description 根据id获取班级
     * @author Fujt
     * @date 2017/10/10 18:59
     * @param id 要获取的班级id
     */
    @GetMapping("/get_class.do")
    @ResponseBody
    public ServerResponse<SClass> getByPrimaryKey(int id){
        SClass sClass = sClassService.findStuClassById(id);
        if (sClass == null) {
            return ServerResponse.createByErrorMsg("未找到");
        }
        return ServerResponse.createBySuccess(sClass);
    }
}

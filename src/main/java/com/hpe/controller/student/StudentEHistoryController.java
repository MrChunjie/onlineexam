package com.hpe.controller.student;

import com.github.pagehelper.PageInfo;
import com.hpe.common.Const;
import com.hpe.pojo.*;
import com.hpe.service.BigQuestionService;
import com.hpe.service.ExamService;
import com.hpe.service.QuestionService;
import com.hpe.service.ScoreService;
import com.hpe.util.ServerResponse;
import com.hpe.vo.ExamHistoryDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/student/history")
public class StudentEHistoryController {

    @Autowired
    private ScoreService paperService;

    @Autowired
    private ExamService examService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private BigQuestionService bigQuestionService;

    /**
     * @Author:TangWenhao
     * @Description:查看自己的试卷历史
     * @Date:16:01 2017/9/27
     */
    @RequestMapping(method = RequestMethod.GET, value = "/exam_history.do")
    @ResponseBody
    public ServerResponse<PageInfo> showHistory(HttpSession session, @RequestParam(name = "pageNum", defaultValue = "1") int pageNum,
                                                @RequestParam(name = "pageSize", defaultValue = "3") int pageSize,
                                                @RequestParam(name = "courseName", defaultValue = "") String courseName) {
        Student student = (Student) session.getAttribute(Const.CURRENT_STUDENT);
        return paperService.findPaperByStudent(student.getId(), courseName, pageNum, pageSize);
    }



    /**
     * @description /exam_history_detail.do 模块抽取，
     * 判断时间是否结束，解决跳入另一界面提示考试未结束的问题
     * @author Fujt
     * @date 2017/10/23 11:21
     */
    @PostMapping("/exam_end.do")
    @ResponseBody
    public ServerResponse examEndOrNot(int examId) throws ParseException {
        Exam exam = examService.selectByPrimaryKey(examId).getData();
        Date endDate = exam.getStarttime();
        // 根据 当前时间，计算 剩余时间  + 考试时长。
        int testTime = exam.getTesttime();// 分钟
        int examtime = testTime * 60 * 1000; // 考试时长 毫秒
        // 时间差
        long curtime = new Date().getTime();// 当前时间毫秒
        long starttime = endDate.getTime();// 考试时间 毫秒
        long diff = starttime - curtime + examtime;// 是否属于考试区间
        if (diff > 0) {
            //当前考试未结束
            return ServerResponse.createByErrorMsg("请在考试结束之后查询");
        }
        return ServerResponse.createBySuccess();
    }


    /**
     * @Author:TangWenhao
     * @Description:查看历史试卷的详细信息
     * @Date:14:17 2017/10/16
     */
    @RequestMapping(method = RequestMethod.GET, value = "/exam_history_detail.do")
    @ResponseBody
    public ServerResponse showHistoryDetail(HttpSession session) throws ParseException {
        Score score = (Score) session.getAttribute("score");
        ServerResponse<Exam> response = examService.selectByPrimaryKey(score.getExamId());
        Exam exam = response.getData();
        Date endDate = exam.getStarttime();
        // 根据 当前时间，计算 剩余时间  + 考试时长。
        int testTime = exam.getTesttime();// 分钟
        int examtime = testTime * 60 * 1000; // 考试时长 毫秒
        // 时间差
        long curtime = new Date().getTime();// 当前时间毫秒
        long starttime = endDate.getTime();// 考试时间 毫秒
        long diff = starttime - curtime + examtime;// 是否属于考试区间
        if (diff > 0) {
            //当前考试未结束
            return ServerResponse.createBySuccess("请在考试结束之后查询", "");
        } else {
            ExamHistoryDetail examHistory = new ExamHistoryDetail();
            Score papers = paperService.findById(score.getsId(), score.getExamId());
            String[] wrongans = papers.getWrongans().split(",");
            String[] wrongqueid = papers.getWrongqueid().split(",");
            examHistory.setWrongans(wrongans);
            examHistory.setWrongqueid(wrongqueid);
            String titles = papers.getContenttitl();
            String ans = papers.getContentans();
            //拆分 简单 和 编程
            String[] titelsArrys = titles.split("@@@");
            String[] ansArray = ans.split("@@@");
            if (ansArray.length > 1) {
                String sigTitle = HtmlUtils.htmlEscape(titelsArrys[0]);
                String codeTitle = HtmlUtils.htmlEscape(titelsArrys[1]);
                String sigAns = HtmlUtils.htmlEscape(ansArray[0]);
                String codeAns = HtmlUtils.htmlEscape(ansArray[1]);
                String[] titles1 = sigTitle.split("~");
                String[] titles2 = codeTitle.split("~");
                String[] ans1 = sigAns.split("~");
                String[] ans2 = codeAns.split("~");
                // 标号。
                //附加 根据examId查询每个题的分值
                //计算每道题的分值
                 /*  double examScore1= exam.getScore1()/titles1.length;
                double examScore2=exam.getScore1()/titles2.length;*/
                examHistory.setAns1(ans1);
                examHistory.setAns2(ans2);
                examHistory.setTitles1(titles1);
                examHistory.setTitles2(titles2);
            }else if(ansArray.length ==1){
                String sigTitle = HtmlUtils.htmlEscape(titelsArrys[0]);
                String sigAns = HtmlUtils.htmlEscape(ansArray[0]);
                String[] titles1 = sigTitle.split("~");
                String[] ans1 = sigAns.split("~");
                examHistory.setAns1(ans1);
                examHistory.setTitles1(titles1);
            }
                String ques = exam.getQuestions(); // 题目主键的字符串 2,3,4,5;12,1,3;123,123,1===> [2,3,4,5]
                String[] typesQue = ques.split(";");
                // 根据test试卷 questions , 查询单选题。
                List<Questions> queList = questionService.findByIdsForStu(typesQue[0].split(","));
                examHistory.setQueList(queList);
                //  查询  简答题 或 编程题。
                List<BigQuestion> que1List = new ArrayList();
                if (typesQue.length >= 2 && typesQue[1].length() >= 1) {
                    que1List = bigQuestionService.findBigQuesByIds(typesQue[1].split(","), 1);
                }
                String[] que1Ans = new String[100];
                for (int i = 0; i < que1List.size(); i++) {
                    que1Ans[i] = que1List.get(i).getContentans();
                }
                List<BigQuestion> que2List = new ArrayList();//编程题。
                if (typesQue.length >= 3 && typesQue[2].length() >= 1) {
                    que2List = bigQuestionService.findBigQuesByIds(typesQue[2].split(","), 2);
                }
                String[] que2Ans = new String[100];
                for (int i = 0; i < que2List.size(); i++) {
                    que2Ans[i] = que2List.get(i).getContentans();
                }
                examHistory.setQue1Ans(que1Ans);
                examHistory.setQue2Ans(que2Ans);
            return ServerResponse.createBySuccess("查询成功", examHistory);
        }
    }
}

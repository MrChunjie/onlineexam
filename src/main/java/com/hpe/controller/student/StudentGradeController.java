package com.hpe.controller.student;

import com.github.pagehelper.PageInfo;
import com.hpe.common.Const;
import com.hpe.pojo.Grade;
import com.hpe.pojo.SGrade;
import com.hpe.pojo.SGradeKey;
import com.hpe.pojo.Student;
import com.hpe.service.GradeService;
import com.hpe.service.SGradeService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/student/grade")
public class StudentGradeController {

    @Autowired
    GradeService gradeService;

    @Autowired
    SGradeService sGradeService;

    /**
     * @Author:Liuli
     * @Description:获取全部技能
     * @Date:19:31 2017/9/25
     */
    @GetMapping(value = "/get_grade.do")
    @ResponseBody
    public Map getGrade() {
        return gradeService.getGrade();
    }

    /**
     * @Author:HouPeng
     * @Description:获取所有技能
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/all_grade.do")
    @ResponseBody
    public ServerResponse<PageInfo<Grade>> getAllGrade(Integer pageNum, Integer pageSize) {
        //获取所有技能
        ServerResponse<PageInfo<Grade>> serverResponse = gradeService.getAllGrade(pageNum, pageSize);
        return serverResponse;
    }


    /**
     * @description 获取所有的技能
     * @author Fujt
     * @date 2017/10/9 17:18
     */
    @GetMapping("/get_all_grade.do")
    @ResponseBody
    public ServerResponse<List<Grade>> getAllGrades() {
        return gradeService.getAllGrade();
    }

    /**
     * @Author:HouPeng
     * @Description:修改技能
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST, value = "/update_grade.do")
    @ResponseBody
    public ServerResponse update(Grade grade) {
        ServerResponse result = gradeService.updateGrade(grade);
        return result;
    }

    /**
     * @Author:HouPeng
     * @Description:根据id 获取数据 传递数据到前台
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/toupdate_grade.do")
    @ResponseBody
    public ServerResponse<Grade> toupdate(Integer gradeId) {
        Grade grade = gradeService.getGradeById(gradeId);
        return ServerResponse.createBySuccess(grade);
    }

    /**
     * @Author:HouPeng
     * @Description:删除
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/delete_grade.do")
    @ResponseBody
    public ServerResponse delete(Integer gradeId) {
        ServerResponse result = gradeService.deleteById(gradeId);
        return result;
    }


    @GetMapping("/delete_sGrade.do")
    @ResponseBody
    public ServerResponse deleteSGrade(HttpSession httpSession, SGradeKey sGradeKey) {
        int id = ((Student)httpSession.getAttribute(Const.CURRENT_STUDENT)).getId();
        sGradeKey.setsId(id);

        //删除操作：存在或者不存在，影响一样
        sGradeService.deleteByPrimaryKey(sGradeKey);

        return ServerResponse.createBySuccess();
    }


    /**
     * @Author:HouPeng
     * @Description:增加技能
     * @Date:16:26 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST, value = "/add_grade.do")
    @ResponseBody
    public ServerResponse add(Grade grade) {
        ServerResponse result = gradeService.addGrade(grade);
        return result;
    }


    /**
     * @description SGrade批量添加数据
     * @author Fujt
     * @date 2017/10/10 14:22
     * @param grades 要添加的数据
     * @param httpSession 获取当前用户的id
     */
    @PostMapping("/add_grades.do")
    @ResponseBody
    public ServerResponse addGrades(HttpSession httpSession, @RequestBody SGrade[] grades) {
        int id = ((Student)httpSession.getAttribute(Const.CURRENT_STUDENT)).getId();

        for (int i = 0; i < grades.length; i++) {
            grades[i].setsId(id);
        }

        System.out.println(grades);

        int result = sGradeService.addGrades(grades);
        if (result != 0) {
            return ServerResponse.createBySuccessMsg("提交成功");
        }
        return ServerResponse.createByErrorMsg("提交失败");
    }

    /**
     * @description 根据学生id获取已选择技能
     * @author Fujt
     * @date 2017/10/10 14:31
     * @param httpSession 获取当前用户的id
     */
    @GetMapping("/get_sGrades.do")
    @ResponseBody
    public ServerResponse<List<SGrade>> getSGrades(HttpSession httpSession){
        int id = ((Student) httpSession.getAttribute(Const.CURRENT_STUDENT)).getId();
        List<SGrade> sGrades = sGradeService.getByStudentId(id);
        if (sGrades.size() == 0) {
            return ServerResponse.createByError();
        }
        return ServerResponse.createBySuccess(sGrades);
    }


}

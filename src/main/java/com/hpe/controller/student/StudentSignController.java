package com.hpe.controller.student;

import com.github.pagehelper.PageInfo;
import com.hpe.common.Const;
import com.hpe.pojo.Sign;
import com.hpe.pojo.Student;
import com.hpe.service.SignService;
import com.hpe.util.ServerResponse;
import com.hpe.vo.EnterpriseInfo;
import com.hpe.vo.SignInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/student/sign")
public class StudentSignController {

    @Autowired
    SignService signService;


    /**
     * @Author:HouPeng
     * @Description:获取当前学生下对应报名所有企业
     * @Date:14:04 2017/10/12
     */
    @RequestMapping(method = RequestMethod.GET, value = "/all_enterprise.do")
    @ResponseBody
    public ServerResponse getAllEnterprise(HttpServletRequest request, Integer pageNum, Integer pageSize) {
        //获取当前学生信息
        Student student = (Student) request.getSession().getAttribute(Const.CURRENT_STUDENT);
        //查询未报名企业信息
        PageInfo<EnterpriseInfo> enterprises = signService.getAllEnterpriseByType(student, pageNum, pageSize);
        //查询已报名企业信息
        List<SignInfo> signInfos = signService.getAllSignInfoByStuId(student);
        Map map = new HashMap();
        map.put("enterprises", enterprises);
        map.put("signInfos", signInfos);
        return ServerResponse.createBySuccess(map);

    }
    /**
     *@Author:HouPeng
     *@Description:已报名所有企业
     *@Date:16:06 2017/10/12
     *@param:
     *@return:
     */
    @RequestMapping(method = RequestMethod.GET, value = "/sign_result.do")
    @ResponseBody
    public ServerResponse<PageInfo<SignInfo>> signResult(HttpServletRequest request, Integer pageNum, Integer pageSize) {
        //获取当前学生信息
        Student student = (Student) request.getSession().getAttribute(Const.CURRENT_STUDENT);
        ServerResponse<PageInfo<SignInfo>> result = signService.getAllSignInfoByStuId(student,pageNum,pageSize);
        return result;
    }
    /**
     *@Author:HouPeng
     *@Description:学生企业报名
     *@Date:19:32 2017/10/16
     *@param:enterpriseId 企业id
     *@param:postId 岗位id
     */
    @RequestMapping(method = RequestMethod.POST, value = "/sign_enterprise.do")
    @ResponseBody
    public ServerResponse enterpriseSign(HttpServletRequest request, Integer enterpriseId, Integer postId) throws ParseException {
        //获取当前学生信息
        Student student = (Student) request.getSession().getAttribute(Const.CURRENT_STUDENT);
        ServerResponse result = signService.enterpriseSign(student,enterpriseId,postId);
        return result;
    }

    /**
     *@Author:HouPeng
     *@Description:学生完善薪资和工作城市
     *@Date:14:20 2017/10/17
     *@param:
     *@return:
     */
    @RequestMapping(method = RequestMethod.POST, value = "/sign_complete.do")
    @ResponseBody
    public ServerResponse signInfoComplete(HttpServletRequest request, Sign sign ) {
        //获取当前学生信息
        Student student = (Student) request.getSession().getAttribute(Const.CURRENT_STUDENT);
        sign.setsId(student.getId());
        ServerResponse result = signService.signInfoComplete(sign);
        return result;
    }
}

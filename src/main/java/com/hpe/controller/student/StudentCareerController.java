package com.hpe.controller.student;

import com.hpe.common.Const;
import com.hpe.pojo.*;
import com.hpe.service.*;
import com.hpe.util.ServerResponse;
import com.hpe.vo.StudentCareerClassVo;
import com.hpe.vo.WordCloudVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/student/career")
public class StudentCareerController {

    @Autowired
    DescriptionService descriptionService;

    @Autowired
    SGradeService sGradeService;

    @Autowired
    GradeService gradeService;

    @Autowired
    ScoreService scoreService;

    @Autowired
    GroupService groupService;

    @Autowired
    SignService signService;

    @Autowired
    DirectionService directionService;

    @Autowired
    StudentInfoService studentInfoService;

    @Autowired
    ClassHistoryService classHistoryService;

    @GetMapping("/word_cloud.do")
    @ResponseBody
    public Map<String, List> getWordCloud(HttpSession session){
        //获取当前学生已选择技能列表
        Student student=(Student) session.getAttribute(Const.CURRENT_STUDENT);
        List<SGrade> sGrades=sGradeService.getByStudentId(student.getId());
        List<Map> result = new ArrayList<Map>();
        for(SGrade sGrade :sGrades){
            Map map1 = new HashMap<>();
            int g_id=sGrade.getGradeId();
            Grade grade=gradeService.getGradeById(g_id);
            //按照技能等级*6+熟练度*4来进行加权计算
            int vlaue=grade.getGrade()*6+sGrade.getValue()*4;
            map1.put("name",grade.getName());
            map1.put("value",vlaue);
            result.add(map1);
        }
        Map<String,List> listMap = new HashMap<>();
        listMap.put("dataCloud",result);
        return listMap;
    }

    /**
    * @Author:Liuli
    * @Description:获取雷达图数据
    * @Date:14:33 2017/10/23
    */
    @GetMapping("/get_radar.do")
    @ResponseBody
    public Map<String ,List>getRadar(HttpSession session){
        Student student=(Student) session.getAttribute(Const.CURRENT_STUDENT);
        int id=student.getId();

        //获取学生沟通数值
        int sign=signService.getCommunicateValueBuStudentId(id);
        //获取学生学习数值
        int study=groupService.getMaxRankByStudentId(id);
        //获取学生实践数值
        int practice=scoreService.findPractice(id);
        //获取学生成长数值
        int process=groupService.getMaxProcessStudentId(id);
        //获取学生技能数值
        int sgrade=sGradeService.getGradeValueByStudentId(id);

        List<Integer> list=new ArrayList<>();
        //插入数据顺序：沟通、学习、实践、成长、技能
        list.add(sign);
        list.add(study);
        list.add(practice);
        list.add(process);
        list.add(sgrade);

        //获取描述
        int index=1;
        List<String >description=new ArrayList<>();
        for(Integer integer:list){
            description.add(descriptionService.getDescription(index,integer.intValue()));
            index++;
        }
        Map<String,List> map=new HashMap<>();
        map.put("radarvalue",list);
        map.put("description",description);
        return map;
    }

    /**
    * @Author:Liuli
    * @Description:获取学生详细信息
    * @Date:9:22 2017/10/24
    */
    @GetMapping("/get_student_info.do")
    @ResponseBody
    public ServerResponse<StudentInfo> getStudentinfo(HttpSession session){
        Student student=(Student) session.getAttribute(Const.CURRENT_STUDENT);
        int id=student.getId();
        return ServerResponse.createBySuccess(studentInfoService.selectByKey(id));
    }

    /**
     * @description 获得方向
     * @author Fujt
     * @date 2017/10/24 16:39
     */
    @PostMapping("/get_one_direction.do")
    @ResponseBody
    public ServerResponse<Direction> getDirectionById(int id){
        return directionService.getByPrimaryKey(id);
    }

    /**
    * @Author:Liuli
    * @Description:获取学生所有的班级、课程、老师
    * @Date:11:00 2017/10/24
    */
    @GetMapping("/get_all_class_career.do")
    @ResponseBody
    public ServerResponse<List<StudentCareerClassVo>>getAllClassCareer(HttpSession session){
        Student student=(Student) session.getAttribute(Const.CURRENT_STUDENT);
        int id=student.getId();
        return ServerResponse.createBySuccess(classHistoryService.getAllStuClassCareer(id));
    }

    /**
    * @Author:Liuli
    * @Description:获取词云图描述
    * @Date:10:20 2017/10/26
    */
    @GetMapping("/get_word_cloud_career.do")
    @ResponseBody
    public ServerResponse<WordCloudVo> getWordcloudCareer(HttpSession session){
        Student student=(Student) session.getAttribute(Const.CURRENT_STUDENT);
        int id=student.getId();
        return ServerResponse.createBySuccess(sGradeService.getWordCloudCareer(id));
    }
}

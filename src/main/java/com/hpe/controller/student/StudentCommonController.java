package com.hpe.controller.student;

import com.hpe.common.Const;
import com.hpe.pojo.*;
import com.hpe.service.*;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by chunjie on 2017/9/26.
 * 注册 登录 修改密码 完善信息
 * 涉及jsp register.jsp  stu-complete .jsp stu-changepwd.jsp
 */
@Controller
@RequestMapping("/student/common")
public class StudentCommonController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentInfoService studentInfoService;

    @Autowired
    private DirectionService directionService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private SettingService settingService;

    @Autowired
    private SClassService sClassService;

    @Autowired
    private ExamService examService;

    /**
    * @Author:CuiCaijin
    * @Description:登录
    * @Date:18:11 2017/9/27
    */
    @RequestMapping(method = RequestMethod.POST,value = "/login.do")
    @ResponseBody
    public ServerResponse<Student> login(HttpSession session, String username, String password) {
        ServerResponse<Student> student = studentService.studentLogin(username, password);
        if (student.isSuccess()) {
            session.setAttribute(Const.CURRENT_STUDENT, student.getData());
        }
        return student;
    }

    /**
     * @description 完善信息
     * @author Fujt
     * @date 2017/9/28 9:46
     */
    //更新学生信息
    @RequestMapping(method = RequestMethod.POST,value = "/update_info.do")
    @ResponseBody
    public ServerResponse<StudentInfo> stuUpdateInfo(HttpSession session, StudentInfo studentInfo) {
        Student student1=(Student)session.getAttribute(Const.CURRENT_STUDENT);
        studentInfo.setId(student1.getId());
        //调用service层方法
        ServerResponse<StudentInfo> response=studentInfoService.updateByPrimaryKeySelective(studentInfo);
        examService.findExamByClassId(Integer.valueOf(response.getData().getSpare()),student1.getId());
        return response;
    }

    /**
     * @description 获取所有的方向
     * @author Fujt
     * @date 2017/9/30 14:51
     */
    @GetMapping(value = "/get_all_direction.do")
    @ResponseBody
    public ServerResponse<List<Direction>> getAllDirection(){
        return directionService.getAllDirections();
    }

    /**
    * @Author:CuiCaijin
    * @Description: 注册功能
    * @Date:18:11 2017/9/27
    */
    @RequestMapping(method = RequestMethod.POST,value = "/register.do")
    @ResponseBody
    public ServerResponse stuRegister(StudentInfo studentInfo,String pwd) {
        if(studentInfo == null){
            return ServerResponse.createByErrorMsg("输入的注册信息不能为空");
        }
        return studentService.register(studentInfo,pwd);
    }

    @GetMapping("/register_open_or_not.do")
    @ResponseBody
    public ServerResponse registerOpenOrNot(){
        Integer result = (Integer) settingService.getRegisterState().getData();
        if (result == 0) {
            return ServerResponse.createByErrorMsg("注册功能尚未开启");
        }

        return ServerResponse.createBySuccess();
    }

    /**
     * 根据当前用户id获取真实姓名
     * @param httpSession 从session中获取当前用户
     * @return
     */
    @GetMapping("/get_name.do")
    @ResponseBody
    public String getCurrentStudentName(HttpSession httpSession){
        Integer id = ((Student)httpSession.getAttribute(Const.CURRENT_STUDENT)).getId();
        return studentInfoService.findNameByPrimaryKey(id);
    }

    /**
     * @description 获取一个用户的信息
     * @author Fujt
     * @date 2017/10/11 10:58
     */
    @GetMapping("/get_studentInfo.do")
    @ResponseBody
    public StudentInfo getStudentInfoByPrimaryKey(HttpSession httpSession){
        Integer id = ((Student)httpSession.getAttribute(Const.CURRENT_STUDENT)).getId();
        return studentInfoService.selectByKey(id);
    }


    /**
     * @description 检索所有省份
     * @author Fujt
     * @date 2017/10/11 11:24
     */
    @GetMapping("/get_all_province.do")
    @ResponseBody
    public ServerResponse<List<String>> getAllProvince(){
        return positionService.selectAllProvince();
    }

    @PostMapping("/get_city.do")
    @ResponseBody
    public ServerResponse<List<Position>> getCityByProvince(String province) {
        return positionService.selectCityByProvince(province);
    }
    @PostMapping("/get_position.do")
    @ResponseBody
    public ServerResponse<Position> getPositionById(Integer positionId) {
        return positionService.getPositionById(positionId);
    }

    /**
     * @description 修改密码
     * @author Fujt
     * @date 2017/10/19 18:49
     */
    @PostMapping("/change_pwd.do")
    @ResponseBody
    public ServerResponse changePwd(HttpSession httpSession, Student student){
        int id = ((Student) httpSession.getAttribute(Const.CURRENT_STUDENT)).getId();
        student.setId(id);
        return studentService.updatePwd(student);
    }

    /**
     * @Author:CuiCaijin
     * @Description:获得所有的班级列表
     * @Date:14:06 2017/10/16
     */
    @GetMapping("/all_class.do")
    @ResponseBody
    public ServerResponse<List<SClass>> getClasses(){
        return sClassService.getClasses();
    }

    /**
    * @Author:Liuli
    * @Description:获取所有可选方向列表
    * @Date:11:18 2017/11/15
    */
    @GetMapping("/all_direction_class.do")
    @ResponseBody
    public ServerResponse<List<Direction>> allDirectionClass(){
        List<Direction> directions = directionService.getAllDirectionClass();
        if (directions.size() <= 0 || directions == null) {
            return ServerResponse.createByErrorMsg("未设置方向");
        }
        return ServerResponse.createBySuccess(directions);
    }

    /**
     * @description 获得方向名称
     * @author Fujt
     * @date 2017/11/16 11:14
     */
    @GetMapping("/get_direction_name.do")
    @ResponseBody
    public ServerResponse<String> getDirectionNam(int id) {
        return directionService.getDirectionNameById(id);
    }

    /**
    * @Author:Liuli
    * @Description:选择方向获取关联班级
    * @Date:11:18 2017/11/15
    */
    @GetMapping("/get_direction_class.do")
    @ResponseBody
    public int getDirectionClass(@RequestParam(name = "directionid") int directionid){
        SClass sClass=sClassService.getDirectionClass(directionid);
        return sClass.getId();
    }
}

package com.hpe.controller;

import com.hpe.common.Const;
import com.hpe.pojo.Admin;
import com.hpe.service.AdminService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Created by chunjie on 2017/9/25.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @PostMapping("/login.do")
    @ResponseBody
    public ServerResponse<Admin> login(HttpSession session,String username,String password){
        Admin admin = new Admin();
        admin.setPwd(password);
        admin.setName(username);
        ServerResponse<Admin> response = adminService.login(admin);
        if(response.isSuccess()){
            session.setAttribute(Const.CURRENT_ADMIN,response.getData());
        }

        return response;
    }

    @PostMapping("/change_pwd.do")
    @ResponseBody
    public ServerResponse updatePwd(HttpSession httpSession, Admin admin) {
        int id = ((Admin) httpSession.getAttribute(Const.CURRENT_ADMIN)).getId();
        admin.setId(id);
        return adminService.updatePwd(admin);
    }

}

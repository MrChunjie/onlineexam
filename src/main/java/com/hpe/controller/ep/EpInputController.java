package com.hpe.controller.ep;

import com.hpe.service.SignService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/ep/input")
public class EpInputController {

    @Autowired
    SignService signService;

    @RequestMapping("/upload_sign.do")
    @ResponseBody
    public ServerResponse uploadExcel(MultipartFile file) throws Exception {
        if (file == null) {
            return ServerResponse.createByErrorMsg("未获取到文件, 请重新上传");
        }
        //文件名
        String name = file.getOriginalFilename();
        //后缀
        String suffix = name.substring(name.indexOf("."));

        if (!".xls".equals(suffix) && !".xlsx".equals(suffix)) {
            return ServerResponse.createByErrorMsg("请选择.xls或者.xlsx后缀的文件");
        }

        InputStream inputStream = file.getInputStream();
        if (inputStream == null) {
            return ServerResponse.createByErrorMsg("输入流为空");
        }

        return signService.inputSignInfo(inputStream);

    }
}

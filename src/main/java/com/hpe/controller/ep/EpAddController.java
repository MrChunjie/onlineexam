package com.hpe.controller.ep;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Direction;
import com.hpe.pojo.Enterprise;
import com.hpe.pojo.Position;
import com.hpe.pojo.Post;
import com.hpe.service.DirectionService;
import com.hpe.service.EnterpriseService;
import com.hpe.service.PositionService;
import com.hpe.service.PostService;
import com.hpe.util.ServerResponse;
import com.hpe.vo.EnterpriseInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/ep/manager")
public class EpAddController {
    @Autowired
    EnterpriseService enterpriseService;

    @Autowired
    DirectionService directionService;

    @Autowired
    PostService postService;

    @Autowired
    private PositionService positionService;

    /**
     * @Author:XuShengJie
     * @Description:字符串转Date
     * @Date:11:17 2017/9/28
     */
    @InitBinder
    protected void init(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
    /**
     * @Author:HouPeng
     * @Description:获取所有企业
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST, value = "/all_enterprise.do")
    @ResponseBody
    public ServerResponse<PageInfo<EnterpriseInfo>> getAllEnterprise(Integer pageNum, Integer pageSize,@RequestParam(name = "condition", defaultValue = "0")int condition,@RequestParam(name = "post", defaultValue = "0")Integer post,@RequestParam(name = "epName", defaultValue = "")String epName) {
        ServerResponse<PageInfo<EnterpriseInfo>> serverResponse = enterpriseService.getAllEnterprise(pageNum, pageSize,condition,post,epName);
        return serverResponse;

    }

    /**
     * @Author:HouPeng
     * @Description:修改企业
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST, value = "/update_enterprise.do")
    @ResponseBody
    public ServerResponse update(String postIds,Enterprise enterprise) throws ParseException {
        ServerResponse result = enterpriseService.updateEnterprise(enterprise,postIds);
        return result;
    }

    /**
     * @Author:HouPeng
     * @Description:根据id 获取数据 传递数据到前台
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/toupdate_enterprise.do")
    @ResponseBody
    public ServerResponse toupdate(Integer enterpriseId) {
        EnterpriseInfo enterprise =  enterpriseService.getEnterpriseById(enterpriseId);
        List<Post> posts = postService.getAllPost();
        Map map = new HashMap();
        map.put("enterprise",enterprise);
        map.put("posts",posts);
        return ServerResponse.createBySuccess(map);
    }

    /**
     * @Author:HouPeng
     * @Description:删除
     * @Date:10:30 2017/9/26
     */
    @RequestMapping(method = RequestMethod.GET, value = "/delete_enterprise.do")
    @ResponseBody
    public ServerResponse delete(Integer enterpriseId) {
        ServerResponse result= enterpriseService.deleteById(enterpriseId);
        return result;
    }
    /**
     *@Author:HouPeng
     *@Description:增加企业
     *@Date:16:26 2017/9/26
     */
    @RequestMapping(method = RequestMethod.POST, value = "/add_enterprise.do")
    @ResponseBody
    public ServerResponse add(Enterprise enterprise,String postIds) throws ParseException {
        ServerResponse result= enterpriseService.addEnterprise(enterprise,postIds);
        return result;
    }

    /**
     *@Author:HouPeng
     *@Description:企业导出功能
     *@Date:16:41 2017/10/17
     *@param:
     *@return:
     */
    @RequestMapping("/get_stime_enterprises.do")
    @ResponseBody
    public ServerResponse<PageInfo<EnterpriseInfo>> getEnterpriseByStime(Integer pageNum,Integer pageSize, Date sTime) throws Exception{
        return enterpriseService.getEnterpriseInfosByStime(pageNum,pageSize,sTime);
    }

    /**
     * @description 检索所有省份
     * @author Fujt
     * @date 2017/10/11 11:24
     */
    @GetMapping("/get_all_province.do")
    @ResponseBody
    public ServerResponse<List<String>> getAllProvince(){
        return positionService.selectAllProvince();
    }

    @PostMapping("/get_city.do")
    @ResponseBody
    public ServerResponse<List<Position>> getCityByProvince(String province) {
        return positionService.selectCityByProvince(province);
    }
    @PostMapping("/get_position.do")
    @ResponseBody
    public ServerResponse<Position> getPositionById(Integer positionId) {
        return positionService.getPositionById(positionId);
    }

    /**
     * @description 获取所有的方向
     * @author Fujt
     * @date 2017/10/12 16:00
     */
    @GetMapping("/get_all_directions.do")
    @ResponseBody
    public ServerResponse<List<Direction>> getAllDirections() {
        return directionService.getAllDirections();
    }
}

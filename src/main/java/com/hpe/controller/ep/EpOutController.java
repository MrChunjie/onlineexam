package com.hpe.controller.ep;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Enterprise;
import com.hpe.service.EnterpriseService;
import com.hpe.service.SignService;
import com.hpe.util.DownloadUtil;
import com.hpe.util.ServerResponse;
import com.hpe.vo.EnterpriseInfo;
import com.hpe.vo.SignInfo;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by chunjie on 2017/9/26.
 */
@Controller
@RequestMapping("/ep/out")
public class EpOutController {

    @Autowired
    SignService signService;

    @Autowired
    EnterpriseService enterpriseService;

    /**
     * @Author:XuShengJie
     * @Description:字符串转Date
     * @Date:11:17 2017/9/28
     */
    @InitBinder
    protected void init(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    /**
     * @Author:HouPeng
     * @Description:企业报名表导出
     * @Date:19:05 2017/10/13
     */
    @RequestMapping("/output_sign.do")
    public void outputSignInfo(HttpServletRequest request, HttpServletResponse response, Integer enterpriseId) throws Exception {
        if (enterpriseId == null) {

        }
        signService.outputSignInfo(request, response, enterpriseId);
    }

    /**
     * @Author:HouPeng
     * @Description:企业导出功能
     * @Date:16:30 2017/10/17
     * @param:sTime
     * @return:
     */
    @RequestMapping("/output_enterprise.do")
    public void outputEnterprise(HttpServletRequest request, HttpServletResponse response, Date sTime) throws Exception {
        enterpriseService.outputEnterpriseByStime(request, response, sTime);
    }


}

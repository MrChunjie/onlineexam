package com.hpe.controller.ep;


import com.hpe.pojo.Settings;
import com.hpe.service.SettingService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/ep/setting")
public class EpSettingController {

    @Autowired
    private SettingService settingService;
    
    /**
     *@Author:HouPeng
     *@Description:获取最大企业报名数
     *@Date:18:50 2017/10/22
     *@param:
     *@return:
     */
    @RequestMapping(method = RequestMethod.GET, value = "/get_setting.do")
    @ResponseBody
    public ServerResponse<Settings> getSetting() {
        ServerResponse<Settings> result = settingService.getSetting();
        return result;
    }


    /**
     *@Author:HouPeng
     *@Description:设置全局变量
     *@Date:18:49 2017/10/22
     *@param:
     *@return:
     */
    @RequestMapping(method = RequestMethod.POST, value = "/update_setting.do")
    @ResponseBody
    public ServerResponse<Settings> update(Settings settings) {
        ServerResponse<Settings> result = settingService.updateSetting(settings);
        return result;
    }


}

package com.hpe.controller.ep;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Post;
import com.hpe.service.PostService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by dell on 2017/10/9.
 */
@Controller
@RequestMapping("/ep/post")
public class EpPostController {

    @Autowired
    PostService postService;

    @GetMapping("/post_list.do")
    @ResponseBody
    public ServerResponse<PageInfo> postList(HttpSession session, @RequestParam(name = "pageNum",defaultValue = "1")int pageNum,
                                             @RequestParam(name = "pageSize",defaultValue = "3")int pageSize){
        return postService.getPosts(pageNum,pageSize);
    }
    @GetMapping("/post_all.do")
    @ResponseBody
    public ServerResponse<List<Post>> postAll(){
        List<Post> posts = postService.getAllPost();
        if(posts != null && posts.size() >0){
            return ServerResponse.createBySuccess(posts);
        }
        return ServerResponse.createByErrorMsg("没有岗位，请添加岗位");
    }

    /**
    * @Author:Liuli
    * @Description:更新岗位名称
    * @Date:20:07 2017/10/9
    */
    @RequestMapping("/update_post.do")
    @ResponseBody
    public ServerResponse<String> updatePost(Post post){
        return postService.updatePost(post);
    }

    /**
    * @Author:Liuli
    * @Description:增加岗位
    * @Date:20:08 2017/10/9
    */
    @RequestMapping("/add_post.do")
    @ResponseBody
    public ServerResponse<String > addPost(Post post){
        return postService.addPost(post);
    }

    /**
    * @Author:Liuli
    * @Description:删除岗位
    * @Date:20:10 2017/10/9
    */
    @RequestMapping("/delete_post.do")
    @ResponseBody
    public ServerResponse<String > deletePost(Post post){
        return postService.deletePost(post.getId());
    }
}

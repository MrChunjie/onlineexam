package com.hpe.common;

/**
 * Created by john on 2017/9/23.
 */
public class Const {
    public static final String CURRENT_TEACHER ="current_teacher";
    public static final String CURRENT_ADMIN ="current_admin";
    public static final String CURRENT_STUDENT ="current_student";

    public interface StudentDirection{
        /**
         * 开发
         */
        String DEVELOP = "开发";

        /**
         * 测试
         */
        String TEST = "测试";
    }

    /**
    * @Author:CuiCaijin
    * @Description: 管理员类型 1.企业合作  2,.教务
    * @Date:15:13 2017/9/25
    */
    public interface AdminType{
        /**
         * 企业合作
         */
        int ENTERPRISE = 1;

        /**
         * 教务
         */
        int EDUCATION = 2;
    }

    /**
    * @Author:CuiCaijin
    * @Description: 班级类型 1 开发  2测试   3定制
    * @Date:9:16 2017/9/26
    */
    public interface ClassType{
        int DEVELOP = 1;
        int TEST = 2;
        int CUSTOM = 3;
    }

    /**
    * @Author:CuiCaijin
    * @Description: 班级状态
    * @Date:14:00 2017/9/28
    */
    public interface ClassState{
        /**
         * 存在
         */
        int ONSCHOOL = 1;

        /**
         * 报废
         */
        int LEVEL = 2;
    }

    /**
    * @Author:CuiCaijin
    * @Description: 学生状态
    * @Date:14:00 2017/9/28
    */
    public interface StudentState{
        /**
         * 未报到
         */
        int NOTARRIVE = 0;

        /**
         * 在读
         */
        int ONREAD = 1;

        /**
         * 毕业
         */
        int GRADUATION = 2;

        /**
         * 退学
         */
        int OUTSCHOOL = 3;

        /**
         * 删除
         */
        int DELETED = 4;
    }
}

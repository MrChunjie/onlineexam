package com.hpe.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * Created by dell on 2017/10/13.
 */
/**
* @Author:Liuli
* @Description:汇总展示视图
* @Date:16:16 2017/10/16
*/
public class GroupVo implements Serializable{

    private static final long serialVersionUID = -5568346814189062525L;

    private Integer groupId;

    private String examName;

    private String interview;

    private String directionName;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Timestamp groupTime;

    public String getInterview() {
        return interview;
    }

    public void setInterview(String interview) {
        this.interview = interview;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getDirectionName() {
        return directionName;
    }

    public void setDirectionName(String directionName) {
        this.directionName = directionName;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Timestamp getGroupTime() {
        return groupTime;
    }

    public void setGroupTime(Timestamp groupTime) {
        this.groupTime = groupTime;
    }

    public GroupVo(Integer groupId, String interview, String examName, String directionName, Timestamp groupTime) {
        this.groupId = groupId;
        this.interview = interview;
        this.examName = examName;
        this.directionName = directionName;
        this.groupTime = groupTime;
    }

    @Override
    public String toString() {
        return "GroupVo{" +
                "groupId=" + groupId +
                ", interview='" + interview + '\'' +
                ", examName='" + examName + '\'' +
                ", directionName='" + directionName + '\'' +
                ", groupTime=" + groupTime +
                '}';
    }

    public GroupVo(){}

}

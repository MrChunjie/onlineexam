package com.hpe.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.DecimalFormat;
import java.util.Date;

/**
 * @Author:TangWenhao
 * @Description:是根据学生查询试卷结果的视图类
 * @Date:9:59 2017/9/26
 */
public class PaperInfo {
    private int id;             //试卷ID
    private String stuName;     //学生姓名
    private String testName;    //试卷名称
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endtime;       //结束时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date starttime;     //开始时间
    private int testTime;       //考试时长
    private double score=0,score1=0,score2=0,scores=0;              //分值
    private String courseName,teacherName,className;        //课程名、出题教师名、班级名
    private int classId;        //班级ID

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }


    public int getTestTime() {
        return testTime;
    }

    public void setTestTime(int testTime) {
        this.testTime = testTime;
    }

    public double getScore() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score));
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getScore1() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score1));    }

    public void setScore1(double score1) {
        this.score1 = score1;
    }

    public double getScore2() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score2));    }

    public void setScore2(double score2) {
        this.score2 = score2;
    }

    public double getScores() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(scores));    }

    public void setScores(double scores) {
        this.scores = scores;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public PaperInfo() {
    }

    public PaperInfo(int id, String stuName, String testName, Date endtime, int testTime, double score, double score1, double score2, double scores, String courseName, String teacherName, String className, int classId) {
        this.id = id;
        this.stuName = stuName;
        this.testName = testName;
        this.endtime = endtime;
        this.testTime = testTime;
        this.score = score;
        this.score1 = score1;
        this.score2 = score2;
        this.scores = scores;
        this.courseName = courseName;
        this.teacherName = teacherName;
        this.className = className;
        this.classId = classId;
    }

    @Override
    public String toString() {
        return "PaperInfo{" +
                "id=" + id +
                ", stuName='" + stuName + '\'' +
                ", testName='" + testName + '\'' +
                ", endtime=" + endtime +
                ", testTime=" + testTime +
                ", score=" + score +
                ", score1=" + score1 +
                ", score2=" + score2 +
                ", scores=" + scores +
                ", courseName='" + courseName + '\'' +
                ", teacherName='" + teacherName + '\'' +
                ", className='" + className + '\'' +
                ", classId=" + classId +
                '}';
    }
}

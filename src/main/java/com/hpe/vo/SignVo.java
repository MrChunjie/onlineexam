package com.hpe.vo;

public class SignVo {

    private Integer enterId;
    private Integer sId;
    private String choice;
    private String epName;
    private String sName;
    private String phone;
    private Integer typeB;
    private Integer typeM;
    private String className;
    private Integer state;
    private String money;
    private String position;

    public SignVo() {
    }

    public SignVo(Integer enterId, Integer sId, String choice, String epName, String sName, String phone, Integer typeB, Integer typeM, String className, Integer state, String money, String position) {
        this.enterId = enterId;
        this.sId = sId;
        this.choice = choice;
        this.epName = epName;
        this.sName = sName;
        this.phone = phone;
        this.typeB = typeB;
        this.typeM = typeM;
        this.className = className;
        this.state = state;
        this.money = money;
        this.position = position;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getEnterId() {
        return enterId;
    }

    public Integer getTypeM() {
        return typeM;
    }

    public void setTypeM(Integer typeM) {
        this.typeM = typeM;
    }

    public void setEnterId(Integer enterId) {
        this.enterId = enterId;
    }

    public Integer getsId() {
        return sId;
    }

    public void setsId(Integer sId) {
        this.sId = sId;
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }

    public String getEpName() {
        return epName;
    }

    public void setEpName(String epName) {
        this.epName = epName;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getTypeB() {
        return typeB;
    }

    public void setTypeB(Integer typeB) {
        this.typeB = typeB;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return "SignVo{" +
                "enterId=" + enterId +
                ", sId=" + sId +
                ", choice='" + choice + '\'' +
                ", epName='" + epName + '\'' +
                ", sName='" + sName + '\'' +
                ", phone='" + phone + '\'' +
                ", typeB=" + typeB +
                ", typeM=" + typeM +
                ", className='" + className + '\'' +
                ", state=" + state +
                ", money='" + money + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}

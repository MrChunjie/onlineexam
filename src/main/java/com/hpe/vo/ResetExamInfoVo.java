package com.hpe.vo;

import java.text.DecimalFormat;
import java.util.Date;

/**
 * @Author:XuShengJie
 * @Description:
 * @Date:
 */
public class ResetExamInfoVo {
    private Integer examId,questionnum,bqt1num,bqt2num;
        private String examName ,courseId,stuId,classId,testTime;
    private Date statrTime;
    private double score1,score2,score3,scores;

    public void setScores(double scores) {
        this.scores = scores;
    }

    public double getScores() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(scores));    }

    public ResetExamInfoVo() {
    }

    public Date getStatrTime() {
        return statrTime;
    }

    public void setStatrTime(Date statrTime) {
        this.statrTime = statrTime;
    }

    public ResetExamInfoVo(Integer examId, Integer questionnum, Integer bqt1num, Integer bqt2num, String examName, String courseId, String stuId, String classId, String testTime, Date statrTime, double score1, double score2, double score3) {
        this.examId = examId;
        this.questionnum = questionnum;
        this.bqt1num = bqt1num;
        this.bqt2num = bqt2num;
        this.examName = examName;
        this.courseId = courseId;
        this.stuId = stuId;
        this.classId = classId;
        this.testTime = testTime;
        this.statrTime = statrTime;
        this.score1 = score1;
        this.score2 = score2;

        this.score3 = score3;
    }

    @Override
    public String toString() {
        return "ResetExamInfoVo{" +
                "examId=" + examId +
                ", questionnum=" + questionnum +
                ", bqt1num=" + bqt1num +
                ", bqt2num=" + bqt2num +
                ", examName='" + examName + '\'' +
                ", courseId='" + courseId + '\'' +
                ", stuId='" + stuId + '\'' +
                ", classId='" + classId + '\'' +
                ", statrTime='" + statrTime + '\'' +
                ", testTime='" + testTime + '\'' +
                ", score1=" + score1 +
                ", score2=" + score2 +
                ", score3=" + score3 +

                '}';
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    public Integer getQuestionnum() {
        return questionnum;
    }

    public void setQuestionnum(Integer questionnum) {
        this.questionnum = questionnum;
    }

    public Integer getBqt1num() {
        return bqt1num;
    }

    public void setBqt1num(Integer bqt1num) {
        this.bqt1num = bqt1num;
    }

    public Integer getBqt2num() {
        return bqt2num;
    }

    public void setBqt2num(Integer bqt2num) {
        this.bqt2num = bqt2num;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getStuId() {
        return stuId;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }


    public String getTestTime() {
        return testTime;
    }

    public void setTestTime(String testTime) {
        this.testTime = testTime;
    }

    public double getScore1() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score1));    }

    public void setScore1(double score1) {
        this.score1 = score1;
    }

    public double getScore2() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score2));    }

    public void setScore2(double score2) {
        this.score2 = score2;
    }

    public double getScore3() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score3));    }

    public void setScore3(double score3) {
        this.score3 = score3;
    }

}

package com.hpe.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

public class EnterpriseOutput {
    private Integer id;

    private String name;

    private String text;

    private String type;

    private String stime;

    private String etime;

    private String position;

    private String category;

    private String posts;


    public String getPosts() {
        return posts;
    }

    public void setPosts(String posts) {
        this.posts = posts;
    }

    public EnterpriseOutput() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text == null ? null : text.trim();
    }

    public String  getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void seteTime(String eTime) {
        this.etime = eTime;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStime() {
        return stime;
    }

    public void setStime(String stime) {
        this.stime = stime;
    }

    public String getEtime() {
        return etime;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public EnterpriseOutput(Integer id, String name, String text, String type, String stime, String etime, String position, String category, String posts) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.type = type;
        this.stime = stime;
        this.etime = etime;
        this.position = position;
        this.category = category;
        this.posts = posts;
    }

    @Override
    public String toString() {
        return "EnterpriseOutput{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", text='" + text + '\'' +
                ", type='" + type + '\'' +
                ", stime='" + stime + '\'' +
                ", etime='" + etime + '\'' +
                ", position='" + position + '\'' +
                ", category='" + category + '\'' +
                ", posts='" + posts + '\'' +
                '}';
    }
}

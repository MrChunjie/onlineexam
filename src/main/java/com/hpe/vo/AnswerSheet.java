package com.hpe.vo;

import java.text.DecimalFormat;
import java.util.Arrays;

/**
 * @Author:TangWenhao
 * @Description:答卷视图类
 * @Date:15:43 2017/9/26
 */
public class AnswerSheet {
    private int courseId;
    private double scores,score,score1,score2;
    private int testId;
    private String countTime;
    private String[] id;
    private String[] queId1;
    private String[] queId2;

    public String[] getQueId1() {
        return queId1;
    }

    public void setQueId1(String[] queId1) {
        this.queId1 = queId1;
    }

    public String[] getQueId2() {
        return queId2;
    }

    public void setQueId2(String[] queId2) {
        this.queId2 = queId2;
    }

    public String[] getId() {
        return id;
    }

    public void setId(String[] id) {
        this.id = id;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public double getScores() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(scores));    }

    public void setScores(double scores) {
        this.scores = scores;
    }

    public double getScore() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score));
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getScore1() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score1));    }

    public void setScore1(double score1) {
        this.score1 = score1;
    }

    public double getScore2() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score2));    }

    public void setScore2(double score2) {
        this.score2 = score2;
    }

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public String getCountTime() {
        return countTime;
    }

    public void setCountTime(String countTime) {
        this.countTime = countTime;
    }

    @Override
    public String toString() {
        return "AnswerSheet{" +
                "courseId=" + courseId +
                ", scores=" + scores +
                ", score=" + score +
                ", score1=" + score1 +
                ", score2=" + score2 +
                ", testId=" + testId +
                ", countTime='" + countTime + '\'' +
                ", id=" + Arrays.toString(id) +
                ", queId1=" + Arrays.toString(queId1) +
                ", queId2=" + Arrays.toString(queId2) +
                '}';
    }
}

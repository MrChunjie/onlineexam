package com.hpe.vo;

import java.text.DecimalFormat;
import java.util.Date;

/**
 * @Author:TangWenhao
 * @Description:待批阅试卷视图类
 * @Date:11:05 2017/10/9
 */
public class ReadOverPapers {
    private int examId,classId,sId;
    private String courseName,testName,className;
    private Date createdate,startDate;
    private double score1,score2,score3,scores,total;
    private String testtime,time;
    private int status;

    public double getTotal() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(total));    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getsId() {
        return sId;
    }

    public void setsId(int sId) {
        this.sId = sId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "ReadOverPapers{" +
                "examId=" + examId +
                ", classId=" + classId +
                ", courseName='" + courseName + '\'' +
                ", testName='" + testName + '\'' +
                ", createdate=" + createdate +
                ", startDate=" + startDate +
                ", score1=" + score1 +
                ", score2=" + score2 +
                ", score3=" + score3 +
                ", scores=" + scores +
                ", testtime='" + testtime + '\'' +
                ", status=" + status +
                '}';
    }

    public int getExamId() {
        return examId;
    }

    public void setExamId(int examId) {
        this.examId = examId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public double getScore1() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score1));
    }

    public void setScore1(double score1) {
        this.score1 = score1;
    }

    public double getScore2() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score2));    }

    public void setScore2(double score2) {
        this.score2 = score2;
    }

    public double getScore3() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score3));    }

    public void setScore3(double score3) {
        this.score3 = score3;
    }

    public double getScores() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(scores));    }

    public void setScores(double scores) {
        this.scores = scores;
    }

    public String getTesttime() {
        return testtime;
    }

    public void setTesttime(String testtime) {
        this.testtime = testtime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}

package com.hpe.vo;

import java.math.BigDecimal;

/**
 * Created by dell on 2017/10/26.
 */
/**
* @Author:Liuli
* @Description:用于个人生涯词云图描述
* @Date:10:24 2017/10/26
*/
public class WordCloudVo {

    private Integer sname;          //学生id

    private Long gradenum;          //技能数量

    private String gradename;       //最熟练的技能的名字

    private Long value;             //最熟练的技能的熟练度

    private BigDecimal avgvalue;    //平均熟练度

    private BigDecimal avggrade;    //技能平均等级

    public Integer getSname() {
        return sname;
    }

    public void setSname(Integer sname) {
        this.sname = sname;
    }

    public Long getGradenum() {
        return gradenum;
    }

    public void setGradenum(Long gradenum) {
        this.gradenum = gradenum;
    }

    public String getGradename() {
        return gradename;
    }

    public void setGradename(String gradename) {
        this.gradename = gradename;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public BigDecimal getAvgvalue() {
        return avgvalue;
    }

    public void setAvgvalue(BigDecimal avgvalue) {
        this.avgvalue = avgvalue;
    }

    public BigDecimal getAvggrade() {
        return avggrade;
    }

    public void setAvggrade(BigDecimal avggrade) {
        this.avggrade = avggrade;
    }

    public WordCloudVo(Integer sname, Long gradenum, String gradename, Long value, BigDecimal avgvalue, BigDecimal avggrade) {
        this.sname = sname;
        this.gradenum = gradenum;
        this.gradename = gradename;
        this.value = value;
        this.avgvalue = avgvalue;
        this.avggrade = avggrade;
    }

    @Override
    public String toString() {
        return "WordCloudVo{" +
                "sname=" + sname +
                ", gradenum=" + gradenum +
                ", gradename='" + gradename + '\'' +
                ", value=" + value +
                ", avgvalue=" + avgvalue +
                ", avggrade=" + avggrade +
                '}';
    }
}

package com.hpe.vo;

/**
 * @Author:XuShengJie
 * @Description:
 * @Date:
 */
public class QuestionVo {
    private Integer id;

    private String courseName;

    private Integer courseid;

    private Integer quetype;

    private String quetitle;

    private String choicea;

    private String choiceb;

    private String choicec;

    private String choiced;

    private String ans;

    private Integer queexist;

    private String spare;

    public QuestionVo(Integer id, Integer courseid, Integer quetype, String quetitle, String choicea, String choiceb, String choicec, String choiced, String ans, Integer queexist, String spare,String courseName) {
        this.id = id;
        this.courseid = courseid;
        this.quetype = quetype;
        this.quetitle = quetitle;
        this.choicea = choicea;
        this.choiceb = choiceb;
        this.choicec = choicec;
        this.choiced = choiced;
        this.ans = ans;
        this.queexist = queexist;
        this.spare = spare;
        this.courseName = courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public QuestionVo() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    public Integer getQuetype() {
        return quetype;
    }

    public void setQuetype(Integer quetype) {
        this.quetype = quetype;
    }

    public String getQuetitle() {
        return quetitle;
    }

    public void setQuetitle(String quetitle) {
        this.quetitle = quetitle == null ? null : quetitle.trim();
    }

    public String getChoicea() {
        return choicea;
    }

    public void setChoicea(String choicea) {
        this.choicea = choicea == null ? null : choicea.trim();
    }

    public String getChoiceb() {
        return choiceb;
    }

    public void setChoiceb(String choiceb) {
        this.choiceb = choiceb == null ? null : choiceb.trim();
    }

    public String getChoicec() {
        return choicec;
    }

    public void setChoicec(String choicec) {
        this.choicec = choicec == null ? null : choicec.trim();
    }

    public String getChoiced() {
        return choiced;
    }

    public void setChoiced(String choiced) {
        this.choiced = choiced == null ? null : choiced.trim();
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans == null ? null : ans.trim();
    }

    public Integer getQueexist() {
        return queexist;
    }

    public void setQueexist(Integer queexist) {
        this.queexist = queexist;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    @Override
    public String toString() {
        return "Questions{" +
                "id=" + id +
                ", courseid=" + courseid +
                ", quetype=" + quetype +
                ", quetitle='" + quetitle + '\'' +
                ", choicea='" + choicea + '\'' +
                ", choiceb='" + choiceb + '\'' +
                ", choicec='" + choicec + '\'' +
                ", choiced='" + choiced + '\'' +
                ", ans='" + ans + '\'' +
                ", queexist=" + queexist +
                ", spare='" + spare + '\'' +
                '}';
    }
}

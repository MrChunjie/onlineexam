package com.hpe.vo;

import java.io.Serializable;

/**
 * Created by dell on 2017/10/10.
 */
public class StudentInfoVo implements Serializable{

    private static final long serialVersionUID = -5568346814189062412L;

    private Integer id;

    private String schoolId;

    private Integer newId;

    private String name;

    private String tel;

    private String number;

    private String sex;

    private String nation;

    private String home;

    private String parent;

    private String dorm;

    private String school;

    private String college;

    private String major;

    private String schoolClass;

    private String job;

    private String counselor;

    private String english;

    private String skill;

    private String stuOrder;

    private String restudyname;

    private Integer restudynum;

    private String restudytime;

    private String state;

    private String direction;

    private String spare;

    private String spare1;

    private String position;

    StudentInfoVo(){}

    public StudentInfoVo(Integer id, String schoolId, Integer newId, String name, String tel, String number, String sex, String nation, String home, String parent, String dorm, String school, String college, String major, String schoolClass, String job, String counselor, String english, String skill, String stuOrder, String restudyname, Integer restudynum, String restudytime, String state, String direction, String spare, String spare1, String position) {
        this.id = id;
        this.schoolId = schoolId;
        this.newId = newId;
        this.name = name;
        this.tel = tel;
        this.number = number;
        this.sex = sex;
        this.nation = nation;
        this.home = home;
        this.parent = parent;
        this.dorm = dorm;
        this.school = school;
        this.college = college;
        this.major = major;
        this.schoolClass = schoolClass;
        this.job = job;
        this.counselor = counselor;
        this.english = english;
        this.skill = skill;
        this.stuOrder = stuOrder;
        this.restudyname = restudyname;
        this.restudynum = restudynum;
        this.restudytime = restudytime;
        this.state = state;
        this.direction = direction;
        this.spare = spare;
        this.spare1 = spare1;
        this.position = position;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getNewId() {
        return newId;
    }

    public void setNewId(Integer newId) {
        this.newId = newId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getDorm() {
        return dorm;
    }

    public void setDorm(String dorm) {
        this.dorm = dorm;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(String schoolClass) {
        this.schoolClass = schoolClass;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCounselor() {
        return counselor;
    }

    public void setCounselor(String counselor) {
        this.counselor = counselor;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getStuOrder() {
        return stuOrder;
    }

    public void setStuOrder(String stuOrder) {
        this.stuOrder = stuOrder;
    }

    public String getRestudyname() {
        return restudyname;
    }

    public void setRestudyname(String restudyname) {
        this.restudyname = restudyname;
    }

    public Integer getRestudynum() {
        return restudynum;
    }

    public void setRestudynum(Integer restudynum) {
        this.restudynum = restudynum;
    }

    public String getRestudytime() {
        return restudytime;
    }

    public void setRestudytime(String restudytime) {
        this.restudytime = restudytime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "StudentInfoVo{" +
                "id=" + id +
                ", schoolId='" + schoolId + '\'' +
                ", newId=" + newId +
                ", name='" + name + '\'' +
                ", tel='" + tel + '\'' +
                ", number='" + number + '\'' +
                ", sex='" + sex + '\'' +
                ", nation='" + nation + '\'' +
                ", home='" + home + '\'' +
                ", parent='" + parent + '\'' +
                ", dorm='" + dorm + '\'' +
                ", school='" + school + '\'' +
                ", college='" + college + '\'' +
                ", major='" + major + '\'' +
                ", schoolClass='" + schoolClass + '\'' +
                ", job='" + job + '\'' +
                ", counselor='" + counselor + '\'' +
                ", english='" + english + '\'' +
                ", skill='" + skill + '\'' +
                ", stuOrder='" + stuOrder + '\'' +
                ", restudyname='" + restudyname + '\'' +
                ", restudynum=" + restudynum +
                ", restudytime='" + restudytime + '\'' +
                ", state='" + state + '\'' +
                ", direction='" + direction + '\'' +
                ", spare='" + spare + '\'' +
                ", spare1='" + spare1 + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}

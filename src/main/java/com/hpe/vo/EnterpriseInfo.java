package com.hpe.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hpe.pojo.Post;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public class EnterpriseInfo {
    private Integer id;

    private String name;

    private String text;

    private Integer type;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date eTime;

    private Integer category;

    private String spare;

    private String spare1;

    private String position;

    private List<Post> posts;

    public EnterpriseInfo(Integer id, String name, String text, Integer type, Date sTime, Date eTime, Integer category, String spare, String spare1, String position) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.type = type;
        this.sTime = sTime;
        this.eTime = eTime;
        this.category = category;
        this.spare = spare;
        this.spare1 = spare1;
        this.position = position;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public EnterpriseInfo() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text == null ? null : text.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getsTime() {
        return sTime;
    }

    public void setsTime(Date sTime) {
        this.sTime = sTime;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date geteTime() {
        return eTime;
    }

    public void seteTime(Date eTime) {
        this.eTime = eTime;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1 == null ? null : spare1.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    @Override
    public String toString() {
        return "EnterpriseInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", text='" + text + '\'' +
                ", type=" + type +
                ", sTime=" + sTime +
                ", eTime=" + eTime +
                ", category=" + category +
                ", spare='" + spare + '\'' +
                ", spare1='" + spare1 + '\'' +
                ", position='" + position + '\'' +
                ", posts=" + posts +
                '}';
    }
}

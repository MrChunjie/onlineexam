package com.hpe.vo;

import java.text.DecimalFormat;

/**
 * @Author:TangWenhao
 * @Description: 教务端对学生的成绩管理
 * @Date:10:55 2017/11/13
 */
public class EduScoreStuVO {
    private String stuName;
    private String className;
    private String courseName;
    private String examName;
    private double score_b,score_m,score_all;
    private String other;

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public double getScore_b() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_b));    }

    public void setScore_b(double score_b) {
        this.score_b = score_b;
    }

    public double getScore_m() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_m));
    }

    public void setScore_m(double score_m) {
        this.score_m = score_m;
    }

    public double getScore_all() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_all));
    }

    public void setScore_all(double score_all) {
        this.score_all = score_all;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}

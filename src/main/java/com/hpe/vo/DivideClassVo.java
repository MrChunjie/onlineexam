package com.hpe.vo;


/**
* @Author:CuiCaijin
* @Description:三段式分班vo
* @Date:20:44 2017/10/16
*/
public class DivideClassVo {

    /**
     * 第一阶段班级id
     */
    private String oneId;

    /**
     * 第二阶段班级id
     */
    private String twoId;

    /**
     * 第三阶段班级id
     */
    private String threeId;

    /**
     * 第一阶段人数
     */
    private int oneStu;

    /**
     * 第二阶段人数
     */
    private int twoStu;

    /**
     * 第三阶段人数
     */
    private int threeStu;

    /**
     * 第一阶段类型
     */
    private String oneType;

    /**
     * 第二阶段类型
     */
    private String twoType;

    /**
     * 第三阶段类型
     */
    private String threeType;

    private int groupId;

    public DivideClassVo() {
    }

    public DivideClassVo(String oneId, String twoId, String threeId, int oneStu, int twoStu, int threeStu, String oneType, String twoType, String threeType, int groupId) {
        this.oneId = oneId;
        this.twoId = twoId;
        this.threeId = threeId;
        this.oneStu = oneStu;
        this.twoStu = twoStu;
        this.threeStu = threeStu;
        this.oneType = oneType;
        this.twoType = twoType;
        this.threeType = threeType;
        this.groupId = groupId;
    }

    public String getOneId() {
        return oneId;
    }

    public void setOneId(String oneId) {
        this.oneId = oneId;
    }

    public String getTwoId() {
        return twoId;
    }

    public void setTwoId(String twoId) {
        this.twoId = twoId;
    }

    public String getThreeId() {
        return threeId;
    }

    public void setThreeId(String threeId) {
        this.threeId = threeId;
    }

    public int getOneStu() {
        return oneStu;
    }

    public void setOneStu(int oneStu) {
        this.oneStu = oneStu;
    }

    public int getTwoStu() {
        return twoStu;
    }

    public void setTwoStu(int twoStu) {
        this.twoStu = twoStu;
    }

    public int getThreeStu() {
        return threeStu;
    }

    public void setThreeStu(int threeStu) {
        this.threeStu = threeStu;
    }

    public String getOneType() {
        return oneType;
    }

    public void setOneType(String oneType) {
        this.oneType = oneType;
    }

    public String getTwoType() {
        return twoType;
    }

    public void setTwoType(String twoType) {
        this.twoType = twoType;
    }

    public String getThreeType() {
        return threeType;
    }

    public void setThreeType(String threeType) {
        this.threeType = threeType;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "DivideClassVo{" +
                "oneId='" + oneId + '\'' +
                ", twoId='" + twoId + '\'' +
                ", threeId='" + threeId + '\'' +
                ", oneStu=" + oneStu +
                ", twoStu=" + twoStu +
                ", threeStu=" + threeStu +
                ", oneType='" + oneType + '\'' +
                ", twoType='" + twoType + '\'' +
                ", threeType='" + threeType + '\'' +
                ", groupId=" + groupId +
                '}';
    }
}

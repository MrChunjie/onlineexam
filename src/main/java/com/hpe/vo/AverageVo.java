package com.hpe.vo;

import java.text.DecimalFormat;
import java.util.Date;

/**
 * @Author:TangWenhao
 * @Description:平均成绩视图类
 * @Date:9:51 2017/10/13
 */
public class AverageVo {
    private String className,courseName,testName;
    private double classAvg,courseAvg,examAvg;
    private int courseid,id,classId;
    private Date starttime;
    private String classids;

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassids() {
        return classids;
    }

    public void setClassids(String classids) {
        this.classids = classids;
    }

    public double getExamAvg() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(examAvg));
    }

    public void setExamAvg(double examAvg) {
        this.examAvg = examAvg;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public int getCourseid() {
        return courseid;
    }

    public void setCourseid(int courseid) {
        this.courseid = courseid;
    }

    @Override
    public String toString() {
        return "AverageVo{" +
                "className='" + className + '\'' +
                ", courseName='" + courseName + '\'' +
                ", testName='" + testName + '\'' +
                ", classAvg=" + classAvg +
                ", courseAvg=" + courseAvg +
                '}';
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public double getClassAvg() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(classAvg));
    }

    public void setClassAvg(double classAvg) {
        this.classAvg = classAvg;
    }

    public double getCourseAvg() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(courseAvg));    }

    public void setCourseAvg(double courseAvg) {
        this.courseAvg = courseAvg;
    }
}

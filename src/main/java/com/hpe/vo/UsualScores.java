package com.hpe.vo;

import java.text.DecimalFormat;

/**
 * @Author:TangWenhao
 * @Description:平时成绩视图类
 * @Date:10:28 2017/10/12
 */
public class UsualScores {
    private int sId,examId,classId,status;
    private String stuName,className,courseName,testName,scoreArray;
    private double scoreM,scores;

    public String getScoreArray() {
        return scoreArray;
    }

    public void setScoreArray(String scoreArray) {
        this.scoreArray = scoreArray;
    }

    @Override
    public String toString() {
        return "UsualScores{" +
                "sId=" + sId +
                ", examId=" + examId +
                ", classId=" + classId +
                ", status=" + status +
                ", stuName='" + stuName + '\'' +
                ", className='" + className + '\'' +
                ", courseName='" + courseName + '\'' +
                ", testName='" + testName + '\'' +
                ", scoreM=" + scoreM +
                ", scores=" + scores +
                '}';
    }

    public int getsId() {
        return sId;
    }

    public void setsId(int sId) {
        this.sId = sId;
    }

    public int getExamId() {
        return examId;
    }

    public void setExamId(int examId) {
        this.examId = examId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public double getScoreM() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(scoreM));    }

    public void setScoreM(double scoreM) {
        this.scoreM = scoreM;
    }

    public double getScores() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(scores));    }

    public void setScores(double scores) {
        this.scores = scores;
    }
}

package com.hpe.vo;

import java.io.Serializable;

/**
* @Author:CuiCaijin
* @Description:获得学生技能分数用
* @Date:11:37 2017/10/23
*/
public class GradeVo implements Serializable {

    private static final long serialVersionUID = -3876492867192052744L;

    /**
     * 技能id
     */
    private int id;

    /**
     * 技能等级
     */
    private int grade;

    /**
     * 技能熟练度
     */
    private int value;

    public GradeVo(int id, int grade, int value) {
        this.id = id;
        this.grade = grade;
        this.value = value;
    }

    public GradeVo(Integer id,Integer grade,Long value){
        this.id=id;
        this.grade=grade;
        this.value=value.intValue();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "GradeVo{" +
                "id=" + id +
                ", grade=" + grade +
                ", value=" + value +
                '}';
    }
}

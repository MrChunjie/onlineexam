package com.hpe.vo;

/**
 * @Author:XuShengJie
 * @Description:
 * @Date:
 */
public class ResetExamVo {
    private  Integer examId;

        private  String classId;

    private String sId;

    private Integer courseId;


    private String examName;


    public ResetExamVo() {
    }

    @Override
    public String toString() {
        return "ResetExamVo{" +
                "examId=" + examId +
                ", classId='" + classId + '\'' +
                ", sId='" + sId + '\'' +
                ", examName='" + examName + '\'' +
                '}';
    }

    public ResetExamVo(Integer examId, String classId, String sId, String examName,Integer courseId) {
        this.examId = examId;
        this.classId = classId;
        this.sId = sId;
        this.examName = examName;
        this.courseId = courseId;
    }

    public String getClassId() {
        return classId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }
}

package com.hpe.vo;

/**
 * Created by dell on 2017/10/17.
 */

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.sql.Timestamp;

/**
* @Author:Liuli
* @Description:班级人员视图
* @Date:10:16 2017/10/17
*/
public class ClassStuVo implements Serializable {

    private static final long serialVersionUID = 6615345802333736700L;

    private Integer classid;

    private Integer sid;

    private String sname;

    private Timestamp historytime;

    private String classname;

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Timestamp getHistorytime() {
        return historytime;
    }

    public void setHistorytime(Timestamp historytime) {
        this.historytime = historytime;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @Override
    public String toString() {
        return "ClassStuVo{" +
                "classid=" + classid +
                ", sid=" + sid +
                ", sname='" + sname + '\'' +
                ", historytime=" + historytime +
                ", classname='" + classname + '\'' +
                '}';
    }

    public ClassStuVo(Integer classid, Integer sid, String sname, Timestamp historytime, String classname) {
        this.classid = classid;
        this.sid = sid;
        this.sname = sname;
        this.historytime = historytime;
        this.classname = classname;
    }

    public ClassStuVo() {
    }
}

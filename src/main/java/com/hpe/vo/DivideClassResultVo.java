package com.hpe.vo;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
* @Author:CuiCaijin
* @Description:学生分班结果类
* @Date:9:15 2017/10/18
*/
public class DivideClassResultVo  implements Serializable{

    private static final long serialVersionUID = 8911046602866851148L;

    /**
     * 学生学号
     */
    private int newId;

    /**
     * 学生姓名
     */
    private String sname;

    /**
     * 学生现班级
     */
    private String className;

    /**
     * 学生排名
     */
    private int rank;

    /**
     * 学生总成绩
     */
    private double allScore;

    /**
     * 学生面试成绩
     */
    private double faceScore;

    /**
     * 学生考试的科目名
     */
    private String examNames;

    /**
     *
     * 学生考试和上面科目对应的成绩
     */
    private String scores;

    /**
     * 拆分之后的结果存储
     */
    private Map<String,String> result = new HashMap<String, String>();

    public DivideClassResultVo() {
    }
    public DivideClassResultVo(int newId, String sname, String className, int rank, double allScore, double faceScore, String examNames, String scores) {
        this.newId = newId;
        this.sname = sname;
        this.className = className;
        this.rank = rank;
        this.allScore = allScore;
        this.faceScore = faceScore;
        this.examNames = examNames;
        this.scores = scores;
    }

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getAllScore() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(allScore));
    }

    public void setAllScore(double allScore) {
        this.allScore = allScore;
    }

    public double getFaceScore() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(faceScore));
    }

    public void setFaceScore(double faceScore) {
        this.faceScore = faceScore;
    }

    public String getExamNames() {
        return examNames;
    }

    public void setExamNames(String examNames) {
        this.examNames = examNames;
    }

    public String getScores() {
        return scores;
    }

    public void setScores(String scores) {
        this.scores = scores;
    }

    public Map<String, String> getResult() {
        String[] exam;
        //判断是否可以被切割
        if(null == examNames && "".equals(examNames)){
            return Collections.emptyMap();
        }
        int examIndex = examNames.indexOf(",");
        if(examIndex == -1){
            if(examNames.length() == 0) {
                return Collections.emptyMap();
            }else{
                exam = new String[1];
                exam[0] = examNames;
            }
        }else{
            exam = examNames.split(",");
        }


        String[] score;
        if((null == scores && "".equals(scores))){
            int length = exam.length;
            score = new String[length];
            for(int i =0;i<length;i++){
                score[i] = "0";
            }
        }else {
            int scoreIndex = scores.indexOf(",");
            if (scoreIndex == -1 || (scoreIndex == -1 && scores.length() ==0)) {
                int length = exam.length;
                score = new String[length];
                for(int i =0;i<length;i++){
                    score[i] = "0";
                }
            }else{
                score= scores.split(",");
            }
        }


        //判断两个成绩和考试是否可以对应
        if(score.length == exam.length) {
            Map<String, String> examWithItsScoreMap = new HashMap<String, String>();
            for (int i = 0, a = exam.length; i < a; i++) {
                examWithItsScoreMap.put(exam[i], score[i]);
            }
            return examWithItsScoreMap;
        }
        return Collections.emptyMap();
    }

    public void setResult(Map<String, String> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "DivideClassResultVo{" +
                "newId=" + newId +
                ", sname='" + sname + '\'' +
                ", className='" + className + '\'' +
                ", rank=" + rank +
                ", allScore=" + allScore +
                ", faceScore=" + faceScore +
                ", examNames='" + examNames + '\'' +
                ", scores='" + scores + '\'' +
                ", result=" + result +
                '}';
    }
}

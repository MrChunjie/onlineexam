package com.hpe.vo;

import com.hpe.pojo.Questions;

import java.util.Arrays;
import java.util.List;

/**
 * @Author:TangWenhao
 * @Description: 历史试卷的详细信息
 * @Date:14:45 2017/10/16
 */
public class ExamHistoryDetail {
    private String[] titles1,titles2,ans1,ans2,
            wrongans,wrongqueid,que1Ans,que2Ans;
    private List<Questions> queList;

    public String[] getWrongans() {
        return wrongans;
    }

    public void setWrongans(String[] wrongans) {
        this.wrongans = wrongans;
    }

    public String[] getWrongqueid() {
        return wrongqueid;
    }

    public void setWrongqueid(String[] wrongqueid) {
        this.wrongqueid = wrongqueid;
    }

    @Override
    public String toString() {
        return "ExamHistoryDetail{" +
                "titles1=" + Arrays.toString(titles1) +
                ", titles2=" + Arrays.toString(titles2) +
                ", ans1=" + Arrays.toString(ans1) +
                ", ans2=" + Arrays.toString(ans2) +
                ", que1Ans=" + Arrays.toString(que1Ans) +
                ", que2Ans=" + Arrays.toString(que2Ans) +
                ", queList=" + queList +
                '}';
    }

    public String[] getTitles1() {
        return titles1;
    }

    public void setTitles1(String[] titles1) {
        this.titles1 = titles1;
    }

    public String[] getTitles2() {
        return titles2;
    }

    public void setTitles2(String[] titles2) {
        this.titles2 = titles2;
    }

    public String[] getAns1() {
        return ans1;
    }

    public void setAns1(String[] ans1) {
        this.ans1 = ans1;
    }

    public String[] getAns2() {
        return ans2;
    }

    public void setAns2(String[] ans2) {
        this.ans2 = ans2;
    }

    public String[] getQue1Ans() {
        return que1Ans;
    }

    public void setQue1Ans(String[] que1Ans) {
        this.que1Ans = que1Ans;
    }

    public String[] getQue2Ans() {
        return que2Ans;
    }

    public void setQue2Ans(String[] que2Ans) {
        this.que2Ans = que2Ans;
    }

    public List<Questions> getQueList() {
        return queList;
    }

    public void setQueList(List<Questions> queList) {
        this.queList = queList;
    }
}

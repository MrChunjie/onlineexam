package com.hpe.vo;

import java.util.Map;

/**
* @Author:CuiCaijin
* @Description:学生信息导出
* @Date:10:08 2017/10/27
*/
public class StudentInfoAndGradeExcelVo {

    private int id;

    private String studentName;

    private String newId;

    private String sex;

    private String nation;

    private String cardNumber;

    private String tel;

    private String home;

    private String parent;

    private String dorm;

    private String school;

    private String college;

    private String major;

    private String schoolClass;

    private String job;

    private String schoolId;

    private String counselor;

    private String english;

    private String skill;

    private String stuOrder;

    private String restudyName;

    private String restudyNum;

    private String restudyTime;

    private String className;

    private String oldClass;

    private String beginTime;

    private String endTime;

    private Map<String,String> grades;

    public StudentInfoAndGradeExcelVo() {
    }

    public StudentInfoAndGradeExcelVo(int id, String studentName, String newId, String sex, String nation, String cardNumber, String tel, String home, String parent, String dorm, String school, String college, String major, String schoolClass, String job, String schoolId, String counselor, String english, String skill, String stuOrder, String restudyName, String restudyNum, String restudyTime, String className, String oldClass, String beginTime, String endTime, Map<String, String> grades) {
        this.id = id;
        this.studentName = studentName;
        this.newId = newId;
        this.sex = sex;
        this.nation = nation;
        this.cardNumber = cardNumber;
        this.tel = tel;
        this.home = home;
        this.parent = parent;
        this.dorm = dorm;
        this.school = school;
        this.college = college;
        this.major = major;
        this.schoolClass = schoolClass;
        this.job = job;
        this.schoolId = schoolId;
        this.counselor = counselor;
        this.english = english;
        this.skill = skill;
        this.stuOrder = stuOrder;
        this.restudyName = restudyName;
        this.restudyNum = restudyNum;
        this.restudyTime = restudyTime;
        this.className = className;
        this.oldClass = oldClass;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.grades = grades;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getNewId() {
        return newId;
    }

    public void setNewId(String newId) {
        this.newId = newId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getDorm() {
        return dorm;
    }

    public void setDorm(String dorm) {
        this.dorm = dorm;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(String schoolClass) {
        this.schoolClass = schoolClass;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getCounselor() {
        return counselor;
    }

    public void setCounselor(String counselor) {
        this.counselor = counselor;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getStuOrder() {
        return stuOrder;
    }

    public void setStuOrder(String stuOrder) {
        this.stuOrder = stuOrder;
    }

    public String getRestudyName() {
        return restudyName;
    }

    public void setRestudyName(String restudyName) {
        this.restudyName = restudyName;
    }

    public String getRestudyNum() {
        return restudyNum;
    }

    public void setRestudyNum(String restudyNum) {
        this.restudyNum = restudyNum;
    }

    public String getRestudyTime() {
        return restudyTime;
    }

    public void setRestudyTime(String restudyTime) {
        this.restudyTime = restudyTime;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getOldClass() {
        return oldClass;
    }

    public void setOldClass(String oldClass) {
        this.oldClass = oldClass;
    }

    public Map<String, String> getGrades() {
        return grades;
    }

    public void setGrades(Map<String, String> grades) {
        this.grades = grades;
    }

    @Override
    public String toString() {
        return "StudentInfoAndGradeExcelVo{" +
                "studentName='" + studentName + '\'' +
                ", newId='" + newId + '\'' +
                ", sex='" + sex + '\'' +
                ", nation='" + nation + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", tel='" + tel + '\'' +
                ", home='" + home + '\'' +
                ", parent='" + parent + '\'' +
                ", dorm='" + dorm + '\'' +
                ", school='" + school + '\'' +
                ", college='" + college + '\'' +
                ", major='" + major + '\'' +
                ", schoolClass='" + schoolClass + '\'' +
                ", job='" + job + '\'' +
                ", schoolId='" + schoolId + '\'' +
                ", counselor='" + counselor + '\'' +
                ", english='" + english + '\'' +
                ", skill='" + skill + '\'' +
                ", stuOrder='" + stuOrder + '\'' +
                ", restudyName='" + restudyName + '\'' +
                ", restudyNum='" + restudyNum + '\'' +
                ", restudyTime='" + restudyTime + '\'' +
                ", className='" + className + '\'' +
                ", oldClass='" + oldClass + '\'' +
                ", grades=" + grades +
                '}';
    }
}

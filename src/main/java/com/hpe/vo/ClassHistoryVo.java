package com.hpe.vo;

/**
 * Created by dell on 2017/10/23.
 */
/**
* @Author:Liuli
* @Description:获取学生历史班级
* @Date:20:07 2017/10/23
*/
public class ClassHistoryVo {

    String classname;

    Long classnumber;

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public Long getClassnumber() {
        return classnumber;
    }

    public void setClassnumber(Long classnumber) {
        this.classnumber = classnumber;
    }

    @Override
    public String toString() {
        return "ClassHistoryVo{" +
                "classname='" + classname + '\'' +
                ", classnumber=" + classnumber +
                '}';
    }

    public ClassHistoryVo(String classname, Long classnumber) {
        this.classname = classname;
        this.classnumber = classnumber;
    }
}

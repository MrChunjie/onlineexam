package com.hpe.vo;

/**
 * @Author:XuShengJie
 * @Description:
 * @Date:
 */
public class BigQuestionVo {

    private Integer id;

    private String courseName;

    private Integer type;

    private Integer courseid;

    private String title;

    private String contentans;

    private String spare;

    public BigQuestionVo(Integer id, Integer type, Integer courseid, String title, String contentans, String spare,String courseName) {
        this.id = id;
        this.type = type;
        this.courseid = courseid;
        this.title = title;
        this.contentans = contentans;
        this.spare = spare;
        this.courseName = courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public BigQuestionVo() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContentans() {
        return contentans;
    }

    public void setContentans(String contentans) {
        this.contentans = contentans == null ? null : contentans.trim();
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    @Override
    public String toString() {
        return "BigQuestion{" +
                "id=" + id +
                ", type=" + type +
                ", courseid=" + courseid +
                ", title='" + title + '\'' +
                ", contentans='" + contentans + '\'' +
                ", spare='" + spare + '\'' +
                '}';
    }
}

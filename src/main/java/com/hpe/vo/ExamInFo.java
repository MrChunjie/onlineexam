package com.hpe.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * @Author:XuShengJie
 * @Description:试卷信息
 * @Date:
 */
public class ExamInFo {
    private Integer id;

    private String name;  //考试名

    private Integer courseid;  //考试的课程

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date starttime;  //开始时间

    private Date endtime;

    private String questions;  //题号

    private Integer teacherid;

    private String[] classids; //考试班级

    private Integer testtime;

    private Double score;   //选择题总分值

    private Double score1;  //简答题总分值

    private Double score2;  //编程题总分值

    private Double scores;  //试卷总分值

    private String spare;

    private String spare1;

    private int questionNum;    //选择题数目

    private int bigQuestionType1Num;    //简答题数目

    private int bigQuestionType2Num;    //编程题数目

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getQuestions() {
        return questions;
    }

    public void setQuestions(String questions) {
        this.questions = questions;
    }

    public Integer getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(Integer teacherid) {
        this.teacherid = teacherid;
    }

    public String[] getClassids() {
        return classids;
    }

    public void setClassids(String[] classids) {
        this.classids = classids;
    }

    public Integer getTesttime() {
        return testtime;
    }

    public void setTesttime(Integer testtime) {
        this.testtime = testtime;
    }

    public Double getScore() {
        if(score==null){
            return score;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score));
        }
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Double getScore1() {
        if(score1==null){
            return score1;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score1));
        }    }

    public void setScore1(Double score1) {
        this.score1 = score1;
    }

    public Double getScore2() {
        if(score2==null){
            return score2;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score2));
        }    }

    public void setScore2(Double score2) {
        this.score2 = score2;
    }

    public Double getScores() {
        if(scores==null){
            return scores;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(scores));
        }
    }

    public void setScores(Double scores) {
        this.scores = scores;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public int getQuestionNum() {
        return questionNum;
    }

    public void setQuestionNum(int questionNum) {
        this.questionNum = questionNum;
    }

    public int getBigQuestionType1Num() {
        return bigQuestionType1Num;
    }

    public void setBigQuestionType1Num(int bigQuestionType1Num) {
        this.bigQuestionType1Num = bigQuestionType1Num;
    }

    public int getBigQuestionType2Num() {
        return bigQuestionType2Num;
    }

    public void setBigQuestionType2Num(int bigQuestionType2Num) {
        this.bigQuestionType2Num = bigQuestionType2Num;
    }

    public ExamInFo(Integer id, String name, Integer courseid, Date starttime, Date endtime, String questions,
                    Integer teacherid, String[] classids, Integer testtime, Double score, Double score1, Double score2,
                    Double scores, String spare, String spare1, int questionNum, int bigQuestionType1Num,
                    int bigQuestionType2Num) {
        super();
        this.id = id;
        this.name = name;
        this.courseid = courseid;
        this.starttime = starttime;
        this.endtime = endtime;
        this.questions = questions;
        this.teacherid = teacherid;
        this.classids = classids;
        this.testtime = testtime;
        this.score = score;
        this.score1 = score1;
        this.score2 = score2;
        this.scores = scores;
        this.spare = spare;
        this.spare1 = spare1;
        this.questionNum = questionNum;
        this.bigQuestionType1Num = bigQuestionType1Num;
        this.bigQuestionType2Num = bigQuestionType2Num;
    }

    public ExamInFo() {
        super();
    }

    @Override
    public String toString() {
        return "ExamInFo [id=" + id + ", name=" + name + ", courseid=" + courseid + ", starttime=" + starttime
                + ", endtime=" + endtime + ", questions=" + questions + ", teacherid=" + teacherid + ", classids="
                + Arrays.toString(classids) + ", testtime=" + testtime + ", score=" + score + ", score1=" + score1
                + ", score2=" + score2 + ", scores=" + scores + ", spare=" + spare + ", spare1=" + spare1
                + ", questionNum=" + questionNum + ", bigQuestionType1Num=" + bigQuestionType1Num
                + ", bigQuestionType2Num=" + bigQuestionType2Num + "]";
    }

}

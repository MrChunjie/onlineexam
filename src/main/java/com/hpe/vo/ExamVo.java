package com.hpe.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DecimalFormat;

/**
 * Created by dell on 2017/10/17.
 */
/**
* @Author:Liuli
* @Description:试卷展示vo
* @Date:9:20 2017/10/17
*/
public class ExamVo implements Serializable {

    private static final long serialVersionUID = -5568346814189062700L;

    private Integer id;

    private String name;

    private String coursename;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp starttime;

    private Integer testtime;

    private String classes;

    private Double scores;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoursename() {
        return coursename;
    }

    public void setCoursename(String coursename) {
        this.coursename = coursename;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Timestamp getStarttime() {
        return starttime;
    }

    public void setStarttime(Timestamp starttime) {
        this.starttime = starttime;
    }

    public Integer getTesttime() {
        return testtime;
    }

    public void setTesttime(Integer testtime) {
        this.testtime = testtime;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    public Double getScores() {
        if(scores==null){
            return scores;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(scores));
        }
    }

    public void setScores(Double scores) {
        this.scores = scores;
    }

    @Override
    public String toString() {
        return "ExamVo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", coursename='" + coursename + '\'' +
                ", starttime=" + starttime +
                ", testtime=" + testtime +
                ", classes='" + classes + '\'' +
                ", scores=" + scores +
                '}';
    }

    public ExamVo(Integer id, String name, String coursename, Timestamp starttime, Integer testtime, String classes, Double scores) {
        this.id = id;
        this.name = name;
        this.coursename = coursename;
        this.starttime = starttime;
        this.testtime = testtime;
        this.classes = classes;
        this.scores = scores;
    }

    public ExamVo(){}
}

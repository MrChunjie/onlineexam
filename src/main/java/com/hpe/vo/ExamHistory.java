package com.hpe.vo;

import java.text.DecimalFormat;
import java.util.Date;

/**
 * @Author:TangWenhao
 * @Description:答卷历史视图类
 * @Date:16:20 2017/9/27
 */
public class ExamHistory {
    private int s_id,exam_id;
    private String courseName;
    private double score_1,score_2,score_3,scores,score_b;
    private String time,testName;
    private Date createdate,starttime;
    private String testtime;

    public double getScore_b() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_b));    }

    public void setScore_b(double score_b) {
        this.score_b = score_b;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public int getExam_id() {
        return exam_id;
    }

    public void setExam_id(int exam_id) {
        this.exam_id = exam_id;
    }

    public double getScore_1() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_1));    }

    public void setScore_1(double score_1) {
        this.score_1 = score_1;
    }

    public double getScore_2() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_2));    }

    public void setScore_2(double score_2) {
        this.score_2 = score_2;
    }

    public double getScore_3() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_3));    }

    public void setScore_3(double score_3) {
        this.score_3 = score_3;
    }

    public double getScores() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(scores));    }

    public void setScores(double scores) {
        this.scores = scores;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public String getTesttime() {
        return testtime;
    }

    public void setTesttime(String testtime) {
        this.testtime = testtime;
    }
}

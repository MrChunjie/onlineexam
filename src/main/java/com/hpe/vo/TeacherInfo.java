package com.hpe.vo;

public class TeacherInfo {

    private Integer id;

    private String name;

    private String pwd;

    private String className;

    public TeacherInfo() {
    }

    public TeacherInfo(Integer id, String name, String pwd, String className) {
        this.id = id;
        this.name = name;
        this.pwd = pwd;
        this.className = className;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPwd() {
        return pwd;
    }

    public String getClassName() {
        return className;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return "TeacherInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pwd='" + pwd + '\'' +
                ", className='" + className + '\'' +
                '}';
    }
}

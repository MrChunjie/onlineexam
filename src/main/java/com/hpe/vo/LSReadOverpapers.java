package com.hpe.vo;

import java.text.DecimalFormat;

/**
 * @Author:XuShengJie
 * @Description:流水阅卷视图类
 * @Date:
 */
public class LSReadOverpapers {
    private int  sId,direction,queType,status,eId,bigQuestionId;
    private String testtime,contentTitle,examName,contentans,bigqueids,smallTitle,bzAns,directionName,allBigQuestionIds;
    private double score1,score2,score3,scoreb;
    private double bqScore;

    public void setBigQuestionId(int bigQuestionId) {
        this.bigQuestionId = bigQuestionId;
    }

    public int getBigQuestionId() {
        return bigQuestionId;
    }

    public void setDirectionName(String directionName) {
        this.directionName = directionName;
    }

    public String getDirectionName() {
        return directionName;
    }

    public void setBzAns(String bzAns) {
        this.bzAns = bzAns;
    }

    public String getBzAns() {
        return bzAns;
    }

    public void setBqScore(double bqScore) {
        this.bqScore = bqScore;
    }
    public double getBqScore() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(bqScore));    }

    public void setSmallTitle(String smallTitle) {this.smallTitle = smallTitle;}
    public String getSmallTitle() {return smallTitle;}
    public int getsId() {
        return sId;
    }
    public void setsId(int sId) {
        this.sId = sId;
    }
    public int getDirection() {
        return direction;
    }
    public void setDirection(int direction) {
        this.direction = direction;
    }
    public int getQueType() {
        return queType;
    }
    public void setQueType(int queType) {
        this.queType = queType;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public int geteId() {
        return eId;
    }
    public void seteId(int eId) {
        this.eId = eId;
    }
    public String getTesttime() {
        return testtime;
    }
    public void setTesttime(String testtime) {
        this.testtime = testtime;
    }
    public String getContentTitle() {
        return contentTitle;
    }
    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }
    public String getExamName() {
        return examName;
    }
    public void setExamName(String examName) {
        this.examName = examName;
    }
    public String getContentans() {
        return contentans;
    }
    public void setContentans(String contentans) {
        this.contentans = contentans;
    }
    public String getBigqueids() {
        return bigqueids;
    }
    public void setBigqueids(String bigqueids) {
        this.bigqueids = bigqueids;
    }
    public double getScore1() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score1));    }
    public void setScore1(double score1) {
        this.score1 = score1;
    }
    public double getScore2() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score2));    }
    public void setScore2(double score2) {
        this.score2 = score2;
    }
    public double getScore3() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score3));    }
    public void setScore3(double score3) {
        this.score3 = score3;
    }
    public double getScoreb() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(scoreb));    }
    public void setScoreb(double scoreb) {
        this.scoreb = scoreb;
    }

    public void setAllBigQuestionIds(String allBigQuestionIds) {
        this.allBigQuestionIds = allBigQuestionIds;
    }

    public String getAllBigQuestionIds() {
        return allBigQuestionIds;
    }

    @Override
    public String toString() {
        return "aa [sId=" + sId + ", direction=" + direction + ", queType=" + queType + ", status=" + status + ", eId="
                + eId + ", testtime=" + testtime + ", contentTitle=" + contentTitle + ", examName=" + examName
                + ", contentans=" + contentans + ", bigqueids=" + bigqueids + ", score1=" + score1 + ", score2="
                + score2 + ", score3=" + score3 + ", scoreb=" + scoreb + "]";
    }
    public LSReadOverpapers(int sId, int direction, int queType, int status, int eId, String testtime, String contentTitle,
                            String examName, String contentans, String bigqueids, double score1, double score2, double score3,
                            double scoreb,String smallTitle,String directionName,int bigQuestionId,String allBigQuestionIds) {
        super();
        this.sId = sId;
        this.direction = direction;
        this.queType = queType;
        this.status = status;
        this.eId = eId;
        this.testtime = testtime;
        this.contentTitle = contentTitle;
        this.examName = examName;
        this.contentans = contentans;
        this.bigqueids = bigqueids;
        this.score1 = score1;
        this.score2 = score2;
        this.score3 = score3;
        this.scoreb = scoreb;
        this.smallTitle = smallTitle;
        this.directionName = directionName;
        this.bigQuestionId = bigQuestionId;
        this.allBigQuestionIds = allBigQuestionIds;
    }

    public LSReadOverpapers() {
        super();
    }

}

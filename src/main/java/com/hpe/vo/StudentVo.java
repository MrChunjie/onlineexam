package com.hpe.vo;

/**
 * Created by chunjie on 2017/10/9.
 */

import java.io.Serializable;

/**
* @Author:CuiCaijin
* @Description: 学生的vo类
* @Date:18:42 2017/10/9
*/
public class StudentVo implements Serializable{

    private static final long serialVersionUID = -5568346814189062414L;

    /**
     * 学生主键id
     */
    private int id;

    /**
     * 惠普学号
     */
    private int newId;

    /**
     * 班级名称
     */
    private String className;

    /**
     * 学生姓名
     */
    private String name;

    /**
     * 学生性别
     */
    private String sex;

    /**
     * 学生电话
     */
    private String tel;

    /**
     * 方向名
     */
    private String directionName;

    /**
     * 学生原学校
     */
    private String school;

    /**
     * 学生状态
     */
    private String state;

    public StudentVo(Integer id, Integer newId, String className, String name, String sex, String tel, String directionName, String school, String state) {
        this.id = id;
        if(newId == null){
            this.newId = 0;
        }else{
            this.newId = newId;
        }
        this.className = className;
        this.name = name;
        this.sex = sex;
        this.tel = tel;
        this.directionName = directionName;
        this.school = school;
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDirectionName() {
        return directionName;
    }

    public void setDirectionName(String directionName) {
        this.directionName = directionName;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "StudentVo{" +
                "id=" + id +
                ", newId=" + newId +
                ", className='" + className + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", tel='" + tel + '\'' +
                ", directionName='" + directionName + '\'' +
                ", school='" + school + '\'' +
                ", state=" + state +
                '}';
    }
}

package com.hpe.vo;

import java.text.DecimalFormat;

/**
 * @Author:TangWenhao
 * @Description: 教师评估学生成绩视图
 * @Date:10:40 2017/10/16
 */
public class EvaVo {
    private int s_id;
    private  String stuName,examName,sClassName,courseName;
    private double score_1,score_2,score_3,score_all,score_b,score_m;

    @Override
    public String toString() {
        return "EvaVo{" +
                "s_id=" + s_id +
                ", stuName='" + stuName + '\'' +
                ", examName='" + examName + '\'' +
                ", sClassName='" + sClassName + '\'' +
                ", courseName='" + courseName + '\'' +
                ", score_1=" + score_1 +
                ", score_2=" + score_2 +
                ", score_3=" + score_3 +
                ", score_all=" + score_all +
                ", score_b=" + score_b +
                ", score_m=" + score_m +
                '}';
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getsClassName() {
        return sClassName;
    }

    public void setsClassName(String sClassName) {
        this.sClassName = sClassName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public double getScore_1() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_1));    }

    public void setScore_1(double score_1) {
        this.score_1 = score_1;
    }

    public double getScore_2() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_2));    }

    public void setScore_2(double score_2) {
        this.score_2 = score_2;
    }

    public double getScore_3() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_3));    }

    public void setScore_3(double score_3) {
        this.score_3 = score_3;
    }

    public double getScore_all() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_all));    }

    public void setScore_all(double score_all) {
        this.score_all = score_all;
    }

    public double getScore_b() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_b));    }

    public void setScore_b(double score_b) {
        this.score_b = score_b;
    }

    public double getScore_m() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(score_m));    }

    public void setScore_m(double score_m) {
        this.score_m = score_m;
    }
}

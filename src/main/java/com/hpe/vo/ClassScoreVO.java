package com.hpe.vo;

import java.text.DecimalFormat;

/**
 * @Author:XuShengJie
 * @Description:
 * @Date:
 */
public class ClassScoreVO {
    private String className, other, teacher, course, examNames;
    double avg;
    private int examId,classId;

    public int getExamId() {
        return examId;
    }

    public void setExamId(int examId) {
        this.examId = examId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    @Override
    public String toString() {
        return "ClassScoreVO{" +
                "className='" + className + '\'' +
                ", other='" + other + '\'' +
                ", teacher='" + teacher + '\'' +
                ", course='" + course + '\'' +
                ", examNames='" + examNames + '\'' +
                ", avg=" + avg +
                '}';
    }

    public ClassScoreVO() {
    }

    public ClassScoreVO(String className, String other, String teacher, String course, String examNames, double avg) {
        this.className = className;
        this.other = other;
        this.teacher = teacher;
        this.course = course;
        this.examNames = examNames;
        this.avg = avg;
    }

    public double getAvg() {
        DecimalFormat df = new DecimalFormat("#0.0");
        return Double.parseDouble(df.format(avg));
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getExamNames() {
        return examNames;
    }

    public void setExamNames(String examNames) {
        this.examNames = examNames;
    }
}

package com.hpe.vo;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * Created by dell on 2017/10/14.
 */
public class GroupStuVo implements Serializable, Comparable {

    private static final long serialVersionUID = -5568346814189062696L;

    private Integer groupId;

    private Integer sId;

    private String sName;

    private Double score1;

    private Double score2;

    private String directionName;

    private Integer rank;

    private String className;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }



    @Override
    public String toString() {
        return "GroupStuVo{" +
                "groupId=" + groupId +
                ", sId=" + sId +
                ", sName='" + sName + '\'' +
                ", className='" + className + '\'' +
                ", directionName='" + directionName + '\'' +
                ", score1=" + score1 +
                ", score2=" + score2 +
                ", rank=" + rank +

                '}';
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public void setsId(Integer sId) {
        this.sId = sId;
    }


    public Integer getsId() {
        return sId;
    }


    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public Double getScore1() {
        if(score1==null){
            return score1;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score1));
        }
    }

    public void setScore1(Double score1) {
        this.score1 = score1;
    }

    public Double getScore2() {
        if(score2==null){
            return score2;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score2));
        }
    }

    public void setScore2(Double score2) {
        this.score2 = score2;
    }

    public String getDirectionName() {
        return directionName;
    }

    public void setDirectionName(String directionName) {
        this.directionName = directionName;
    }


    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }


    public GroupStuVo(Integer groupId, Integer sId, String sName, Double score1, Double score2, String directionName, Integer rank, String className) {
        this.groupId = groupId;
        this.sId = sId;
        this.sName = sName;
        this.score1 = score1;
        this.score2 = score2;
        this.directionName = directionName;
        this.rank = rank;
        this.className = className;
    }


    public GroupStuVo(){}

    @Override
    public int compareTo(Object o) {
        GroupStuVo stuVo = (GroupStuVo) o;

        double thisAll = this.score1 + this.score2;
        double oAll = stuVo.getScore1() + stuVo.getScore2();

        if (thisAll > oAll) {
            return -1;
        }else if (thisAll == oAll) {
            return 0;
        }else {
            return 1;
        }

    }
}

package com.hpe.vo;

/**
 * Created by chunjie on 2017/11/8.
 */

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
* @Author:CuiCaijin
* @Description:更新班级vo
* @Date:21:50 2017/11/8
*/
public class UpdateClassVo {

    /**
     * 汇总编号
     */
    private int groupId;

    /**
     * 班级方向
     */
    private int direction;

    /**
     * 班级创建日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    public UpdateClassVo() {
    }

    public UpdateClassVo(int groupId, int direction, Date createDate) {
        this.groupId = groupId;
        this.direction = direction;
        this.createDate = createDate;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "UpdateClassVo{" +
                "groupId=" + groupId +
                ", direction=" + direction +
                ", createDate=" + createDate +
                '}';
    }
}

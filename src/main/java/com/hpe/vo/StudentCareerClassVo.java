package com.hpe.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;

/**
 * Created by dell on 2017/10/24.
 */
/**
* @Author:Liuli
* @Description:用于个人生涯展示班级变更历史的视图
* @Date:10:47 2017/10/24
*/
public class StudentCareerClassVo {

    String sname;

    String coursename;

    String teachername;

    String classname;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    Timestamp changetime;

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getCoursename() {
        return coursename;
    }

    public void setCoursename(String coursename) {
        this.coursename = coursename;
    }

    public String getTeachername() {
        return teachername;
    }

    public void setTeachername(String teachername) {
        this.teachername = teachername;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Timestamp getChangetime() {
        return changetime;
    }

    public void setChangetime(Timestamp changetime) {
        this.changetime = changetime;
    }

    @Override
    public String toString() {
        return "StudentCareerClass{" +
                "sname='" + sname + '\'' +
                ", coursename='" + coursename + '\'' +
                ", teachername='" + teachername + '\'' +
                ", classname='" + classname + '\'' +
                ", changetime=" + changetime +
                '}';
    }

    public StudentCareerClassVo(String sname, String coursename, String teachername, String classname, Timestamp changetime) {
        this.sname = sname;
        this.coursename = coursename;
        this.teachername = teachername;
        this.classname = classname;
        this.changetime = changetime;
    }
}

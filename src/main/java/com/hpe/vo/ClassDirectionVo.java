package com.hpe.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by chunjie on 2017/10/24.
 */
public class ClassDirectionVo implements Serializable{
    private static final long serialVersionUID = 7503234035019470517L;
    private Integer id;

    private String name;

    private String direction;

    private Integer state;

    private Date createTime;

    private String other;

    private String spare;

    public ClassDirectionVo() {
    }

    public ClassDirectionVo(Integer id, String name, String direction, Integer state, Date createTime, String other, String spare) {
        this.id = id;
        this.name = name;
        this.direction = direction;
        this.state = state;
        this.createTime = createTime;
        this.other = other;
        this.spare = spare;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare;
    }

    @Override
    public String toString() {
        return "ClassDirectionVo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", direction='" + direction + '\'' +
                ", state=" + state +
                ", createTime=" + createTime +
                ", other='" + other + '\'' +
                ", spare='" + spare + '\'' +
                '}';
    }
}

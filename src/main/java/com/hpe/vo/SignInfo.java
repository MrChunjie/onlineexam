package com.hpe.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hpe.pojo.Enterprise;
import com.hpe.pojo.StudentInfo;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class SignInfo {
    private String choice;
    private Enterprise enterprise;
    private StudentInfo student;
    private Date time;

    private Integer typeB;

    private Integer typeM;

    private String money;

    private Integer state;

    private String spare;

    private String position;

    public SignInfo() {
        super();
    }

    public SignInfo(String choice, Enterprise enterprise, StudentInfo student, Date time, Integer typeB, Integer typeM, String money, Integer state, String spare, String position) {
        this.choice = choice;
        this.enterprise = enterprise;
        this.student = student;
        this.time = time;
        this.typeB = typeB;
        this.typeM = typeM;
        this.money = money;
        this.state = state;
        this.spare = spare;
        this.position = position;
    }

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice == null ? null : choice.trim();
    }

    public StudentInfo getStudent() {
        return student;
    }

    public void setStudent(StudentInfo student) {
        this.student = student;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getTypeB() {
        return typeB;
    }

    public void setTypeB(Integer typeB) {
        this.typeB = typeB;
    }

    public Integer getTypeM() {
        return typeM;
    }

    public void setTypeM(Integer typeM) {
        this.typeM = typeM;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money == null ? null : money.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    @Override
    public String toString() {
        return "SignInfo{" +
                "choice='" + choice + '\'' +
                ", enterprise=" + enterprise +
                ", student=" + student +
                ", time=" + time +
                ", typeB=" + typeB +
                ", typeM=" + typeM +
                ", money='" + money + '\'' +
                ", state=" + state +
                ", spare='" + spare + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}

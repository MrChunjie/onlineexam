package com.hpe.vo;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * Created by dell on 2017/10/14.
 */
public class ClassExamVo implements Serializable{

    private static final long serialVersionUID = 6615345802333736854L;

    private String examNames;

    private Integer courseId;

    private String courseName;

    private Double avg;

    public String getExamNames() {
        return examNames;
    }

    public void setExamNames(String examNames) {
        this.examNames = examNames;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Double getAvg() {
        if(avg==null){
            return avg;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(avg));
        }
    }

    public void setAvg(Double avg) {
        this.avg = avg;
    }

    public ClassExamVo(String examNames, Integer courseId, String courseName, Double avg) {
        this.examNames = examNames;
        this.courseId = courseId;
        this.courseName = courseName;
        this.avg = avg;
    }

    @Override
    public String toString() {
        return "ClassExamVo{" +
                "examNames='" + examNames + '\'' +
                ", courseId=" + courseId +
                ", courseName='" + courseName + '\'' +
                ", avg=" + avg +
                '}';
    }
}

package com.hpe.vo;

/**
 * Created by dell on 2017/10/11.
 */

import java.io.Serializable;
import java.text.DecimalFormat;

/**
* @Author:Liuli
* @Description:老师-课程-班级视图
* @Date:10:20 2017/10/11
*/
public class ClassTeacherVo implements Serializable{

    private static final long serialVersionUID = 6615345802333735835L;

    public ClassTeacherVo(){}

    public ClassTeacherVo(String className, Integer classId, String teacher, Integer tId, String course, Integer courseId, Double avg, Integer hours, Double satisfaction, String spare) {
        this.className = className;
        this.classId = classId;
        this.teacher = teacher;
        this.tId = tId;
        this.course = course;
        this.courseId = courseId;
        this.avg = avg;
        this.hours = hours;
        this.satisfaction = satisfaction;
        this.spare = spare;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer gettId() {
        return tId;
    }

    public void settId(Integer tId) {
        this.tId = tId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
    * @Author:Liuli
    * @Description:班级名
    * @Date:10:29 2017/10/11
    */

    private String className;

    private Integer classId;
    /**
    * @Author:Liuli
    * @Description:老师名
    * @Date:10:29 2017/10/11
    */
    private String teacher;

    private Integer tId;

    /**
    * @Author:Liuli
    * @Description:课程名
    * @Date:10:29 2017/10/11
    */
    private String course;

    private Integer courseId;

    private Double avg;

    public String getClassName() {
        return className;
    }


    public void setClassName(String className) {
        this.className = className;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public Double getAvg() {
        if(avg==null){
            return avg;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(avg));
        }
    }

    public void setAvg(Double avg) {
        this.avg = avg;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Double getSatisfaction() {
        if(satisfaction==null){
            return satisfaction;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(satisfaction));
        }
    }

    public void setSatisfaction(Double satisfaction) {
        this.satisfaction = satisfaction;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare;
    }


    private Integer hours;

    private Double satisfaction;

    private String spare;

    @Override
    public String toString() {
        return "ClassTeacherVo{" +
                "className='" + className + '\'' +
                ", classId=" + classId +
                ", teacher='" + teacher + '\'' +
                ", tId=" + tId +
                ", course='" + course + '\'' +
                ", courseId=" + courseId +
                ", avg=" + avg +
                ", hours=" + hours +
                ", satisfaction=" + satisfaction +
                ", spare='" + spare + '\'' +
                '}';
    }
}

package com.hpe.pojo;

import java.io.Serializable;

public class Admin implements Serializable{

    private static final long serialVersionUID = -867732979987770106L;

    private Integer id;

    private String name;

    private String pwd;

    private Integer type;

    private String spare;

    public Admin(Integer id, String name, String pwd, Integer type, String spare) {
        this.id = id;
        this.name = name;
        this.pwd = pwd;
        this.type = type;
        this.spare = spare;
    }

    public Admin() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd == null ? null : pwd.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pwd='" + pwd + '\'' +
                ", type=" + type +
                ", spare='" + spare + '\'' +
                '}';
    }
}
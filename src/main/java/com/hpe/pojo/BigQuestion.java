package com.hpe.pojo;

import java.io.Serializable;

public class BigQuestion implements Serializable{
    private static final long serialVersionUID = -577126410344151497L;

    private Integer id;

    private Integer type;

    private Integer courseid;

    private String title;

    private String contentans;

    private String spare;

    public BigQuestion(Integer id, Integer type, Integer courseid, String title, String contentans, String spare) {
        this.id = id;
        this.type = type;
        this.courseid = courseid;
        this.title = title;
        this.contentans = contentans;
        this.spare = spare;
    }

    public BigQuestion() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContentans() {
        return contentans;
    }

    public void setContentans(String contentans) {
        this.contentans = contentans == null ? null : contentans.trim();
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    @Override
    public String toString() {
        return "BigQuestion{" +
                "id=" + id +
                ", type=" + type +
                ", courseid=" + courseid +
                ", title='" + title + '\'' +
                ", contentans='" + contentans + '\'' +
                ", spare='" + spare + '\'' +
                '}';
    }
}
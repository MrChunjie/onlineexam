package com.hpe.pojo;

import java.io.Serializable;

public class Grade implements Serializable {

    private static final long serialVersionUID = -879781447628118626L;

    private Integer id;

    private String name;

    private Integer grade;

    private String spare;

    public Grade(Integer id, String name, Integer grade, String spare) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.spare = spare;
    }

    public Grade() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    @Override
    public String toString() {
        return "Grade{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", grade=" + grade +
                ", spare='" + spare + '\'' +
                '}';
    }
}
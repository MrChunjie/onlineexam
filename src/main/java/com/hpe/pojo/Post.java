package com.hpe.pojo;

import java.io.Serializable;

public class Post implements Serializable{
    private static final long serialVersionUID = -8969092833180736596L;
    private Integer id;

    private String name;

    private String spare;

    public Post(Integer id, String name, String spare) {
        this.id = id;
        this.name = name;
        this.spare = spare;
    }

    public Post() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", spare='" + spare + '\'' +
                '}';
    }
}
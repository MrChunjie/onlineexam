package com.hpe.pojo;

import java.io.Serializable;

public class ClassTeacherKey implements Serializable{
    private static final long serialVersionUID = -5943553773834196693L;
    private Integer classId;

    private Integer tId;

    private Integer courseId;

    public ClassTeacherKey(Integer classId, Integer tId, Integer courseId) {
        this.classId = classId;
        this.tId = tId;
        this.courseId = courseId;
    }

    public ClassTeacherKey() {
        super();
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer gettId() {
        return tId;
    }

    public void settId(Integer tId) {
        this.tId = tId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    @Override
    public String toString() {
        return "ClassTeacherKey{" +
                "classId=" + classId +
                ", tId=" + tId +
                ", courseId=" + courseId +
                '}';
    }
}
package com.hpe.pojo;

import java.io.Serializable;

public class Teacher implements Serializable {
    private static final long serialVersionUID = -7111902202164251136L;
    private Integer id;

    private String name;

    private String pwd;

    private Integer classId;

    public Teacher(Integer id, String name, String pwd, Integer classId) {
        this.id = id;
        this.name = name;
        this.pwd = pwd;
        this.classId = classId;
    }

    public Teacher() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd == null ? null : pwd.trim();
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pwd='" + pwd + '\'' +
                ", classId=" + classId +
                '}';
    }
}
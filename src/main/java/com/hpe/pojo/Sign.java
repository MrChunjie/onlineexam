package com.hpe.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class Sign extends SignKey implements Serializable {
    private static final long serialVersionUID = -7661118103551012542L;
    private String choice;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date time;

    private Integer typeB;

    private Integer typeM;

    private Integer state;

    private String spare;

    private String money;

    private String position;

    public Sign(Integer enterId, Integer sId, String choice, Date time, Integer typeB, Integer typeM, String money, Integer state, String spare, String position) {
        super(enterId, sId);
        this.choice = choice;
        this.time = time;
        this.typeB = typeB;
        this.typeM = typeM;
        this.money = money;
        this.state = state;
        this.spare = spare;
        this.position = position;
    }

    public Sign() {
        super();
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice == null ? null : choice.trim();
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getTypeB() {
        return typeB;
    }

    public void setTypeB(Integer typeB) {
        this.typeB = typeB;
    }

    public Integer getTypeM() {
        return typeM;
    }

    public void setTypeM(Integer typeM) {
        this.typeM = typeM;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money == null ? null : money.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    @Override
    public String toString() {
        return "Sign{" +
                "choice='" + choice + '\'' +
                ", time=" + time +
                ", typeB=" + typeB +
                ", typeM=" + typeM +
                ", money='" + money + '\'' +
                ", state=" + state +
                ", spare='" + spare + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
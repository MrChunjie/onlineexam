package com.hpe.pojo;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

public class Score extends ScoreKey implements Serializable {

    private Integer classId;

    private String time;

    private Double score1;

    private Double score2;

    private Double score3;

    private Double scoreB;

    private Double scoreM;

    private Double scoreAll;

    private String wrongqueid;

    private String wrongans;

    private String contenttitl;

    private String contentans;

    private Integer status;

    private Date createdate;

    private String bigqueids;

    private Integer rank;

    private Integer classRank;

    private String spare;

    private String spare1;

    public Score(Integer sId, Integer examId, Integer classId, String time, Double score1, Double score2, Double score3, Double scoreB, Double scoreM, Double scoreAll, String wrongqueid, String wrongans, String contenttitl, String contentans, Integer status, Date createdate, String bigqueids, Integer rank, Integer classRank, String spare, String spare1) {
        super(sId, examId);
        this.classId = classId;
        this.time = time;
        this.score1 = score1;
        this.score2 = score2;
        this.score3 = score3;
        this.scoreB = scoreB;
        this.scoreM = scoreM;
        this.scoreAll = scoreAll;
        this.wrongqueid = wrongqueid;
        this.wrongans = wrongans;
        this.contenttitl = contenttitl;
        this.contentans = contentans;
        this.status = status;
        this.createdate = createdate;
        this.bigqueids = bigqueids;
        this.rank = rank;
        this.classRank = classRank;
        this.spare = spare;
        this.spare1 = spare1;
    }

    public Score() {
        super();
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time == null ? null : time.trim();
    }

    public Double getScore1() {
        if(score1==null){
            return score1;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score1));
        }    }

    public void setScore1(Double score1) {
        this.score1 = score1;
    }

    public Double getScore2() {
        if(score2==null){
            return score2;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score2));
        }    }

    public void setScore2(Double score2) {
        this.score2 = score2;
    }

    public Double getScore3() {
        if(score3==null){
            return score3;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score3));
        }    }

    public void setScore3(Double score3) {
        this.score3 = score3;
    }

    public Double getScoreB() {
        if(scoreB==null){
            return scoreB;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(scoreB));
        }    }

    public void setScoreB(Double scoreB) {
        this.scoreB = scoreB;
    }

    public Double getScoreM() {
        if(scoreM==null){
            return scoreM;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(scoreM));
        }    }

    public void setScoreM(Double scoreM) {
        this.scoreM = scoreM;
    }

    public Double getScoreAll() {
        if(scoreAll==null){
            return scoreAll;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(scoreAll));
        }    }

    public void setScoreAll(Double scoreAll) {
        this.scoreAll = scoreAll;
    }

    public String getWrongqueid() {
        return wrongqueid;
    }

    public void setWrongqueid(String wrongqueid) {
        this.wrongqueid = wrongqueid == null ? null : wrongqueid.trim();
    }

    public String getWrongans() {
        return wrongans;
    }

    public void setWrongans(String wrongans) {
        this.wrongans = wrongans == null ? null : wrongans.trim();
    }

    public String getContenttitl() {
        return contenttitl;
    }

    public void setContenttitl(String contenttitl) {
        this.contenttitl = contenttitl == null ? null : contenttitl.trim();
    }

    public String getContentans() {
        return contentans;
    }

    public void setContentans(String contentans) {
        this.contentans = contentans == null ? null : contentans.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getBigqueids() {
        return bigqueids;
    }

    public void setBigqueids(String bigqueids) {
        this.bigqueids = bigqueids == null ? null : bigqueids.trim();
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getClassRank() {
        return classRank;
    }

    public void setClassRank(Integer classRank) {
        this.classRank = classRank;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1 == null ? null : spare1.trim();
    }

    @Override
    public String toString() {
        return "Score{" +
                "classId=" + classId +
                ", time='" + time + '\'' +
                ", score1=" + score1 +
                ", score2=" + score2 +
                ", score3=" + score3 +
                ", scoreB=" + scoreB +
                ", scoreM=" + scoreM +
                ", scoreAll=" + scoreAll +
                ", wrongqueid='" + wrongqueid + '\'' +
                ", wrongans='" + wrongans + '\'' +
                ", contenttitl='" + contenttitl + '\'' +
                ", contentans='" + contentans + '\'' +
                ", status=" + status +
                ", createdate=" + createdate +
                ", bigqueids='" + bigqueids + '\'' +
                ", rank=" + rank +
                ", classRank=" + classRank +
                ", spare='" + spare + '\'' +
                ", spare1='" + spare1 + '\'' +
                '}';
    }
}
package com.hpe.pojo;

import java.io.Serializable;

public class ScoreKey implements Serializable{

    private static final long serialVersionUID = -3154639300507498077L;

    private Integer sId;

    private Integer examId;

    public ScoreKey(Integer sId, Integer examId) {
        this.sId = sId;
        this.examId = examId;
    }

    public ScoreKey() {
        super();
    }

    public Integer getsId() {
        return sId;
    }

    public void setsId(Integer sId) {
        this.sId = sId;
    }

    public Integer getExamId() {
        return examId;
    }

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    @Override
    public String toString() {
        return "ScoreKey{" +
                "sId=" + sId +
                ", examId=" + examId +
                '}';
    }
}
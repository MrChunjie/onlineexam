package com.hpe.pojo;

import java.io.Serializable;

public class GroupKey implements Serializable {

    private static final long serialVersionUID = 648023692106895585L;

    private Integer groupId;

    private Integer sId;

    public GroupKey(Integer groupId, Integer sId) {
        this.groupId = groupId;
        this.sId = sId;
    }

    public GroupKey() {
        super();
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getsId() {
        return sId;
    }

    public void setsId(Integer sId) {
        this.sId = sId;
    }
}
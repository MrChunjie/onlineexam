package com.hpe.pojo;

import java.io.Serializable;

public class Student implements Serializable{

    private static final long serialVersionUID = 6584974469547970500L;

    private Integer id;

    private String sName;

    private String sPwd;

    public Student(Integer id, String sName, String sPwd) {
        this.id = id;
        this.sName = sName;
        this.sPwd = sPwd;
    }

    public Student() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName == null ? null : sName.trim();
    }

    public String getsPwd() {
        return sPwd;
    }

    public void setsPwd(String sPwd) {
        this.sPwd = sPwd == null ? null : sPwd.trim();
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", sName='" + sName + '\'' +
                ", sPwd='" + sPwd + '\'' +
                '}';
    }
}
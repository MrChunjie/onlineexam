package com.hpe.pojo;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

public class Exam implements Serializable{
    private static final long serialVersionUID = -2202378450374340758L;
    private Integer id;

    private String name;

    private Integer courseid;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date starttime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endtime;

    private String questions;

    private Integer teacherid;

    private String classids;

    private Integer testtime;

    private Double score;

    private Double score1;

    private Double score2;

    private Double scores;

    private String spare;

    private String spare1;

    public Exam(Integer id, String name, Integer courseid, Date starttime, Date endtime, String questions, Integer teacherid, String classids, Integer testtime, Double score, Double score1, Double score2, Double scores, String spare, String spare1) {
        this.id = id;
        this.name = name;
        this.courseid = courseid;
        this.starttime = starttime;
        this.endtime = endtime;
        this.questions = questions;
        this.teacherid = teacherid;
        this.classids = classids;
        this.testtime = testtime;
        this.score = score;
        this.score1 = score1;
        this.score2 = score2;
        this.scores = scores;
        this.spare = spare;
        this.spare1 = spare1;
    }

    public Exam() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getCourseid() {
        return courseid;
    }

    public void setCourseid(Integer courseid) {
        this.courseid = courseid;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getQuestions() {
        return questions;
    }

    public void setQuestions(String questions) {
        this.questions = questions == null ? null : questions.trim();
    }

    public Integer getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(Integer teacherid) {
        this.teacherid = teacherid;
    }

    public String getClassids() {
        return classids;
    }

    public void setClassids(String classids) {
        this.classids = classids == null ? null : classids.trim();
    }

    public Integer getTesttime() {
        return testtime;
    }

    public void setTesttime(Integer testtime) {
        this.testtime = testtime;
    }

    public Double getScore() {
        if(score==null){
            return score;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score));
        }
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Double getScore1() {
        if(score1==null){
            return score1;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score1));
        }    }

    public void setScore1(Double score1) {
        this.score1 = score1;
    }

    public Double getScore2() {
        if(score2==null){
            return score2;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score2));
        }
    }

    public void setScore2(Double score2) {
        this.score2 = score2;
    }

    public Double getScores() {
        if(scores==null){
            return scores;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(scores));
        }
    }

    public void setScores(Double scores) {
        this.scores = scores;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1 == null ? null : spare1.trim();
    }

    @Override
    public String toString() {
        return "Exam{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", courseid=" + courseid +
                ", starttime=" + starttime +
                ", endtime=" + endtime +
                ", questions='" + questions + '\'' +
                ", teacherid=" + teacherid +
                ", classids='" + classids + '\'' +
                ", testtime=" + testtime +
                ", score=" + score +
                ", score1=" + score1 +
                ", score2=" + score2 +
                ", scores=" + scores +
                ", spare='" + spare + '\'' +
                ", spare1='" + spare1 + '\'' +
                '}';
    }
}
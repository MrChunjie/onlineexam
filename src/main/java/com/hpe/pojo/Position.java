package com.hpe.pojo;

import java.io.Serializable;

public class Position implements Serializable {

    private static final long serialVersionUID = 4361145589813932474L;

    private Integer id;

    private String province;

    private String city;

    private String longitude;

    private String latitude;

    private String spare;

    public Position(Integer id, String province, String city, String longitude, String latitude, String spare) {
        this.id = id;
        this.province = province;
        this.city = city;
        this.longitude = longitude;
        this.latitude = latitude;
        this.spare = spare;
    }

    public Position() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude == null ? null : longitude.trim();
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude == null ? null : latitude.trim();
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    @Override
    public String toString() {
        return "Position{" +
                "longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", spare='" + spare + '\'' +
                '}';
    }
}
package com.hpe.pojo;

public class Description {
    private Integer id;

    private Integer dimension;

    private Integer score;

    private String description;

    private String spare;

    public Description(Integer id, Integer dimension, Integer score, String description, String spare) {
        this.id = id;
        this.dimension = dimension;
        this.score = score;
        this.description = description;
        this.spare = spare;
    }

    public Description() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDimension() {
        return dimension;
    }

    public void setDimension(Integer dimension) {
        this.dimension = dimension;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }
}
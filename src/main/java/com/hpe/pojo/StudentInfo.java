package com.hpe.pojo;

import java.io.Serializable;

public class StudentInfo implements Serializable {

    private static final long serialVersionUID = -1201445309698503204L;

    private Integer id;

    private String schoolId;

    private Integer newId;

    private String name;

    private String tel;

    private String number;

    private String sex;

    private String nation;

    private String home;

    private String parent;

    private String dorm;

    private String school;

    private String college;

    private String major;

    private String schoolClass;

    private String job;

    private String counselor;

    private String english;

    private String skill;

    private String stuOrder;

    private String restudyname;

    private Integer restudynum;

    private String restudytime;

    private String state;

    private String direction;

    private String spare;//新班级id

    private String spare1;

    private String position;

    public StudentInfo(Integer id, String schoolId, Integer newId, String name, String tel, String number, String sex, String nation, String home, String parent, String dorm, String school, String college, String major, String schoolClass, String job, String counselor, String english, String skill, String stuOrder, String restudyname, Integer restudynum, String restudytime, String state, String direction, String spare, String spare1, String position) {
        this.id = id;
        this.schoolId = schoolId;
        this.newId = newId;
        this.name = name;
        this.tel = tel;
        this.number = number;
        this.sex = sex;
        this.nation = nation;
        this.home = home;
        this.parent = parent;
        this.dorm = dorm;
        this.school = school;
        this.college = college;
        this.major = major;
        this.schoolClass = schoolClass;
        this.job = job;
        this.counselor = counselor;
        this.english = english;
        this.skill = skill;
        this.stuOrder = stuOrder;
        this.restudyname = restudyname;
        this.restudynum = restudynum;
        this.restudytime = restudytime;
        this.state = state;
        this.direction = direction;
        this.spare = spare;
        this.spare1 = spare1;
        this.position = position;
    }

    public StudentInfo() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String  getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getNewId() {
        return newId;
    }

    public void setNewId(Integer newId) {
        this.newId = newId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel == null ? null : tel.trim();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation == null ? null : nation.trim();
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home == null ? null : home.trim();
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent == null ? null : parent.trim();
    }

    public String getDorm() {
        return dorm;
    }

    public void setDorm(String dorm) {
        this.dorm = dorm == null ? null : dorm.trim();
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school == null ? null : school.trim();
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college == null ? null : college.trim();
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major == null ? null : major.trim();
    }

    public String getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(String schoolClass) {
        this.schoolClass = schoolClass == null ? null : schoolClass.trim();
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job == null ? null : job.trim();
    }

    public String getCounselor() {
        return counselor;
    }

    public void setCounselor(String counselor) {
        this.counselor = counselor == null ? null : counselor.trim();
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english == null ? null : english.trim();
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill == null ? null : skill.trim();
    }

    public String getStuOrder() {
        return stuOrder;
    }

    public void setStuOrder(String stuOrder) {
        this.stuOrder = stuOrder == null ? null : stuOrder.trim();
    }

    public String getRestudyname() {
        return restudyname;
    }

    public void setRestudyname(String restudyname) {
        this.restudyname = restudyname == null ? null : restudyname.trim();
    }

    public Integer getRestudynum() {
        return restudynum;
    }

    public void setRestudynum(Integer restudynum) {
        this.restudynum = restudynum;
    }

    public String getRestudytime() {
        return restudytime;
    }

    public void setRestudytime(String restudytime) {
        this.restudytime = restudytime == null ? null : restudytime.trim();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction == null ? null : direction.trim();
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1 == null ? null : spare1.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    @Override
    public String toString() {
        return "StudentInfo{" +
                "id=" + id +
                ", schoolId=" + schoolId +
                ", newId=" + newId +
                ", name='" + name + '\'' +
                ", tel='" + tel + '\'' +
                ", number='" + number + '\'' +
                ", sex='" + sex + '\'' +
                ", nation='" + nation + '\'' +
                ", home='" + home + '\'' +
                ", parent='" + parent + '\'' +
                ", dorm='" + dorm + '\'' +
                ", school='" + school + '\'' +
                ", college='" + college + '\'' +
                ", major='" + major + '\'' +
                ", schoolClass='" + schoolClass + '\'' +
                ", job='" + job + '\'' +
                ", counselor='" + counselor + '\'' +
                ", english='" + english + '\'' +
                ", skill='" + skill + '\'' +
                ", stuOrder='" + stuOrder + '\'' +
                ", restudyname='" + restudyname + '\'' +
                ", restudynum=" + restudynum +
                ", restudytime='" + restudytime + '\'' +
                ", state='" + state + '\'' +
                ", direction='" + direction + '\'' +
                ", spare='" + spare + '\'' +
                ", spare1='" + spare1 + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
package com.hpe.pojo;

import java.io.Serializable;
import java.util.Date;

public class ClassNum extends ClassNumKey implements Serializable{
    private static final long serialVersionUID = 7138795198187675255L;
    private Integer cNum;

    private Date changeDate;

    private String spare;

    public ClassNum(Integer id, Integer cId, Integer cNum, Date changeDate, String spare) {
        super(id, cId);
        this.cNum = cNum;
        this.changeDate = changeDate;
        this.spare = spare;
    }

    public ClassNum() {
        super();
    }

    public Integer getcNum() {
        return cNum;
    }

    public void setcNum(Integer cNum) {
        this.cNum = cNum;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    @Override
    public String toString() {
        return "ClassNum{" +
                "cNum=" + cNum +
                ", changeDate=" + changeDate +
                ", spare='" + spare + '\'' +
                '}';
    }
}
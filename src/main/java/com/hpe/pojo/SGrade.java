package com.hpe.pojo;

import java.io.Serializable;

public class SGrade extends SGradeKey implements Serializable{
    private static final long serialVersionUID = -857294462278266445L;
    private Integer value;

    public SGrade(Integer gradeId, Integer sId, Integer value) {
        super(gradeId, sId);
        this.value = value;
    }

    public SGrade() {
        super();
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return super.toString()+" SGrade{" +
                "value=" + value +
                '}';
    }
}
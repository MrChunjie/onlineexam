package com.hpe.pojo;

import java.io.Serializable;

public class SignKey implements Serializable {
    private static final long serialVersionUID = -4229273137064058775L;
    private Integer enterId;

    private Integer sId;

    public SignKey(Integer enterId, Integer sId) {
        this.enterId = enterId;
        this.sId = sId;
    }

    public SignKey() {
        super();
    }

    public Integer getEnterId() {
        return enterId;
    }

    public void setEnterId(Integer enterId) {
        this.enterId = enterId;
    }

    public Integer getsId() {
        return sId;
    }

    public void setsId(Integer sId) {
        this.sId = sId;
    }

    @Override
    public String toString() {
        return "SignKey{" +
                "enterId=" + enterId +
                ", sId=" + sId +
                '}';
    }
}
package com.hpe.pojo;

import java.io.Serializable;
import java.util.Date;

public class SClass implements Serializable {

    private static final long serialVersionUID = -8091564449509829039L;

    private Integer id;

    private String name;

    private Integer type;

    private Integer state;

    private Date createTime;

    private String other;

    private String spare;

    public SClass(Integer id, String name, Integer type, Integer state, Date createTime, String other, String spare) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.state = state;
        this.createTime = createTime;
        this.other = other;
        this.spare = spare;
    }

    public SClass() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other == null ? null : other.trim();
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    @Override
    public String toString() {
        return "SClass{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", state=" + state +
                ", createTime=" + createTime +
                ", other='" + other + '\'' +
                ", spare='" + spare + '\'' +
                '}';
    }
}
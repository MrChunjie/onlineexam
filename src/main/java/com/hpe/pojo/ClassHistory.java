package com.hpe.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class ClassHistory {
    private Integer id;

    private Integer sId;

    private Integer classId;

    private String sName;

    private String spare;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date historyTime;

    public ClassHistory(Integer id, Integer sId, Integer classId, String sName, String spare, Date historyTime) {
        this.id = id;
        this.sId = sId;
        this.classId = classId;
        this.sName = sName;
        this.spare = spare;
        this.historyTime = historyTime;
    }

    public ClassHistory() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getsId() {
        return sId;
    }

    public void setsId(Integer sId) {
        this.sId = sId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName == null ? null : sName.trim();
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getHistoryTime() {
        return historyTime;
    }

    public void setHistoryTime(Date historyTime) {
        this.historyTime = historyTime;
    }
}
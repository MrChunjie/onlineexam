package com.hpe.pojo;

import java.io.Serializable;

public class EnterPost implements Serializable {
    private static final long serialVersionUID = 4124011792486058243L;

    private Integer enterId;

    private Integer postId;

    public EnterPost(Integer enterId, Integer postId) {
        this.enterId = enterId;
        this.postId = postId;
    }

    public EnterPost() {
        super();
    }

    public Integer getEnterId() {
        return enterId;
    }

    public void setEnterId(Integer enterId) {
        this.enterId = enterId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    @Override
    public String toString() {
        return "EnterPost{" +
                "enterId=" + enterId +
                ", postId=" + postId +
                '}';
    }
}
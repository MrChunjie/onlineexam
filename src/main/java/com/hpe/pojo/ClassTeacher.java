package com.hpe.pojo;

import java.io.Serializable;
import java.text.DecimalFormat;

public class ClassTeacher extends ClassTeacherKey implements Serializable {
    private static final long serialVersionUID = 6615345802333735826L;
    private Double avg;

    private Integer hours;

    private Double satisfaction;

    private String spare;

    public ClassTeacher(Integer classId, Integer tId, Integer courseId, Double avg, Integer hours, Double satisfaction, String spare) {
        super(classId, tId, courseId);
        this.avg = avg;
        this.hours = hours;
        this.satisfaction = satisfaction;
        this.spare = spare;
    }

    public ClassTeacher() {
        super();
    }

    public Double getAvg() {
        if(avg==null){
            return avg;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(avg));
        }
    }

    public void setAvg(Double avg) {
        this.avg = avg;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Double getSatisfaction() {
        if(satisfaction==null){
            return satisfaction;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(satisfaction));
        }
    }

    public void setSatisfaction(Double satisfaction) {
        this.satisfaction = satisfaction;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    @Override
    public String toString() {
        return "ClassTeacher{" +
                "avg=" + avg +
                ", hours=" + hours +
                ", satisfaction=" + satisfaction +
                ", spare='" + spare + '\'' +
                '}';
    }
}
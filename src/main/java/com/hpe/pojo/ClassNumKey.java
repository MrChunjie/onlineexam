package com.hpe.pojo;

import java.io.Serializable;

public class ClassNumKey implements Serializable {
    private static final long serialVersionUID = 1536150697796320551L;
    private Integer id;

    private Integer cId;

    public ClassNumKey(Integer id, Integer cId) {
        this.id = id;
        this.cId = cId;
    }

    public ClassNumKey() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    @Override
    public String toString() {
        return "ClassNumKey{" +
                "id=" + id +
                ", cId=" + cId +
                '}';
    }
}
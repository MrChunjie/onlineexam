package com.hpe.pojo;

import java.io.Serializable;

public class SGradeKey implements Serializable{
    private Integer gradeId;

    private Integer sId;

    public SGradeKey(Integer gradeId, Integer sId) {
        this.gradeId = gradeId;
        this.sId = sId;
    }

    public SGradeKey() {
        super();
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public Integer getsId() {
        return sId;
    }

    public void setsId(Integer sId) {
        this.sId = sId;
    }

    @Override
    public String toString() {
        return "SGradeKey{" +
                "gradeId=" + gradeId +
                ", sId=" + sId +
                '}';
    }
}
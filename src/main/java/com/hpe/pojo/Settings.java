package com.hpe.pojo;

public class Settings {
    private Integer maxSign;

    private Integer registerState;

    public Settings(Integer maxSign, Integer registerState) {
        this.maxSign = maxSign;
        this.registerState = registerState;
    }

    public Settings() {
        super();
    }

    public Integer getMaxSign() {
        return maxSign;
    }

    public void setMaxSign(Integer maxSign) {
        this.maxSign = maxSign;
    }

    public Integer getRegisterState() {
        return registerState;
    }

    public void setRegisterState(Integer registerState) {
        this.registerState = registerState;
    }
}
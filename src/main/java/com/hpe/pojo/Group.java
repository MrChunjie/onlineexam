package com.hpe.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

public class Group extends GroupKey  implements Serializable{

    private static final long serialVersionUID = -1585370814924009793L;

    private Double score1;

    private Double score2;

    private Double scoreAll;

    private Integer rank;

    private String spare;

    private String examIds;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date groupTime;

    private Integer direction;

    private String spare1;

    @Override
    public String toString() {
        return "Group{" +
                "score1=" + score1 +
                ", score2=" + score2 +
                ", scoreAll=" + scoreAll +
                ", rank=" + rank +
                ", spare='" + spare + '\'' +
                ", examIds='" + examIds + '\'' +
                ", groupTime=" + groupTime +
                ", direction=" + direction +
                ", spare1='" + spare1 + '\'' +
                '}';
    }

    public Group(Integer groupId, Integer sId, Double score1, Double score2, Double scoreAll, Integer rank, String spare, String examIds, Date groupTime, Integer direction, String spare1) {
        super(groupId, sId);
        this.score1 = score1;
        this.score2 = score2;
        this.scoreAll = scoreAll;
        this.rank = rank;
        this.spare = spare;
        this.examIds = examIds;
        this.groupTime = groupTime;
        this.direction = direction;
        this.spare1 = spare1;
    }

    public Group() {
        super();
    }

    public Double getScore1() {
        if(score1==null){
            return score1;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score1));
        }    }

    public void setScore1(Double score1) {
        this.score1 = score1;
    }

    public Double getScore2() {
        if(score2==null){
            return score2;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(score2));
        }    }

    public void setScore2(Double score2) {
        this.score2 = score2;
    }

    public Double getScoreAll() {
        if(scoreAll==null){
            return scoreAll;
        }else {
            DecimalFormat df = new DecimalFormat("#0.0");
            return Double.parseDouble(df.format(scoreAll));
        }    }

    public void setScoreAll(Double scoreAll) {
        this.scoreAll = scoreAll;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare == null ? null : spare.trim();
    }

    public String getExamIds() {
        return examIds;
    }

    public void setExamIds(String examIds) {
        this.examIds = examIds == null ? null : examIds.trim();
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date getGroupTime() {
        return groupTime;
    }

    public void setGroupTime(Date groupTime) {
        this.groupTime = groupTime;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1 == null ? null : spare1.trim();
    }
}
package com.hpe.service;

import java.util.Map;

/**
 * Created by dell on 2017/10/16.
 */
public interface ClassNumService {
    /**
    * @Author:Liuli
    * @Description:获取班级现人数
    * @Date:23:36 2017/10/16
    */
    int getClassNum(int id);

    Map<String, Object> getAllClassNum (int classid);
}

package com.hpe.service;


import com.github.pagehelper.PageInfo;
import com.hpe.pojo.BigQuestion;
import com.hpe.util.ServerResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.List;
import java.util.Map;


/**
 * @Author:TangWenhao
 * @Description:
 * @Date:19:29 2017/9/25
 */
public interface BigQuestionService {

    /**
     * @Author:XuShengJie
     * @Description:显示所有的简答题和编程题
     * @Date:17:18 2017/9/26
     */
    ServerResponse<PageInfo> showAllBigQuestions(String bqquestionTitle , String bqcourseName,int pageNum, int pageSize);

    /**
     * @Author:XuShengJie
     * @Description:添加答题的页面和修改答题的页面(回显原来的值)
     * @Date:19:45 2017/9/26
     */
    ServerResponse<Map<String,Object>> editBigQuestion(BigQuestion bigQuestion);

    /**
     * @Author:XuShengJie
     * @Description:修改和添加操作
     * @Date:19:47 2017/9/26
     */
    ServerResponse updateBigQuestion(BigQuestion bigQuestion);

    /**
     * @Author:XuShengJie
     * @Description:删除大题
     * @Date:19:48 2017/9/26
     */
    ServerResponse deleteBigQuestion(BigQuestion bigQuestion);

    List<BigQuestion> findBigQuesByIds(String[] ids, int type);

    /**
     * @Author:XuShengJie
     * @Description:大题题库导入
     * @Date:19:45 2017/10/25
     */
    ServerResponse loadinExcelForBq(InputStream inputStream,Integer courseId, Integer queType) throws Exception;
    
    /**
     * @Author:XuShengJie
     * @Description:导出大题excel表头
     * @Date:9:05 2017/10/26
     */
    void outputSelectBQuestionExcelTitle(HttpServletRequest req, HttpServletResponse resp)throws Exception;
    
    /**
     * @Author:XuShengJie
     * @Description:根据大题id获取题目信息
     * @Date:8:53 2017/11/3
     */
    BigQuestion getBQById(int bqId);
}

package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.ClassHistory;
import com.hpe.util.ServerResponse;
import com.hpe.vo.StudentCareerClassVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by dell on 2017/10/12.
 */
public interface ClassHistoryService {

    /**
    * @Author:Liuli
    * @Description:增加班级历史
    * @Date:16:06 2017/10/12
    */
    int insert(ClassHistory classHistory);

    /**
    * @Author:Liuli
    * @Description:根据学号获取学生班级历史
    * @Date:16:31 2017/10/12
    */
    List<ClassHistory> getHistoryByStudentId(int id);

    /**
    * @Author:Liuli
    * @Description:根据学号获取学生班级历史（echarts）
    * @Date:16:22 2017/10/16
    */
    Map<String, Object> getHistoryByStuId(int id);

    /**
    * @Author:Liuli
    * @Description:获取班级所有学生（历史）
    * @Date:23:32 2017/10/16
    */
    List<ClassHistory> getAllStudent(int id);

    /**
    * @Author:Liuli
    * @Description:获取班级所有学生（分页）
    * @Date:23:32 2017/10/16
    */
    ServerResponse<PageInfo> getAllStudentByClassId(int pageNum, int pageSize,int id);
    /**
     * @Author:CuiCaijin
     * @Description:查看班级历史某个时间点的人
     * @Date:9:43 2017/11/14
     */
    ServerResponse<PageInfo> getStuVoByClassIdAndHistory(int pageNum, int pageSize,int classId,String date);

    /**
     * @Author:CuiCaijin
     * @Description:根据姓名获得班级历史某个时间的人
     * @Date:9:43 2017/11/14
     */
    ServerResponse<PageInfo> getStuVoByClassIdAndHistoryAndName(int pageNum, int pageSize,int classId,String date,String name);

    /**
    * @Author:Liuli
    * @Description:获得某班级学生的历史班级
    * @Date:20:14 2017/10/23
    */
    Map<String, Object> getAllStuHistory(int classId);

    /**
    * @Author:Liuli
    * @Description:获取学生个人生涯的历史班级、课程、老师
    * @Date:10:54 2017/10/24
    */
    List<StudentCareerClassVo> getAllStuClassCareer(int studentId);
}

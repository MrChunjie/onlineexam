package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.util.ServerResponse;
import com.hpe.vo.UpdateClassVo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

/**
 * Created by chunjie on 2017/10/12.
 */
public interface GroupService {

    /**
    * @Author:CuiCaijin
    * @Description:判断是否已经分班
    * @Date:14:43 2017/10/12
    */
    ServerResponse<Boolean> ifClassDivide();

    /**
    * @Author:CuiCaijin
    * @Description:展示考试的学生数量
    * @Date:14:44 2017/10/12
    */
    ServerResponse<Integer> showStudentCount();

    /**
    * @Author:Liuli
    * @Description:获取所有的汇总视图
    * @Date:23:33 2017/10/13
    */
    ServerResponse<PageInfo> showAllGroupVo(int pageNum, int pageSize);


    /**
     * @description 插入汇总数据
     * @author Fujt
     * @date 2017/10/13 15:39
     */
    ServerResponse findAndInsertGroup(String type, String exams);

    /**
     * @description 获得所有的分组
     * @author Fujt
     * @date 2017/10/13 19:37
     */
    ServerResponse<PageInfo> getAllGroup(int pageNum, int pageSize);


    /**
     * @description 输出excel流到客户端
     * @author Fujt
     * @date 2017/10/14 20:18
     */
    void outputExcelByGroupId(HttpServletRequest req, HttpServletResponse resp, int id) throws Exception;

    /**
     * @description 读取excel
     * @author Fujt
     * @date 2017/10/15 13:07
     * @param inputStream 文件的输入流
     */
    ServerResponse inputExcel(InputStream inputStream) throws Exception;

    /**
    * @Author:CuiCaijin
    * @Description:根据id获得学生数量
    * @Date:11:56 2017/10/16
    */
    ServerResponse<Integer> getStudentCountByGroupId(int groupId);

    /**
    * @Author:CuiCaijin
    * @Description:根据groupId获得方向
    * @Date:11:56 2017/10/16
    */
    ServerResponse<Integer> getDirectionByGroupId(int groupId);

    /**
    * @Author:CuiCaijin
    * @Description:输出分班结果excel
    * @Date:9:55 2017/10/18
    */
    void outputDivideCLassResultExcelByGroupId(HttpServletRequest req, HttpServletResponse resp, int groupId) throws Exception;

    /**
    * @Author:CuiCaijin
    * @Description:根据学号获得最好的成绩，作为雷达图学习部分
    * @Date:16:08 2017/10/23
    */
    int getMaxRankByStudentId(int studentId);

    /**
    * @Author:CuiCaijin
    * @Description:根据学号获得最大的进步，作为雷达图的学习部分
    * @Date:16:09 2017/10/23
    */
    int getMaxProcessStudentId(int studentId);

    /**
    * @Author:Liuli
    * @Description:获得学生详情下载表
    * @Date:23:30 2017/10/27
    */
    void outputStudentInfoAndGradeExcel(HttpServletRequest req, HttpServletResponse resp,String begin,String end) throws IOException;

    /**
    * @Author:CuiCaijin
    * @Description:根据groupId获得日期，然后判断是否存在未注销的班级
    * @Date:21:46 2017/11/8
    */
    ServerResponse<UpdateClassVo> getClassCountByGroupId(UpdateClassVo classVo);

    /**
    * @Author:CuiCaijin
    * @Description:更新
    * @Date:21:47 2017/11/8
    */
    ServerResponse<String> updateClassStateByDate(UpdateClassVo classVo);
}

package com.hpe.service;

import com.hpe.pojo.SGrade;
import com.hpe.pojo.SGradeKey;
import com.hpe.vo.WordCloudVo;

import java.util.List;

/**
 * Created by dell on 2017/9/25.
 */
public interface SGradeService {

    List<SGrade> getByStudentId (int studentid);

    int addGrade(SGrade sGrade);

    /**
     * @description 批量添加数据
     * @author Fujt
     * @date 2017/10/10 14:50
     */
    int addGrades(SGrade[] sGrades);


    int deleteByPrimaryKey(SGradeKey sGradeKey);

    /**
    * @Author:CuiCaijin
    * @Description:根据学生id获得雷达图的学生技能值
    * @Date:16:51 2017/10/23
    */
    int getGradeValueByStudentId(int studentId);

    /**
    * @Author:Liuli
    * @Description:获取词云描述
    * @Date:10:11 2017/10/26
    */
    WordCloudVo getWordCloudCareer(int studentid);
}

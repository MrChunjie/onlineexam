package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Post;
import com.hpe.util.ServerResponse;

import java.util.List;

/**
* @Author:Liuli
* @Description:岗位表业务逻辑
* @Date:20:02 2017/10/9
*/
public interface PostService {

    /**
    * @Author:Liuli
    * @Description:获取全部岗位
    * @Date:11:35 2017/10/9
    */
    List<Post> getAllPost();

    /**
    * @Author:Liuli
    * @Description:获取全部岗位（分页）
    * @Date:14:16 2017/10/9
    */
    ServerResponse<PageInfo> getPosts(int pageNum, int pageSize);


    /**
    * @Author:Liuli
    * @Description:更新岗位名称
    * @Date:20:02 2017/10/9
    */
    ServerResponse<String > updatePost(Post post);

    /**
    * @Author:Liuli
    * @Description:增加岗位
    * @Date:20:04 2017/10/9
    */
    ServerResponse<String > addPost(Post post);

    /**
    * @Author:Liuli
    * @Description:删除岗位
    * @Date:20:11 2017/10/9
    */
    ServerResponse<String> deletePost(int id);
}

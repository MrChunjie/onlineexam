package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Teacher;
import com.hpe.util.ServerResponse;
import com.hpe.vo.TeacherInfo;

import java.util.List;

/**
 * Created by john on 2017/9/23.
 */

/**
 * @Author:XuShengJie
 * @Description:
 * @Date:14:01 2017/9/24
 */
public interface TeacherService {
    /**
     * @Author:XuShengJie
     * @Description:教师登录
     * @Date:14:01 2017/9/24
     */
    ServerResponse<Teacher> login(Teacher teacher);


    /**
     * @Author:XuShengJie
     * @Description:修改密码
     * @Date:19:27 2017/9/25
     */
    public ServerResponse<Teacher> updatePwd(Teacher teacher);


    /**
     * @description 重置密码
     * @author Fujt
     * @date 2017/10/26 11:37
     */
    ServerResponse resetPwd(String id);


    /**
     * @Author:HouPeng
     * @Description: 获取所有老师 使用分页
     * @Date:10:43 2017/9/26
     * @param pageNum  当前页面
     * @param pageSize 每页显示多少条
     */
    ServerResponse<PageInfo<TeacherInfo>> getAllTeacher(Integer pageNum, Integer pageSize);

    /**
     * @Author:HouPeng
     * @Description:根据id获取老师
     * @Date:16:14 2017/9/26
     * @param teacherId 教师id
     * @return 老师个人信息
     */
    ServerResponse<Teacher> getTeacherById(Integer teacherId);

    /**
     * @Author:HouPeng
     * @Description:根据id删除老师
     * @Date:16:14 2017/9/26
     * @param teacherId 教师id
     * @return 响应信息
     */
    ServerResponse deleteById(Integer teacherId);

    /**
     * @Author:HouPeng
     * @Description:更新老师信息
     * @Date:16:14 2017/9/26
     * @param teacher 老师修改后信息
     * @return 响应信息
     */
    ServerResponse updateTeacher(Teacher teacher);

    /**
     * @Author:HouPeng
     * @Description:增加老师信息
     * @Date:16:14 2017/9/26
     * @param teacher 老师新增信息
     * @return 响应信息
     */
    ServerResponse addTeacher(Teacher teacher);

    /**
    * @Author:Liuli
    * @Description:获取所有老师
    * @Date:19:23 2017/10/12
    */
    List<TeacherInfo> getAllTeacher();
}

package com.hpe.service;

import com.hpe.pojo.Admin;
import com.hpe.util.ServerResponse;

/**
 * Created by chunjie on 2017/9/25.
 */
public interface AdminService {

    /**
    * @Author:CuiCaijin
    * @Description: 管理员登录
    * @Date:15:18 2017/9/25
    */
    ServerResponse<Admin> login(Admin admin);

    /**
     * @description 修改密码
     * @author Fujt
     * @date 2017/9/26 10:32
     * @param admin 只有pwd属性有值
     */
    ServerResponse updatePwd(Admin admin);


}

package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Sign;
import com.hpe.pojo.Student;
import com.hpe.util.ServerResponse;
import com.hpe.vo.EnterpriseInfo;
import com.hpe.vo.SignInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;

/**
 * Create By HouPeng on 2017/10/12
 */
public interface SignService {

    /**
     * @Author:HouPeng
     * @Description:根据面向方向类型获取当前学生对应报名企业
     * @Date:14:13 2017/10/12
     * @param:student 当前学生信息
     * @param:pageNum 当前页数
     * @param:pageSize 页面行记录数
     * @return:
     */
    PageInfo<EnterpriseInfo> getAllEnterpriseByType(Student student, Integer pageNum, Integer pageSize);

    /**
     * @Author:HouPeng
     * @Description: 根据学生id获取当前学生报名信息
     * @Date:15:44 2017/10/12
     * @param:
     * @return:
     */
    List<SignInfo> getAllSignInfoByStuId(Student student);

    /**
     * @Author:HouPeng
     * @Description: 企业报名
     * @Date:16:14 2017/10/12
     * @param:student 当前学生信息
     * @param:enterpriseId 企业id
     * @param:postId 岗位id
     * @return:响应信息
     */
    ServerResponse enterpriseSign(Student student, Integer enterpriseId, Integer postId) throws ParseException;

    /**
     *@Author:HouPeng
     *@Description:根据学生id获取报名信息（分页）
     *@Date:21:50 2017/10/23
     *@param:student 当前学生
     *@param:pageNum 当前页数
     *@param:pageSize 页面显示记录数
     *@return:
     */
    ServerResponse<PageInfo<SignInfo>> getAllSignInfoByStuId(Student student, Integer pageNum, Integer pageSize);

    /**
     * @Author:HouPeng
     * @Description:根据企业id获取报名信息 用于打印企业报名学生信息
     * @Date:15:27 2017/10/13
     * @param:enterpriseId 企业id
     * @return: 报名信息集合
     */
    List<SignInfo> signInfoByEnterId(Integer enterpriseId);

    /**
     * @Author:HouPeng
     * @Description:导入企业报名表
     * @Date:22:03 2017/10/15
     * @param:inputStream excel输入流
     * @return:
     */
    ServerResponse inputSignInfo(InputStream inputStream) throws Exception;

    /**
     * @Author:HouPeng
     * @Description:输出企业报名表
     * @Date:22:03 2017/10/15
     * @param:enterpriseId 企业id
     */
    void outputSignInfo(HttpServletRequest request, HttpServletResponse response, Integer enterpriseId) throws IOException, Exception;

    /**
     * @Author:HouPeng
     * @Description:学生报名信息 完善（薪资 城市完善）
     * @Date:19:35 2017/10/16
     * @param:sign 需要完善的信息
     * @return:响应信息
     */
    ServerResponse signInfoComplete(Sign sign);

    /**
     * @param studentId 当前学生id
     * @Author:HouPeng
     * @Description:雷达图学生沟通维度数据计算
     * @Date:10:38 2017/10/23
     * @return: 沟通维度结果
     */
    int getCommunicateValueBuStudentId(Integer studentId);
}

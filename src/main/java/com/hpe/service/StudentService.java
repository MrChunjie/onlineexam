package com.hpe.service;

import com.hpe.pojo.Student;
import com.hpe.pojo.StudentInfo;
import com.hpe.util.ServerResponse;


/**
 * Created by john on 2017/9/23.
 */
public interface StudentService {

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    Student selectByPrimaryKey(Integer id);

    /**
     * 学生登录接口
     * @param name
     * @param pwd
     * @return
     */
    ServerResponse<Student> studentLogin(String name, String pwd);

    /**
     * 更新密码
     * @param student
     * @return
     */
    ServerResponse updatePwd(Student student);


    /**
     * 注册学生
     * @param record
     * @return
     */
    ServerResponse register(StudentInfo record,String pwd);

    /**
     * @description 重置密码
     * @author Fujt
     * @date 2017/10/26 11:56
     */
    ServerResponse resetPwd(String id);
}

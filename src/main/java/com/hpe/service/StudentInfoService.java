package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.StudentInfo;
import com.hpe.util.ServerResponse;
import com.hpe.vo.StudentInfoVo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by chunjie on 2017/9/23.
 */
public interface StudentInfoService {

    /**
     * 更新学生信息
     * @param record
     * @return
     */
    ServerResponse<StudentInfo> updateByPrimaryKeySelective(StudentInfo record);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    StudentInfo selectByKey(Integer id);

    /**
     * @description 登录成功后获取当前用户的真实姓名
     * @author Fujt
     * @date 2017/9/25 15:52
     */
    String findNameByPrimaryKey(Integer id);

    /**
    * @Author:CuiCaijin
    * @Description: 获得所有学生列表
    * @Date:9:32 2017/9/28
    */
    ServerResponse<PageInfo> getStudentList(int pageNum,int pageSize);

    /**
    * @Author:CuiCaijin
    * @Description:获得所有非在读学生列表
    * @Date:19:28 2017/10/18
    */
    ServerResponse<PageInfo> getNotOnlineStudentList(int pageNum,int pageSize);
    /**
    * @Author:CuiCaijin
    * @Description:更新学生班级
    * @Date:20:16 2017/10/8
    */
    ServerResponse<String> updateStudentInfoSpare(int id,int spare);

    /**
    * @Author:CuiCaijin
    * @Description: 更新学生方向
    * @Date:21:25 2017/10/8
    */
    ServerResponse<String> updateStudentInfoDirection(int id,int direction);

    /**
    * @Author:CuiCaijin
    * @Description: 更新学生状态
    * @Date:21:25 2017/10/8
    */
    ServerResponse<String> updateStudentInfoState(int id,int state);

    /**
    * @Author:CuiCaijin
    * @Description:更新学生学号
    * @Date:21:25 2017/10/8
    */
    ServerResponse<String> updateStudentInfoNewId(int id,int newId);

    /**
    * @Author:CuiCaijin
    * @Description:根据id获得学生信息
    * @Date:21:34 2017/10/8
    */
    ServerResponse<StudentInfoVo> getDetailById(int id);

    /**
    * @Author:CuiCaijin
    * @Description:根据id获得部分信息
    * @Date:21:51 2017/10/8
    */
    ServerResponse<StudentInfo> getStudentPartInfo(int id);

    /**
    * @Author:CuiCaijin
    * @Description:模糊查询
    * @Date:21:19 2017/10/8
    */
    ServerResponse<PageInfo> getStudentInfoByCondition(int pageNum,int pageSize,String school,int direction,String spare,String name);

    /*
     * @Author:TangWenhao
     * @Description:查找答卷中所需的班级id
     * @Date:16:19 2017/9/28
     */
    String findClassByPrimaryKey(Integer id);

    /**
    * @Author:CuiCaijin
    * @Description:使当前所有在读的学生毕业
    * @Date:14:21 2017/10/9
    */
    ServerResponse<String> graduationStudent();

    /**
    * @Author:CuiCaijin
    * @Description:更新学生的部分信息
    * @Date:10:35 2017/10/11
    */
    ServerResponse<String> updateStudentPartInfomation(int id,int state,int newId,int direction,String spare);


    /**
     * @description 删除方向时检索是否具有该方向的学生
     * @author Fujt
     * @date 2017/10/20 11:09
     */
    boolean findByDirectionExistOrNot(int direction);

    /**
    * @Author:CuiCaijin
    * @Description:输出学生注册表头
    * @Date:19:02 2017/11/7
    */
    void outputStudentRegisterExcelTitle(HttpServletRequest req, HttpServletResponse resp) throws IOException;

    /**
    * @Author:CuiCaijin
    * @Description:批量注册学生
    * @Date:19:03 2017/11/7
    */
    ServerResponse<String> registerStudentFromExcel(InputStream is) throws Exception;
}

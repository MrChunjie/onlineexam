package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Score;
import com.hpe.util.ServerResponse;
import com.hpe.vo.LSReadOverpapers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.List;

/**
 * @Author:TangWenhao
 * @Description:个人答卷表的业务逻辑
 * @Date:18:54 2017/9/25
 */
public interface ScoreService {


    /**
     * @Author:XuShengJie
     * @Description:流水阅卷-获取所有未改试卷和根据方向和题型查
     * @Date:11:08 2017/10/10
     */
    ServerResponse<PageInfo> lsFindAllPapers( Integer deptName,Integer queType,int pageNum,int pageSize);
    
    /**
     * @Author:XuShengJie
     * @Description:流水阅卷显示选中要改的题目
     * @Date:19:51 2017/10/10
     */
    ServerResponse showUpdate(int sId,int examId);
    /**
     * @Author:XuShengJie
     * @Description:流水改卷批改试卷
     * @Date:22:38 2017/10/10
     */
    Object lsUpdateScore(int examId,int sId,String type,String direct,String score,String bigQuestionId);

    ServerResponse<PageInfo> findAllPapers( int stuclassId, int id,int pageNum,int pageSize);


    /**
    * @Author:TangWenhao
    * @Description:查询下一个该班级、该考卷的学生答卷
    * @Date:10:03 2017/10/11
    */
    Score findNextPaper( int stuClassId, int examId,int teaId);

    /**
     * @Author:XuShengJie
     * @Description:流水阅卷改完一题之后获取下一道题
     * @Date:14:15 2017/10/12
     */
    LSReadOverpapers lsFindNextPaper(Integer deptName,Integer queType,String bigQuestionId,Integer eId);
    /**
     *
     * @param s
     * @return
     */
    ServerResponse insert(Score s);

    /**
     * 修改答卷
     * @param s
     * @return
     */
    ServerResponse updatePagers(Score s);

    /**
     * 根据id查找答卷，注意答卷是双主键。
     * @param sid,examid
     * @return
     */
    Score findById(int sid,int examid);

    /**
    * @Author:TangWenhao
    * @Description:根据courseName分组查询
    * @Date:16:28 2017/9/27
    */
    ServerResponse<PageInfo> findPaperByStudent(int id,String courseName,int pageNum,int pageSize);

    /**
    * @Author:TangWenhao
    * @Description:根据学生信息，查询可考试卷。
    * @Date:20:07 2017/9/28
    */
    List<Score> getPaperByStudentId(int studentId, int testId);

    /**
     * @Author:TangWenhao
     * @Description:插入学生的答卷
     * @Date:17:11 2017/9/26
     */
    ServerResponse insertPapers(Score record);

    /**
     * @Author:TangWenhao
     * @Description:查询学生平时成绩列表
     * @Date:20:07 2017/9/28
     */
    ServerResponse<PageInfo> findUsualScores(int stuClassId, int testId,int sId, int pageNum, int pageSize);

    /**
    * @Author:TangWenhao
    * @Description:查询平均成绩列表
    * @Date:9:48 2017/10/13
    */
    ServerResponse<PageInfo> findAvg(int stuClassId, int teaId, int pageNum, int pageSize);

    /**
    * @Author:TangWenhao
    * @Description: 查询参与该课程下所有考试的，所有学生成绩
    * @Date:11:04 2017/10/16
    */
    ServerResponse<PageInfo> findEvaDetail(int classId, int examId, int pageNum, int pageSize);

    /**
    * @Author:TangWenhao
    * @Description: 输出excel流到客户端
    * @Date:9:17 2017/10/17
    */
    void outputExcelByEva(HttpServletRequest req, HttpServletResponse resp, int classId,int courseId) throws Exception;

    /**
    * @Author:TangWenhao
    * @Description: 某一班级的某一试卷的平时成绩导出
    * @Date:16:30 2017/11/10
    */
    void outputExcelByUsual(HttpServletRequest req, HttpServletResponse resp, int classId,int examId) throws Exception;

    /**
    * @Author:TangWenhao
    * @Description: 查询sId、examId所确定的唯一学生平时成绩
    * @Date:11:29 2017/10/17
    */
    ServerResponse<PageInfo> findUsualByStuExam(int stuClassId,int sId,int examsId,int pageNum, int pageSize);

    /**
    * @Author:TangWenhao
    * @Description: 推算下一个属于该班级、该试卷的学生平时成绩信息
    * @Date:13:53 2017/10/21
    */
    ServerResponse<PageInfo> findNextUsualScores(int stuClassId,int examId,int pageNum, int pageSize);



    /**
     * @Author:XuShengJie
     * @Description:获取需要补考的考试信息
     * @Date:20:04 2017/10/23
     */
    ServerResponse getRestetExamInfo();
    /**
     * @Author:TangWenhao
     * @Description: 实践成绩
     * @Date:10:54 2017/10/23
     */
    Integer findPractice(int sId);

    /**
    * @Author:Liuli
    * @Description:获得某时间段试卷
    * @Date:23:40 2017/10/27
    */
    Integer countScores(String beginTime, String endTime);
    
    /**
     * @Author:XuShengJie
     * @Description:根据大题id找到对应的答卷答案
     * @Date:9:46 2017/11/3
     */
    String getAnsForPaper(int eId , int stuId ,int bigQuestionId);

    /**
    * @Author:TangWenhao
    * @Description: 导入平时成绩
    * @Date:10:41 2017/11/12
    */
    ServerResponse loadinExcel(InputStream inputStream) throws Exception;

    /**
    * @Author:TangWenhao
    * @Description: 查找平时成绩
    * @Date:15:41 2017/11/12
    */
    ServerResponse findUsualByClassAndExam( int classId, int examId);

    /**
    * @Author:TangWenhao
    * @Description: 教务对学生成绩管理
    * @Date:11:00 2017/11/13
    */
    ServerResponse<PageInfo> findStuScore(String stuName,String other,int pageNum, int pageSize);
}

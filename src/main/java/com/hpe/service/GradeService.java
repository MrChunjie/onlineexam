package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Grade;
import com.hpe.util.ServerResponse;

import java.util.List;
import java.util.Map;

/**
 * Created by dell on 2017/9/25.
 */
public interface GradeService {

    /**
     * @Author:Liuli
     * @Description:获取技能等级列表
     * @Date:20:25 2017/9/25
     */
    Map<String, Object> getGrade();

    Grade getGradeById(Integer id);

    /**
     * @param pageNum  当前页面
     * @param pageSize 每页显示多少条
     * @Author:HouPeng
     * @Description: 获取所有课程 使用分页
     * @Date:10:43 2017/9/26
     */
    ServerResponse<PageInfo<Grade>> getAllGrade(Integer pageNum, Integer pageSize);



    /**
     * @description 获得所有的技能
     * @author Fujt
     * @date 2017/10/9 17:03
     */
    ServerResponse<List<Grade>> getAllGrade();


    /**
     * @param gradeId 课程id
     * @return 响应信息
     * @Author:HouPeng
     * @Description:根据id删除课程
     * @Date:16:14 2017/9/26
     */
    ServerResponse deleteById(Integer gradeId);

    /**
     * @param grade 课程修改后信息
     * @return 响应信息
     * @Author:HouPeng
     * @Description:更新课程信息
     * @Date:16:14 2017/9/26
     */
    ServerResponse updateGrade(Grade grade);

    /**
     * @param grade 课程新增信息
     * @return 响应信息
     * @Author:HouPeng
     * @Description:增加课程信息
     * @Date:16:14 2017/9/26
     */
    ServerResponse addGrade(Grade grade);
}

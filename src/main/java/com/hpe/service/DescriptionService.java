package com.hpe.service;

/**
 * Created by dell on 2017/10/24.
 */
public interface DescriptionService {

    /**
    * @Author:Liuli
    * @Description:获取某维度某分数描述
    * @Date:9:40 2017/10/24
    */
    String getDescription(int dimension,int score);
}

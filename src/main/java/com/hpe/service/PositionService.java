package com.hpe.service;

import com.hpe.pojo.Position;
import com.hpe.util.ServerResponse;

import java.util.List;

/**
 * @author Fujt
 * @description
 * @date 10:542017/10/11
 */
public interface PositionService {

    /**
     * @description 检索所有省份
     * @author Fujt
     * @date 2017/10/11 11:23
     */
    ServerResponse<List<String>> selectAllProvince();

    /**
     * @description 根据省份检索城市
     * @author Fujt
     * @date 2017/10/11 11:21
     */
    ServerResponse<List<Position>> selectCityByProvince(String province);

    /**
     *@Author:HouPeng
     *@Description:
     *@Date:15:44 2017/10/16
     *@param:positionId 城市id
     *@return:
     */
    ServerResponse<Position> getPositionById(Integer positionId);
}

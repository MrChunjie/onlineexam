package com.hpe.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hpe.dao.GradeMapper;
import com.hpe.pojo.Grade;
import com.hpe.service.GradeService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dell on 2017/9/25.
 */
@Service
public class GradeServiceImpl implements GradeService{

    @Autowired
    GradeMapper gradeMapper;

    /**
    * @Author:Liuli
    * @Description:获取全部技能
    * @Date:19:31 2017/9/25
    */
    @Override
    public Map<String,Object> getGrade() {
        List<Grade> grades=gradeMapper.selectAllGrade();
        List<String> name = new ArrayList<>();
        List<Integer> grade = new ArrayList<>();
        for(Grade grade1 : grades){
            name.add(grade1.getName());
            grade.add(grade1.getGrade());
        }
        Map<String,Object> map = new java.util.HashMap<String, Object>();
        map.put("name",name);
        map.put("grade",grade);
        return map;
    }

    /**
    * @Author:Liuli
    * @Description:通过主键获取技能
    * @Date:23:14 2017/9/27
    */
    @Override
    public Grade getGradeById(Integer id) {
        return gradeMapper.selectByPrimaryKey(id);
    }



    /**
     * @Author:HouPeng
     * @Description: 获取所有技能 使用分页
     * @Date:10:43 2017/9/26
     * @param pageNum 当前页面
     * @param pageSize 每页显示多少条
     */
    @Override
    public ServerResponse<PageInfo<Grade>> getAllGrade(Integer pageNum, Integer pageSize) {
        // 设置分页信息
        PageHelper.startPage(pageNum, pageSize);
        //从grade表中查询所有技能记录
        List<Grade> list = gradeMapper.selectAllGrade();
        // 取查询结果
        PageInfo<Grade> pageInfo = new PageInfo<>(list);
        //返回成功信息
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * @description 获得所有的技能
     * @author Fujt
     * @date 2017/10/9 17:07
     */
    @Override
    public ServerResponse<List<Grade>> getAllGrade() {
        List<Grade> grades = gradeMapper.selectAllGrade();
        return ServerResponse.createBySuccess(grades);
    }


    @Override
    public ServerResponse deleteById(Integer gradeId) {
        //根据id删除grade表记录
        int deleteResult = gradeMapper.deleteByPrimaryKey(gradeId);
        //判断是否删除成功
        if(deleteResult > 0){
            return ServerResponse.createBySuccessMsg("删除成功");
        }
        return ServerResponse.createByErrorMsg("删除失败");
    }

    @Override
    public ServerResponse updateGrade(Grade grade) {
        if(grade.getName()==null ||"".equals(grade.getName().trim())){
            return ServerResponse.createByErrorMsg("技能名不能为空");
        }
        //根据id获取对应技能
        Grade gradeResult = gradeMapper.selectByPrimaryKey(grade.getId());
        if(!gradeResult.getName().equals(grade.getName())){
            //校验技能名是否已存在
            Grade checkGrade = gradeMapper.selectGradeByName(grade.getName());
            if(checkGrade != null){
                return ServerResponse.createByErrorMsg("技能名已存在，请重新输入");
            }
        }
        //执行修改
        int updateResult = gradeMapper.updateByPrimaryKey(grade);
        //判断是否修改成功
        if(updateResult > 0){
            return ServerResponse.createBySuccessMsg("修改成功");
        }
        return ServerResponse.createByErrorMsg("增加失败");
    }

    @Override
    public ServerResponse addGrade(Grade grade) {
        if(grade.getName()==null ||"".equals(grade.getName().trim())){
            return ServerResponse.createByErrorMsg("技能名不能为空");
        }
        //校验技能名是否已存在
        Grade checkGrade = gradeMapper.selectGradeByName(grade.getName());
        if(checkGrade != null){
            return ServerResponse.createByErrorMsg("技能名已存在，请重新输入");
        }
        //向数据库插入数据
        int insertResult = gradeMapper.insert(grade);
        if(insertResult > 0){
            return ServerResponse.createBySuccessMsg("增加成功");
        }
        return ServerResponse.createByErrorMsg("增加失败");
    }

}

package com.hpe.service.Impl;

import com.hpe.dao.ClassNumMapper;
import com.hpe.pojo.ClassNum;
import com.hpe.service.ClassNumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dell on 2017/10/16.
 */
@Service
public class ClassNumServiceImpl implements ClassNumService{

    @Autowired
    ClassNumMapper classNumMapper;

    @Override
    public int getClassNum(int id) {
        ClassNum classNum=classNumMapper.selectLastClassNumById(id);
        if(classNum == null){
            return 0;
        }
        return classNum.getcNum();
    }

    @Override
    public Map<String, Object> getAllClassNum(int classid) {
        List<ClassNum> classNums=classNumMapper.selectAllClassNum(classid);
        List<String> dates=new ArrayList<>();
        List<Integer> numbers=new ArrayList<>();
        for(ClassNum classNum:classNums){
            dates.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(classNum.getChangeDate()));
            numbers.add(classNum.getcNum());
        }
        Map<String,Object> map = new java.util.HashMap<String, Object>();
        map.put("dates",dates);
        map.put("numbers",numbers);
        return map;
    }
}

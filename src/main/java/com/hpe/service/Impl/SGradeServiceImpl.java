package com.hpe.service.Impl;

import com.hpe.dao.SGradeMapper;
import com.hpe.pojo.SGrade;
import com.hpe.pojo.SGradeKey;
import com.hpe.service.GroupService;
import com.hpe.service.SGradeService;
import com.hpe.vo.GradeVo;
import com.hpe.vo.WordCloudVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dell on 2017/9/25.
 */
@Service
public class SGradeServiceImpl implements SGradeService{

    @Autowired
    SGradeMapper sGradeMapper;

    @Autowired
    private GroupService groupService;

    /**
    * @Author:Liuli
    * @Description:获取学生已选择技能
    * @Date:11:19 2017/9/25
    */
    @Override
    public List<SGrade> getByStudentId(int studentid) {
        return sGradeMapper.selectByStudentId(studentid);
    }

    /**
    * @Author:Liuli
    * @Description:增加学生技能
    * @Date:11:19 2017/9/25
    */
    @Override
    public int addGrade(SGrade sGrade) {
        return sGradeMapper.insertSelective(sGrade);
    }


    /**
     * @description 批量添加数据
     * @author Fujt
     * @date 2017/10/10 11:37
     */
    @Override
    public int addGrades(SGrade[] sGrades) {
        return sGradeMapper.insertGrades(sGrades);
    }

    /**
     * @description 删除一个数据
     * @author Fujt
     * @date 2017/10/10 14:51
     */
    @Override
    public int deleteByPrimaryKey(SGradeKey sGradeKey) {
        return sGradeMapper.deleteByPrimaryKey(sGradeKey);
    }

    @Override
    public int getGradeValueByStudentId(int studentId) {
        List<GradeVo> gradeVo = sGradeMapper.getGradeValueByStudentId(studentId);
        if(gradeVo.size() == 0){
            return 60;
        }
        int value = 0;
        for(GradeVo grade : gradeVo){
            value += grade.getValue() * grade.getGrade();
        }
        //技能只占百分之60
        int gradeResult = (int)((value/gradeVo.size())*0.6);
        int studyResult = (int)(groupService.getMaxRankByStudentId(studentId)*0.4);
        int result = gradeResult + studyResult;
        return result > 60 ? result : 60;
    }

    @Override
    public WordCloudVo getWordCloudCareer(int studentid) {
        return sGradeMapper.getWordCloudCareer(studentid);
    }
}

package com.hpe.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hpe.dao.DirectionMapper;
import com.hpe.pojo.Direction;
import com.hpe.service.DirectionService;
import com.hpe.service.SClassService;
import com.hpe.service.StudentInfoService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dell on 2017/9/28.
 */
@Service
public class DirectionServiceImpl implements DirectionService {

    @Autowired
    DirectionMapper directionMapper;

    @Autowired
    StudentInfoService studentInfoService;

    @Autowired
    SClassService sClassService;

    @Override
    public ServerResponse<String> addDirection(Direction direction) {
        //检索名称是否存在
        if (directionMapper.findByName(direction.getName()) != null) {
            return ServerResponse.createByError();
        }
        int result = directionMapper.insertSelective(direction);
        if (result > 0) {
            return ServerResponse.createBySuccessMsg("添加方向成功");
        }
        return ServerResponse.createByErrorMsg("添加方向失败");
    }

    @Override
    public ServerResponse<String> updateDirectionName(Direction direction) {
        //检索名称是否存在
        if (directionMapper.findByName(direction.getName()) != null) {
            return ServerResponse.createByError();
        }
        int result = directionMapper.updateByPrimaryKeySelective(direction);
        if (result > 0) {
            return ServerResponse.createBySuccessMsg("修改方向成功");
        }
        return ServerResponse.createByErrorMsg("修改方向失败");
    }

    @Override
    public ServerResponse<PageInfo> getDirections(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Direction> directionList = directionMapper.selectAllDirection();
        PageInfo pageInfo = new PageInfo(directionList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse<List<Direction>> getAllDirections() {
        List<Direction> directions = directionMapper.selectAllDirection();
        if (directions == null) {
            return ServerResponse.createByErrorMsg("方向查询失败");
        }
        return ServerResponse.createBySuccess(directions);
    }

    /**
     * @param id 要获取的方向id
     * @description 根据主键获取方向
     * @author Fujt
     * @date 2017/10/10 18:49
     */
    @Override
    public ServerResponse<Direction> getByPrimaryKey(int id) {
        Direction direction = directionMapper.selectByPrimaryKey(id);
        if (direction == null) {
            return ServerResponse.createByErrorMsg("未选择");
        }
        return ServerResponse.createBySuccess(direction);
    }

    /**
     * @description 添加或者更新方向名字时，检索是否存在
     * @author Fujt
     * @date 2017/10/20 11:23
     * @return true 存在 / false 不存在
     */
    @Override
    public boolean findByName(String name) {
        return directionMapper.findByName(name) != null;
    }

    /**
     * @description 根据id删除方向
     * @author Fujt
     * @date 2017/10/20 11:32
     */
    @Override
    public ServerResponse deleteByPrimaryKey(int id) {
        if (sClassService.findByTypeExistOrNot(id)) {
            return ServerResponse.createByErrorMsg("学生班级中存在该方向, 不能删除");
        }
        if (studentInfoService.findByDirectionExistOrNot(id)) {
            return ServerResponse.createByErrorMsg("学生信息中存在该方向, 不能删除");
        }
        int result = directionMapper.deleteByPrimaryKey(id);
        if (result <= 0) {
            return ServerResponse.createByErrorMsg("删除失败");
        }
        return ServerResponse.createBySuccessMsg("删除成功");
    }

    @Override
    public ServerResponse<String> getDirectionNameById(int id) {
        String name = directionMapper.getDirectionNameByDirectionId(id);
        if(name == null){
            return ServerResponse.createBySuccess("未查询到指定的名","未确定");
        }
        return ServerResponse.createBySuccess(name);
    }

    @Override
    public Integer openDirection(Direction direction) {
        direction.setSpare("1");
        Integer integer=directionMapper.updateByPrimaryKeySelective(direction);
        return integer;
    }

    @Override
    public Integer closeDirection(Direction direction) {
        direction.setSpare(null);
        Integer integer=directionMapper.updateByPrimaryKey(direction);
        return integer;
    }

    @Override
    public Direction getDirection(int id) {
        return directionMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Direction> getAllDirectionClass() {
        return directionMapper.getAllOpenedDirection();
    }


}

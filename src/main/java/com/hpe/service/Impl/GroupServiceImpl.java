package com.hpe.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hpe.dao.*;
import com.hpe.pojo.Direction;
import com.hpe.pojo.Group;
import com.hpe.pojo.SClass;
import com.hpe.service.GroupService;
import com.hpe.util.DownloadUtil;
import com.hpe.util.ExcelUtil;
import com.hpe.util.ServerResponse;
import com.hpe.vo.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by chunjie on 2017/10/12.
 */
@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupMapper groupMapper;

    @Autowired
    private ScoreMapper scoreMapper;

    @Autowired
    private SClassMapper sClassMapper;

    @Autowired
    private StudentInfoMapper studentInfoMapper;

    @Autowired
    private DirectionMapper directionMapper;

    @Override
    public ServerResponse<Boolean> ifClassDivide() {
        boolean flag = false;
        int groupId = getGroupId().getData();
        int count = groupMapper.getSpareByGroupId(groupId);
        if (count > 0) {
            flag = true;
        }
        return ServerResponse.createBySuccess(flag);
    }

    @Override
    public ServerResponse<Integer> showStudentCount() {
        int groupId = getGroupId().getData();
        int result = groupMapper.getStudentCountByGroupId(groupId);
        if (result < 0) {
            return ServerResponse.createByErrorMsg("获取考试学生数量失败请重新尝试");
        } else if (result == 0) {
            return ServerResponse.createByErrorMsg("没有已经考试的学生，请检查是否提交考试成绩");
        }
        return ServerResponse.createBySuccess(result);
    }

    /**
     * @description 插入汇总数据
     * @author Fujt
     * @date 2017/10/13 15:40
     */
    @Override
    public ServerResponse findAndInsertGroup(String type, String exams) {
        //查询开发班（测试班）若干次次考试的成绩总和并排序
        String[] examArray = new String[1];
        if(exams.contains(",") && exams.length()>1){
            examArray = exams.split(",");
        }else if(exams.length() > 0){
            examArray[0] = exams;
        }else{
            examArray[0] = "-1";
        }
        List<Group> groups = scoreMapper.findScoreByExamId(type,examArray);
        if (groups.size() <= 0) {
            return ServerResponse.createByErrorMsg("没有任何学生参与!");
        }

        //组装
        int maxResultId = groupMapper.getLastGroupId() + 1;
        for (Group group : groups) {
            group.setGroupId(maxResultId);
            group.setExamIds(exams);
            group.setDirection(Integer.parseInt(type));
        }

        //插入数据
        int result = groupMapper.insertGroups(groups);

        //插入成功则检索所有数据
        if (result > 0) {
            return ServerResponse.createBySuccessMsg("汇总成功");
        } else {
            return ServerResponse.createByErrorMsg("汇总失败!");
        }
    }

    /**
     * @description 获得所有的分组
     * @author Fujt
     * @date 2017/10/13 19:38
     */
    @Override
    public ServerResponse<PageInfo> getAllGroup(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Group> groups = groupMapper.selectGroupGroupByGroupId();
        if (groups.size() <= 0) {
            return ServerResponse.createByErrorMsg("暂时没有任何分组");
        }
        PageInfo pageInfo = new PageInfo(groups);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public void outputExcelByGroupId(HttpServletRequest req, HttpServletResponse resp, int id) throws Exception {
        List<GroupStuVo> groups = groupMapper.selectGroupStuVo(id);

        String[] beanProperty = {"GroupId", "sId", "sName", "ClassName", "DirectionName", "Score1", "Score2", "Rank"};
        String[] colName = {"分组编号", "学生id", "姓名", "班级", "方向", "笔试成绩", "面试成绩", "排名"};
        //1/锁定  0/不锁定
        int[] lock = {1, 1, 1, 1, 1, 1, 0, 1};

        ExcelUtil excelUtil = new ExcelUtil("2007");

        ByteArrayOutputStream arrayOutputStream =
                excelUtil.getExcelStreamWithPointOutProperties(groups.toArray(new GroupStuVo[groups.size()]), beanProperty, colName, "汇总", "", lock);

        new DownloadUtil().download(arrayOutputStream, resp, req, "汇总表-" + id + ".xlsx");

    }

    @Override
    public ServerResponse inputExcel(InputStream inputStream) throws Exception {
        ExcelUtil excelUtil = new ExcelUtil(inputStream, "2007");
        String[] propertyNames = {"GroupId", "sId", "sName", "ClassName", "DirectionName", "Score1", "Score2", "Rank"};
        Class<?>[] propertyTypes = {Integer.class, Integer.class, String.class, String.class, String.class, Double.class, Double.class, Integer.class};
        List<GroupStuVo> groupStuVos = excelUtil.readExcel(propertyNames, propertyTypes, GroupStuVo.class);

        Collections.sort(groupStuVos);

        for (int i = 0; i < groupStuVos.size(); i++) {
            groupStuVos.get(i).setRank(i + 1);
            int result = groupMapper.updateGroupByPrimaryKey(groupStuVos.get(i));
            if (result <= 0) {
                return ServerResponse.createByErrorMsg("导入中途失败, 请重试");
            }

        }

        return ServerResponse.createBySuccessMsg("导入成功");

    }

    @Override
    public ServerResponse<Integer> getStudentCountByGroupId(int groupId) {
        int count = groupMapper.getStudentCountByGroupId(groupId);
        if (count == 0) {
            return ServerResponse.createByErrorMsg("暂无数据");
        }
        return ServerResponse.createBySuccess(count);
    }

    @Override
    public ServerResponse<Integer> getDirectionByGroupId(int groupId) {
        int direction = groupMapper.getDirection(groupId);
        return ServerResponse.createBySuccess(direction);
    }

    @Override
    public void outputDivideCLassResultExcelByGroupId(HttpServletRequest req, HttpServletResponse resp, int groupId) throws Exception {

        List<DivideClassResultVo> result = groupMapper.getDivideClassResult(groupId);

        String[] colName = {"学号", "姓名", "现班级", "总排名", "总分", "面试分数"};

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("sheet1");
        XSSFRow row = sheet.createRow(0);
        XSSFCellStyle style = workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font = workbook.createFont();
        font.setFontName("楷体");
        font.setFontHeightInPoints((short) 13);
        style.setFont(font);
        for (int i = 0; i < colName.length; i++) {
            XSSFCell cell = row.createCell(i);
            cell.setCellValue(colName[i]);
            cell.setCellStyle(style);
        }
        if (result.get(0).getResult() != Collections.EMPTY_MAP) {
            int j = colName.length;
            for (String key : result.get(0).getResult().keySet()) {
                XSSFCell cell = row.createCell(j);
                cell.setCellValue(key);
                cell.setCellStyle(style);
                j++;
            }
        }
        for (int i = 0; i < result.size(); i++) {
            DivideClassResultVo resultVo = result.get(i);
            XSSFRow currentRow = sheet.createRow(i + 1);
            currentRow.createCell(0).setCellValue(resultVo.getNewId());
            currentRow.createCell(1).setCellValue(resultVo.getSname());
            currentRow.createCell(2).setCellValue(resultVo.getClassName());
            currentRow.createCell(3).setCellValue(resultVo.getRank());
            currentRow.createCell(4).setCellValue(resultVo.getAllScore());
            currentRow.createCell(5).setCellValue(resultVo.getFaceScore());
            int k = colName.length;
            for (String value : resultVo.getResult().values()) {
                currentRow.createCell(k).setCellValue(value);
                currentRow.getCell(k).setCellStyle(style);
                k++;
            }
            for (int j = 0; j < colName.length; j++) {
                currentRow.getCell(j).setCellStyle(style);
            }
        }

        for (int i = 0; i < colName.length; i++) {
            sheet.autoSizeColumn(i);
        }

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        workbook.write(os);
        String time = groupMapper.getGroupTimeByGroupId(groupId);
        if (!"".equals(time) || time != null) {
            time = time.replaceAll("-| |:","_");
        }
        new DownloadUtil().download(os, resp, req, time);

    }

    @Override
    public int getMaxRankByStudentId(int studentId) {
        String value = groupMapper.getRankByStudentId(studentId);
        //获得所有在校学生，算出一个排名，尽量大，使成绩显得更好
        int count = studentInfoMapper.getOnSchoolStudentCount();
        int result = 60;
        if (value.length() == 1) {
            //如果一个成绩都没有，就直接返60
            if ("0".equals(value)) {
                return result;
            } else {
                //根据排名算出学习，如果小于60就按60算
                int currentRank = Integer.parseInt(value);
                result = ((1 - currentRank) / count) * 100;
                return result > 60 ? result : 60;
            }
        }
        //拆分找出最小的排名，即最好的
        String[] ranks = value.split(",");
        double good = count;
        for (String rank : ranks) {
            if (Integer.parseInt(rank) < good) {
                good = Integer.parseInt(rank);
            }
        }
        //算出总分（计算公式（1-最好排名/总人数）*100）
        int total = (int) ((1- good/count)*100);

        return total > 60 ? total : 60;
    }

    @Override
    public int getMaxProcessStudentId(int studentId) {
        String value = groupMapper.getRankByStudentId(studentId);
        int count = studentInfoMapper.getOnSchoolStudentCount();
        if (value.length() == 1) {
            getMaxRankByStudentId(studentId);
        }
        String[] result = value.split(",");
        int max = 0;
        for (int i = 0; i < result.length - 1; i++) {
            int currentProcess = Integer.parseInt(result[i]) - Integer.parseInt(result[i+1]);
            if (currentProcess > max) {
                max = currentProcess;
            }
        }
        //防止极限情况，学生一次进步退步都没有
        if (max == 0) {
            return 60;
        }
        int process = (int) ((1-max / (double)count) * 100);
        return process > 60 ? process : 60;
    }


    /**
     * @description all group vo
     * @author Fujt
     * @date 2017/10/14 11:41
     */
    @Override
    public ServerResponse<PageInfo> showAllGroupVo(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        //获取所有的group_id
        List<Group> groups = groupMapper.findAllGroupId();
        //初始化groupVo视图列表
        List<GroupVo> groupVos = new ArrayList<GroupVo>();
        for (Group group : groups) {
            //查询视图并插入到groupVo视图列表中
            GroupVo groupVo = groupMapper.findGroupVo(group.getGroupId());
            groupVos.add(groupVo);
        }
        PageInfo pageInfo = new PageInfo(groups);
        pageInfo.setList(groupVos);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * @Author:CuiCaijin
     * @Description:获得最新groupId
     * @Date:14:55 2017/10/12
     */
    private ServerResponse<Integer> getGroupId() {
        int groupId = groupMapper.getLastGroupId();
        if (groupId <= 0) {
            return ServerResponse.createByErrorMsg("获得最新考试结果失败，请重新尝试");
        }
        return ServerResponse.createBySuccess(groupId);
    }

    @Override
    public void outputStudentInfoAndGradeExcel(HttpServletRequest req, HttpServletResponse resp, String begin, String end) throws IOException {

        List<StudentInfoAndGradeExcelVo> result = groupMapper.getStudentInfoExcel(begin, end);

        String[] colName = {"序号", "慧与班级（前）", "慧与班级", "姓名", "慧与学号", "性别", "民族", "身份证号", "手机号", "家庭详细信息"
                , "父母姓名及联系电话", "宿舍号", "原学校", "学院", "专业", "原学校班级", "原学校学号", "在原校职务", "原学校辅导员电话"
                , "外语水平", "个人特长及获奖情况", "目前意向（选一项）", "重修课名称", "数量", "预计补考时间", "备注"};

        int commentNum = colName.length;

        String head = "2017秋季学员信息表";

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("sheet1");

        //头部标题字体
        Font headFont = workbook.createFont();
        headFont.setFontName("宋体");
        headFont.setBold(true);
        headFont.setFontHeightInPoints((short) 10);

        //头部标题样式
        XSSFCellStyle headTitleStyle = workbook.createCellStyle();
        headTitleStyle.setAlignment(HorizontalAlignment.CENTER);
        headTitleStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headTitleStyle.setFont(headFont);
        headTitleStyle.setLocked(true);

        //头部标题的值组装
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4));
        XSSFRow titleRow = sheet.createRow(0);
        titleRow.setHeight((short) (15.95 * 20));
        XSSFCell titleRowCell = titleRow.createCell(0);
        titleRowCell.setCellStyle(headTitleStyle);
        titleRowCell.setCellValue(head);

        //头部样式1 序号，慧与班级(前) 慧与班级 姓名 学号
        XSSFCellStyle headStyle = workbook.createCellStyle();
        headStyle.setAlignment(HorizontalAlignment.CENTER);
        headStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headStyle.setFont(headFont);
        headStyle.setBorderRight(BorderStyle.THIN);
        headStyle.setBorderLeft(BorderStyle.THIN);
        headStyle.setBorderTop(BorderStyle.THIN);
        headStyle.setLocked(true);
        headStyle.setFillBackgroundColor(IndexedColors.LIME.getIndex());
        headStyle.setFillForegroundColor(IndexedColors.LIME.getIndex());
        headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        //头部样式2，考试内容显示
        XSSFCellStyle examHeadStyle = workbook.createCellStyle();
        examHeadStyle.setAlignment(HorizontalAlignment.CENTER);
        examHeadStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        examHeadStyle.setFont(headFont);
        examHeadStyle.setBorderRight(BorderStyle.THIN);
        examHeadStyle.setBorderLeft(BorderStyle.THIN);
        examHeadStyle.setBorderTop(BorderStyle.THIN);
        examHeadStyle.setLocked(true);
        examHeadStyle.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
        examHeadStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
        examHeadStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);


        //普通单元格样式
        XSSFCellStyle commonStyle = workbook.createCellStyle();
        commonStyle.setAlignment(HorizontalAlignment.CENTER);
        commonStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        Font commonFont = workbook.createFont();
        commonFont.setFontName("宋体");
        commonFont.setFontHeightInPoints((short) 10);
        commonStyle.setFont(commonFont);
        commonStyle.setBorderRight(BorderStyle.THIN);
        commonStyle.setBorderLeft(BorderStyle.THIN);
        commonStyle.setBorderTop(BorderStyle.THIN);
        commonStyle.setBorderBottom(BorderStyle.THIN);

        //创建第二行
        XSSFRow row = sheet.createRow(1);
        row.setHeight((short) (15.95 * 20));

        //创建第三行
        XSSFRow mergeRow = sheet.createRow(2);
        mergeRow.setHeight((short) (46.5 * 20));

        //给1,2行创建cell
        for (int i = 1; i < 3; i++) {
            XSSFRow currentRow = sheet.getRow(i);
            for (int j = 0; j < commentNum; j++) {
                XSSFCell currentCell = currentRow.createCell(j);
                currentCell.setCellStyle(examHeadStyle);
                if (j < colName.length) {
                    sheet.setColumnWidth(j, (int) (colName[j].getBytes().length * 3 * 256));
                    if ("姓名".equals(colName[j])) {
                        sheet.setColumnWidth(j, colName[j].getBytes().length * 4 * 256);
                    }
                    if ("身份证号".equals(colName[j])) {
                        sheet.setColumnWidth(j, colName[j].getBytes().length * 6 * 256);
                    }
                    currentCell.setCellValue(colName[j]);
                }
            }
        }

        for (int i = 0; i < 5; i++) {
            XSSFCell cell = mergeRow.getCell(i);
            cell.setCellStyle(commonStyle);
        }
        //合并前五列的一二行
        for (int i = 0; i < 5; i++) {
            sheet.addMergedRegion(new CellRangeAddress(1, 2, i, i));
        }

        XSSFCell infoCell = row.getCell(5);
        infoCell.setCellValue("个人基本信息");
        infoCell.setCellStyle(headStyle);
        XSSFCell schoolCell = row.getCell(12);
        schoolCell.setCellValue("原学校信息");
        schoolCell.setCellStyle(headStyle);
        XSSFCell reInfoCell = row.getCell(22);
        reInfoCell.setCellValue("重修课信息");
        reInfoCell.setCellStyle(headStyle);

        //合并需要合并的单元格
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 5, 11));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 12, 18));
        sheet.addMergedRegion(new CellRangeAddress(1, 1, 22, 24));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 19, 19));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 20, 20));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 21, 21));
        sheet.addMergedRegion(new CellRangeAddress(1, 2, 25, 25));

        if (result.size() > 0) {
            int col = commentNum;
            for (int i = 0; i < result.get(0).getGrades().size(); i++) {
                sheet.addMergedRegion(new CellRangeAddress(1, 1, col, col + 3));
                col = col + 4;
            }
        }

        sheet.createFreezePane(5, 0, 5, 0);

        if (result.size() > 0) {
            String[] gradeTitle = {"考核成绩","平时成绩",  "总成绩", "补考"};
            int index = 0;
            for (int i = 1; i < 3; i++) {
                XSSFRow xssfRow = sheet.getRow(i);
                for (int j = 26; j < 26 + result.get(0).getGrades().size() * 4; j++) {
                    XSSFCell cell = xssfRow.createCell(j);
                    cell.setCellStyle(headStyle);
                    if (i == 2) {
                        cell.setCellValue(gradeTitle[index++]);
                        if (index == 4) {
                            index = 0;
                        }
                    }
                }
            }
        }
        for (int i = 3; i < 3 + result.size(); i++) {
            StudentInfoAndGradeExcelVo excelVo = result.get(i - 3);
            XSSFRow valueRow = sheet.createRow(i);
            XSSFCell cell1 = valueRow.createCell(0);
            cell1.setCellValue(i - 2);
            XSSFCell cell2 = valueRow.createCell(1);
            cell2.setCellValue(excelVo.getOldClass());
            XSSFCell cell3 = valueRow.createCell(2);
            cell3.setCellValue(excelVo.getClassName());
            XSSFCell cell4 = valueRow.createCell(3);
            cell4.setCellValue(excelVo.getStudentName());
            XSSFCell cell5 = valueRow.createCell(4);
            cell5.setCellValue(excelVo.getNewId());
            XSSFCell cell6 = valueRow.createCell(5);
            cell6.setCellValue(excelVo.getSex());
            XSSFCell cell7 = valueRow.createCell(6);
            cell7.setCellValue(excelVo.getNation());
            XSSFCell cell8 = valueRow.createCell(7);
            cell8.setCellValue(excelVo.getCardNumber());
            XSSFCell cell9 = valueRow.createCell(8);
            cell9.setCellValue(excelVo.getTel());
            XSSFCell cell10 = valueRow.createCell(9);
            cell10.setCellValue(excelVo.getHome());
            XSSFCell cellParent = valueRow.createCell(10);
            cellParent.setCellValue(excelVo.getParent());
            XSSFCell cell11 = valueRow.createCell(11);
            cell11.setCellValue(excelVo.getDorm());
            XSSFCell cell12 = valueRow.createCell(12);
            cell12.setCellValue(excelVo.getSchool());
            XSSFCell cell13 = valueRow.createCell(13);
            cell13.setCellValue(excelVo.getCollege());
            XSSFCell cell14 = valueRow.createCell(14);
            cell14.setCellValue(excelVo.getMajor());
            XSSFCell cell15 = valueRow.createCell(15);
            cell15.setCellValue(excelVo.getSchoolClass());
            XSSFCell cell16 = valueRow.createCell(16);
            cell16.setCellValue(excelVo.getSchoolId());
            XSSFCell cell17 = valueRow.createCell(17);
            cell17.setCellValue(excelVo.getJob());
            XSSFCell cell18 = valueRow.createCell(18);
            cell18.setCellValue(excelVo.getCounselor());
            XSSFCell cell19 = valueRow.createCell(19);
            cell19.setCellValue(excelVo.getEnglish());
            XSSFCell cell20 = valueRow.createCell(20);
            cell20.setCellValue(excelVo.getSkill());
            XSSFCell cell21 = valueRow.createCell(21);
            cell21.setCellValue(excelVo.getStuOrder());
            XSSFCell cell22 = valueRow.createCell(22);
            cell22.setCellValue(excelVo.getRestudyName());
            XSSFCell cell23 = valueRow.createCell(23);
            cell23.setCellValue(excelVo.getRestudyNum());
            XSSFCell cell24 = valueRow.createCell(24);
            cell24.setCellValue(excelVo.getRestudyTime());
            Map<String, String> grades = excelVo.getGrades();
            int j = 26, k = 26;
            for (Map.Entry<String, String> entry : grades.entrySet()) {
                XSSFCell titleCell = row.getCell(j);
                j = j + 4;
                titleCell.setCellValue(entry.getKey());
                String value = entry.getValue();
                if (value != null && value.contains(",")) {
                    String[] values = value.split(",");
                    for (int m = 0; m < values.length; m++) {
                        XSSFCell currentValueCell = valueRow.createCell(k++);
                        currentValueCell.setCellStyle(commonStyle);
                        currentValueCell.setCellValue(values[m]);
                    }
                    valueRow.createCell(k++);
                }else{
                    k = k + 4;
                }
            }
        }
        for (int i = 3; i < 3 + result.size(); i++) {
            XSSFRow row1 = sheet.getRow(i);
            for (int j = 0; j < 25; j++) {
                XSSFCell cell = row1.getCell(j);
                cell.setCellStyle(commonStyle);
            }
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        workbook.write(os);
        workbook.close();
        new DownloadUtil().download(os, resp, req, "学生信息表.xlsx");

    }

    @Override
    public ServerResponse getClassCountByGroupId(UpdateClassVo classVo) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        UpdateClassVo classVo1 = groupMapper.getDirectionAndGroupDateByGroupId(classVo.getGroupId());
        List<String> names = sClassMapper.getClassCountByDirectionAndDate(classVo1.getDirection(),format.format(classVo1.getCreateDate()));
        if(names.size() == 0){
            return ServerResponse.createByErrorMsg("班级都已经被注销");
        }
        Map result = new HashMap();
        result.put("names",names);
        result.put("classVo",classVo1);
        return ServerResponse.createBySuccess("存在未注销班级",result);
    }

    @Override
    public ServerResponse<String> updateClassStateByDate(UpdateClassVo classVo) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        //解除方向班级绑定
        Direction direction = directionMapper.selectByPrimaryKey(classVo.getDirection());
        direction.setSpare(null);
        directionMapper.updateByPrimaryKey(direction);
        SClass sClass = sClassMapper.selectDirectionClass(classVo.getDirection());
        sClass.setSpare(null);
        sClassMapper.updateByPrimaryKey(sClass);
        //注销班级
        int count = sClassMapper.updateClassStateByDirectionAndDate(classVo.getDirection(),format.format(classVo.getCreateDate()));
        if(count == 0){
            return ServerResponse.createByErrorMsg("注销班级失败，请重新尝试");
        }
        return ServerResponse.createBySuccessMsg("注销所有班级成功");
    }

}

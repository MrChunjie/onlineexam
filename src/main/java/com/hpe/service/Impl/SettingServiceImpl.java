package com.hpe.service.Impl;

import com.hpe.dao.SettingsMapper;
import com.hpe.pojo.Settings;
import com.hpe.service.SettingService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by chunjie on 2017/9/25.
 */
@Service
public class SettingServiceImpl implements SettingService{

    @Autowired
    private SettingsMapper settingsMapper;


    /**
     * @Author:HouPeng
     * @Description:获取全局设置参数
     * @Date:18:23 2017/10/22
     * @return:Settings 对象
     */
    @Override
    public ServerResponse<Settings> getSetting() {
        Settings settings = settingsMapper.selectByCondition();
        return ServerResponse.createBySuccess(settings);
    }

    /**
     * @param settings
     * @Author:HouPeng
     * @Description:
     * @Date:18:25 2017/10/22
     * @param:
     * @return:
     */
    @Override
    public ServerResponse updateSetting(Settings settings) {
        int result = settingsMapper.updateSetting(settings);
        if(result >0){
            return ServerResponse.createBySuccessMsg("设置成功");
        }
        return ServerResponse.createByErrorMsg("设置失败");
    }

    @Override
    public ServerResponse getRegisterState() {
        int result = settingsMapper.selectRegisterState();
        return ServerResponse.createBySuccess(result);
    }

    @Override
    public ServerResponse updateRegisterState(int registerState) {
        int result = settingsMapper.updateRegisterState(registerState);
        if (result <= 0) {
            ServerResponse.createByErrorMsg("更新失败");
        }

        if (registerState == 1) {
            return ServerResponse.createBySuccessMsg("开启成功");
        }else {
            return ServerResponse.createBySuccessMsg("关闭成功");
        }

    }
}

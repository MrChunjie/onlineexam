package com.hpe.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hpe.dao.*;
import com.hpe.pojo.BigQuestion;
import com.hpe.pojo.Course;
import com.hpe.pojo.Exam;
import com.hpe.pojo.Questions;
import com.hpe.service.CourseService;
import com.hpe.util.ServerResponse;
import com.hpe.vo.ClassTeacherVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:TangWenhao
 * @Description:
 * @Date:19:02 2017/9/25
 */
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    CourseMapper courseMapper;
    @Autowired
    private BigQuestionMapper bigQuestionMapper;
    @Autowired
    private ExamMapper examMapper;
    @Autowired
    private QuestionsMapper questionsMapper;
    @Autowired
    private ClassTeacherMapper classTeacherMapper;


    @Override
    public Course getCourseById(int id) {
        return courseMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Course> findAllCourse() {
        return courseMapper.getAllCourseOrderById();
    }


    /**
     * @param pageNum  当前页面
     * @param pageSize 每页显示多少条
     * @Author:HouPeng
     * @Description: 获取所有课程 使用分页
     * @Date:10:43 2017/9/26
     */
    @Override
    public ServerResponse<PageInfo<Course>> getAllCourse(Integer pageNum, Integer pageSize) {
        // 设置分页信息
        PageHelper.startPage(pageNum, pageSize);
        //从course表中查询所有课程记录
        List<Course> list = courseMapper.getAllCourseOrderById();
        // 取查询结果
        PageInfo<Course> pageInfo = new PageInfo<>(list);
        //返回成功信息
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse deleteById(Integer courseId) {
        //校验课程是否被引用
        List<BigQuestion> bigQuestions = bigQuestionMapper.findQuestionByTypeCid(courseId, null);
        if(bigQuestions != null && bigQuestions.size()>0){
            return ServerResponse.createByErrorMsg("课程被引用，不能被删除");
        }
        List<Exam> exams = examMapper.getExamIdForCCId(null, courseId);
        if(exams != null && exams.size()>0){
            return ServerResponse.createByErrorMsg("课程被引用，不能被删除");
        }
        List<Questions> questions = questionsMapper.findQuestionByCourseId(courseId);
        if(questions != null && questions.size()>0){
            return ServerResponse.createByErrorMsg("课程被引用，不能被删除");
        }
        List<ClassTeacherVo> classTeacherVos = classTeacherMapper.findByCourseId(courseId);
        if(classTeacherVos != null && classTeacherVos.size()>0){
            return ServerResponse.createByErrorMsg("课程被引用，不能被删除");
        }
        //根据id删除course表记录
        int deleteResult = courseMapper.deleteByPrimaryKey(courseId);
        //判断是否删除成功
        if (deleteResult > 0) {
            return ServerResponse.createBySuccessMsg("删除成功");
        }
        return ServerResponse.createByErrorMsg("删除失败");
    }

    @Override
    public ServerResponse updateCourse(Course course) {
        if (course.getName() == null || "".equals(course.getName().trim())) {
            return ServerResponse.createByErrorMsg("课程名不能为空");
        }
        //根据id获取对应课程
        Course courseResult = courseMapper.selectByPrimaryKey(course.getId());
        if (courseResult.getName().equals(course.getName())) {
            return ServerResponse.createByErrorMsg("课程名已存在，请重新输入");
        }
        //校验课程名是否已存在
        Course checkCourse = courseMapper.selectCourseByName(course.getName());
        if (checkCourse != null) {
            return ServerResponse.createByErrorMsg("课程名已存在，请重新输入");
        }
        //执行修改
        int updateResult = courseMapper.updateByPrimaryKey(course);
        //判断是否修改成功
        if (updateResult > 0) {
            return ServerResponse.createBySuccessMsg("修改成功");
        }
        return ServerResponse.createByErrorMsg("增加失败");
    }

    @Override
    public ServerResponse addCourse(Course course) {
        if (course.getName() == null || "".equals(course.getName().trim())) {
            return ServerResponse.createByErrorMsg("课程名不能为空");
        }
        //校验用户名是否已存在
        Course checkCourse = courseMapper.selectCourseByName(course.getName());
        if (checkCourse != null) {
            return ServerResponse.createByErrorMsg("课程名已存在，请重新输入");
        }
        //向数据库插入数据
        int insertResult = courseMapper.insert(course);
        if (insertResult > 0) {
            return ServerResponse.createBySuccessMsg("增加成功");
        }
        return ServerResponse.createByErrorMsg("增加失败");

    }
}

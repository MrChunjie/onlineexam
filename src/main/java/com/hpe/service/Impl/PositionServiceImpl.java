package com.hpe.service.Impl;

import com.hpe.dao.PositionMapper;
import com.hpe.pojo.Position;
import com.hpe.service.PositionService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Fujt
 * @description
 * @date 10:552017/10/11
 */
@Service
public class PositionServiceImpl implements PositionService {


    @Autowired
    PositionMapper positionMapper;

    /**
     * @description 获取所有的'province'字段
     * @author Fujt
     * @date 2017/10/11 10:55
     */
    @Override
    public ServerResponse<List<String>> selectAllProvince() {
        List<String> provinces = positionMapper.selectAllProvince();
        return ServerResponse.createBySuccess(provinces);
    }

    @Override
    public ServerResponse<List<Position>> selectCityByProvince(String province) {
        List<Position> positions = positionMapper.selectCityByProvince(province);
        if (positions.size() <= 0) {
            return ServerResponse.createByError();
        }
        return ServerResponse.createBySuccess(positions);
    }

    @Override
    public ServerResponse<Position> getPositionById(Integer positionId) {
        Position position = positionMapper.selectByPrimaryKey(positionId);
        return ServerResponse.createBySuccess(position);
    }
}

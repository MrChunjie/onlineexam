package com.hpe.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hpe.dao.EnterPostMapper;
import com.hpe.dao.EnterpriseMapper;
import com.hpe.dao.PositionMapper;
import com.hpe.pojo.EnterPost;
import com.hpe.pojo.Enterprise;
import com.hpe.service.EnterpriseService;
import com.hpe.util.DownloadUtil;
import com.hpe.util.ExcelUtil;
import com.hpe.util.ServerResponse;
import com.hpe.vo.EnterpriseInfo;
import com.hpe.vo.EnterpriseOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.*;

@Service
public class EnterpriseServiceImpl implements EnterpriseService {

    @Autowired
    EnterpriseMapper enterpriseMapper;

    @Autowired
    EnterPostMapper enterPostMapper;

    @Autowired
    PositionMapper positionMapper;


    @Override
    public ServerResponse<PageInfo<EnterpriseInfo>> getAllEnterprise(Integer pageNum, Integer pageSize, int condition, Integer post, String epName) {
        // 设置分页信息
        PageHelper.startPage(pageNum, pageSize);
        //设置查询条件
        Map map = new HashMap();
        map.put("condition",condition);
        map.put("post",post);
        map.put("epName",epName);
        //从enterprise表中查询所有企业记录
        List<EnterpriseInfo> list = enterpriseMapper.getEnterpriseAll(map);
        // 取查询结果
        PageInfo<EnterpriseInfo> pageInfo = new PageInfo<>(list);
        //返回成功信息
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse<PageInfo<EnterpriseInfo>> getInvalidEnterpriseAll(Integer pageNum, Integer pageSize) {
        // 设置分页信息
        PageHelper.startPage(pageNum, pageSize);
        //从enterprise表中查询所有企业记录
        List<EnterpriseInfo> list = enterpriseMapper.getInvalidEnterpriseAll();
        // 取查询结果
        PageInfo<EnterpriseInfo> pageInfo = new PageInfo<>(list);
        //返回成功信息
        return ServerResponse.createBySuccess(pageInfo);
    }


    @Override
    public EnterpriseInfo getEnterpriseById(Integer enterpriseId) {
        //根据enterpriseId查询对应企业信息
        EnterpriseInfo enterprise = enterpriseMapper.getEnterpriseById(enterpriseId);
        return enterprise;
    }

    @Override
    public ServerResponse deleteById(Integer enterpriseId) {
        //根据id删除enterprise表记录
        int deleteResult = enterpriseMapper.deleteByPrimaryKey(enterpriseId);
        //判断是否删除成功
        if(deleteResult > 0){
            return ServerResponse.createBySuccessMsg("删除成功");
        }
        return ServerResponse.createByErrorMsg("删除失败");
    }

    @Override
    public ServerResponse updateEnterprise(Enterprise enterprise, String postIdsStr) throws ParseException {
        //校验企业名
        if(enterprise.getName()==null ||"".equals(enterprise.getName().trim())){
            return ServerResponse.createByErrorMsg("企业名不能为空");
        }
        //根据id获取对应企业
        Enterprise enterpriseResult = enterpriseMapper.selectByPrimaryKey(enterprise.getId());
        //根据报名终止时间校验已过期企业不能修改
        if(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime().after(enterpriseResult.geteTime())){
            return ServerResponse.createByErrorMsg("企业已过期不能修改");
        }

        //校验终止时间不能低于起始时间
        if(enterprise.getsTime().after(enterprise.geteTime())){
            return ServerResponse.createByErrorMsg("终止时间不能低于起始时间");
        }

        //企业岗位enter_post表信息更新 删除后插入
        enterPostMapper.deleteByEnterId(enterprise.getId());
        String[] postIds = postIdsStr.split(",");
        List<EnterPost> list = new ArrayList<>();
        for (String postId:postIds) {
            EnterPost enterPost = new EnterPost();
            enterPost.setEnterId(enterprise.getId());
            enterPost.setPostId(Integer.parseInt(postId));
            list.add(enterPost);
        }
        enterPostMapper.insertByBatch(list);

        //执行企业表修改
        int updateResult = enterpriseMapper.updateByPrimaryKey(enterprise);
        //判断是否修改成功
        if(updateResult > 0){
            return ServerResponse.createBySuccessMsg("修改成功");
        }
        return ServerResponse.createByErrorMsg("增加失败");
    }

    @Override
    public ServerResponse addEnterprise(Enterprise enterprise, String postIdsStr) throws ParseException {
        //校验企业名
        if(enterprise.getName()==null ||"".equals(enterprise.getName().trim())){
            return ServerResponse.createByErrorMsg("企业名不能为空");
        }
        //校验终止时间不能低于起始时间
        if(enterprise.getsTime().after(enterprise.geteTime())){
            return ServerResponse.createByErrorMsg("终止时间不能低于起始时间");
        }

        //向数据库插入数据
        int insertResult = enterpriseMapper.insertResultId(enterprise);

        //批量插入enter_post表数据
        String[] postIds = postIdsStr.split(",");
        List<EnterPost> list = new ArrayList<>();
        for (String postId:postIds) {
            EnterPost enterPost = new EnterPost();
            enterPost.setEnterId(enterprise.getId());
            enterPost.setPostId(Integer.parseInt(postId));
            list.add(enterPost);
        }
        insertResult = enterPostMapper.insertByBatch(list);

        if(insertResult > 0){
            return ServerResponse.createBySuccessMsg("增加成功");
        }
        return ServerResponse.createByErrorMsg("增加失败");
    }

    /**
     *
     * @Author:HouPeng
     * @Description:根据报名起始时间获取企业
     * @Date:16:38 2017/10/17
     * @param:stime
     * @return:
     */
    @Override
    public ServerResponse<PageInfo<EnterpriseInfo>> getEnterpriseInfosByStime(Integer pageNum, Integer pageSize, Date stime) {
        // 设置分页信息
        PageHelper.startPage(pageNum, pageSize);
        //从enterprise表中查询所有企业记录
        List<EnterpriseInfo> list = enterpriseMapper.getEnterpriseInfosByStime(stime);
        // 取查询结果
        PageInfo<EnterpriseInfo> pageInfo = new PageInfo<>(list);
        //返回成功信息
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * @Author:HouPeng
     * @Description: 根据时间导出企业信息
     * @Date:19:31 2017/10/17
     * @param:sTime 起始报名时间
     * @return:
     */
    @Override
    public void outputEnterpriseByStime(HttpServletRequest request, HttpServletResponse response, Date sTime) throws Exception {
        List<EnterpriseOutput> enterpriseOutputs = enterpriseMapper.selectEnterpriseOutputByStime(sTime);
        String[] beanProperty = {"Id","Name","Type","Category","Stime","Etime","Position","Posts","Text"};
        //设置导出表头
        String[] colName = {"企业id","企业名称","企业方向","企业类型","报名起始时间","报名终止时间","所在地","岗位需求","正文"};
        ExcelUtil excelUtil = new ExcelUtil("2007");
        ByteArrayOutputStream arrayOutputStream =
                excelUtil.getExcelStreamWithPointOutProperties(enterpriseOutputs.toArray(new EnterpriseOutput[enterpriseOutputs.size()]), beanProperty, colName, "企业列表", "");

        new DownloadUtil().download(arrayOutputStream, response, request, "企业列表.xlsx");
    }


}

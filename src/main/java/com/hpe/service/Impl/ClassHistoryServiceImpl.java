package com.hpe.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hpe.dao.ClassHistoryMapper;
import com.hpe.pojo.ClassHistory;
import com.hpe.pojo.SClass;
import com.hpe.service.ClassHistoryService;
import com.hpe.service.SClassService;
import com.hpe.util.ServerResponse;
import com.hpe.vo.ClassHistoryVo;
import com.hpe.vo.ClassStuVo;
import com.hpe.vo.StudentCareerClassVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dell on 2017/10/12.
 */
@Service
public class ClassHistoryServiceImpl implements ClassHistoryService{

    @Autowired
    ClassHistoryMapper classHistoryMapper;

    @Autowired
    SClassService sClassService;

    @Override
    public int insert(ClassHistory classHistory) {
        return classHistoryMapper.insertSelective(classHistory);
    }

    @Override
    public List<ClassHistory> getHistoryByStudentId(int id) {
        return classHistoryMapper.selectByStudentId(id);
    }

    @Override
    public Map<String, Object> getHistoryByStuId(int id) {
        List<ClassHistory> classes=classHistoryMapper.selectByStudentId(id);
        List<String> dates=new ArrayList<>();
        List<Integer> classId=new ArrayList<>();
        List<String> classname=new ArrayList<>();
        for(ClassHistory classHistory:classes){
            dates.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(classHistory.getHistoryTime()));
            int i=classHistory.getClassId();
            classId.add(i);
            SClass sClass=sClassService.selectByPrimaryKey(i);
            classname.add(sClass.getName());
        }
        Map<String,Object> map = new java.util.HashMap<String, Object>();
        map.put("dates",dates);
        map.put("classId",classId);
        map.put("classname",classname);
        return map;
    }

    @Override
    public List<ClassHistory> getAllStudent(int id) {
        return classHistoryMapper.getAllStudentByClassId(id);
    }

    @Override
    public ServerResponse<PageInfo> getAllStudentByClassId(int pageNum, int pageSize,int id) {
        PageHelper.startPage(pageNum,pageSize);
        List<ClassHistory> classHistories= classHistoryMapper.getAllStudentByClassId(id);
        PageInfo pageInfo=new PageInfo(classHistories);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse<PageInfo> getStuVoByClassIdAndHistory(int pageNum, int pageSize, int classId, String date) {
        PageHelper.startPage(pageNum, pageSize);
        List<ClassStuVo> classStuVos = classHistoryMapper.getStuVoByClassIdAndHistory(classId, date);
        PageInfo pageInfo = new PageInfo(classStuVos);
        pageInfo.setList(classStuVos);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse<PageInfo> getStuVoByClassIdAndHistoryAndName(int pageNum, int pageSize, int classId, String date,String name) {
        PageHelper.startPage(pageNum, pageSize);
        List<ClassStuVo> classStuVos = classHistoryMapper.getStuVoByClassIdAndHistoryAndName(classId, date, name);
        PageInfo pageInfo = new PageInfo(classStuVos);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public Map<String, Object> getAllStuHistory(int classId) {
        List<ClassHistoryVo> classHistoryVos =classHistoryMapper.getClassStuHistory(classId);
        List<String> strings =new ArrayList<>();
        List<Map> result = new ArrayList<>();
        for(ClassHistoryVo classHistoryVo:classHistoryVos){
            Map map1 = new HashMap<>();
            strings.add(classHistoryVo.getClassname());
            map1.put("name",classHistoryVo.getClassname());
            map1.put("value",classHistoryVo.getClassnumber());
            result.add(map1);
        }
        Map<String,Object> map = new java.util.HashMap<String, Object>();
        map.put("classname",strings);
        map.put("pievalue",result);
        return map;
    }

    @Override
    public List<StudentCareerClassVo> getAllStuClassCareer(int studentId) {
        return classHistoryMapper.getStudentClassCareer(studentId);
    }

}

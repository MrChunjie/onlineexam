package com.hpe.service.Impl;

import com.hpe.dao.DescriptionMapper;
import com.hpe.service.DescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by dell on 2017/10/24.
 */
@Service
public class DescriptionServiceImpl implements DescriptionService{

    @Autowired
    DescriptionMapper descriptionMapper;

    @Override
    public String getDescription(int dimension, int score) {
        return descriptionMapper.selectDescription(dimension,score);
    }
}

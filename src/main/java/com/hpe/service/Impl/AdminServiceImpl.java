package com.hpe.service.Impl;

import com.hpe.dao.AdminMapper;
import com.hpe.pojo.Admin;
import com.hpe.service.AdminService;
import com.hpe.util.ServerResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by chunjie on 2017/9/25.
 */
@Service
public class AdminServiceImpl implements AdminService{

    @Autowired
    private AdminMapper adminMapper;


    @Override
    public ServerResponse<Admin> login(Admin admin) {
        Admin result = adminMapper.adminLogin(admin.getName(),admin.getPwd());
        if(result == null){
            return  ServerResponse.createByErrorMsg("用户名或者密码错误");
        }
        result.setPwd(StringUtils.EMPTY);
        return ServerResponse.createBySuccess("登录成功",result);
    }

    /**
     * @description 修改密码
     * @author Fujt
     * @date 2017/9/26 14:46
     */
    @Override
    public ServerResponse updatePwd(Admin admin) {
        int result = adminMapper.updateByPrimaryKeySelective(admin);
        if (result <= 0) {
            return ServerResponse.createByErrorMsg("修改失败");
        }
        return ServerResponse.createBySuccessMsg("修改成功");
    }

}

package com.hpe.service.Impl;

import com.hpe.dao.ClassHistoryMapper;
import com.hpe.dao.ClassNumMapper;
import com.hpe.dao.StudentInfoMapper;
import com.hpe.dao.StudentMapper;
import com.hpe.pojo.ClassHistory;
import com.hpe.pojo.ClassNum;
import com.hpe.pojo.Student;
import com.hpe.pojo.StudentInfo;
import com.hpe.service.StudentService;
import com.hpe.util.ServerResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


/**
 * Created by john on 2017/9/23.
 */
@Service
public class StudentServiceImpl implements StudentService{

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private StudentInfoMapper studentInfoMapper;

    @Autowired
    private ClassHistoryMapper classHistoryMapper;

    @Autowired
    private ClassNumMapper classNumMapper;

    @Override
    public ServerResponse<Student> studentLogin(String username, String password) {
        Student student = studentMapper.studentLogin(username, password);
        if(student == null) {
            return ServerResponse.createByErrorMsg("用户名或密码错误");
        }
        //把密码设置成空
        student.setsPwd(StringUtils.EMPTY);
        return ServerResponse.createBySuccess("登录成功",student);
    }

    @Override
    public Student selectByPrimaryKey(Integer id) {
        return studentMapper.selectByPrimaryKey(id);
    }

    /**
     * @description 修改密码
     * @author Fujt
     * @date 2017/9/26 11:25
     */
    @Override
    public ServerResponse updatePwd(Student student) {
        //更新
        int result = studentMapper.updateByPrimaryKeySelective(student);
        if (result <= 0) {
            return ServerResponse.createByErrorMsg("修改失败");
        }
        return ServerResponse.createBySuccessMsg("修改成功");
    }

    @Override
    @Transactional
    public ServerResponse register(StudentInfo record,String pwd) {
        //身份证号查重
        if (studentInfoMapper.findNumberExistOrNot(record.getNumber()) != null) {
            return ServerResponse.createByErrorMsg("身份证号重复, 请不要重复注册");
        }
        //状态
        record.setState("1");
        record.setSpare("0");
        //先更新StudentInfo表，获得主键作为Student表的主键
        int result = studentInfoMapper.insert(record);
        if (result <= 0) {
            return ServerResponse.createByErrorMsg("注册失败");
        }
        Student student = new Student();
        student.setId(record.getId());
        //学生登录表密码设置为身份证号后六位
        student.setsPwd(pwd);
        //学生登录表账户为身份证后12位
        String name = record.getNumber().substring(record.getNumber().length()-12);
        student.setsName(name);
        int result1=studentMapper.insert(student);
        if(result1<=0) {
            return ServerResponse.createByErrorMsg("注册失败");
        }
        //设置默认班级人数
        //获取默认班级
        ClassNum classNum = classNumMapper.selectLastClassNumById(0);//默认班级id=0
        //注册时默认班级人数加1
        classNum.setcNum(classNum.getcNum() + 1);
        //设置变更时间
        classNum.setChangeDate(new Date());
        //执行默认班级人数更新
        int result2 = classNumMapper.updateByPrimaryKey(classNum);
        if(result2<=0) {
            return ServerResponse.createByErrorMsg("注册失败");
        }
        //班级历史添加
        ClassHistory classHistory = new ClassHistory();
        //classHistory对象属性赋值
        classHistory.setClassId(0);
        classHistory.setsId(record.getId());
        //学生姓名
        classHistory.setsName(record.getName());
        //添加时间
        classHistory.setHistoryTime(new Date());
        //执行班级历史添加
        int result3 = classHistoryMapper.insertSelective(classHistory);
        if (result3 >0) {
            return ServerResponse.createBySuccess("注册成功");
        }
        return ServerResponse.createByErrorMsg("注册失败");
    }

    /**
     * @description 重置学生密码
     * @author Fujt
     * @date 2017/10/26 11:56
     */
    @Override
    public ServerResponse resetPwd(String id) {
        int result = studentMapper.updatePwdByPk(id);
        if (result <= 0) {
            return ServerResponse.createByErrorMsg("密码重置失败");
        }

        return ServerResponse.createBySuccessMsg("密码重置成功: 123456");
    }
}

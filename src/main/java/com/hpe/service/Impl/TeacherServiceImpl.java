package com.hpe.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hpe.dao.ClassTeacherMapper;
import com.hpe.dao.TeacherMapper;
import com.hpe.pojo.Teacher;
import com.hpe.service.TeacherService;
import com.hpe.util.ServerResponse;
import com.hpe.vo.ClassTeacherVo;
import com.hpe.vo.TeacherInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by john on 2017/9/23.
 */
@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherMapper teacherMapper;
    @Autowired
    private ClassTeacherMapper classTeacherMapper;

    /**
     * @description 修改密码
     * @author Fujt
     * @date 2017/9/26 15:04
     */
    @Override
    public ServerResponse updatePwd(Teacher teacher) {
        int result = teacherMapper.updateByPrimaryKeySelective(teacher);
        if (result <= 0) {
            return ServerResponse.createByErrorMsg("修改失败");
        }
        return ServerResponse.createBySuccessMsg("修改成功");
    }

    @Override
    public ServerResponse resetPwd(String id) {
        int result = teacherMapper.updatePwdByPk(id);
        if (result <= 0) {
            ServerResponse.createByErrorMsg("密码重置失败");
        }
        return ServerResponse.createBySuccessMsg("密码重置成功: 123456");
    }


    @Override
    public ServerResponse<Teacher> login(Teacher teacher) {
        Teacher loginTeacher = teacherMapper.teacherLogin(teacher.getName(),teacher.getPwd());
        if (loginTeacher == null) {
            return ServerResponse.createByErrorMsg("用户名或密码错误");
        }
        //把密码设置为空
        teacher.setPwd(StringUtils.EMPTY);
        return ServerResponse.createBySuccess("登录成功", loginTeacher);
    }


    @Override
    public ServerResponse<PageInfo<TeacherInfo>> getAllTeacher(Integer pageNum, Integer pageSize) {
        // 设置分页信息
        PageHelper.startPage(pageNum, pageSize);
        //从teacher表中查询所有老师记录
        List<TeacherInfo> list = teacherMapper.getTeacherAll();

        // 取查询结果
        PageInfo<TeacherInfo> pageInfo = new PageInfo<>(list);
        //返回成功信息
        return ServerResponse.createBySuccess(pageInfo);
    }


    @Override
    public ServerResponse<Teacher> getTeacherById(Integer teacherId) {
        //根据teacherId查询对应老师信息
        Teacher teacher = teacherMapper.selectByPrimaryKey(teacherId);
        return ServerResponse.createBySuccess(teacher);
    }

    @Override
    public ServerResponse deleteById(Integer teacherId) {
        //校验该老师是否被引用
        List<ClassTeacherVo> classTeacherVos = classTeacherMapper.findByTeacherId(teacherId);
        if(classTeacherVos != null && classTeacherVos.size()>0){
            return ServerResponse.createByErrorMsg("该老师被引用，无法被删除");
        }
        //根据id删除teacher表记录
        int deleteResult = teacherMapper.deleteByPrimaryKey(teacherId);
        //判断是否删除成功
        if(deleteResult > 0){
            return ServerResponse.createBySuccessMsg("删除成功");
        }
        return ServerResponse.createByErrorMsg("删除失败");
    }

    @Override
    public ServerResponse updateTeacher(Teacher teacher) {
        if("".equals(teacher.getName().trim())){
            return ServerResponse.createByErrorMsg("老师名不能为空");
        }
        //根据id获取对应老师
        Teacher teacherResult = teacherMapper.selectByPrimaryKey(teacher.getId());
        if(teacherResult.getName().equals(teacher.getName())){
            return ServerResponse.createByErrorMsg("老师名已存在，请重新输入");
        }
        //校验用户名是否已存在
        Teacher checkTeacher = teacherMapper.selectTeacherByName(teacher.getName());
        if(checkTeacher != null){
            return ServerResponse.createByErrorMsg("老师名已存在，请重新输入");
        }
        //teacher里面属性赋值
        teacher.setPwd(teacherResult.getPwd());
        teacher.setClassId(teacherResult.getClassId());
        //执行修改
        int updateResult = teacherMapper.updateByPrimaryKey(teacher);
        //判断是否修改成功
        if(updateResult > 0){
            return ServerResponse.createBySuccessMsg("修改成功");
        }
        return ServerResponse.createByErrorMsg("增加失败");
    }

    @Override
    public ServerResponse addTeacher(Teacher teacher) {
        if("".equals(teacher.getName().trim())){
            return ServerResponse.createByErrorMsg("老师名不能为空");
        }
        //校验用户名是否已存在
        Teacher checkTeacher = teacherMapper.selectTeacherByName(teacher.getName());
        if(checkTeacher != null){
            return ServerResponse.createByErrorMsg("老师名已存在，请重新输入");
        }
        //初始化密码
        teacher.setPwd("7C4A8D09CA3762AF61E59520943DC26494F8941B");
        //向数据库插入数据
        int insertResult = teacherMapper.insert(teacher);
        if(insertResult > 0){
            return ServerResponse.createBySuccessMsg("增加成功");
        }
        return ServerResponse.createByErrorMsg("增加失败");
    }

    @Override
    public List<TeacherInfo> getAllTeacher() {
        return teacherMapper.getTeacherAll();
    }
}

package com.hpe.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hpe.dao.EnterPostMapper;
import com.hpe.dao.PostMapper;
import com.hpe.pojo.Post;
import com.hpe.service.PostService;
import com.hpe.util.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dell on 2017/10/9.
 */
@Service
public class PostServiceImpl implements PostService {

    @Autowired
    PostMapper postMapper;

    @Autowired
    EnterPostMapper enterPostMapper;

    @Override
    public List<Post> getAllPost() {
        return postMapper.selectAllPost();
    }

    @Override
    public ServerResponse<PageInfo> getPosts(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Post> postList = postMapper.selectAllPost();
        PageInfo pageInfo = new PageInfo(postList);
        return ServerResponse.createBySuccess(pageInfo);
    }


    @Override
    public ServerResponse<String> updatePost(Post post) {
        if (post.getName() == null || "".equals(post.getName().trim())) {
            return ServerResponse.createByErrorMsg("岗位名不能为空");
        }
        //根据id获取对应岗位
        Post postResult = postMapper.selectByPrimaryKey(post.getId());
        if (postResult.getName().equals(post.getName())) {
            return ServerResponse.createByErrorMsg("岗位名已存在，请重新输入");
        }
        //校验用户名是否已存在
        Post checkPost = postMapper.selectPostByName(post.getName());
        if (checkPost != null) {
            return ServerResponse.createByErrorMsg("岗位名已存在，请重新输入");
        }
        //执行修改
        int updateResult = postMapper.updateByPrimaryKey(post);
        //判断是否修改成功
        if (updateResult > 0) {
            return ServerResponse.createBySuccessMsg("修改成功");
        }
        return ServerResponse.createByErrorMsg("增加失败");
    }

    @Override
    public ServerResponse<String> addPost(Post post) {
        if (post.getName() == null || "".equals(post.getName().trim())) {
            return ServerResponse.createByErrorMsg("岗位名不能为空");
        }
        //校验用户名是否已存在
        Post checkPost = postMapper.selectPostByName(post.getName());
        if (checkPost != null) {
            return ServerResponse.createByErrorMsg("岗位名已存在，请重新输入");
        }
        //向数据库插入数据
        int insertResult = postMapper.insert(post);
        if (insertResult > 0) {
            return ServerResponse.createBySuccessMsg("增加成功");
        }
        return ServerResponse.createByErrorMsg("增加失败");
    }

    @Override
    public ServerResponse<String> deletePost(int id) {
        //校验该岗位是否被引用(企业岗位验证-添加企业后删除岗位，已添加的企业岗位为空)
        int count = enterPostMapper.getPostCountByPostId(id);
        if (count > 0) {
            return ServerResponse.createByErrorMsg("该岗位被引用，无法被删除");
        }
        int result = postMapper.deleteByPrimaryKey(id);
        if (result > 0) {
            return ServerResponse.createBySuccessMsg("删除岗位成功");
        }
        return ServerResponse.createByErrorMsg("删除岗位失败");
    }
}

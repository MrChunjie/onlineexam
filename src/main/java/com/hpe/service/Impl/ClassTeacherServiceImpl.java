package com.hpe.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hpe.dao.ClassTeacherMapper;
import com.hpe.pojo.ClassTeacher;
import com.hpe.pojo.Course;
import com.hpe.service.ClassTeacherService;
import com.hpe.util.ServerResponse;
import com.hpe.vo.AverageVo;
import com.hpe.vo.ClassTeacherVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dell on 2017/10/11.
 */
@Service
public class ClassTeacherServiceImpl implements ClassTeacherService{

    @Autowired
    ClassTeacherMapper classTeacherMapper;

    @Override
    public ServerResponse<String> addClassTeacher(ClassTeacher classTeacher) {
        ClassTeacher classTeacherResult = classTeacherMapper.selectByPrimaryKey(classTeacher);
        if (classTeacherResult != null) {
            return ServerResponse.createByErrorMsg("该关系已经存在");
        }

        int result=classTeacherMapper.insertSelective(classTeacher);
        if(result > 0) {
            return ServerResponse.createBySuccessMsg("添加关系成功");
        }
        return ServerResponse.createByErrorMsg("添加关系失败");
    }

    @Override
    public List<ClassTeacher> allClassTeacher() {
        return classTeacherMapper.selectAll();
    }

    @Override
    public ServerResponse<PageInfo> getAllClassTeacher(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<ClassTeacherVo> classTeacherVos=classTeacherMapper.findAll();
        PageInfo pageInfo=new PageInfo(classTeacherVos);
        return ServerResponse.createBySuccess(pageInfo);
    }

    @Override
    public ServerResponse<String> changeClassTeacher(ClassTeacher classTeacher) {
        int result=classTeacherMapper.updateByPrimaryKeySelective(classTeacher);
        if(result >0){
            return ServerResponse.createBySuccessMsg("更新关系成功");
        }
        return ServerResponse.createByErrorMsg("更新关系失败");
    }

    @Override
    public ServerResponse<List<ClassTeacherVo>> getClassTeacherByTeacherId(int teacherId) {
        List<ClassTeacherVo> classTeacherVos=classTeacherMapper.findByTeacherId(teacherId);
        return ServerResponse.createBySuccess(classTeacherVos);
    }

    @Override
    public ServerResponse<List<ClassTeacherVo>> getClassTeacherByClassId(int classId) {
        List<ClassTeacherVo> classTeacherVos=classTeacherMapper.findByClassId(classId);
        return ServerResponse.createBySuccess(classTeacherVos);
    }

    @Override
    public ServerResponse<List<ClassTeacherVo>> getClassTeacherByCourseId(int courseId) {
        List<ClassTeacherVo> classTeacherVos=classTeacherMapper.findByCourseId(courseId);
        return ServerResponse.createBySuccess(classTeacherVos);
    }

    @Override
    public void getClassAvgByCourse(int classId , int courseId) {
        classTeacherMapper.updateAvgByClassidAndCoureseid(classId,courseId);
    }

    @Override
    public boolean judgmentClassOpenCourse(String[] classIds, int courseId) {
        for(int i = 0; i<classIds.length;i++){
            List<Integer> result = classTeacherMapper.selectCourseIdByClassId(Integer.valueOf(classIds[i]));
            if(result == null||result.size()==0){
                return false;
            }
            //判断返回的课程是否包含组卷课程
            if(!result.contains(courseId)){
                return false;
            }
        }
        return true;
    }

    /**
     * @description 根据条件检索数据，条件可为空
     * @author Fujt
     * @date 2017/10/18 9:44
     */
    @Override
    public ServerResponse<PageInfo> findByConditions(int pageSize, int pageNum, String courseId, String teacherId, String classId) {
        PageHelper.startPage(pageNum, pageSize);
        Map<String, String> map = new HashMap<>();
        map.put("courseId", courseId);
        map.put("teacherId", teacherId);
        map.put("classId", classId);
        System.out.println("========="+map.toString());
        List<ClassTeacherVo> classTeacherVos = classTeacherMapper.findByConditions(map);
        if (classTeacherVos.size() <= 0) {
            return ServerResponse.createByErrorMsg("未检索到数据");
        }
        PageInfo pageInfo = new PageInfo(classTeacherVos);
        return ServerResponse.createBySuccess(pageInfo);
    }


    /**
     * @description 根据主键删除数据
     * @author Fujt
     * @date 2017/10/18 16:13
     */
    @Override
    public ServerResponse deleteByPrimaryKey(ClassTeacher classTeacher) {
        int result = classTeacherMapper.deleteByPrimaryKey(classTeacher);
        if (result <= 0) {
            return ServerResponse.createByErrorMsg("取消失败");
        }
        return ServerResponse.createBySuccessMsg("取消成功");
    }

    @Override
    public List<Integer> getCoursesByClassId(Integer classId) {
        return classTeacherMapper.selectCourseIdByClassId(classId);
    }

    @Override
    public List<Course> getCourseByClassId(Integer classId) {
        return classTeacherMapper.selectCoursesByClassId(classId);
    }
}

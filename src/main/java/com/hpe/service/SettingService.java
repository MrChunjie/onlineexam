package com.hpe.service;

import com.hpe.pojo.Settings;
import com.hpe.util.ServerResponse;

/**
 * Created by chunjie on 2017/9/25.
 */
public interface SettingService {


    /**
     *@Author:HouPeng
     *@Description:获取全局设置参数
     *@Date:18:23 2017/10/22
     *@return:Settings 对象
     */
    ServerResponse<Settings> getSetting();

    /**
     *@Author:HouPeng
     *@Description:
     *@Date:18:25 2017/10/22
     *@param:
     *@return:
     */
    ServerResponse updateSetting(Settings settings);

    /**
     * @description 获取注册开关状态
     * @author Fujt
     * @date 2017/10/23 11:42
     */
    ServerResponse getRegisterState();


    /**
     * @description 更新注册状态
     * @author Fujt
     * @date 2017/10/23 11:42
     */
    ServerResponse updateRegisterState(int registerState);
}

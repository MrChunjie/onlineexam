package com.hpe.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.hpe.pojo.BigQuestion;
import com.hpe.pojo.Questions;
import com.hpe.util.ServerResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * @Author:TangWenhao
 * @Description:选择题的业务逻辑
 * @Date:18:56 2017/9/25
 */
public interface QuestionService {
    void insert(Questions q);
    void delete(int id);
    void update(Questions q);
    void insert(BigQuestion bigQue);

    /**
    * @Author:TangWenhao
    * @Description: 查询选择题目
    * @Date:14:22 2017/9/28
    */
    List<Questions> findByIds(String[] ids);

    List<Questions> findByIdsForStu(String[] ids);

    Questions findQuestionById(int id);

    /**
     * @Author:XuShengJie
     * @Description:添加和修改单选题填写题目信息的方法
     * @Date:19:27 2017/9/25
     */
    public ServerResponse<Map<String,Object>> editQuestions(Questions questions);

    /**
     * @Author:XuShengJie
     * @Description:填写完题目信息之后添加和修改题目的操作
     * @Date:23:04 2017/9/25
     */
    public ServerResponse updateQuestion(Questions question);

    /**
     * @Author:XuShengJie
     * @Description:单选题库中删除一条,修改queExit为0
     * @Date:9:42 2017/9/26
     */
    public ServerResponse<Questions> deleteQuestion(Questions question);

    // 根据制定题目个数，随机获取题目。
    List<Questions> findQuestion(int courseId,int num);
    //2017-08-08新增
    List<BigQuestion> findQuestionByType(int courseId, int num, int tpye);

    /**
     * @Author:XuShengJie
     * @Description:显示所有选择题默认显示第一页
     * @Date:10:23 2017/9/26
     */
    ServerResponse<PageInfo> showAllQuestions(String courseName,String questionTitle,int pageNum,int pageSize);

    // 分页查询
    Page findPage(Page page);
    Page findBigPage(Page page);

    /**
     * @Author:XuShengJie
     * @Description:导入选择题题库
     * @Date:16:15 2017/10/23
     */
    public ServerResponse uploadQuestion(InputStream inputStream) throws Exception;


    /**
     * @Author:XuShengJie
     * @Description:Excel导入选择题
     * @Date:17:48 2017/10/25
     */
    ServerResponse loadinExcel(InputStream inputStream,Integer courseId) throws Exception;

    /**
    * @Author:CuiCaijin
    * @Description:导出选择题excel表头
    * @Date:19:36 2017/10/25
    */
    void outputSelectQuestionExcelTitle(HttpServletRequest req, HttpServletResponse resp)throws Exception;
}

package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.ClassTeacher;
import com.hpe.pojo.Course;
import com.hpe.util.ServerResponse;
import com.hpe.vo.ClassTeacherVo;

import java.util.List;

/**
 * Created by dell on 2017/10/11.
 */
public interface ClassTeacherService {

    /**
    * @Author:Liuli
    * @Description:添加老师-课程-班级关系
    * @Date:10:06 2017/10/11
    */
    ServerResponse<String> addClassTeacher(ClassTeacher classTeacher);

    /**
    * @Author:Liuli
    * @Description:获取所有老师-课程-班级关系
    * @Date:10:08 2017/10/11
    */
    List<ClassTeacher> allClassTeacher();

    /**
    * @Author:Liuli
    * @Description:获取所有老师-课程-班级关系（分页）(视图）
    * @Date:10:09 2017/10/11
    */
    ServerResponse<PageInfo> getAllClassTeacher(int pageNum,int pageSize);

    /**
    * @Author:Liuli
    * @Description:更新老师-课程-班级关系
    * @Date:10:10 2017/10/11
    */
    ServerResponse<String> changeClassTeacher(ClassTeacher classTeacher);

    /**
    * @Author:Liuli
    * @Description:按教师id查找关系(视图）
    * @Date:11:07 2017/10/11
    */
    ServerResponse<List<ClassTeacherVo>> getClassTeacherByTeacherId(int teacherId);

    /**
    * @Author:Liuli
    * @Description:按班级id查找关系(视图）
    * @Date:11:07 2017/10/11
    */
    ServerResponse<List<ClassTeacherVo>> getClassTeacherByClassId(int classId);

    /**
    * @Author:Liuli
    * @Description:按课程id查找关系(视图）
    * @Date:11:08 2017/10/11
    */
    ServerResponse<List<ClassTeacherVo>> getClassTeacherByCourseId(int courseId);
    
    /**
     * @Author:XuShengJie
     * @Description:求班级对应课程的平均成绩
     * @Date:10:17 2017/10/15
     */
    void getClassAvgByCourse(int classId ,int courseId);
    
    /**
     * @Author:XuShengJie
     * @Description:判断某班级是否开了某课程
     * @Date:21:07 2017/10/15
     */
    boolean judgmentClassOpenCourse(String[] classIds,int courseId);


    /**
     * @description 根据条件检索数据，条件可为空
     * @author Fujt
     * @date 2017/10/18 9:42
     */
    ServerResponse<PageInfo> findByConditions(int pageSize, int pageNum, String courseId, String teacherId, String classId);


    /**
     * @description 根据主键删除数据
     * @author Fujt
     * @date 2017/10/18 16:12
     */
    ServerResponse deleteByPrimaryKey(ClassTeacher classTeacher);

    /**
     * @Author:XuShengJie
     * @Description:根据班级获取id所有开课id
     * @Date:18:52 2017/10/26
     */
    List<Integer> getCoursesByClassId(Integer classId);
    
    /**
     * @Author:XuShengJie
     * @Description:根据班级id获取所有开课信息
     * @Date:15:57 2017/10/27
     */
    List<Course> getCourseByClassId(Integer classId);
}

package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Course;
import com.hpe.util.ServerResponse;

import java.util.List;

/**
 * @Author:TangWenhao
 * @Description:课程表的业务逻辑
 * @Date:18:59 2017/9/25
 */
public interface CourseService {


    /**
    * @Author:TangWenhao
    * @Description:根据主键获取课程
    * @Date:15:08 2017/9/27
    */
    Course getCourseById(int id);

    /**
    * @Author:Liuli
    * @Description:获取所有课程
    * @Date:19:40 2017/10/12
    */
    List<Course> findAllCourse();

    /**
     *@Author:HouPeng
     *@Description:获取课程分页信息
     *@Date:19:36 2017/9/27
     * @param pageNum 当前页
     * @param pageSize 页面显示记录数量
     * @return
     */
    ServerResponse<PageInfo<Course>> getAllCourse(Integer pageNum, Integer pageSize);

    /**
     *@Author:HouPeng
     *@Description: 修改课程信息
     *@Date:19:37 2017/9/27
     *@param course 课程修改后信息
     *@return 响应信息
     */
    ServerResponse updateCourse(Course course);

    /**
     *@Author:HouPeng
     *@Description: 根据id删除课程
     *@Date:19:38 2017/9/27
     * @param courseId 课程id
     * @return 响应信息
     */
    ServerResponse deleteById(Integer courseId);

    /**
     *@Author:HouPeng
     *@Description: 增加课程
     *@Date:19:38 2017/9/27
     * @param course 课程新增信息
     * @return
     */
    ServerResponse addCourse(Course course);
}

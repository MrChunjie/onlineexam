package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Course;
import com.hpe.pojo.Exam;
import com.hpe.pojo.SClass;
import com.hpe.util.ServerResponse;
import com.hpe.vo.ExamInFo;
import com.hpe.vo.ResetExamInfoVo;
import com.itextpdf.text.DocumentException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @Author:TangWenhao
 * @Description:试题表的业务逻辑
 * @Date:18:55 2017/9/25
 */
public interface ExamService {

    /**
     * @Author:TangWenhao
     * @Description:Exam主键查询
     * @Date:20:00 2017/9/25
     */
    ServerResponse<Exam> selectByPrimaryKey(Integer id);

    /**
    * @Author:TangWenhao
    * @Description:通过学生ID查询 已发布的试卷。chorming
    * @Date:11:19 2017/9/26
    */
    ServerResponse<PageInfo> findListByStuId(int id,int pageNum,int pageSize);

    /**
     * @Author:XuShengJie
     * @Description:获取可以组卷的班级和课程
     * @Date:15:24 2017/9/27
     */
    ServerResponse<List<Course>> getCourse();

    /**
     * @Author:XuShengJie
     * @Description:准备组卷
     * @Date:15:53 2017/9/27
     */
    ServerResponse<Map<String,Object>> readyAddExam(ExamInFo examInFo,String questions,Integer queType);

    /**
     * @Author:XuShengJie
     * @Description:发布试卷
     * @Date:9:35 2017/9/28
     */
    void addExam(Exam exam,Integer examId,String stuId) throws ParseException;

    /**
     * @Author:XuShengJie
     * @Description:根据老师id查询曾经发布过的试卷
     * @Date:10:18 2017/9/28
     */
    ServerResponse<PageInfo> showMyCommitExam(int teacherId,int pageNum,int pageSize);

    /**
     * @description 检索所有开发班级或者测试班级的考试
     * @author Fujt
     * @date 2017/10/12 15:31
     * @param classIds 所有的开发班级或者测试班级
     */
    ServerResponse<List<Exam>> getExamByClassIds(String classIds);


    /**
     * @description 根据班级类型获取班级
     * @author Fujt
     * @date 2017/10/12 16:32
     */
    ServerResponse<List<SClass>> getSClassByType(int type);


    /**
     * @description 根据 exam ids 获取exam
     * @author Fujt
     * @date 2017/10/13 21:31
     */
    ServerResponse<List<Exam>> getExamByIds(String[] ids);

    /**
    * @Author:TangWenhao
    * @Description:教务查询课程的平均成绩进行成绩评估
    * @Date:10:24 2017/10/14
    */
    ServerResponse<PageInfo> findCourseAvgByEdu(int pageNum, int pageSize);
    
    /**
     * @Author:XuShengJie
     * @Description:获取试卷的平均成绩
     * @Date:11:31 2017/10/15
     */
    void getExamAvg(int classId, int courseId);

    /**
    * @Author:TangWenhao
    * @Description:教务查询班级的平均成绩进行成绩评估
    * @Date:10:29 2017/10/15
    */
    ServerResponse<PageInfo> findAllExamByClassId(int classId,int pageNum, int pageSize);
    
    /**
     * @Author:XuShengJie
     * @Description:出卷之后生成相关考试班级所有人的初始化值
     * @Date:19:35 2017/10/17
     */
    void initScoreAfterExam(Exam exam);

    /**
     * @Author:XuShengJie
     * @Description:补考组卷
     * @Date:10:33 2017/10/24
     */
    ServerResponse<Map<String,Object>> addResetExam(ResetExamInfoVo resetExamInfoVo);
    
    /**
     * @Author:XuShengJie
     * @Description:补考初始化答卷
     * @Date:16:09 2017/10/24
     */
    public void  initScoreAfterResetExam(Exam exam,String[] classId,String[] stuId);


    /**
     * @Author:TangWenhao
     * @Description: 完善信息，得到班级之后 插入该班级待考试试卷
     * @Date:9:46 2017/11/22
     */
    void findExamByClassId(int classId,int stuId);

    /**
     * @description 获得所有的考试
     * @author Fujt
     * @date 2017/10/23 18:51
     */
    ServerResponse<PageInfo> findAllExam(int pageNum, int pageSize);


    /**
     * @description 根据考试id导出所有考生答案pdf
     * @author Fujt
     * @date 2017/10/23 20:00
     */
    ServerResponse exportPdf(HttpServletRequest request, HttpServletResponse response, int examId) throws IOException, DocumentException;
}

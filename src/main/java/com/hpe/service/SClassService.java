package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.SClass;
import com.hpe.util.ServerResponse;
import com.hpe.vo.ClassDirectionVo;
import com.hpe.vo.ClassStuVo;
import com.hpe.vo.DivideClassVo;

import java.util.List;

/**
 * @Author:TangWenhao
 * @Description:班级表的业务逻辑
 * @Date:19:01 2017/9/25
 */
public interface SClassService {

    /**
    * @Author:CuiCaijin
    * @Description: 添加班级
    * @Date:11:11 2017/9/26
    */
    ServerResponse<String> saveClass(SClass stu);

    /**
    * @Author:CuiCaijin
    * @Description:根据状态分页查询班级
    * @Date:11:12 2017/9/26
    */
    ServerResponse<PageInfo> getClassesByState(int state,int pageNum,int pageSize);

    /**
     * @Author:XuShengJie
     * @Description:查询所有未注销班级
     * @Date:11:08 2017/11/13
     */
    List<ClassDirectionVo> getCurrentClasss();
    /**
     * @Author:XuShengJie
     * @Description:获取所有班级的成绩信息并进行分页
     * @Date:15:26 2017/11/12
     */
    ServerResponse<PageInfo> getAllClassScore(String className,String teacherName,String courseName, int pageNum,int pageSize);
    /**
     * @Author:CuiCaijin
     * @Description:根据状态分页查询班级
     * @Date:11:12 2017/9/26
     */
    ServerResponse<PageInfo> getClassesByTeacher(int teaId,int state,int pageNum,int pageSize);

    /**
    * @Author:CuiCaijin
    * @Description:更新班级备注
    * @Date:16:26 2017/9/26
    */
    ServerResponse<String> updateClassOther(SClass sClass);

    /**
    * @Author:CuiCaijin
    * @Description: 更新班级状态
    * @Date:20:21 2017/9/26
    */
    ServerResponse<String> updateClassState(SClass sClass);

    /**
    * @Author:CuiCaijin
    * @Description: 获得当前所有班级
    * @Date:9:43 2017/10/11
    */
    ServerResponse<List<SClass>> getClasses(int type);

    /**
     * @Author:CuiCaijin
     * @Description: 获得当前所有班级
     * @Date:9:43 2017/10/11
     */
    ServerResponse<List<SClass>> getClasses();

    /**
    * @Author:TangWenhao
    * @Description:根据学生ID查找班级
    * @Date:15:07 2017/9/27
    */
    SClass findStuClassById(int id);

    /**
    * @Author:TangWenhao
    * @Description:根据班级主键查询
    * @Date:17:36 2017/10/10
    */
    SClass selectByPrimaryKey(Integer id);

    /**
    * @Author:Liuli
    * @Description:获取某种方向的在读班级列表
    * @Date:14:39 2017/10/12
    */
    List<SClass> selectByType(Integer type);

    /**
    * @Author:CuiCaijin
    * @Description:获取某种方向的在读班级列表
    * @Date:14:35 2017/10/16
    */
    ServerResponse<List<SClass>> getSClassListByType(Integer type);

    /**
    * @Author:CuiCaijin
    * @Description:s曲线分班
    * @Date:16:54 2017/10/16
    */
    ServerResponse<String> divideClassBySCurve(String classIds,int groupId);

    /**
    * @Author:CuiCaijin
    * @Description:三段式分班
    * @Date:20:34 2017/10/16
    */
    ServerResponse<String> divideClassByThree(DivideClassVo divideClassVo);


    /**
     *@Author:HouPeng
     *@Description: 根据班级id获取当前班级的所有学生及新班级
     *@Date:14:56 2017/10/17
     *@param:classId 班级id
     *@param:pageNum 当前页
     *@param:pageSize 页面行记录数
     *@return:
     */
    ServerResponse<PageInfo<ClassStuVo>> getAllStuVoByClassId(Integer classId, Integer pageNum, Integer pageSize);

    /**
     * @description 删除方向时检索是否有该方向的班级存在
     * @author Fujt
     * @date 2017/10/20 11:17
     */
    boolean findByTypeExistOrNot(int type);

    /**
    * @Author:Liuli
    * @Description:将班级关联到方向
    * @Date:10:41 2017/11/15
    */
    Integer addClassDirection(SClass sClass);

    /**
    * @Author:Liuli
    * @Description:将班级解除关联
    * @Date:10:43 2017/11/15
    */
    Integer deleteClassDirection(SClass sClass);

    /**
    * @Author:Liuli
    * @Description:获取班级
    * @Date:10:58 2017/11/15
    */
    SClass getSClass(int id);

    /**
    * @Author:Liuli
    * @Description:获取某方向关联班级
    * @Date:11:02 2017/11/15
    */
    SClass getDirectionClass(int id);
}

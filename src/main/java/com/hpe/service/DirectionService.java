package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Direction;
import com.hpe.util.ServerResponse;

import java.util.List;

/**
* @Author:Liuli
* @Description:方向表业务逻辑
* @Date:10:02 2017/9/28
*/
public interface DirectionService {

    /**
    * @Author:Liuli
    * @Description:增加方向
    * @Date:10:05 2017/9/28
    */
    ServerResponse<String> addDirection(Direction direction);

    /**
    * @Author:Liuli
    * @Description:更改方向名称
    * @Date:10:05 2017/9/28
    */
    ServerResponse<String > updateDirectionName(Direction direction);

    /**
    * @Author:Liuli
    * @Description:分页
    * @Date:10:21 2017/9/28
    */
    ServerResponse<PageInfo> getDirections(int pageNum,int pageSize);

    /**
     * @description 获取所有的方向
     * @author Fujt
     * @date 2017/9/30 14:19
     */
    ServerResponse<List<Direction>> getAllDirections();


    /**
     * @description 根据主键获取方向
     * @author Fujt
     * @date 2017/10/10 18:49
     */
    ServerResponse<Direction> getByPrimaryKey(int id);

    /**
     * @description 添加或者更新方向名字时，检索是否存在
     * @author Fujt
     * @date 2017/10/20 11:22
     */
    boolean findByName(String name);

    /**
     * @description 根据id删除方向
     * @author Fujt
     * @date 2017/10/20 11:32
     */
    ServerResponse deleteByPrimaryKey(int id);

    /**
    * @Author:CuiCaijin
    * @Description:根据主键获得用户id
    * @Date:20:26 2017/10/24
    */
    ServerResponse<String> getDirectionNameById(int id);

    /**
    * @Author:Liuli
    * @Description:开启方向选择
    * @Date:10:47 2017/11/15
    */
    Integer openDirection(Direction direction);

    /**
    * @Author:Liuli
    * @Description:关闭方向选择
    * @Date:10:47 2017/11/15
    */
    Integer closeDirection(Direction direction);

    /**
    * @Author:Liuli
    * @Description:通过主键查询方向
    * @Date:10:55 2017/11/15
    */
    Direction getDirection(int id);

    /**
    * @Author:Liuli
    * @Description:获取所有开启的方向
    * @Date:11:20 2017/11/15
    */
    List<Direction> getAllDirectionClass();
}

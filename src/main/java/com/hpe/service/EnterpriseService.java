package com.hpe.service;

import com.github.pagehelper.PageInfo;
import com.hpe.pojo.Enterprise;
import com.hpe.util.ServerResponse;
import com.hpe.vo.EnterpriseInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.Date;

/**
 * Create By HouPeng on 2017/10/10
 */
public interface EnterpriseService {

    /**
     * @Author:HouPeng
     * @Description: 获取报名未失效所有企业 使用分页
     * @Date:10:43 2017/10/11
     * @param pageNum  当前页面
     * @param pageSize 每页显示多少条
     * @param condition
     * @param post
     * @param epName
     */
    ServerResponse<PageInfo<EnterpriseInfo>> getAllEnterprise(Integer pageNum, Integer pageSize, int condition, Integer post, String epName);
    /**
     *@Author:HouPeng
     *@Description:获取报名失效所有企业 使用分页
     *@Date:21:02 2017/10/11
     *@param pageNum  当前页面
     *@param pageSize 每页显示多少条
     */
    ServerResponse<PageInfo<EnterpriseInfo>> getInvalidEnterpriseAll(Integer pageNum, Integer pageSize);

    /**
     * @Author:HouPeng
     * @Description:根据id获取企业
     * @Date:16:14 2017/10/11
     * @param enterpriseId 教师id
     * @return 企业个人信息
     */
    EnterpriseInfo getEnterpriseById(Integer enterpriseId);

    /**
     * @Author:HouPeng
     * @Description:根据id删除企业
     * @Date:16:14 2017/10/11
     * @param enterpriseId 教师id
     * @return 响应信息
     */
    ServerResponse deleteById(Integer enterpriseId);

    /**
     * @Author:HouPeng
     * @Description:更新企业信息
     * @Date:16:14 2017/10/11
     * @param enterprise 企业修改后信息
     * @param postIds
     * @return 响应信息
     */
    ServerResponse updateEnterprise(Enterprise enterprise, String postIds) throws ParseException;

    /**
     * @Author:HouPeng
     * @Description:增加企业信息
     * @Date:16:14 2017/10/11
     * @param enterprise 企业新增信息
     * @return 响应信息
     */
    ServerResponse addEnterprise(Enterprise enterprise, String postIdsStr) throws ParseException;

    /**
     *@Author:HouPeng
     *@Description:根据报名起始时间获取企业
     *@Date:16:38 2017/10/17
     *@param:
     *@return:
     */
    ServerResponse<PageInfo<EnterpriseInfo>> getEnterpriseInfosByStime(Integer pageNum, Integer pageSize, Date stime);

    /**
     *@Author:HouPeng
     *@Description: 根据时间导出企业信息
     *@Date:19:31 2017/10/17
     *@param:sTime 起始报名时间
     *@return:
     */
    void outputEnterpriseByStime(HttpServletRequest request, HttpServletResponse response, Date sTime) throws Exception;
}

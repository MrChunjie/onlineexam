package com.hpe.dao;

import com.hpe.pojo.EnterPost;

import java.util.List;

public interface EnterPostMapper {
    int deleteByPrimaryKey(EnterPost key);

    int insert(EnterPost record);

    int insertSelective(EnterPost record);

    /**
     *@Author:HouPeng
     *@Description: 批量插入
     *@Date:18:58 2017/10/11
     *@param:enterPosts 企业岗位信息
     *@return:
     */
    int insertByBatch(List<EnterPost> enterPosts);

    /**
     *@Author:HouPeng
     *@Description: 根据企业id删除
     *@Date:18:56 2017/10/11
     *@param: enterpriseId 企业表id
     *@return:
     */
    int deleteByEnterId(Integer enterpriseId);

    /**
     *@Author:HouPeng
     *@Description:根据岗位id查询记录数
     *@Date:9:58 2017/11/13
     *@param:postId 岗位id
     *@return:记录数
     */
    int getPostCountByPostId(Integer postId);

}
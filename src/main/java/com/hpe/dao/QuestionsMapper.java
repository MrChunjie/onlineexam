package com.hpe.dao;

import com.hpe.pojo.BigQuestion;
import com.hpe.pojo.Questions;
import com.hpe.vo.QuestionVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

import java.util.List;

public interface QuestionsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Questions record);

    int insertSelective(Questions record);

    Questions selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Questions record);

    int updateByPrimaryKey(Questions record);

    List<QuestionVo> showAllQuestions(@Param("courseName")String courseName,@Param("questionTitle")String questionTitle);

    /**
     * @Author:TangWenhao
     * @Description:根据选择题的ids查找选择题干和选项
     * @Date:11:40 2017/9/26
     */
    Questions findByIds(@Param("id") String id);

    /**
     * @Author:XuShengJie
     * @Description:根据课程Id出题
     * @Date:16:39 2017/9/27
     */
    List<Questions> findQuestionByCourseId(int courseId);
    /**
     * @Author:XuShengJie
     * @Description:根据课程id,查找除了一部分id的题目
     * @Date:17:28 2017/11/3
     */
    List<Questions> findQuestionByCourseId2(@Param("courseId") int courseId,@Param("questionsid") String[] questionids);
}
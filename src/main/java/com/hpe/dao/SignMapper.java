package com.hpe.dao;

import com.hpe.pojo.Sign;
import com.hpe.pojo.SignKey;
import com.hpe.vo.SignInfo;
import com.hpe.vo.SignVo;

import java.util.List;

public interface SignMapper {
    int deleteByPrimaryKey(SignKey key);

    int insert(Sign record);

    int insertSelective(Sign record);

    Sign selectByPrimaryKey(SignKey key);

    /**
     *@Author:HouPeng
     *@Description:根据学生id查询已报名企业
     *@Date:15:20 2017/10/12
     *@param:sId 学生id
     *@return: 报名信息
     */
    List<Sign> selectByStudentId(int sId);

    List<SignInfo> selectSignInfoByStudentId(int sId);

    int updateByPrimaryKeySelective(Sign record);

    int updateByPrimaryKey(Sign record);
    /**
     *@Author:HouPeng
     *@Description: 根据企业id查询已报名学生信息
     *@Date:15:38 2017/10/13
     *@param:enterpriseId 企业id
     *@return:
     */
    List<Sign> selectByEnterId(Integer enterpriseId);

    /**
     *@Author:HouPeng
     *@Description: 根据企业id查询已报名学生信息SignInfo
     *@Date:15:38 2017/10/13
     *@param:enterpriseId 企业id
     *@return:
     */
    List<SignInfo> selectSignInfoByEnterpriseId(Integer enterpriseId);

    /**
     *@Author:HouPeng
     *@Description:根据企业id查询已报名学生信息SignVo
     *@Date:11:19 2017/10/18
     *@param:enterpriseId 企业id
     *@return:
     */
    List<SignVo> selectSignVoByEnterpriseId(Integer enterpriseId);
}
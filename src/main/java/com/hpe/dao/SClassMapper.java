package com.hpe.dao;

import com.hpe.pojo.SClass;
import com.hpe.vo.ClassDirectionVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SClassMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SClass record);

    int insertSelective(SClass record);
    /**
     *@Author:HouPeng
     *@Description:添加学生班级信息 返回学生班级Id
     *@Date:9:17 2017/10/17
     *@param:record 学生班级信息
     *@return:
     */
    int insertSelectiveReturnId(SClass record);

    SClass selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SClass record);

    int updateByPrimaryKey(SClass record);

    /**
    * @Author:CuiCaijin
    * @Descriptio:获得所有班级
    * @Date:16:44 2017/9/28
    */
    List<SClass> getClasses(@Param("state") int state,@Param("type")int type);


    /**
     * @Author:CuiCaijin
     * @Descriptio:获得所有班级
     * @Date:16:44 2017/9/28
     */
    List<SClass> getClassesNotType(@Param("state") int state);

    /**
    * @Author:CuiCaijin
    * @Description:获得所有班级的vo
    * @Date:21:11 2017/10/24
    */
    List<ClassDirectionVo> getClassVos(@Param("state")int state);
    /**
    * @Author:TangWenhao
    * @Description:获取该老师所教学的班级list
    * @Date:19:49 2017/10/10
    */
    List<SClass> getClassesByTeacher(@Param("teaId") int teaId,@Param("state") int state);
    
    /**
    * @Author:CuiCaijin
    * @Description:更新班级备注
    * @Date:16:45 2017/9/28
    */
    int updateOther(@Param("other")String other,@Param("id")int id);

    /**
    * @Author:CuiCaijin
    * @Description:更新班级状态
    * @Date:16:45 2017/9/28
    */
    int updateState(@Param("state")int state,@Param("id")int id);

    SClass findStuClassById(@Param("id") int id);

    /**
    * @Author:CuiCaijin
    * @Description:判断备注是否存在
    * @Date:16:45 2017/9/28
    */
    int ifExistOther(@Param("state")int state,@Param("other")String other);

    /**
    * @Author:Liuli
    * @Description:获取某种方向的在读班级列表
    * @Date:14:38 2017/10/12
    */
    List<SClass> getClassListByType(int type);

    /**
    * @Author:CuiCaijin
    * @Description:通过方向获得classId和className
    * @Date:14:39 2017/10/16
    */
    List<SClass> getClassIdAndNameByType(int type);
    
    /**
     * @Author:XuShengJie
     * @Description:根据多个主键id返回班级的list
     * @Date:10:07 2017/10/17
     */
    List<SClass> getListForIds(@Param("ids") String[] ids);


    /**
     * @description 删除方向时检索是否有该方向的班级存在
     * @author Fujt
     * @date 2017/10/20 11:17
     */
    SClass findByTypeExistOrNot(int type);

    /**
    * @Author:CuiCaijin
    * @Description:根据方向和日期获得班级数量
    * @Date:22:00 2017/11/8
    */
    List<String> getClassCountByDirectionAndDate(@Param("direction")int direction, @Param("date")String date);

    /**
    * @Author:CuiCaijin
    * @Description:根据班级方向和日期更新班级状态
    * @Date:22:01 2017/11/8
    */
    int updateClassStateByDirectionAndDate(@Param("direction")int direction, @Param("date")String date);

    /**
    * @Author:Liuli
    * @Description:获取该方向关联班级
    * @Date:11:02 2017/11/15
    */
    SClass selectDirectionClass(int id);
}
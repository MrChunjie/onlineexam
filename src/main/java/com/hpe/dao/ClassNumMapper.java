package com.hpe.dao;

import com.hpe.pojo.ClassNum;
import com.hpe.pojo.ClassNumKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ClassNumMapper {
    int deleteByPrimaryKey(ClassNumKey key);

    int insert(ClassNum record);

    int insertSelective(ClassNum record);

    ClassNum selectByPrimaryKey(ClassNumKey key);

    int updateByPrimaryKeySelective(ClassNum record);

    int updateByPrimaryKey(ClassNum record);

    int insertMap(@Param("class_nums") Map<Integer,Integer> classNums);

    /**
    * @Author:Liuli
    * @Description:获取该班级最后一条人数统计数据
    * @Date:23:24 2017/10/16
    */
    ClassNum selectLastClassNumById(int classId);

    /**
    * @Author:Liuli
    * @Description:获取该班级所有历史人数
    * @Date:8:40 2017/10/18
    */
    List<ClassNum> selectAllClassNum(int classId);
}
package com.hpe.dao;

import com.hpe.pojo.BigQuestion;
import com.hpe.vo.BigQuestionVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

import java.util.List;


public interface BigQuestionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BigQuestion record);

    int insertSelective(BigQuestion record);

    BigQuestion selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BigQuestion record);

    int updateByPrimaryKey(BigQuestion record);

    /**
     * @Author:TangWenhao
     * @Description:根据type和ids查询简单题以及程序题
     * @Date:11:54 2017/9/26
     */
    BigQuestion findBigQuesByIds(@Param("id") Integer id, @Param("type") int type);

    /**
     * @Author:XuShengJie
     * @Description:获取所有简答题和编程题
     * @Date:18:48 2017/9/26
     */
    List<BigQuestionVo> showAllBigQuestions(@Param("bqcourseName")String bqcourseName,@Param("bqquestionTitle") String bqquestionTitle);

    /**
     * @Author:XuShengJie
     * @Description:根据课程id和需要的类型出题
     * @Date:16:39 2017/9/27
     */
    List<BigQuestion> findQuestionByTypeCid(@Param("courseId") Integer courseId, @Param("type") Integer bigQuestionType);
    
    /**
     * @Author:XuShengJie
     * @Description:改卷根据题目获取大题
     * @Date:22:43 2017/10/10
     */
    BigQuestion findByTitle(@Param("title") String title);
    
    /**
     * @Author:XuShengJie
     * @Description:根据课程id和需要的题型换题，除了部分id（被出过的题目）
     * @Date:17:41 2017/11/3
     */
    List<BigQuestion> findQuestionByTypeCid2(@Param("courseId") Integer courseId, @Param("type") Integer bigQuestionType,@Param("bqids") String[] bqids);
}
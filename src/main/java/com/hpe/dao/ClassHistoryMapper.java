package com.hpe.dao;

import com.hpe.pojo.ClassHistory;
import com.hpe.vo.ClassHistoryVo;
import com.hpe.vo.ClassStuVo;
import com.hpe.vo.StudentCareerClassVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ClassHistoryMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClassHistory record);

    int insertSelective(ClassHistory record);

    ClassHistory selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClassHistory record);

    int updateByPrimaryKey(ClassHistory record);

    /**
    * @Author:Liuli
    * @Description:获取某个学生班级变更历史
    * @Date:16:30 2017/10/12
    */
    List<ClassHistory> selectByStudentId(int sId);

    /**
    * @Author:CuiCaijin
    * @Description:插入一个List
    * @Date:19:03 2017/10/16
    */
    int insertList(@Param("histories")List<ClassHistory> histories);

    /**
    * @Author:Liuli
    * @Description:获取班级人员（历史所有人员）
    * @Date:23:25 2017/10/16
    */
    List<ClassHistory> getAllStudentByClassId(int classId);

    /**
    * @Author:Liuli
    * @Description:获取班级人员（视图）
    * @Date:10:23 2017/10/17
    */
    List<ClassStuVo> getAllStuVoByClassId(int classId);


    /**
     * @Author:CuiCaijin
     * @Description:查看班级历史某个时间点的人
     * @Date:9:43 2017/11/14
     */
    List<ClassStuVo> getStuVoByClassIdAndHistory(@Param("classId")int classId,@Param("date")String date);

    /**
     * @Author:CuiCaijin
     * @Description:根据姓名获得班级历史某个时间的人
     * @Date:9:43 2017/11/14
     */
    List<ClassStuVo> getStuVoByClassIdAndHistoryAndName(@Param("classId")int classId,@Param("date")String date,@Param("name")String name);

    /**
    * @Author:Liuli
    * @Description:获取该班级所有学生的历史班级
    * @Date:20:10 2017/10/23
    */
    List<ClassHistoryVo> getClassStuHistory(int classid);

    /**
    * @Author:Liuli
    * @Description:获取该学生经历过的所有班级、所有老师、所有课程
    * @Date:10:50 2017/10/24
    */
    List<StudentCareerClassVo> getStudentClassCareer(int studentid);
}
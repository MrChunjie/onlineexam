package com.hpe.dao;

import com.hpe.pojo.StudentInfo;
import com.hpe.vo.StudentInfoVo;
import com.hpe.vo.StudentVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StudentInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(StudentInfo record);

    int insertSelective(StudentInfo record);

    StudentInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StudentInfo record);

    int updateByPrimaryKey(StudentInfo record);

    /**
     * @description 登录成功后获取当前用户的真实姓名
     * @author Fujt
     * @date 2017/9/25 15:52
     */
    String findNameByPrimaryKey(Integer id);

    /**
    * @Author:CuiCaijin
    * @Description: 获得学生列表
    * @Date:9:47 2017/9/28
    */
    List<StudentVo> getStudentList();

    /**
    * @Author:CuiCaijin
    * @Description:返回所有非在读的学生
    * @Date:19:27 2017/10/18
    */
    List<StudentVo> getNotOnlineStudentList();
    /**
    * @Author:CuiCaijin
    * @Description:获得学生的部分信息，用做更新用(状态 state 方向 direction 班级 spare  惠普学号newId)
    * @Date:17:06 2017/9/28
    */
    StudentInfo getStudentPartInfo(@Param("id")int id);

    /**
    * @Author:CuiCaijin
    * @Description: 根据id更新方向
    * @Date:17:21 2017/9/28
    */
    int updateStudentDirection(@Param("id")int id,@Param("direction")int direction);

    /**
    * @Author:CuiCaijin
    * @Description: 根据id更新班级
    * @Date:17:21 2017/9/28
    */
    int updateStudentSpare(@Param("id")int id,@Param("spare")int spare);

    /**
    * @Author:CuiCaijin
    * @Description:根据id更新学号
    * @Date:17:22 2017/9/28
    */
    int updateStudentNewId(@Param("id")int id,@Param("newId")int newId);

    /**
     * @Author:CuiCaijin
     * @Description: 根据id更新状态
     * @Date:11:53 2017/9/28
     */
    int updateStudentState(@Param("id")int id,@Param("state")int state);

    /**
    * @Author:CuiCaijin
    * @Description: 根据学生状态，姓名，班级，方向模糊查询
    * @Date:20:06 2017/10/8
    */
    List<StudentVo> getStudentsByCondition(@Param("studentName")String studentName,@Param("school")String school,
                                             @Param("spare")String spare,@Param("direction")int direction);
    /**
    * @Author:TangWenhao
    * @Description:查找答卷中所需的班级id
    * @Date:16:19 2017/9/28
    */
    String findClassByPrimaryKey(Integer id);

    /**
    * @Author:CuiCaijin
    * @Description:使当前在读的所有学生毕业
    * @Date:14:20 2017/10/9
    */
    int graduationStudent();

    /**
    * @Author:CuiCaijin
    * @Description:获得当前在校人数
    * @Date:14:32 2017/10/9
    */
    int getOnSchoolStudentCount();

    StudentInfoVo findStudentByPrimaryKey(Integer id);

    /**
    * @Author:CuiCaijin
    * @Description: 更新学生的部分信息
     *@param id 主键
     *@param state 学生状态
     *@param newId 慧与学号
     *@param direction 学生方向
     *@param spare 班级
    * @Date:10:32 2017/10/11
    */
    int updateStudentPartInfomation(@Param("id") int id,@Param("state") int state,@Param("newId") int newId,
                                    @Param("direction") int direction,@Param("spare") String spare);
    
    /**
     * @Author:XuShengJie
     * @Description:根据班级id获取学生
     * @Date:19:50 2017/10/17
     */
    List<Integer> getAllStudentForClassId(@Param("classid") int classId);

    /**
     * @description 删除方向时检索是否具有该方向的学生
     * @author Fujt
     * @date 2017/10/20 11:07
     */
    StudentInfo findByDirectionExistOrNot(int direction);

    /**
     * @description 身份证号查重
     * @author Fujt
     * @date 2017/10/20 14:45
     */
    StudentInfo findNumberExistOrNot(String number);

    /**
     * @Author:CuiCaijin
     * @Description:根据studentId获得学生现在的班级id
     * @Date:19:49 2017/10/23
     */
    String getClassIdByStudentId(int id);

    /**
     * @description 根据学校查询学生
     * @author Fujt
     * @date 2017/10/23 19:57
     */
    List<StudentInfo> findBySchoolName(String school);

    /**
     * @description 获得所有的学校
     * @author Fujt
     * @date 2017/10/23 19:57
     */
    List<String> findAllSchool();
}
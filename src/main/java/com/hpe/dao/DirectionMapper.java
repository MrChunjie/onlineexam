package com.hpe.dao;

import com.hpe.pojo.Direction;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DirectionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Direction record);

    int insertSelective(Direction record);

    Direction selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Direction record);

    int updateByPrimaryKey(Direction record);

    /**
     * @Author:Liuli
     * @Description:查看所有方向
     * @Date:10:57 2017/9/28
     */
    List<Direction> selectAllDirection();

    /**
     * @description 添加或者更新方向名字时，检索是否存在
     * @author Fujt
     * @date 2017/10/20 11:22
     */
    Direction findByName(String name);

    /**
    * @Author:CuiCaijin
    * @Description:根据方向id获得方向名
    * @Date:20:21 2017/10/24
    */
    String getDirectionNameByDirectionId(@Param("id")int directionId);

    /**
    * @Author:Liuli
    * @Description:获取所有被开放选择的方向
    * @Date:10:36 2017/11/15
    */
    List<Direction> getAllOpenedDirection();
}
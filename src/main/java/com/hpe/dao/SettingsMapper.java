package com.hpe.dao;

import com.hpe.pojo.Settings;

public interface SettingsMapper {
    int deleteByPrimaryKey(Integer maxSign);

    int insert(Settings record);

    int insertSelective(Settings record);

    Settings selectByPrimaryKey(Integer maxSign);

    int updateByPrimaryKeySelective(Settings record);

    int updateByPrimaryKey(Settings record);

    // String selectByCondition(@Param("max_sign")String maxSign);
    Settings selectByCondition();

    int updateSetting(Settings settings);

    int selectRegisterState();

    int updateRegisterState(int registerState);
}
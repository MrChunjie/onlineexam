package com.hpe.dao;

import com.hpe.pojo.ClassType;

public interface ClassTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ClassType record);

    int insertSelective(ClassType record);

    ClassType selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ClassType record);

    int updateByPrimaryKey(ClassType record);
}
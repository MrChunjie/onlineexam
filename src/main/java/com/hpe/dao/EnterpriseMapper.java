package com.hpe.dao;

import com.hpe.pojo.Enterprise;
import com.hpe.vo.EnterpriseInfo;
import com.hpe.vo.EnterpriseOutput;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface EnterpriseMapper {
    int deleteByPrimaryKey(Integer id);
    int insert(Enterprise record);

    int insertResultId(Enterprise record);

    int insertSelective(Enterprise record);

    Enterprise selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Enterprise record);

    int updateByPrimaryKey(Enterprise record);

    /**
     *@Author:HouPeng
     *@Description:获取报名未失效所有企业信息
     *@Date:11:06 2017/10/9
     * @return 所有企业信息集合
     * @param map 查询条件
     */
    List<EnterpriseInfo> getEnterpriseAll(Map map);

    /**
     *@Author:HouPeng
     *@Description:获取报名失效所有企业信息
     *@Date:11:06 2017/10/9
     * @return 所有企业信息集合
     */
    List<EnterpriseInfo> getInvalidEnterpriseAll();

    /**
     *@Author:HouPeng
     *@Description:根据name查找企业
     *@Date:11:21 2017/9/27
     * @param name 企业名字
     * @return 企业信息
     */
    Enterprise selectEnterpriseByName(String name);

    /**
     *@Author:HouPeng
     *@Description: 根据企业id获取企业信息
     *@Date:18:57 2017/10/11
     *@param:enterpriseId 企业表id
     *@return:
     */
    EnterpriseInfo getEnterpriseById(Integer enterpriseId);

    /**
     *@Author:HouPeng
     *@Description:根据方向查询企业
     *@Date:20:51 2017/10/12
     *@param:
     *@return:
     */
    List<EnterpriseInfo> getEnterpriseAllByType(int directionId);

    /**
     *@Author:HouPeng
     *@Description:根据时间查询企业信息
     *@Date:20:51 2017/10/12
     *@param:
     *@return:
     */
    List<EnterpriseInfo> getEnterpriseInfosByStime(Date stime);

    /**
     *@Author:HouPeng
     *@Description:根据时间查询企业导出信息
     *@Date:9:52 2017/10/18
     *@param:
     *@return:
     */
    List<EnterpriseOutput> selectEnterpriseOutputByStime(Date stime);
}
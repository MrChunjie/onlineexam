package com.hpe.dao;

import com.hpe.pojo.Student;
import org.apache.ibatis.annotations.Param;

public interface StudentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Student record);

    int insertSelective(Student record);

    int updateByPrimaryKeySelective(Student record);

    int updateByPrimaryKey(Student record);

    Student selectByPrimaryKey(Integer id);

    /**
     * 学生登录
     * @param name
     * @param pwd
     * @return
     */
    Student studentLogin(@Param("name") String name,@Param("pwd") String pwd);

    /**
     * @description 重置密码
     * @author Fujt
     * @date 2017/10/26 11:56
     */
    int updatePwdByPk(String id);

}
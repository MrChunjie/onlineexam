package com.hpe.dao;

import com.hpe.pojo.Teacher;
import com.hpe.vo.TeacherInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TeacherMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Teacher record);

    int insertSelective(Teacher record);

    Teacher selectByPrimaryKey(Integer id);

    //教师登录接口
    Teacher teacherLogin(@Param("name") String name,@Param("pwd") String pwd);

    int updateByPrimaryKeySelective(Teacher record);

    int updateByPrimaryKey(Teacher record);


    /**
     *@Author:HouPeng
     *@Description:获取所有老师信息
     *@Date:11:06 2017/10/9
     * @return 所有老师信息集合
     */
    List<TeacherInfo> getTeacherAll();

    /**
     *@Author:HouPeng
     *@Description:根据name查找老师
     *@Date:11:21 2017/9/27
     * @param name 老师名字
     * @return 老师信息
     */
    Teacher selectTeacherByName(String name);

    /**
     * @description 重置密码 123456
     * @author Fujt
     * @date 2017/10/26 11:42
     */
    int updatePwdByPk(String id);
}
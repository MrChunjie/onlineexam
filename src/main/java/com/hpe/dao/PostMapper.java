package com.hpe.dao;

import com.hpe.pojo.Post;

import java.util.List;

public interface PostMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Post record);

    int insertSelective(Post record);

    Post selectByPrimaryKey(Integer id);
    
    /**
     *@Author:HouPeng
     *@Description:根据name查询岗位
     *@Date:20:11 2017/10/25
     *@param:name 岗位名
     *@return: 岗位信息
     */
    Post selectPostByName(String name);

    int updateByPrimaryKeySelective(Post record);

    int updateByPrimaryKey(Post record);

    List<Post> selectAllPost();


}
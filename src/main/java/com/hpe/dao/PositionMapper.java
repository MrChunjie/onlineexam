package com.hpe.dao;

import com.hpe.pojo.Position;

import java.util.List;

public interface PositionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Position record);

    int insertSelective(Position record);

    Position selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Position record);

    int updateByPrimaryKey(Position record);

    /**
     * @description 检索所有的'province' 字段
     * @author Fujt
     * @date 2017/10/11 10:52
     */
    List<String> selectAllProvince();

    /**
     * @description 根据省份检索城市
     * @author Fujt
     * @date 2017/10/11 11:21
     */
    List<Position> selectCityByProvince(String province);
}
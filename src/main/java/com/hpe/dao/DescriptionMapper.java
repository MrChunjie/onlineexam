package com.hpe.dao;

import com.hpe.pojo.Description;
import org.apache.ibatis.annotations.Param;

public interface DescriptionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Description record);

    int insertSelective(Description record);

    Description selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Description record);

    int updateByPrimaryKey(Description record);

    /**
    * @Author:Liuli
    * @Description:获取某维度某分数的描述
    * @Date:9:49 2017/10/24
    */
    String selectDescription(@Param("dimension") int dimension , @Param("score") int score);

}
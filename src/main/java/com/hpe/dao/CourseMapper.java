package com.hpe.dao;

import com.hpe.pojo.Course;

import java.util.List;

public interface CourseMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Course record);

    int insertSelective(Course record);

    Course selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Course record);

    int updateByPrimaryKey(Course record);

    /**
     * @Author:XuShengJie
     * @Description:1.获取所有的课程信息用于操作题库时选择题库对应的课程
     *               2.组试卷的时候获取所有课程信息
     * @Date:19:56 2017/9/25
     */
    List<Course> getAllCourseOrderById();

    Course selectCourseByName(String name);

}
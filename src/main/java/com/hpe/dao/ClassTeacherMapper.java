package com.hpe.dao;

import com.hpe.pojo.ClassTeacher;
import com.hpe.pojo.ClassTeacherKey;
import com.hpe.pojo.Course;
import com.hpe.vo.AverageVo;
import com.hpe.vo.ClassScoreVO;
import com.hpe.vo.ClassTeacherVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ClassTeacherMapper {
    int deleteByPrimaryKey(ClassTeacherKey key);

    int insert(ClassTeacher record);

    int insertSelective(ClassTeacher record);

    ClassTeacher selectByPrimaryKey(ClassTeacherKey key);

    int updateByPrimaryKeySelective(ClassTeacher record);

    int updateByPrimaryKey(ClassTeacher record);

    /**
    * @Author:Liuli
    * @Description:查询全部记录
    * @Date:11:24 2017/10/11
    */
    List<ClassTeacher> selectAll();

    /**
    * @Author:Liuli
    * @Description:左链接查询全部老师名、课程名、班级名（视图）
    * @Date:10:50 2017/10/11
    */
    List<ClassTeacherVo> findAll();

    /**
    * @Author:Liuli
    * @Description:通过老师id来左链接查询
    * @Date:10:51 2017/10/11
    */
    List<ClassTeacherVo> findByTeacherId(int teacherId);

    /**
    * @Author:Liuli
    * @Description:通过班级id来左链接查询
    * @Date:10:51 2017/10/11
    */
    List<ClassTeacherVo> findByClassId(int classId);

    /**
    * @Author:Liuli
    * @Description:通过课程id来左链接查询
    * @Date:10:51 2017/10/11
    */
    List<ClassTeacherVo> findByCourseId(int courseId);

    /**
     * @Author:XuShengJie
     * @Description:添加班级对应课程的平均成绩
     * @Date:10:05 2017/10/15
     */
    int updateAvgByClassidAndCoureseid(@Param("classId") int classId , @Param("courseId") int courseId);
    
    /**
     * @Author:XuShengJie
     * @Description:根据班级id获取开课Id
     * @Date:21:21 2017/10/15
     */
    List<Integer> selectCourseIdByClassId(@Param("classId") int classId);
    
    /**
     * @Author:XuShengJie
     * @Description:根据班级id获取所有开课信息
     * @Date:15:50 2017/10/27
     */
    List<Course> selectCoursesByClassId(@Param("classId")int classId);

    /**
     * @description 根据条件查询信息，条件个数不定
     * @author Fujt
     * @date 2017/10/18 9:38
     */
    List<ClassTeacherVo> findByConditions(Map<String, String> map);

    /**
     * @Author:XuShengJie
     * @Description:获取所有班级所有课程的成绩信息
     * @Date:14:48 2017/11/12
     */
    List<ClassScoreVO> findAllClassScore(@Param("className") String className);
}

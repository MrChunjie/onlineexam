package com.hpe.dao;

import com.hpe.pojo.Exam;
import com.hpe.vo.AverageVo;
import com.hpe.vo.ClassExamVo;
import com.hpe.vo.ExamVo;
import com.hpe.vo.PaperInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExamMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Exam record);

    int insertSelective(Exam record);

    /**
    * @Author:TangWenhao
    * @Description:根据主键查询考卷详细信息
    * @Date:11:18 2017/9/26
    */
    Exam selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Exam record);

    int updateByPrimaryKey(Exam record);

    /**
    * @Author:TangWenhao
    * @Description: 通过学生ID查询 已发布的试卷。
    * @Date:20:00 2017/9/25
    */
    List<PaperInfo> findListByStuId(int id);

    /**
     * @Author:XuShengJie
     * @Description:根据老师id查询发布过的试卷
     * @Date:10:23 2017/9/28
     */
    List<Exam> findExamByTeaId(int teacherId);


    /**
     * @description 检索所有开发班级或者测试班级的考试
     * @author Fujt
     * @date 2017/10/12 15:31
     * @param classIds 所有的开发班级或者测试班级
     */
    List<Exam> findExamByClassIds(String classIds);


    /**
     * @description 根据exam ids 获取若干个exam
     * @author Fujt
     * @date 2017/10/13 21:29
     */
    List<Exam> findExamByIds(String[] ids);

    /**
     * @Author:TangWenhao
     * @Description:教务得到课程成绩评估
     * @Date:10:17 2017/10/14
     */
    List<AverageVo> findCourseAvgByEdu();

    /**
    * @Author:Liuli
    * @Description:通过班级id获取班级所有的课程和所有课程的考试
    * @Date:0:09 2017/10/15
    */
    List<ClassExamVo> findAllExamByClassId(int classId);
    
    
    /**
     * @Author:XuShengJie
     * @Description:求一份试卷的全体平均成绩
     * @Date:11:25 2017/10/15
     */
    int updateExamAvg(@Param("examid") int examId);
    
    /**
     * @Author:XuShengJie
     * @Description:根据班级和课程获取他们出过的考试
     * @Date:14:31 2017/10/15
     */
    List<Exam> getExamIdForCCId(@Param("classId") Integer classId , @Param("courseId") Integer courseId);

    /**
    * @Author:Liuli
    * @Description:获取该老师发布的所有考试（视图）
    * @Date:9:24 2017/10/17
    */
    List<ExamVo> findExamVoByTeacherId(int tid);
    
    /**
     * @Author:XuShengJie
     * @Description:插入后获取主键
     * @Date:20:39 2017/10/17
     */
    int insertAndGetId(Exam record);


    /**
     * @description 获得所有的考试
     * @author Fujt
     * @date 2017/10/23 18:49
     */
    List<ExamVo> findAllExam();


    /**
     * @Author:TangWenhao
     * @Description: 完善信息，得到班级之后 插入该班级待考试试卷
     * @Date:9:46 2017/11/22
     */
    List<Exam> findExamByClassId(@Param("classId") int classId);
}

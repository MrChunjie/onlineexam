package com.hpe.dao;

import com.hpe.pojo.Group;
import com.hpe.pojo.Score;
import com.hpe.pojo.ScoreKey;
import com.hpe.vo.*;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

public interface ScoreMapper {
    int deleteByPrimaryKey(ScoreKey key);

    int insert(Score record);

    /**
     * @Author:TangWenhao
     * @Description:插入答卷
     * @Date:17:10 2017/9/26
     */
    int insertSelective(Score record);

    Score selectByPrimaryKey(ScoreKey key);

    int updateByPrimaryKeySelective(Score record);

    int updateByPrimaryKey(Score record);

    //查找答卷
    Score findById(int id);

    /**
    * @Author:TangWenhao
    * @Description: 根据stuid查询
    * @Date:16:08 2017/9/27
    */
    List<ExamHistory> findPaperByStudent(@Param("id")int id,@Param("courseName")String courseName);

    /**
    * @Author:TangWenhao
    * @Description: 根据学生信息，查询可考试卷。
    * @Date:20:24 2017/9/25
    */
    List<Score> getPaperByStudentId(@Param("studentId") int studentId,@Param("testId") int testId);

    /**
    * @Author:TangWenhao
    * @Description:查询待批阅试卷
    * @Date:10:40 2017/10/9
    */
    List<ReadOverPapers> findPaper(@Param("stuClassId") int stuClassId, @Param("teacherId") int teacherId);

    /**
     * @Author:XuShengJie
     * @Description:流水阅卷查询为批改试卷
     * @Date:11:54 2017/10/10
     */
    List<LSReadOverpapers> lsFindPaper(@Param("deptName") Integer deptName);
    
    /**
     * @Author:XuShengJie
     * @Description:找到所有满足条件的试卷
     * @Date:16:11 2017/11/28
     */
    List<LSReadOverpapers> lsFindAllPaper(@Param("deptName") Integer deptName,@Param("eId") Integer eId, @Param("bqId")String bqId);
    
    /**
     * @Author:XuShengJie
     * @Description:提交分数
     * @Date:23:15 2017/10/10
     */
    int updateBQScore(@Param("score2") double score2,@Param("score3") double score3,@Param("bigqueids") String bigqueids,@Param("sId") int sId,@Param("eId") int eId);
    
    /**
     * @Author:XuShengJie
     * @Description:当一个人的所有大题全部改完则把这个元祖的status变为1
     * @Date:14:59 2017/10/12
     */
    void updateBQAndStatusScore(@Param("score2") double score2,@Param("score3") double score3,@Param("bigqueids") String bigqueids,@Param("sId") int sId,@Param("eId") int eId);
    /**
    * @Author:TangWenhao
    * @Description:查询下一个该班级、该考卷的学生答卷,如果未选择班级，则查该老师权限所在的班级
    * @Date:10:01 2017/10/11
    */
    Score findNextPaper(@Param("stuClassId") int stuClassId, @Param("examId") int examId,@Param("teaId") int teaId);

    /**
    * @Author:TangWenhao
    * @Description:查询学生平时成绩列表,复用推算下一个学生
    * @Date:10:32 2017/10/12
    */
    List<UsualScores> findUsualScores(@Param("stuClassId") int stuClassId, @Param("teaId") int teaId,@Param("sId") int sId);

    /**
    * @Author:TangWenhao
    * @Description:根据教师ID得到成绩评估
    * @Date:9:50 2017/10/13
    */
    List<AverageVo> findAvg(@Param("stuClassId") int stuClassId, @Param("teaId") int teaId);

    /**
     * @description 查询开发班（测试班）若干次次考试的成绩总和并排序
     * @author Fujt
     * @date 2017/10/13 14:45
     */
    List<Group> findScoreByExamId(@Param("type")String type,@Param("examArray")String[] examArray);

    /**
    * @Author:TangWenhao
    * @Description: 查询参与该课程下所有考试的，所有学生成绩
    * @Date:11:02 2017/10/16
    */
    List<EvaVo> findEvaDetail(@Param("classId") int classId, @Param("examId") int examId);

    /**
    * @Author:TangWenhao
    * @Description: 查询sId、examId所确定的唯一学生平时成绩
    * @Date:11:16 2017/10/17
    */
    List<UsualScores> findUsualByStuExam(@Param("stuClassId") int stuClassId,@Param("sId") int sId,@Param("examId") int examId);

    /**
    * @Author:TangWenhao
    * @Description: 推算下一个属于该班级、该试卷的学生平时成绩信息
    * @Date:13:55 2017/10/21
    */
    List<UsualScores> findNextUsualScores(@Param("stuClassId") int stuClassId,@Param("examId") int examId);
    /**
     * @Author:TangWenhao
     * @Description: 实践成绩
     * @Date:10:54 2017/10/23
     */
    UsualScores findPractice(@Param("sId") int sId);

    /**
     * @Author:XuShengJie
     * @Description:获取小于总成绩百分之60的答卷
     * @Date:20:05 2017/10/23
     */
    List<ResetExamVo> findResetScore();

    /**
    * @Author:Liuli
    * @Description:获取某时间段之内的试卷数量
    * @Date:23:31 2017/10/27
    */
    Long countScore (@Param("beginTime")String beginTime,@Param("endTime")String endTime);

    /**
    * @Author:TangWenhao
    * @Description: 根据班级和试卷导出所有学生的平时成绩信息
    * @Date:16:43 2017/11/10
    */
    List<UsualScores> findUsualByClassAndExam(@Param("classId") int classId,@Param("examId") int examId);

    /**
     * @description 根据试卷id检索所有成绩
     * @author Fujt
     * @date 2017/11/13 9:08
     */
    List<Score> findScoresByExamId(int examId);

    /**
    * @Author:TangWenhao
    * @Description: 教务端学生成绩管理
    * @Date:11:03 2017/11/13
    */
    List<EduScoreStuVO> findStuScore(@Param("stuName") String stuName,@Param("other") String other);


    /**
    * @Author: X
    * @Description:查询班级对应考试的平均成绩
    * @Date:10:17 2017/11/14
    */
    List<ClassScoreVO> getAllClassAvgForExam(@Param("className") String className,@Param("teacherName") String teacherName,@Param("courseName") String courseName);
    List<ClassScoreVO> getAllClassAvgForExam(@Param("className") String className);

    /**
    * @Author:TangWenhao
    * @Description: 教师端查询自己负责班级的平均成绩
    * @Date:16:39 2017/11/14
    */
    List<ClassScoreVO> teaFindClassAvgForExam(@Param("stuClassId") int stuClassId,@Param("teaId") int teaId);
}
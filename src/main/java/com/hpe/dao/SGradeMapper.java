package com.hpe.dao;

import com.hpe.pojo.SGrade;
import com.hpe.pojo.SGradeKey;
import com.hpe.vo.GradeVo;
import com.hpe.vo.WordCloudVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SGradeMapper {
    int deleteByPrimaryKey(SGradeKey key);

    int insert(SGrade record);

    int insertSelective(SGrade record);

    SGrade selectByPrimaryKey(SGradeKey key);

    int updateByPrimaryKeySelective(SGrade record);

    int updateByPrimaryKey(SGrade record);

    List<SGrade> selectByStudentId(int studentid);

    /**
     * @description 批量添加数据
     * @author Fujt
     * @date 2017/10/10 10:35
     * @param grades 要添加对象数组
     */
    int insertGrades(SGrade[] grades);

    /**
    * @Author:CuiCaijin
    * @Description:根据学生id获得技能熟练度和技能等级
    * @Date:15:15 2017/10/23
    */
   List<GradeVo> getGradeValueByStudentId(@Param("studentId")int studentId);

   /**
   * @Author:Liuli
   * @Description:获得词云描述
   * @Date:10:03 2017/10/26
   */
   WordCloudVo getWordCloudCareer(int studentid);
}
package com.hpe.dao;

import com.hpe.pojo.Group;
import com.hpe.pojo.GroupKey;
import com.hpe.vo.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GroupMapper {
    int deleteByPrimaryKey(GroupKey key);

    int insert(Group record);

    int insertSelective(Group record);

    Group selectByPrimaryKey(GroupKey key);

    int updateByPrimaryKeySelective(Group record);

    int updateByPrimaryKey(Group record);

    /**
     * @Author:CuiCaijin
     * @Description:获得最后一条记录的groupId
     * @Date:14:22 2017/10/12
     */
    int getLastGroupId();

    /**
     * @Author:CuiCaijin
     * @Description:根据groupId获得是否已经分班
     * @Date:14:29 2017/10/12
     */
    int getSpareByGroupId(@Param("group_id") int groupId);

    /**
     * @Author:CuiCaijin
     * @Description:根据groupId获得当前学生的数量
     * @Date:14:27 2017/10/12
     */
    int getStudentCountByGroupId(@Param("group_id") int groupId);


    /**
     * @Author:CuiCaijin
     * @Description:根据groupId获得这次考试的学生id和考试排名
     * @Date:14:24 2017/10/12
     */
    List<Group> getStudentIdsAndRankByGroupId(@Param("group_id") int groupId);

    /**
     * @Author:CuiCaijin
     * @Description:每次分班都让备用字段加一
     * @Date:14:30 2017/10/12
     */
    int updateSpareByGroupId();

    /**
     * @param groups 插入的数据
     * @description 插入多条group数据
     * @author Fujt
     * @date 2017/10/13 15:53
     */
    int insertGroups(List<Group> groups);


    /**
     * @description 根据groupId分组并查询
     * @author Fujt
     * @date 2017/10/13 16:02
     */
    List<Group> selectGroupGroupByGroupId();


    /**
     * @Author:Liuli
     * @Description:汇总展示视图
     * @Date:23:19 2017/10/13
     */
    GroupVo findGroupVo(int groupId);

    /**
     * @Author:Liuli
     * @Description:获取所有的groupid
     * @Date:23:34 2017/10/13
     */
    List<Group> findAllGroupId();


    /**
     * @description 根据 group_id 检索
     * @author Fujt
     * @date 2017/10/14 20:16
     */
    List<Group> selectByGroupId(int id);


    /**
     * @Author:Liuli
     * @Description:获取某次汇总所有学生信息
     * @Date:21:42 2017/10/14
     */
    List<GroupStuVo> selectGroupStuVo(int gId);


    /**
     * @description 更新group的score_2, rank
     * @author Fujt
     * @date 2017/10/15 14:29
     */
    int updateGroupByPrimaryKey(GroupStuVo groupStuVo);

    /**
    * @Author:CuiCaijin
    * @Description:根据groupId获得方向
    * @Date:12:00 2017/10/16
    */
    int getDirection(int groupId);
    
    /**
    * @Author:CuiCaijin
    * @Description:
    * @Date:9:48 2017/10/18
    */
    List<DivideClassResultVo> getDivideClassResult(@Param("groupId")int groupId);

    /**
     * @Author:XuShengJie
     * @Description:根据学生Id获取历史rank
     * @Date:17:20 2017/10/23
     */
    List<Group> getRankByStuId(@Param("stuId") int stuId);

    /**
    * @Author:CuiCaijin
    * @Description:根据学生id获得学生的所有排名，如果学生没有排名则返回0
    * @Date:14:57 2017/10/23
    */
    String getRankByStudentId(@Param("studentId")int studentId);


    /**
    * @Author:CuiCaijin
    * @Description:获得学生信息
    * @Date:21:56 2017/11/8
    */
    List<StudentInfoAndGradeExcelVo> getStudentInfoExcel(@Param("beginTime")String beginTime,@Param("endTime")String endTime);

    /**
    * @Author:CuiCaijin
    * @Description:根据groupId获得方向和日期
    * @Date:21:58 2017/11/8
    */
    UpdateClassVo getDirectionAndGroupDateByGroupId(int groupId);

    /**
     * @description 获取汇总时间
     * @author Fujt
     * @date 2017/11/21 10:41
     */
    String getGroupTimeByGroupId(int id);
}
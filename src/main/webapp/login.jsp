<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>登录</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/login.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="js/user/common.js"></script>
    <script src="js/common/login.js"></script>

</head>
<body>
<div class="toolbar">
    <div class="container">
        <div style="float: left; vertical-align: middle">
            <div style="padding-top: 1px;">
                <img src="imgs/sign.png" height="45px" width="85px">
                <a href="#" class="title hidden-xs" onclick="javascript: location.reload();">慧与济宁-实训中心</a>
            </div>
        </div>
        <div style="float: right">
            <div style="padding-top: 8px">
                <a href="javascript: void(0);" onclick="register();" class="register">注册</a>
            </div>
        </div>
    </div>
</div>
<div class="row" id="loginDiv">
    <div class="col-md-offset-7">
        <div class="col-md-11">
            <div class="col-md-10">
                <div class="loginBg">
                    <form id="form" action="#" method="post" class="form-horizontal bv-form">
                        <div class="form-group has-feedback has-success" style="padding-bottom: 10px">
                            <div class="col-md-offset-1 col-md-10 col-xs-offset-1 col-xs-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"
                                                                          aria-hidden="true"></span></span>
                                    <input spellcheck="false" id="username" name="username" class="form-control"
                                           placeholder="用户名"
                                           style="min-height: 38px;"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-feedback has-success" style="padding-bottom: 10px">
                            <div class="col-md-offset-1 col-md-10 col-xs-offset-1 col-xs-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"
                                                                          aria-hidden="true"></span></span>
                                    <input id="password" name="password" type="password" class="form-control"
                                           placeholder="密码" style="min-height: 38px"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success">
                            <div class="col-md-offset-1 col-md-10 col-xs-offset-1 col-xs-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"
                                                                          aria-hidden="true"></span></span>
                                    <select id="identity" class="form-control" style="min-height: 38px">
                                        <option value="admin">管理员</option>
                                        <option value="teacher">教师</option>
                                        <option value="student" selected>学生</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-1 col-md-10 col-xs-offset-1 col-xs-10"
                                 style="padding-top: 10px; padding-bottom: 2px">
                                <button type="submit" class="btn btn-success btn-block" style="height: 40px">登录</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/user/common.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/t-lscorrect.js"></script>

</head>
<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>流水阅卷</b></li>
        <div style="float: right;">
            <span class="label label-danger label-sm" style="vertical-align: middle">切勿和批改试卷同时使用,阅卷过程中关闭浏览器和回退页面(误操作将score表的spare字段修改为0)</span>
        </div>
    </ol>
    <hr/>


    <div class="row"  style="padding-bottom: 20px;">
        <div class="col-md-3">
            <select name="deptName" id="deptName" class="form-control">
                <option value="">请选择方向</option>
            </select>
        </div>
        <div class="col-md-3">
            <select name="queType" id="queType" class="form-control">
                <option value="">请选择题型</option>
                <option value="1">简答题</option>
                <option value="2">程序题</option>
            </select>
        </div>
        <div class="col-md-3">
            <button class="btn btn-primary btn-sm" id="btn-user" value="查询" onclick="select(1)"><span
                    class="glyphicon glyphicon-search" aria-hidden="true" style="padding-right: 8px;"></span>查询
            </button>
        </div>
    </div>
    <c:if test="${alertMsg != null}">
        <span class="label label-danger label-sm" style="vertical-align: middle">所有试卷的该题目已批阅完成，并且该试卷正在被其他教师批改中</span>
    </c:if>
    <table class="table table-striped table-hover table-condensed">
        <thead style="display: none" id="ls_correct">
        <th style="text-align: center">试卷名</th>
        <th style="text-align: center">学生方向</th>
        <th style="text-align: center">题目</th>
        <th style="text-align: center">题型</th>
        <th style="text-align: center">单选成绩</th>
        <th style="text-align: center">简答成绩</th>
        <th style="text-align: center">编程成绩</th>
        <th style="text-align: center">总成绩</th>
        <th style="text-align: center">用时</th>
        <th style="text-align: center">阅卷</th>
        </thead>
        <tbody id="score_result">
        </tbody>

        <tr style="display: none" id="m_template">
            <td class="e_name" style="text-align: center; vertical-align: middle;"></td>
            <td class="s_direction" style="text-align: center; vertical-align: middle;"></td>
            <td class="q_contentitle" style="text-align: center; vertical-align: middle;"></td>
            <td class="q_type" style="text-align: center; vertical-align: middle;"></td>
            <td class="s_score1" style="text-align: center; vertical-align: middle;"></td>
            <td class="s_score2" style="text-align: center; vertical-align: middle;"></td>
            <td class="s_score3" style="text-align: center; vertical-align: middle;"></td>
            <td class="s_scoreb" style="text-align: center; vertical-align: middle;"></td>
            <td class="testtime" style="text-align: center; vertical-align: middle;"></td>
            <td class="center" id="choice" style="text-align: center; vertical-align: middle;">
                <a class="btn btn-info btn-xs" id="q_update" href="">
                    <span class="glyphicon glyphicon-edit icon-white" style="padding-right: 6px;"></span>
                    开始阅卷
                </a>
            </td>
        </tr>
    </table>
    <div class="row" style="text-align: center">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>

</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>注册</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/register.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="js/user/common.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/common/register.js"></script>

</head>
<body>
<div class="toolbar">
    <div class="container">
        <div style="float: left; vertical-align: middle">
            <div style="padding-top: 4px">
                <img src="imgs/sign.png" height="45px" width="85px">
                <a href="javascript: void(0);" class="title hidden-xs">慧与济宁-实训中心</a>
            </div>
        </div>
        <div style="float: right">
            <div style="padding-top: 8px">
                <a href="<%=basePath%>index.do" class="login">登录</a>
            </div>
        </div>
    </div>
</div>
<div class="show father">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center" style="margin-top: 20px;">
                <h3>用户注册</h3>
            </div>
        </div>

        <form id="form" class="col-xs-12 col-md-offset-3 col-md-6 form-horizontal bv-form" style="margin-top: 30px;">
            <div class="form-group has-success has-feedback">
                <label for="name" class="col-sm-3 col-xs-3 control-label" style="padding-left: 0px; padding-right: 0px;">姓名：</label>
                <div class="col-md-6 col-xs-9">
                    <input id="name" type="text" class="form-control" name="name" placeholder="姓名"
                           data-bv-field="name">
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="sex" class="col-sm-3 col-xs-3 control-label" style="padding-left: 0px; padding-right: 0px;">性别：</label>
                <div class="col-md-6 col-xs-9">
                    <select id="sex" name="sex" class="form-control">
                        <option value="男" selected="selected">男</option>
                        <option value="女">女</option>
                    </select>

                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="phone" class="col-sm-3 col-xs-3 control-label" style="padding-left: 0px; padding-right: 0px;">电话：</label>
                <div class="col-md-6 col-xs-9">
                    <input id="phone" class="form-control" name="phone"
                           placeholder="电话" data-bv-field="phone">
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="nation" class="col-sm-3 col-xs-3 control-label" style="padding-left: 0px; padding-right: 0px;">民族：</label>
                <div class="col-md-6 col-xs-9">
                    <input id="nation" type="text" class="form-control" name="nation"
                           placeholder="民族" data-bv-field="nation">
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="idCard" class="col-sm-3 col-xs-3 control-label" style="padding-left: 0px;padding-right: 0px">身份证号：</label>
                <div class="col-md-6 col-xs-9">
                    <input id="idCard" type="text" class="form-control" name="idCard"
                           placeholder="身份证号" data-bv-field="idCard">
                </div>
            </div>
            <div class="form-group has-success">
                <label for="school" class="col-sm-3 col-xs-3 control-label" style="padding-left: 0px; padding-right: 0px;">学校：</label>
                <div class="col-md-6 col-xs-9">
                    <input id="school" type="text" class="form-control" name="school"
                           placeholder="学校(全称)" data-bv-field="school">
                </div>
            </div>
            <div class="form-group has-success">
                <label for="college" class="col-sm-3 col-xs-3 control-label" style="padding-left: 0px; padding-right: 0px;">院系：</label>
                <div class="col-md-6 col-xs-9">
                    <input id="college" type="text" class="form-control" name="college"
                           placeholder="院系(全称)" data-bv-field="college">
                </div>
            </div>

            <div class="form-group has-success">
                <label for="major" class="col-sm-3 col-xs-3 control-label" style="padding-left: 0px; padding-right: 0px;">专业：</label>
                <div class="col-md-6 col-xs-9">
                    <input id="major" type="text" class="form-control" name="major"
                           placeholder="专业(全称)" data-bv-field="major">
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="s_class" class="col-sm-3 col-xs-3 control-label" style="padding-left: 0px; padding-right: 0px;">原班级：</label>
                <div class="col-md-6 col-xs-9">
                    <input id="s_class" type="text" class="form-control" name="s_class"
                           placeholder="原学校班级" data-bv-field="s_class">
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="id" class="col-sm-3 col-xs-3 control-label" style="padding-left: 0px; padding-right: 0px;">原学号：</label>
                <div class="col-md-6 col-xs-9">
                    <input id="id" type="text" class="form-control" name="id"
                           placeholder="原学校学号" data-bv-field="id">
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="intention" class="col-sm-3 col-xs-3 control-label" style="padding-left: 0px; padding-right: 0px;">目前意向：</label>
                <div class="col-md-6 col-xs-9">
                    <select id="intention" name="intention" class="form-control">
                        <option value="考研">考研</option>
                        <option value="考公">考公</option>
                        <option value="基地就业" selected>基地就业</option>
                        <option value="自主就业">自主择业</option>
                    </select>
                </div>
            </div>
            <div class="form-group" style="margin-top: 30px; margin-bottom: 100px;">
                <div class="col-md-offset-3 col-md-6 col-xs-12">
                    <button id="register" class="btn btn-success btn-block" onclick="doRegister()">注册</button>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>完善信息</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="js/loading.js"></script>
    <script type="text/javascript" src="js/common/stu-complete.js"></script>

</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>完善信息</b></li>
    </ol>
    <hr/>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h3>完善信息</h3>
            </div>
        </div>

        <form id="form" class="col-xs-12 col-md-offset-0 col-md-12 form-horizontal bv-form" style="margin-top: 30px;">
            <div class="form-group has-success has-feedback">
                <label for="direction" class="col-sm-2 col-xs-3 control-label">方向：</label>
                <div class="col-md-4 col-xs-9">
                    <select id="direction" class="form-control" name="direction"
                            data-bv-field="direction" onchange="reminding()">
                    </select>
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="dorm" class="col-sm-2 col-xs-3 control-label">宿舍号：</label>
                <div class="col-md-4 col-xs-9">
                    <input id="dorm" type="text" class="form-control" name="dorm" placeholder="宿舍号"
                           data-bv-field="dorm">
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="fatherName" class="col-md-2 control-label">父亲姓名：</label>
                <div class="col-md-3">
                    <input id="fatherName" class="form-control" name="fatherName"
                           placeholder="父亲姓名" data-bv-field="fatherName">
                </div>
                <label for="fatherPhone" class="col-md-2 control-label">父亲电话：</label>
                <div class="col-md-4">
                    <input id="fatherPhone" class="form-control" name="fatherPhone"
                           placeholder="父亲电话" data-bv-field="fatherPhone">
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="matherName" class="col-md-2 control-label">母亲姓名：</label>
                <div class="col-md-3">
                    <input id="matherName" class="form-control" name="matherName"
                           placeholder="母亲姓名" data-bv-field="matherName">
                </div>
                <label for="matherPhone" class="col-md-2 control-label">母亲电话：</label>
                <div class="col-md-4">
                    <input id="matherPhone" class="form-control" name="matherPhone"
                           placeholder="母亲电话" data-bv-field="matherPhone">
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="province" class="col-md-2 col-xs-3 control-label">家庭住址：</label>
                <div class="col-md-10" style="padding-left: 0px;">
                    <div class="col-md-3">
                        <select id="province" class="form-control" onchange="getCity1()">

                        </select>
                    </div>
                    <div class="col-md-3">
                        <select id="city" class="form-control">
                            <option value="北京市">北京市</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <input id="detail" spellcheck="false" class="form-control" name="detail"
                               placeholder="详细地址" data-bv-field="detail">
                    </div>
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="job" class="col-sm-2 col-xs-3 control-label">原在校职务：</label>
                <div class="col-md-4 col-xs-9">
                    <input id="job" type="text" class="form-control" name="job"
                           placeholder="原在校职务" data-bv-field="job">
                </div>
            </div>
            <div class="form-group has-success has-feedback">
                <label for="counselorName" class="col-md-2 control-label">原辅导员姓名：</label>
                <div class="col-md-3">
                    <input id="counselorName" class="form-control" name="counselorName"
                           placeholder="原辅导员姓名" data-bv-field="counselorName">
                </div>
                <label for="counselorPhone" class="col-md-2 control-label">原辅导员电话：</label>
                <div class="col-md-4">
                    <input id="counselorPhone" class="form-control" name="counselorPhone"
                           placeholder="原辅导员电话" data-bv-field="counselorPhone">
                </div>
            </div>
            <div class="form-group has-success">
                <label for="english" class="col-md-2 control-label">外语水平：</label>
                <div class="col-md-5">
                    <select id="english" class="form-control" name="english">
                        <option value="PET-1" selected="selected">公共英语一级(PET-1)</option>
                        <option value="PET-2">公共英语二级(PET-2)</option>
                        <option value="PET-3">公共英语三级(PET-3)</option>
                        <option value="PET-4">公共英语四级(PET-4)</option>
                        <option value="PET-5">公共英语五级(PET-5)</option>
                        <option value="CET-4">大学英语四级(CET-4)</option>
                        <option value="CET-6">大学英语六级(CET-6)</option>
                        <option value="MET-4">专业英语四级(MET-4)</option>
                        <option value="MET-8">专业英语八级(MET-8)</option>
                    </select>
                </div>
            </div>
            <div class="form-group has-success">
                <label for="skill" class="col-sm-2 col-xs-3 control-label">个人特长：</label>
                <div class="col-md-8">
                <textarea id="skill" spellcheck="false" type="text" class="form-control" name="skill"
                          placeholder="个人特长" data-bv-field="skill" rows="5"></textarea>
                </div>
            </div>

            <div class="form-group has-success">
                <label for="award" class="col-sm-2 col-xs-3 control-label">获奖情况：</label>
                <div class="col-md-8">
                <textarea id="award" spellcheck="false" type="text" class="form-control" name="award"
                          placeholder="获奖情况" data-bv-field="award" rows="5"></textarea>
                </div>
            </div>
            <div class="form-group has-success">
                <label for="restudyname" class="col-sm-2 col-xs-3 control-label">重修课名称：</label>
                <div class="col-md-8">
                <textarea id="restudyname" spellcheck="false" type="text" class="form-control" name="restudyname"
                          placeholder="重修课名称" data-bv-field="restudyname" rows="5"></textarea>
                </div>
            </div>
            <div class="form-group has-success">
                <label for="restudynum" class="col-sm-2 col-xs-3 control-label">重修课数量：</label>
                <div class="col-md-2">
                    <input id="restudynum" type="text" class="form-control" name="restudynum"
                           placeholder="重修课数量" data-bv-field="restudynum"/>
                </div>
            </div>
            <div class="form-group has-success">
                <label for="restudytime" class="col-sm-2 col-xs-3 control-label">预计补考时间：</label>
                <div class="col-md-4 col-xs-9">
                    <div id="restudytime" class="input-group date form_datetime" data-date-format="dd-MM-yyyy"
                         data-link-field="dtp_input1">
                        <input id="time" class="form-control" size="16" type="text" name="time"
                               value="" readonly="" data-bv-field="time">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                    </div>
                </div>
            </div>
            <div class="form-group" style="margin-top: 30px;">
                <div class="col-md-offset-4 col-md-4 col-xs-12">
                    <button id="submit" type="submit" class="btn btn-success btn-block" onclick="complete()">提交</button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>

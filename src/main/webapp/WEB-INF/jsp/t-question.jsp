<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>首页</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <link rel="stylesheet" href="font/css/font-awesome.css">
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/loading.js"></script>
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

    <script src="js/loading.js"></script>
    <script src="js/jquery.form.js"></script>

    <script src="js/common/t-question.js"></script>

</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>题库管理</b></li>
    </ol>
    <hr/>


    <div class="col-md-5" style="padding-bottom: 20px; padding-left: 0px; padding-right: 0px;">
        <button type='button' class='btn btn-sm btn-primary' id="questions_list" onclick="getQuestionList(1)">
            <span class="glyphicon glyphicon-eye-open" aria-hidden="true" style="margin-right: 6px;"></span><span
                id="list_span">查看单选题库</span>
        </button>
        <button type='button' class='btn btn-sm btn-primary' onclick="updateQuestios(0)" id="add_question">
            <span class="glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 6px;"></span><span
                id="add_span">添加单选题</span>
        </button>

        <button type='button' class='btn btn-sm btn-info' id="bigquestions_list"
                onclick="getBigQuestionList(1)">
                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"
                          style="margin-right: 6px;"></span><span id="biglist_span">查看大题题库</span>
        </button>
        <button type='button' class='btn btn-sm btn-info' onclick="updateBigQuestion(0,0)"
                id="add_bigquestion">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"
                          style="margin-right: 6px;"></span><span id="add_span2">添加大题</span>
        </button>
    </div>

    <div id="outin" style="display: none">
        <div class="col-md-7">
            <div class="col-md-4">
                <button type='button' class='btn btn-sm btn-success' onclick="loadOutExcel()"
                        id="loadoutexcel">
                    <span class="glyphicon glyphicon-download" aria-hidden="true"
                          style="margin-right: 6px;"></span><span id="loadout">导出选择题表头</span>
                </button>
            </div>
            <div class="col-md-8" style="padding-left: 0px;">
                <div class="col-md-7" style="padding-left: 0px;">
                    <select class="form-control loadincourseid">
                        <option value="-1">请选择导入的课程</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-success btn-sm" style="width:120px;">
                        <span class='glyphicon glyphicon-upload' aria-hidden='true' style="padding-right: 6px;"></span>导入选择题
                        <div style="filter:alpha(opacity=0);cursor: pointer; opacity: 0; position: absolute;  width: 110px;margin: -18px 0 0 -6px;height:20px;overflow: hidden; ">
                            <form id="uploadForm" enctype="multipart/form-data">
                                <input type="file" id="txtFile" onchange="toUpload()" name="file"
                                       style="font-size: 200px;cursor: pointer;direction: rtl !important; float: right\9; "/>
                            </form>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="padding-bottom: 10px;">
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="courseName" class="col-md-1 col-lg-1 control-label"
                           style="text-align: left">课程名: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <input type="text" name="className" id="courseName" class="form-control">
                        </input>
                    </div>
                    <label for="questionTitle" class="col-md-1 col-lg-1 control-label"
                           style="text-align: left">题干: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <input type="text" name="questionTitle" id="questionTitle" class="form-control">
                        </input>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-primary btn-sm" id="btn-class" onclick="getQuestionList(1)"
                                title="根据输入条件，查询题库"><span
                                class="glyphicon glyphicon-search" aria-hidden="true"
                                style="padding-right: 10px;"></span>查询
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="outinbq" style="display: none">
        <div class="col-md-7">
            <div class="col-md-3">
                <button type='button' class='btn btn-sm btn-success' onclick="loadOutBQExcel()"
                        id="loadoutexce2">
                    <span class="glyphicon glyphicon-download" aria-hidden="true"
                          style="margin-right: 6px;"></span><span id="loadout2">导出大题表头</span>
                </button>
            </div>
            <div class="col-md-3" style="padding-left: 0px;">
                <select class="form-control loadincourseid1">
                    <option value="-1">选择导入课程</option>
                </select>
            </div>
            <div class="col-md-3" style="padding-left: 0px;">
                <select id="loadinquetype" class="form-control">
                    <option value="0">选择导入题型</option>
                    <option value="1">简答题</option>
                    <option value="2">编程题</option>
                </select>
            </div>
            <div class="col-md-3" style="padding-left: 0px;">
                <a class="btn btn-success btn-sm" style="width:100px;"><span
                        class='glyphicon glyphicon-upload' aria-hidden='true' style="padding-right: 6px;"></span>导入大题
                    <div style="filter:alpha(opacity=0);cursor: pointer; opacity: 0; position: absolute;  width: 110px;margin: -18px 0 0 -6px;height:20px;overflow: hidden; ">
                        <form id="uploadForm2" enctype="multipart/form-data">
                            <input type="file" id="txtFile2" onchange="toUploadBQ()" name="file"
                                   style="font-size: 200px;cursor: pointer;direction: rtl !important; float: right\9; "/>
                        </form>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-12" style="padding-bottom: 10px;">
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="courseName" class="col-md-1 col-lg-1 control-label"
                           style="text-align: left">课程名: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <input type="text" name="className" id="bqcourseName" class="form-control">
                        </input>
                    </div>
                    <label for="questionTitle" class="col-md-1 col-lg-1 control-label"
                           style="text-align: left">题干: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <input type="text" name="questionTitle" id="bqquestionTitle" class="form-control">
                        </input>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-primary btn-sm" id="btn-class2"
                                onclick="getBigQuestionList(1)"
                                title="根据输入条件，查询题库"><span
                                class="glyphicon glyphicon-search" aria-hidden="true"
                                style="padding-right: 10px;"></span>查询
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <table class="table table-striped table-hover table-condensed" style="margin-bottom: 0px;">
        <thead style="display: none" id="questionstitle">
        <th style="text-align: center; word-break: keep-all;">题号</th>
        <th style="text-align: center; word-break: keep-all;">所属科目</th>
        <th style="text-align: center; word-break: break-all;" class="col-md-6">题干</th>
        <th style="text-align: center; word-break: keep-all;">答案</th>
        <th style="text-align: center; word-break: keep-all;">修改</th>
        <th style="text-align: center; word-break: keep-all;">删除</th>
        </thead>
        <tbody id="result">
        </tbody>
        <!--循环设置样式模板-->
        <tr style="display: none" id="m_template">
            <td class="q_num" style="text-align: center"></td>
            <td class="c_id" style="text-align: center"></td>
            <td class="q_title col-md-6" style="text-align: center; word-break: keep-all;"></td>
            <td class="q_ans" style="text-align: center"></td>
            <td class="q_update" style="text-align: center"></td>
            <td class="q_delete" style="text-align: center"></td>
        </tr>
    </table>

    <!-- 修改和添加 -->
    <div class="modal fade bs-example-modal-lg" id="changQuestion" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" style="padding-bottom: 50px;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" onclick="clearSmallModal()">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">添加或修改题目</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="updateForm">
                        <div class="form-group has-success has-feedback">
                            <!-- for是什么 -->
                            <label for="courseid" class="col-md-3 control-label">所属科目: </label>
                            <div class="col-md-6">
                                <select id="courseid" class="form-control">
                                </select>
                            </div>

                        </div>
                        <div class="form-group has-success has-feedback">
                            <!-- for是什么 -->
                            <label for="quetitle" class="col-md-3 control-label">题干: </label>
                            <div class="col-md-6">
                                <textarea id="quetitle" name="quetitle" class="ckeditor" cols="40" rows="60"></textarea>
                                <input type="hidden" id="id"/>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="choicea" class="col-md-3 control-label">A: </label>
                            <div class="col-md-6">
                                <input id="choicea" name="choicea" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="choiceb" class="col-md-3 control-label">B: </label>
                            <div class="col-md-6">
                                <input id="choiceb" name="choiceb" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="choicec" class="col-md-3 control-label">C: </label>
                            <div class="col-md-6">
                                <input id="choicec" name="choicec" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="choiced" class="col-md-3 control-label">D: </label>
                            <div class="col-md-6">
                                <input id="choiced" name="choiced" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="ans" class="col-md-3 control-label">答案: </label>
                            <div class="col-md-6">
                                <%--<input id="ans" name="newTeacherName" type="text" class="form-control"/>--%>
                                <select id="ans" class="form-control">
                                    <option value="A" selected>A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                </select>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearSmallModal()">关闭</button>
                    <button type="button" class="btn btn-primary" onclick="commitUpdate()">确认</button>
                </div>
            </div>
        </div>
    </div>


    <!--大题部分-->


    <table class="table table-striped table-hover table-condensed" style="margin-bottom: 0px;">
        <thead style="display: none" id="bigquestiontitle">
        <th style="text-align: center; word-break: keep-all;">题号</th>
        <th style="text-align: center; word-break: keep-all;">类型</th>
        <th style="text-align: center; word-break: keep-all;">课程</th>
        <th style="text-align: center; word-break: break-all;" class="col-md-4">题干</th>
        <th style="text-align: center; word-break: break-all;" class="col-md-4">答案</th>
        <th style="text-align: center; word-break: keep-all;">修改</th>
        <th style="text-align: center; word-break: keep-all;">删除</th>
        </thead>
        <tbody id="big_result">
        </tbody>
        <!--循环设置样式模板-->
        <tr style="display: none" id="big_m_template">
            <td class="bq_num" style="text-align: center"></td>
            <td class="bq_type" style="text-align: center; word-break: keep-all;"></td>
            <td class="bq_c_id" style="text-align: center"></td>
            <td class="bq_title col-md-4" style="text-align: center; word-break: break-all;"></td>
            <td class="bq_ans col-md-4" style="text-align: center; word-break: break-all;"></td>
            <td class="bq_update" style="text-align: center"></td>
            <td class="bq_delete" style="text-align: center"></td>
        </tr>
    </table>
    <!-- 大题修改和添加 -->
    <div class="modal fade bs-example-modal-lg" id="changBigQuestion" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" style="padding-bottom: 50px;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" onclick="clearBigModal()">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabe2">添加或修改题目</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="updateBigForm">
                        <div class="form-group has-success has-feedback">
                            <!-- for是什么 -->
                            <label for="b_courseid" class="col-md-3 control-label">所属科目: </label>
                            <div class="col-md-6">
                                <select id="b_courseid" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="bigQuestionType" class="col-md-3 control-label">类型: </label>
                            <div class="col-md-6">
                                <select id="bigQuestionType" class="form-control">
                                    <option value="1">简答题</option>
                                    <option value="2">编程题</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <!-- for是什么 -->
                            <label for="b_quetitle" class="col-md-3 control-label">题干: </label>
                            <div class="col-md-6">
                                <%--<input id="b_quetitle" name="newTeacherName" type="text" class="form-control"/>--%>
                                <textarea id="b_quetitle" name="b_quetitle" class="ckeditor" cols="40"
                                          rows="60"></textarea>
                                <input type="hidden" id="b_id"/>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="b_ans" class="col-md-3 control-label">答案: </label>
                            <div class="col-md-6">
                                <textarea id="b_ans" name="b_ans" class="ckeditor" cols="40" rows="60"></textarea>
                                <%--<input id="b_ans" name="newTeacherName" type="text" class="form-control"/>--%>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearBigModal()">关闭</button>
                    <button type="button" class="btn btn-primary" onclick="commitBUpdate()">确认</button>
                </div>
            </div>
        </div>
    </div>


    <div class="row" style="text-align: center">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>

</div>
</div>

</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="root" value="${pageContext.request.contextPath}"></c:set>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.css">
    <link rel="stylesheet" href="css/content.css">
    <link rel="stylesheet" href="font/css/font-awesome.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-datetimepicker.css">
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/pagetool.js" type="text/javascript"></script>
    <!-- 表单验证 -->
    <script src="bootstrap/js/bootstrapValidator.js" type="text/javascript"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.form.js"></script>

    <link rel="stylesheet" href="bootstrap/css/bootstrap-select.min.css">
    <script src="bootstrap/js/bootstrap-select.min.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/ep-out.js"></script>
</head>
<body>
<div class="col-md-12 col-lg-12 father">


    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>导出名单</b></li>
    </ol>
    <hr/>

    <div style="padding-bottom: 20px;">
        <form class="form-horizontal bv-form" id="search_form">
            <div class="form-group has-success has-feedback">
                <label for="signStime" class="col-md-2 control-label" style="text-align: left; padding-left: 25px;">报名起始时间：</label>
                <div class="col-md-3" style="padding-left: 0px;">
                    <div id="signStime" class="input-group date form_datetime"
                         data-date-format="dd-MM"
                         data-link-field="dtp_input1">
                        <input id="stime" class="form-control" size="16" type="text" name="time"
                               value="" readonly="" data-bv-field="time">
                        <span class="input-group-addon"><span
                                class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                    </div>
                </div>

                <div class="col-md-2" style="margin-left: 20px; margin-right: 20px;">
                    <button type="button" id="search_btn" class="col-md-1 btn btn-primary btn-block"
                            onclick="search(1)"><span
                            class="glyphicon glyphicon-search" aria-hidden="true" style="padding-right: 8px;"></span>查询
                    </button>
                </div>
                <div class="col-md-2" style="margin-left: 20px; margin-right: 20px; padding-left: 0px;">
                    <button type="button" id="print_btn" class="col-md-1 btn btn-info btn-block" style="margin-left: 5px"
                            onclick="print();"><span class="glyphicon glyphicon-download" aria-hidden="true"
                                                     style="padding-right: 8px;"></span>导出
                    </button>
                </div>

            </div>
        </form>
    </div>

    <div style="padding-left: 20px;display: none" id="displayList">
        <div class="row">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                <th style="text-align: center">序号</th>
                <th style="text-align: center">企业名</th>
                <th style="text-align: center">报名起始时间</th>
                <th style="text-align: center">报名终止时间</th>
                <th style="text-align: center">面向方向</th>
                <th style="text-align: center">类型</th>
                <th style="text-align: center" id="infoTitle">企业介绍</th>

                </thead>
                <tbody id="result">

                </tbody>
                <tr style="display: none" id="m_template">
                    <td class="ep_num" style="text-align: center"></td>
                    <td class="ep_name" style="text-align: center"></td>
                    <td class="ep_stime" style="text-align: center"></td>
                    <td class="ep_etime" style="text-align: center"></td>
                    <td class="ep_type" style="text-align: center"></td>
                    <td class="ep_category" style="text-align: center"></td>
                    <td class="ep_info" style="text-align: center"></td>
                </tr>
            </table>
        </div>
        <div class="row" style="text-align: center" id="pagination">
            <ul class="pagination pagination-lg">
                <li class="head"><a href="#">首页</a></li>
                <li class="lastpage"><a href="#">&laquo;</a></li>
                <li class="disabled morehead"><a>...</a></li>
                <li class="page-4"><a></a></li>
                <li class="page-3"><a></a></li>
                <li class="page-2"><a></a></li>
                <li class="page-1"><a></a></li>
                <li class="currentpage active"><a>1</a></li>
                <li class="page_1"><a></a></li>
                <li class="page_2"><a></a></li>
                <li class="page_3"><a></a></li>
                <li class="page_4"><a></a></li>
                <li class="disabled moretail"><a>...</a></li>
                <li class="nextpage"><a href="#">&raquo;</a></li>
                <li class="tail"><a href="#">尾页</a></li>
            </ul>
        </div>

    </div>
    <%--企业正文model--%>
    <!-- Modal -->
    <div class="modal fade" id="enterpriseInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="enterpriseInfoModalLabel">企业介绍</h4>
                </div>
                <div class="modal-body">
                    <p name="enterpriseTextInfo" id="enterpriseTextInfo" rows="10" cols="80">
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

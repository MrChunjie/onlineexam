<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>首页</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="css/content.css"/>
    <link rel="stylesheet" href="font/css/font-awesome.css">
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/edu-stu.js"></script>
</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>学生管理</b></li>
    </ol>
    <hr/>
    <div class="row" style="padding-left: 10px;margin-bottom: 20px;">
        <div class="col-md-5">
            <button class='btn btn-danger btn-sm' onclick="graduation();"><span class="glyphicon glyphicon-book"
                                                                                aria-hidden="true"
                                                                                style="padding-right: 6px;"></span>毕业当前学生
            </button>
            <button class='btn btn-primary btn-sm' id="get_stu_list_btn"><span class="glyphicon glyphicon-eye-open"
                                                                               aria-hidden="true"
                                                                               style="padding-right: 6px;"></span><span
                    id="get_stu_list">查看非在读学生</span></button>
            <button class='btn btn-info btn-sm' onclick="getExcel();"><span class="glyphicon glyphicon-download"
                                                                            aria-hidden="true"
                                                                            style="padding-right: 6px;"></span>下载学生信息及成绩
            </button>
        </div>
        <div class="col-md-2">
            <button class='btn btn-info btn-sm' onclick="getStudentInfoExcel();"><span
                    class="glyphicon glyphicon-download"
                    aria-hidden="true"
                    style="padding-right: 6px;"></span>下载注册学生信息表头
            </button>
        </div>
        <div class="col-md-2">
            <a class="btn btn-primary btn-sm btn-block"><span
                    class='glyphicon glyphicon-upload' aria-hidden='true' style='padding-right: 8px;'></span>导入学生信息
                <div style="filter:alpha(opacity=0);cursor: pointer; opacity: 0; position: absolute;  width: 200px;margin: -18px 0 0 -6px; height:20px;overflow: hidden; ">
                    <form id="updateForm" enctype="multipart/form-data">
                        <input type="file" id="txtFile" onchange="toUploadStudentInfoExcel()" name="file"
                               style="font-size: 200px;cursor: pointer;direction: rtl !important; float: right\9; "/>
                    </form>
                </div>
            </a>
        </div>
    </div>
    <div style="padding-bottom: 20px;">
        <form class="form-horizontal bv-form" id="search_form">
            <div class="col-md-11 col-lg-11">
                <div class="form-group has-success has-feedback">
                    <label for="stu_name" class="col-md-1 col-lg-1 control-label" style="text-align: left">姓名: </label>
                    <div class="col-md-2 col-lg-2" style="padding-left: 0px;">
                        <input id="stu_name" name="stu_names" type="text" class="form-control"/>
                    </div>
                    <label for="stu_school" class="col-md-1 control-label" style="padding-left: 0px;">学校名称: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <input id="stu_school" name="stu_school" type="text" class="form-control"/>
                    </div>
                    <label for="stu_direction" class="col-md-1 control-label" style="padding-left: 0px;">方向: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <select id="stu_direction" name="stu_direction" class="form-control" onchange="allClassBySearch(null)">
                        </select>
                    </div>
                    <label for="stu_class" class="col-md-1 control-label" style="padding-left: 0px;">班级: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <select id="stu_class" class="form-control">
                        </select>
                    </div>
                </div>
            </div>
            <button type="button" id="search_btn" class="col-md-1 btn btn-primary"><span
                    class="glyphicon glyphicon-search" aria-hidden="true" style="padding-right: 6px;"></span>查询
            </button>
        </form>
    </div>
    <div style="padding-left: 20px;">

        <div class="row">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                <th style="text-align: center">序号</th>
                <th style="text-align: center">姓名</th>
                <th style="text-align: center">慧与学号</th>
                <th style="text-align: center">慧与班级</th>
                <th style="text-align: center">电话</th>
                <th style="text-align: center">性别</th>
                <th style="text-align: center">学校</th>
                <th style="text-align: center">状态</th>
                <th style="text-align: center">方向</th>
                <th style="text-align: center">修改</th>
                <th style="text-align: center">详细</th>
                <th style="text-align: center">重置</th>
                </thead>
                <tbody id="result">

                </tbody>
                <tr style="display: none" id="m_template">
                    <td class="s_num" style="text-align: center"></td>
                    <td class="s_name" style="text-align: center"></td>
                    <td class="s_new_id" style="text-align: center"></td>
                    <td class="s_spare" style="text-align: center"></td>
                    <td class="s_tel" style="text-align: center"></td>
                    <td class="s_sex" style="text-align: center"></td>
                    <td class="s_school" style="text-align: center"></td>
                    <td class="s_state" style="text-align: center"></td>
                    <td class="s_direction" style="text-align: center"></td>
                    <td class="s_op" style="text-align: center"></td>
                    <td class="s_detail" style="text-align: center"></td>
                    <td class="s_reset" style="text-align: center"></td>
            </table>
        </div>
        <div class="row" style="text-align: center" id="pagination">
            <ul class="pagination pagination-lg">
                <li class="head"><a href="#">首页</a></li>
                <li class="lastpage"><a href="#">&laquo;</a></li>
                <li class="disabled morehead"><a>...</a></li>
                <li class="page-4"><a></a></li>
                <li class="page-3"><a></a></li>
                <li class="page-2"><a></a></li>
                <li class="page-1"><a></a></li>
                <li class="currentpage active"><a>1</a></li>
                <li class="page_1"><a></a></li>
                <li class="page_2"><a></a></li>
                <li class="page_3"><a></a></li>
                <li class="page_4"><a></a></li>
                <li class="disabled moretail"><a>...</a></li>
                <li class="nextpage"><a href="#">&raquo;</a></li>
                <li class="tail"><a href="#">尾页</a></li>
            </ul>
        </div>
    </div>

    <%--修改内容model--%>
    <div class="modal fade" id="updateStudent" tabindex="-1" role="dialog"
         aria-labelledby="updateStudentModalLabel">
        <div class="modal-dialog dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="updateStudentModalLabel">修改学生信息</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="update_form_id">
                        <div class="form-group has-success has-feedback">
                            <label for="hp_id" class="col-md-3 control-label">慧与学号: </label>
                            <div class="col-md-6">
                                <input id="hp_id" name="hp_id" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="hp_class" class="col-md-3 control-label">慧与班级: </label>
                            <div class="col-md-6">
                                <select id="hp_class" name="hp_class" class="form-control" autocomplete="off">
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="hp_state" class="col-md-3 control-label">学生状态: </label>
                            <div class="col-md-6">
                                <select id="hp_state" class="form-control">
                                    <option value="0">未报到</option>
                                    <option value="1">在读</option>
                                    <option value="2">毕业</option>
                                    <option value="3">退学</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="hp_direction" class="col-md-3 control-label">学生方向: </label>
                            <div class="col-md-6">
                                <select id="hp_direction" class="form-control" onchange="allClass(null)">
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="this_id"/>
                    <button type="button" class="btn btn-default" onclick="clearModal()">关闭</button>
                    <button ype="button" class="btn btn-primary" id="stu_update_btn">修改</button>
                </div>
            </div>
        </div>
    </div>
    <%--修改内容model--%>


    <div class="modal fade" id="getStudentExcel" tabindex="-1" role="dialog"
         aria-labelledby="Excel">
        <div class="modal-dialog dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="Excel">选择时间范围</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="outForm">
                        <div class="form-group has-success has-feedback">
                            <label for="studentBeginTime" class="col-md-3 control-label">起始时间：</label>
                            <div class="col-md-8">
                                <div id="studentBeginTime" class="input-group date form_datetime"
                                     data-date-format="dd-MM-yyyy HH:mm:ss "
                                     data-link-field="dtp_input1">
                                    <input id="newstime" class="form-control" size="16" type="text" name="time"
                                           value="" data-bv-field="time" readonly="">
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                </div>
                                <small id="newSTimeSmall" class="help-block" data-bv-validator="notEmpty"
                                       data-bv-for="studentBeginTime" data-bv-result="NOT_VALIDATED"
                                       style="display: none; color: #a94442">起始时间不能为空
                                </small>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="studentEndTime" class="col-md-3 control-label">终止时间：</label>
                            <div class="col-md-8">
                                <div id="studentEndTime" class="input-group date form_datetime"
                                     data-date-format="dd-MM-yyyy HH:mm:ss"
                                     data-link-field="dtp_input1">
                                    <input id="newetime" class="form-control" size="16" type="text" name="time"
                                           value="" readonly="" data-bv-field="time">
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                </div>
                                <small id="newETimeSmall" class="help-block" data-bv-validator="notEmpty"
                                       data-bv-for="studentEndTime" data-bv-result="NOT_VALIDATED"
                                       style="display: none; color: #a94442">终止时间不能为空
                                </small>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearTimeModal()">关闭</button>
                    <button ype="button" class="btn btn-primary" onclick="downloadExcel()">下载</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

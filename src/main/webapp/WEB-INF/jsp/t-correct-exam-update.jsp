<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>阅卷</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="js/dateformat.js"></script>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="js/common/t-correct-exam-update.js"></script>

</head>

<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>批改试卷</b></li>
        <li><b>开始阅卷</b></li>
    </ol>
    <hr/>

    <div class="row content">
        <form action="<%=basePath%>teacher/correct/update_papers.do" method="post" class="form-horizontal">
            <input type="hidden" value="${papers.examId }" name="examId"/>
            <input type="hidden" value="${papers.sId }" name="sId"/>
            <input type="hidden" value="${warn}" id="warn"/>
            <input type="hidden" value="${stuClassId }" name="stuClassId"/>
            <input type="hidden" value="${num1 }" id="num1" name="num1"/>
            <input type="hidden" value="${num2 }" id="num2" name="num2"/>

            <c:if test="${num1>0}">
            <h4>二、简答题</h4>

            <c:forEach items="${titles1 }" var="v" varStatus="i">
                <div class="form-group" style="padding-left: 15px;">
                    <p><b>${i.index + 1}、${v }</b></p>
                    <div class="row">
                        <label class="control-label col-md-1" style="text-align: left; padding-right: 0px;">分值:
                            <f:formatNumber type="number" value="${examScore1}" pattern="0.00" maxIntegerDigits="2"></f:formatNumber>
                        </label>
                        <input type="hidden" id="score1" value="${examScore1}">
                        <label class="control-label col-md-1" style="text-align: left;">得分: </label>
                        <div class="col-md-2">
                            <input type="text" name="jianScore${i.index }" value="0" class="form-control"
                                   style="padding-left: 0px;" id="score1Update${i.index }" onblur="checkScore1()"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <textarea rows="10" cols="130" name="" class="form-control">${ans1[i.index] }</textarea>
                    </div>
                </div>
            </c:forEach>
            </c:if>
            <c:if test="${num2>0}">
            <h4>三、编程题</h4>
            <c:forEach items="${titles2 }" var="v" varStatus="i">
                <div class="form-group" style="padding-left: 15px;">
                    <p><b>${i.index + 1}、${v }</b></p>
                    <div class="row">
                        <label class="control-label col-md-1" style="text-align: left; padding-right: 0px;">分值:
                            <f:formatNumber type="number" value="${examScore2}" pattern="0.00" maxIntegerDigits="2"></f:formatNumber>
                        </label>
                        <input type="hidden" id="score2" value="${examScore2}">
                        <label class="control-label col-md-1" style="text-align: left;">得分: </label>
                        <div class="col-md-2">
                            <input type="text" name="bianScore${i.index }" value="0" class="form-control"
                                   id="score2Update${i.index }" onblur="checkScore2()"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <textarea rows="10" cols="130" name="" class="form-control">${ans2[i.index]}</textarea>
                    </div>
                </div>
            </c:forEach>
            </c:if>
            <br>
            <div class="form-group">
                <div class="col-md-offset-4 col-md-4">
                    <button type="submit" id="submit" class="btn btn-primary btn-block">
                        <span class="glyphicon glyphicon-pencil"></span> 提交并批阅下一张本班答卷
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>

</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>首页</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/loading.js"></script>
    <script type="text/javascript" src="js/common/edu-class-teacher.js"></script>

</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>教师课程分配</b></li>
    </ol>
    <hr/>

    <div class="row content">
        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#addModal"><span
                class="glyphicon glyphicon-plus" aria-hidden="true" style="padding-right: 8px;"></span>添加关系
        </button>
    </div>
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <form class="form-horizontal bv-form" id="search_form">
            <div class="col-md-11 col-lg-11">
                <div class="form-group has-success has-feedback">
                    <label for="q_course" class="col-md-1 control-label" style="text-align: left;">课程: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <select id="q_course" name="q_course" class="form-control">
                        </select>
                    </div>
                    <label for="q_teacher" class="col-md-1 control-label" style="padding-left: 0px;">教师: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <select id="q_teacher" name="q_teacher" class="form-control">
                        </select>
                    </div>
                    <label for="q_class" class="col-md-1 control-label" style="padding-left: 0px;">班级: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <select id="q_class" class="form-control">
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="button" id="search_btn" class="btn btn-primary btn-sm"><span
                                class="glyphicon glyphicon-search" aria-hidden="true"
                                style="padding-right: 8px;"></span><span style="padding-right: 6px;">查询</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div style="padding-left: 20px;">

        <div class="row">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                <th style="text-align: center">序号</th>
                <th style="text-align: center">班级</th>
                <th style="text-align: center">教师</th>
                <th style="text-align: center">课程</th>
                <%--<th style="text-align: center">平均成绩</th>--%>
                <th style="text-align: center">课时量</th>
                <th style="text-align: center">满意度</th>
                <th style="text-align: center">操作</th>
                </thead>
                <tbody id="result">

                </tbody>
                <tr style="display: none" id="m_template">
                    <td class="s_num" style="text-align: center"></td>
                    <td class="s_class" style="text-align: center"></td>
                    <td class="s_teacher" style="text-align: center"></td>
                    <td class="s_course" style="text-align: center"></td>
                    <td class="s_avg" style="display: none"></td>
                    <td class="s_hours" style="text-align: center"></td>
                    <td class="s_satisfaction" style="text-align: center"></td>
                    <td class="s_op" style="text-align: center"></td>
            </table>
        </div>
        <div class="row" style="text-align: center" id="pagination">
            <ul class="pagination pagination-lg">
                <li class="head"><a href="#">首页</a></li>
                <li class="lastpage"><a href="#">&laquo;</a></li>
                <li class="disabled morehead"><a>...</a></li>
                <li class="page-4"><a></a></li>
                <li class="page-3"><a></a></li>
                <li class="page-2"><a></a></li>
                <li class="page-1"><a></a></li>
                <li class="currentpage active"><a>1</a></li>
                <li class="page_1"><a></a></li>
                <li class="page_2"><a></a></li>
                <li class="page_3"><a></a></li>
                <li class="page_4"><a></a></li>
                <li class="disabled moretail"><a>...</a></li>
                <li class="nextpage"><a href="#">&raquo;</a></li>
                <li class="tail"><a href="#">尾页</a></li>
            </ul>
        </div>
    </div>
</div>


<%--添加班级model--%>
<div class="modal fade" id="addModal" tabindex="-1" role="dialog"
     aria-labelledby="addClassModalLabel">
    <div class="modal-dialog dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" onclick="clearAddModal()">&times;</span>
                </button>
                <h4 class="modal-title" id="addClassModalLabel">添加课程教师班级关系</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal bv-form" id="addForm">
                    <div class="form-group has-success has-feedback">
                        <label for="m_course" class="col-md-3 control-label">课程: </label>
                        <div class="col-md-6">
                            <select id="m_course" name="m_course" class="form-control">

                            </select>
                        </div>
                    </div>
                    <div class="form-group has-success has-feedback">
                        <label for="m_teacher" class="col-md-3 control-label">教师: </label>
                        <div class="col-md-6">
                            <select id="m_teacher" name="m_teacher" class="form-control">

                            </select>
                        </div>
                    </div>
                    <div class="form-group has-success has-feedback">
                        <label for="m_class" class="col-md-3 control-label">班级: </label>
                        <div class="col-md-6">
                            <select name="m_class" id="m_class" class="form-control">

                            </select>
                        </div>
                    </div>
                    <div class="form-group has-success has-feedback">
                        <label for="m_hours" class="col-md-3 control-label">课时: </label>
                        <div class="col-md-6">
                            <input name="m_hours" id="m_hours" type="text" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="clearAddModal()">关闭</button>
                <button type="button" class="btn btn-primary" onclick="add()">确认</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>

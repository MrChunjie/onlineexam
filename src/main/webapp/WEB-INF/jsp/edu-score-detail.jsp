<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>阅卷</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>

    <script src="page/pagetool.js"></script>
    <script src="js/dateformat.js"></script>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/edu-score-detail.js"></script>
</head>

<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>课程成绩评估</b></li>
        <li><b>班级成绩评估</b></li>
    </ol>
    <hr/>
    <table class="table table-striped">
        <thead>
        <tr>
            <th  style="text-align: center; word-break: keep-all;">编号</th>
            <th style="text-align: center; word-break: keep-all;">课程名称</th>
            <th style="text-align: center; word-break: keep-all;">涉及考试</th>
            <th style="text-align: center; word-break: keep-all;">平均成绩</th>
        </tr>
        </thead>
        <tbody id="result"></tbody>
        <tr style="display: none" id="m_template">
            <td class="p_id" style="padding: 2px; text-align: center"></td>
            <td class="p_courseName" style="padding: 2px; text-align: center"></td>
            <td class="p_examNames" style="padding: 2px; text-align: center"></td>
            <td class="p_courseAvg" style="padding: 2px; text-align: center"></td>
        </tr>
    </table>
    <div class="row" style="text-align: center">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>
</div>

</body>
</html>

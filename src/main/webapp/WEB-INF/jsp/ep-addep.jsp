<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.css">
    <link rel="stylesheet" href="css/content.css">
    <link rel="stylesheet" href="font/css/font-awesome.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-datetimepicker.css">
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/pagetool.js" type="text/javascript"></script>
    <!-- 表单验证 -->
    <script src="bootstrap/js/bootstrapValidator.js" type="text/javascript"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.form.js"></script>

    <link rel="stylesheet" href="bootstrap/css/bootstrap-select.min.css">
    <script src="bootstrap/js/bootstrap-select.min.js"></script>
    <script src="js/loading.js"></script>

    <title></title>
    <script src="js/common/ep-addep.js"></script>
</head>
<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>企业管理</b></li>
    </ol>
    <hr/>
    <div style="padding-left: 20px;">
        <div class="row" style="padding-bottom: 30px;">

            <button type='button' class='btn btn-primary btn-sm btn-success' data-toggle='modal' onclick="toAdd();"
                    data-target='#addEnterprise'>
                <span class="glyphicon glyphicon-plus" aria-hidden="true" style="padding-right: 6px;"></span>添加企业
            </button>

            <a class="btn btn-info btn-sm" style="width:80px;" title="导入企业报名表"><span
                    class='glyphicon glyphicon-upload' aria-hidden='true'  style="padding-right: 6px;"></span>导入
                <div style="filter:alpha(opacity=0);cursor: pointer; opacity: 0; position: absolute;  width: 70px;margin: -18px 0 0 -6px;height:20px;overflow: hidden; ">
                    <form id="uploadForm" enctype="multipart/form-data">
                        <input type="file" id="txtFile" onchange="toUpload()" name="file"
                               style="font-size: 200px;cursor: pointer;direction: rtl !important; float: right\9; "/>
                    </form>
                </div>
            </a>

        </div>
        <div style="padding-bottom: 20px;">
            <form class="form-horizontal bv-form" id="search_form">
                <div class="col-md-11 col-lg-11">
                    <div class="form-group has-success has-feedback">

                        <label for="ep_name" class="col-md-1 col-lg-1 control-label" style="text-align: left">企业名: </label>
                        <div class="col-md-2 col-lg-2" style="padding-left: 0px;">
                            <input id="ep_name" name="ep_name" type="text" class="form-control"/>
                        </div>

                        <label for="ep_post" class="col-md-1 control-label" style="padding-left: 0px;">岗位: </label>
                        <div class="col-md-2" style="padding-left: 0px;">
                            <select id="ep_post" class="form-control">
                                <option value="0">全部</option>
                            </select>
                        </div>
                        <label for="ep_condition" class="col-md-1 control-label" style="padding-left: 0px;">显示条件: </label>
                        <div class="col-md-2" style="padding-left: 0px;">
                            <select id="ep_condition" name="ep_condition" class="form-control">
                                <option value="0">全部</option>
                                <option value="1">正在报名</option>
                                <option value="2">已过期</option>
                            </select>
                        </div>
                        <button type="button" id="search_btn" class="col-md-1 btn btn-primary" onclick="getEnterprises(1);"><span
                                class="glyphicon glyphicon-search" aria-hidden="true" style="padding-right: 6px;"></span>查询
                        </button>
                    </div>
                </div>

            </form>
        </div>
        <div class="row">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                <th style="text-align: center">序号</th>
                <th style="text-align: center">企业名</th>
                <th style="text-align: center">报名起始时间</th>
                <th style="text-align: center">报名终止时间</th>
                <th style="text-align: center">面向方向</th>
                <th style="text-align: center">类型</th>
                <th style="text-align: center" id="infoTitle">企业介绍</th>
                <th style="text-align: center" id="update">修改</th>
                <th style="text-align: center" id="output">导出</th>
                </thead>
                <tbody id="result">
                </tbody>
                <tr style="display: none" id="m_template">
                    <td class="ep_num" style="text-align: center"></td>
                    <td class="ep_name" style="text-align: center"></td>
                    <td class="ep_stime" style="text-align: center"></td>
                    <td class="ep_etime" style="text-align: center"></td>
                    <td class="ep_type" style="text-align: center"></td>
                    <td class="ep_category" style="text-align: center"></td>
                    <td class="ep_info" style="text-align: center"></td>
                    <td class="ep_update" style="text-align: center"></td>
                    <td class="ep_output" style="text-align: center"></td>

                </tr>
            </table>
        </div>
        <div class="row" style="text-align: center" id="pagination">
            <ul class="pagination pagination-lg">
                <li class="head"><a href="#">首页</a></li>
                <li class="lastpage"><a href="#">&laquo;</a></li>
                <li class="disabled morehead"><a>...</a></li>
                <li class="page-4"><a></a></li>
                <li class="page-3"><a></a></li>
                <li class="page-2"><a></a></li>
                <li class="page-1"><a></a></li>
                <li class="currentpage active"><a>1</a></li>
                <li class="page_1"><a></a></li>
                <li class="page_2"><a></a></li>
                <li class="page_3"><a></a></li>
                <li class="page_4"><a></a></li>
                <li class="disabled moretail"><a>...</a></li>
                <li class="nextpage"><a href="#">&raquo;</a></li>
                <li class="tail"><a href="#">尾页</a></li>
            </ul>
        </div>

    </div>

    <%--修改企业model--%>
    <div class="modal fade" id="changEnterprise" tabindex="-1" role="dialog" data-keyboard="false" aria-hidden="true" data-backdrop="static" aria-labelledby="myModalLabel"
         style="margin-bottom: 50px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            onclick="clearUpdateModel()"><span
                            aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="updateEnterpriseModalLabel">修改企业</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="updateForm">
                        <div class="form-group has-success has-feedback">
                            <label for="newEnterpriseName" class="col-md-3 control-label">企业名: </label>
                            <div class="col-md-6">
                                <input id="newEnterpriseName" name="newEnterpriseName" type="text"
                                       class="form-control"/>
                                <input type="hidden" id="newEnterpriseNameId"/>
                            </div>
                        </div>

                        <div class="form-group has-success has-feedback">
                            <label for="newEnterpriseStime" class="col-md-3 control-label">报名起始时间：</label>
                            <div class="col-md-6">
                                <div id="newEnterpriseStime" class="input-group date form_datetime"
                                     data-date-format="dd-MM-yyyy"
                                     data-link-field="dtp_input1">
                                    <input id="newstime" class="form-control" size="16" type="text" name="time"
                                           value="" readonly="" data-bv-field="time">
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                </div>
                                <small id="newSTimeSmall" class="help-block" data-bv-validator="notEmpty"
                                       data-bv-for="newEnterpriseName" data-bv-result="NOT_VALIDATED"
                                       style="display: none; color: #a94442">报名起始时间不能为空
                                </small>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="newEnterpriseEtime" class="col-md-3 control-label">报名终止时间：</label>
                            <div class="col-md-6">
                                <div id="newEnterpriseEtime" class="input-group date form_datetime"
                                     data-date-format="dd-MM-yyyy"
                                     data-link-field="dtp_input1">
                                    <input id="newetime" class="form-control" size="16" type="text" name="time"
                                           value="" readonly="" data-bv-field="time">
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                </div>
                                <small id="newETimeSmall" class="help-block" data-bv-validator="notEmpty"
                                       data-bv-for="newEnterpriseName" data-bv-result="NOT_VALIDATED"
                                       style="display: none; color: #a94442">报名结束时间不能为空
                                </small>
                            </div>
                        </div>

                        <div class="form-group has-success has-feedback">
                            <label for="newEnterprisePost" class="col-md-3 control-label">企业岗位: </label>
                            <div class="col-md-6">
                                <select id="newEnterprisePost" name="newEnterprisePost"
                                        class="selectpicker show-tick form-control" multiple data-live-search="false">

                                </select>
                            </div>
                        </div>


                        <div class="form-group has-success has-feedback">
                            <label for="newEnterpriseCategory" class="col-md-3 control-label">企业类型: </label>
                            <div class="col-md-6">
                                <select name="newEnterpriseCategory" id="newEnterpriseCategory" class="form-control">
                                    <option value="0">招聘</option>
                                    <option value="1">定制</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="newEnterpriseType" class="col-md-3 control-label">面向方向: </label>
                            <div class="col-md-6">
                                <select name="newEnterpriseType" id="newEnterpriseType" class="form-control">

                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="newProvince" class="col-md-3 control-label">企业所在地：</label>
                            <div class="col-md-6" style="padding-left: 0px;">
                                <div class="col-md-6" style="padding-right: 0px">
                                    <select id="newProvince" class="form-control" onchange="getCity()"  style="padding-right: 0px;padding-left: 7px">

                                    </select>
                                </div>
                                <div class="col-md-6" style="padding-right: 0px">
                                    <select id="newCity" class="form-control"  style="padding-right: 0px;padding-left: 7px">
                                        <option value="1">北京市</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="newEnterpriseText" class="col-md-3 control-label">企业正文: </label>
                            <div class="col-md-6">
                                 <textarea name="newEnterpriseText" id="newEnterpriseText" rows="10" cols="80">
                                 </textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearUpdateModel()">关闭</button>
                    <button type="button" class="btn btn-primary" id="update_enterprise_btn">确认修改</button>
                </div>
            </div>
        </div>
    </div>


    <%--企业正文model--%>
    <!-- Modal -->
    <div class="modal fade" id="enterpriseInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         style="padding-bottom: 50px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="enterpriseInfoModalLabel">企业介绍</h4>
                </div>
                <div class="modal-body">
                    <p name="enterpriseTextInfo" id="enterpriseTextInfo" rows="10" cols="80">
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <%--添加企业model--%>
    <div class="modal fade" id="addEnterprise" tabindex="-1" role="dialog"
         aria-labelledby="addEnterpriseModalLabel" data-keyboard="false" aria-hidden="true" data-backdrop="static" style="margin-bottom: 50px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            onclick="clearAddModel()"><span
                            aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addEnterpriseModalLabel">添加企业</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="addForm">
                        <div class="form-group has-success has-feedback">
                            <label for="enterpriseName1" class="col-md-3 control-label">企业名: </label>
                            <div class="col-md-6">
                                <input id="enterpriseName1" name="enterpriseName1" type="text" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group has-success has-feedback">
                            <label for="enterpriseStime" class="col-md-3 control-label">报名起始时间：</label>
                            <div class="col-md-6">
                                <div id="enterpriseStime" class="input-group date form_datetime"
                                     data-date-format="dd-MM-yyyy"
                                     data-link-field="dtp_input1">
                                    <input id="stime" class="form-control" size="16" type="text" name="time"
                                           value="" readonly="" data-bv-field="time">
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                </div>
                                <small id="sTimeSmall" class="help-block" data-bv-validator="notEmpty"
                                       data-bv-for="newEnterpriseName" data-bv-result="NOT_VALIDATED"
                                       style="display: none; color: #a94442">报名起始时间不能为空
                                </small>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="enterpriseEtime" class="col-md-3 control-label">报名终止时间：</label>
                            <div class="col-md-6">
                                <div id="enterpriseEtime" class="input-group date form_datetime"
                                     data-date-format="dd-MM-yyyy"
                                     data-link-field="dtp_input1">
                                    <input id="etime" class="form-control" size="16" type="text" name="time"
                                           value="" readonly="" data-bv-field="time">
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                </div>
                                <small id="eTimeSmall" class="help-block" data-bv-validator="notEmpty"
                                       data-bv-for="newEnterpriseName" data-bv-result="NOT_VALIDATED"
                                       style="display: none; color: #a94442">报名结束时间不能为空
                                </small>
                            </div>
                        </div>

                        <div class="form-group has-success has-feedback">
                            <label for="enterprisePost" class="col-md-3 control-label">企业岗位: </label>
                            <div class="col-md-6">
                                <select id="enterprisePost" name="enterprisePost"
                                        class="selectpicker show-tick form-control" multiple data-live-search="false">

                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="enterpriseCategory" class="col-md-3 control-label">企业类型: </label>
                            <div class="col-md-6">
                                <select name="enterpriseCategory" id="enterpriseCategory" class="form-control">
                                    <option value="0">招聘</option>
                                    <option value="1">定制</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="enterpriseType" class="col-md-3 control-label">面向方向: </label>
                            <div class="col-md-6">
                                <select name="enterpriseType" id="enterpriseType" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="province" class="col-md-3 control-label">企业所在地：</label>
                            <div class="col-md-6" style="padding-left: 0px;">
                                <div class="col-md-6" style="padding-right: 0px">
                                    <select id="province" class="form-control" onchange="getCity1()"  style="padding-right: 0px;padding-left: 7px">

                                    </select>
                                </div>
                                <div class="col-md-6" style="padding-right: 0px">
                                    <select id="city" class="form-control"  style="padding-right: 0px;padding-left: 7px">
                                        <option value="1">北京市</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="enterpriseText" class="col-md-3 control-label">企业正文: </label>
                            <div class="col-md-6">
                                 <textarea name="enterpriseText" id="enterpriseText" rows="10" cols="80">
                                 </textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearAddModel()">关闭</button>
                    <button type="button" class="btn btn-primary" id="add_enterprise_btn">确认添加</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="root" value="${pageContext.request.contextPath}"></c:set>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>"/>
    <script src="${root}/js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="${root}/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="${root}/bootstrap/css/bootstrapValidator.css">
    <link rel="stylesheet" href="${root}/css/content.css">
    <link rel="stylesheet" href="${root}/bootstrap/css/bootstrap-datetimepicker.css">
    <script src="${root}/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="${root}/js/pagetool.js" type="text/javascript"></script>
    <!-- 表单验证 -->
    <script src="${root}/bootstrap/js/bootstrapValidator.js" type="text/javascript"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="${root}/bootstrap/js/bootstrap-datetimepicker.js"></script>
    <script src="${root}/bootstrap/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="${root}/ckeditor/ckeditor.js"></script>
    <script src="${root}/js/bootbox.min.js"></script>


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="${root}/bootstrap/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="${root}/bootstrap/js/bootstrap-select.min.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/stu-result.js"></script>

</head>
<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>报名结果</b></li>
    </ol>
    <hr/>
    <div style="padding-left: 20px;">
        <div class="row">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                <th style="text-align: center">序号</th>
                <th style="text-align: center">企业名称</th>
                <th style="text-align: center">报名终止时间</th>
                <th style="text-align: center">类型</th>
                <th style="text-align: center">岗位</th>
                <th style="text-align: center;">录取状态</th>
                <th style="text-align: center">完善信息</th>

                </thead>
                <tbody id="result">

                </tbody>
                <tr style="display: none" id="m_template">
                    <td class="ep_num" style="text-align: center"></td>
                    <td class="ep_name" style="text-align: center"></td>
                    <td class="ep_etime" style="text-align: center"></td>
                    <td class="ep_category" style="text-align: center"></td>
                    <td class="ep_post" style="text-align: center"></td>
                    <td class="ep_state" style="text-align: center; vertical-align: middle;"></td>
                    <td class="ep_info" style="text-align: center; vertical-align: middle;"></td>
                </tr>
            </table>
        </div>
        <div class="row" style="text-align: center" id="pagination">
            <ul class="pagination pagination-lg">
                <li class="head"><a href="#">首页</a></li>
                <li class="lastpage"><a href="#">&laquo;</a></li>
                <li class="disabled morehead"><a>...</a></li>
                <li class="page-4"><a></a></li>
                <li class="page-3"><a></a></li>
                <li class="page-2"><a></a></li>
                <li class="page-1"><a></a></li>
                <li class="currentpage active"><a>1</a></li>
                <li class="page_1"><a></a></li>
                <li class="page_2"><a></a></li>
                <li class="page_3"><a></a></li>
                <li class="page_4"><a></a></li>
                <li class="disabled moretail"><a>...</a></li>
                <li class="nextpage"><a href="#">&raquo;</a></li>
                <li class="tail"><a href="#">尾页</a></li>
            </ul>
        </div>

    </div>

    <%--已完善信息model--%>
    <!-- Modal -->
    <div class="modal fade" id="alreadyCompleteInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         style="padding-bottom: 50px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">您已完善信息</h4>
                </div>
                <div class="modal-body">
                    <p name="textInfo" id="textInfo" rows="10" cols="80">
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <%--完善信息model--%>
    <div class="modal fade" id="completeInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         style="margin-bottom: 50px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            onclick="clearCompleteModel()"><span
                            aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="updateEnterpriseModalLabel">完善信息</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="completeForm">
                        <div class="form-group has-success has-feedback">
                            <label for="money" class="col-md-3 control-label">希望薪资范围: </label>
                            <div class="col-md-6">
                                        <select class="form-control" id="money" name="money">
                                    <option value="2000-3000">2000-3000</option>
                                    <option value="3000-4000">3000-4000</option>
                                    <option value="4000-5000">4000-5000</option>
                                    <option value="5000-6000">5000-6000</option>
                                    <option value="6000+">6000+</option>
                                </select>
                                <input type="hidden" id="enterpriseId"/>
                            </div>
                        </div>

                        <div class="form-group has-success has-feedback">
                            <label for="province" class="col-md-3 control-label">意向工作城市：</label>
                            <div class="col-md-6" style="padding-left: 0px;">
                                <div class="col-md-6" style="padding-right: 0px">
                                    <select id="province" class="form-control"
                                            style="padding-right: 0px;padding-left: 7px" onchange="getCity()">
                                    </select>
                                </div>
                                <div class="col-md-6" style="padding-right: 0px">
                                    <select id="city" class="form-control" style="padding-right: 0px;padding-left: 7px">
                                        <option value="1">北京市</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearCompleteModel()">关闭</button>
                    <button type="button" class="btn btn-primary" id="complete_sign_btn">确认修改</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

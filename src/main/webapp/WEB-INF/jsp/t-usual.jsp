<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>平时打分</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>

    <script src="page/pagetool.js"></script>
    <script src="js/dateformat.js"></script>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/t-usual.js"></script>
</head>

<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>平时成绩</b></li>
    </ol>
    <hr/>
    <form class="form-horizontal">
        <div class="form-group">
            <div class="col-md-3">
                <select name="stuClassId" id="stuClassId" class="form-control">
                    <option value="-1">请选择班级</option>
                    <c:if test="${stuClassId !=null&& stuClassId !=-1}">
                        <option value=" ${stuClassId}" selected="selected"> ${stuClassName}</option>
                    </c:if>
                </select>
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-primary btn-sm" id="btn-user" onclick="getPapers()"><span class="glyphicon glyphicon-search" aria-hidden="true" style="padding-right: 8px;"></span>查询</button>
            </div>
        </div>
    </form>
    <table class="table table-striped">
        <thead>
        <tr>
            <th  style="text-align: center; word-break: keep-all;">编号</th>
            <th style="text-align: center; word-break: keep-all;">班级名称</th>
            <%--<th style="text-align: center; word-break: keep-all;">学生姓名</th>--%>
            <th style="text-align: center; word-break: keep-all;">课程名称</th>
            <th style="text-align: center; word-break: keep-all;">试卷名</th>
           <%-- <th style="text-align: center; word-break: keep-all;">平时成绩</th>--%>
           <%-- <th style="text-align: center; word-break: keep-all;">总成绩</th>--%>
            <th style="text-align: center; word-break: keep-all;">打分</th>
        </tr>
        </thead>
        <tbody id="result"></tbody>
        <tr style="display: none" id="m_template">
            <td class="p_id" style="text-align: center"></td>
            <td class="p_classId" style="text-align: center"></td>
           <%-- <td class="p_stuName" style="padding: 2px; text-align: center"></td>--%>
            <td class="p_courseName" style="text-align: center"></td>
            <td class="p_testName" style="text-align: center"></td>
           <%-- <td class="p_scoreM" style="padding: 2px; text-align: center"></td>--%>
           <%-- <td class="p_sum" style="padding: 2px; text-align: center"></td>--%>
            <td class="center" id="choice" style="text-align: center">
                <a class="btn btn-info btn-xs" id="enter" href="" style="padding: 0 2px;">
                    <span class="glyphicon glyphicon-edit" style="padding-right: 6px;"></span>
                    开始打分
                </a>
            </td>
        </tr>
    </table>
    <div class="row" style="text-align: center">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>
</div>

</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>首页</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>

    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/echarts.min.js"></script>
    <script src="js/roma.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/edu-setting.js"></script>

</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>全局设置</b></li>
    </ol>
    <hr/>

    <form class="form-horizontal" style="padding-bottom: 30px;">
        <div class="form-group has-success has-feedback">
            <label class="col-md-2 control-label">注册功能: </label>
            <div class="col-md-2">
            <button type="button" class="btn btn-sm" onclick="updateRegisterState()">
                <span id="registerIcon" class="glyphicon glyphicon-ok-circle" aria-hidden="true" style="margin-right: 6px;"></span><span id="registerState" ></span>
            </button>
            </div>
        </div>
    </div>
</div>
</body>
</html>

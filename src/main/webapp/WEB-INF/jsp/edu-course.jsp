<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>"/>
    <script src="${root}/js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="${root}/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="${root}/bootstrap/css/bootstrapValidator.css">
    <link rel="stylesheet" href="${root}/bootstrap/css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="${root}/css/content.css">
    <script src="${root}/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="${root}/js/pagetool.js" type="text/javascript"></script>
    <script src="${root}/js/bootbox.min.js" type="text/javascript"></script>
    <script src="js/bootbox.min.js"></script>
    <!-- 表单验证 -->
    <script src="${root}/bootstrap/js/bootstrapValidator.js" type="text/javascript"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/edu-course.js"></script>
</head>
<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>课程管理</b></li>
    </ol>
    <hr/>
    <div style="padding-left: 20px;">
        <div id="after_div" class="row" style="padding-bottom: 30px;">
            <button type='button' class='btn btn-primary btn-sm btn-success' data-toggle='modal'
                    data-target='#addCourse'>
                <span class="glyphicon glyphicon-plus" aria-hidden="true" style="padding-right: 6px;"></span>添加课程
            </button>
        </div>
        <div id="table" class="row">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                <th style="text-align: center">序号</th>
                <th style="text-align: center">课程名</th>
                <th style="text-align: center">修改</th>
                <th style="text-align: center">删除</th>

                </thead>
                <tbody id="result">

                </tbody>
                <tr style="display: none" id="m_template">
                    <td class="c_num" style="text-align: center"></td>
                    <td class="c_name" style="text-align: center"></td>
                    <td class="c_update" style="text-align: center"></td>
                    <td class="c_operate" style="text-align:center"></td>
                </tr>
            </table>
        </div>
        <div class="row" style="text-align: center" id="pagination">
            <ul class="pagination pagination-lg">
                <li class="head"><a href="#">首页</a></li>
                <li class="lastpage"><a href="#">&laquo;</a></li>
                <li class="disabled morehead"><a>...</a></li>
                <li class="page-4"><a></a></li>
                <li class="page-3"><a></a></li>
                <li class="page-2"><a></a></li>
                <li class="page-1"><a></a></li>
                <li class="currentpage active"><a>1</a></li>
                <li class="page_1"><a></a></li>
                <li class="page_2"><a></a></li>
                <li class="page_3"><a></a></li>
                <li class="page_4"><a></a></li>
                <li class="disabled moretail"><a>...</a></li>
                <li class="nextpage"><a href="#">&raquo;</a></li>
                <li class="tail"><a href="#">尾页</a></li>
            </ul>
        </div>

    </div>

    <%--修改课程model--%>
    <div class="modal fade" id="changCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" onclick="clearUpdateModel()">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">修改课程</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="updateForm">
                        <div class="form-group has-success has-feedback">
                            <label for="newCourseName" class="col-md-3 control-label">新课程名称: </label>
                            <div class="col-md-6">
                                <input id="newCourseName" name="newCourseName" type="text" class="form-control"/>
                                <input type="hidden" id="newCourseNameId"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearUpdateModel()">关闭</button>
                    <button type="button" class="btn btn-primary" id="update_course_btn">确认修改</button>
                </div>
            </div>
        </div>
    </div>
    <%--end修改班级model--%>

    <%--添加课程model--%>
    <div class="modal fade" id="addCourse" tabindex="-1" role="dialog"
         aria-labelledby="addClassModalLabel">
        <div class="modal-dialog dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" onclick="clearAddModel()">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addCourseModalLabel">添加课程</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="addForm">
                        <div class="form-group has-success has-feedback">
                            <label for="courseName1" class="col-md-3 control-label">课程名: </label>
                            <div class="col-md-6">
                                <input id="courseName1" name="courseName1" type="text" class="form-control"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearAddModel()">关闭</button>
                    <button type="button" class="btn btn-primary" id="add_course_btn">确认添加</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

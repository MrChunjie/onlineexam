<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>首页</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="js/common/edu-stu-detail.js"></script>

</head>
<body>

<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>学生管理</b></li>
        <li><b>学生详细信息</b></li>
    </ol>
    <hr/>
    <button class='btn btn-primary btn-sm' id="return_btn" style="margin-bottom: 20px;">回到学生列表</button>
    <table class="table" style="margin-bottom: 40px;">
        <tbody>
            <tr>
                <td class="s_name" colspan="2"><b>姓名 ： </b>${student.name} </td>
                <td class="s_sex"><b>性别 ：</b>${student.sex} </td>
                <td class="s_nation"><b>民族 ： </b>${student.nation}</td>
            </tr>
            <tr>
                <td class="s_new_id"><b>慧与学号 ：</b>${student.newId}</td>
                <td class="s_spare"><b>慧与班级 ： </b>${student.spare}</td>
                <td class="s_dorm"><b>宿舍号 ： </b>${student.dorm}</td>
                <td class="s_direction"><b>方向 ： </b>${student.direction}</td>
            </tr>
            <tr>
                <td class="s_state"><b>状态 ：</b>
                    <c:choose>
                        <c:when test="${student.state == 0}">
                            未报到
                        </c:when>
                        <c:when test="${student.state == 1}">
                            在读
                        </c:when>
                        <c:when test="${student.state == 2}">
                            毕业
                        </c:when>
                        <c:otherwise>
                            退学
                        </c:otherwise>
                    </c:choose>
                </td>
                <td class="s_tel"><b>电话 ：</b>${student.tel}</td>
                <td class="s_number" colspan="2"><b>身份证号 ：</b>${student.number}</td>
            </tr>
            <tr>
                <td class="s_school"><b>原学校 ： </b>${student.school}</td>
                <td class="s_college"><b>原学院 ： </b>${student.college}</td>
                <td class="s_major"><b>原专业 ：</b>${student.major} </td>
                <td class="s_school_class"><b>原班级 ：</b>${student.schoolClass} </td>
            </tr>
            <tr>
                <td class="s_school_id"><b>原学号 ：</b>${student.schoolId} </td>
                <td class="s_job"><b>原学校职务 ：</b>${student.job} </td>
                <td class="s_english"><b>外语水平 ： </b>${student.english}</td>
                <td class="s_stu_order"><b>目前意向 ： </b>${student.stuOrder}</td>
            </tr>
            <tr>
                <td class="s_parent" colspan="4"><b>父母姓名及联系电话 ： </b>${student.parent}</td>
            </tr>
            <tr>
                <td class="s_home" colspan="4"><b>家庭住址 ：</b> ${student.home}</td>
            </tr>
            <tr>
                <td class="s_counselor" colspan="4"><b>辅导员姓名及联系电话 ：</b>${student.counselor}</td>
            </tr>
            <tr>
                <td class="s_skill" colspan="4"><b>个人特长及获奖情况 ： </b>${student.skill}</td>
            </tr>
            <tr>
                <td class="s_restudy_num"><b>重修课数量 ：</b>${student.restudynum} </td>
                <td class="s_restudy_name" colspan="2"><b>重修课名字 ：</b>${student.restudyname} </td>
                <td class="s_restudy_time"><b>预计补考时间 ： </b>${student.restudytime}</td>
            </tr>
        </tbody>
    </table>
</div>
</body>
</html>

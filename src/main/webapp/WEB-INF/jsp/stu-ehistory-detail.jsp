<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>历史成绩</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="js/dateformat.js"></script>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/loading.js"></script>
    <script type="text/javascript" src="js/common/stu-ehistory-detail.js"></script>

</head>

<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>历史成绩</b></li>
        <li><b>试卷详情</b></li>
    </ol>
    <hr/>
    <div class="row content" style="padding-left: 30px;">
        <p><h4>一、单选题</h4></p>
        <div style="display: none" id="p_template0">
            <p><B id="p_title0"></B></p>
            <div class="radio" style="margin: 10px">
                <label id="p_choiceA"></label>
                <span class='label label-success' id="correctA"></span>
            </div>
            <div class="radio" style="margin: 10px">
                <label id="p_choiceB"></label>
                <span class='label label-success' id="correctB"></span>
            </div>
            <div class="radio" style="margin: 10px">
                <label id="p_choiceC"></label>
                <span class='label label-success' id="correctC"></span>
            </div>
            <div class="radio" style="margin: 10px">
                <label id="p_choiceD"></label>
                <span class='label label-success' id="correctD"></span>
            </div>
        </div>

        <div style="padding-left: 30px;">
            <div id="result0">
            </div>
        </div>

        <div style="display: none" id="p_template1">
            <div class="form-group" style="padding-left: 15px;">
                <p><b id="p_title1" style="font-size: 15px;"></b></p>
            </div>
            <div class="form-group">
                <div style="padding-left: 20px;">
                    <div class="row">
                        <b>你的作答：</b>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <div style="background-color: rgba(255,255,255,1);border-radius: 8px; padding: 15px; border: 2px solid rgba(221, 221, 221, 0.6);">
                                <span id="p_ans1"></span>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <b>参考答案：</b>
                    </div>
                    <div class="row">
                        <div class="col-md-11" style="white-space:pre-wrap;">
                            <div style="background-color: rgba(255,255,255,1);border-radius: 8px; padding: 15px; border: 2px solid rgba(221, 221, 221, 0.6); ">
                                <span id="p_correct_ans1"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="padding-left: 30px;">
            <div id="result1">
            </div>
        </div>

        <div style="display: none" id="p_template2">
            <div class="form-group" style="padding-left: 15px;">
                <p><b id="p_title2" style="font-size: 15px;"></b></p>
            </div>
            <div class="form-group">
                <div style="padding-left: 20px;">
                    <div class="row">
                        <b>你的作答：</b>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <div style="background-color: rgba(255,255,255,1);border-radius: 8px; padding: 15px; border: 2px solid rgba(221, 221, 221, 0.6);">
                                <span id="p_ans2"></span>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <b>参考答案：</b>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <div style="background-color: rgba(255,255,255,1);border-radius: 8px; padding: 15px; border: 2px solid rgba(221, 221, 221, 0.6);">
                                <span id="p_correct_ans2"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="padding-left: 30px;">
            <div id="result2">
            </div>
        </div>
    </div>
</div>

</body>
</html>

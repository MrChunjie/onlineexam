<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="font/css/font-awesome.css">
    <script src="js/jquery-2.0.3.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap-select.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/edu-group.js"></script>
</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>

    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>分班管理</b></li>
        <div style="float: right;">
            <button class="btn btn-warning btn-xs" type="button" data-toggle="collapse" data-target="#collapseDivide"
                    aria-expanded="false" aria-controls="collapseExample">
                <span class="glyphicon glyphicon-file" aria-hidden="true" style="padding-right: 5px;"></span>分班流程说明
            </button>
        </div>
    </ol>
    <hr/>


    <div class="collapse" id="collapseDivide">
        <div class="well" style="font-family: 楷体,serif; font-size: 15px; font-weight: bolder;">
            1.发布面向某方向(如开发)所有班级的考试;<br/>
            2.待考试完毕、批改完毕后进行分班;<br/>
            3.选择开发/测试方向，选择依据的考试进行成绩汇总;<br/>
            4.在成绩汇总后可下载查看汇总结果并补充面试成绩,下载得到的excel文件中的前两列id不允许更改;<br/>
            5.在有面试的情况下可将面试成绩导入;<br/>
            6.创建新的班级;<br/>
            7.注销原班级;<br/>
            8.在该条汇总右侧点击分班;<br/>
            9.选择分班规则及分班后到达的班级，点击分班;<br/>
            10.等待分班结束并下载分班表格。<br/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <form class="form-horizontal" style="padding-bottom: 10px;">
                <div class="form-group">
                    <label class="col-md-1 control-label" for="direction">方向: </label>
                    <div class="col-md-2">
                        <select class="form-control" id="direction" onchange="findExam()">
                        </select>
                    </div>
                    <label class="col-md-1 control-label">考试: </label>
                    <div class="col-md-3">
                        <select style="height: 100px;" class="selectpicker form-control" id="exam" multiple
                                data-live-search="false">
                        </select>
                    </div>
                    <div class="col-md-offset-1 col-md-2">
                        <button type="button" class="btn btn-success btn-block btn-md"
                                onclick="return summaryExam()" data-toggle="tooltip" data-placement="bottom"
                                title="<p align='left'><span style='white-space:nowrap;'>1.面向于该方向现全部班级的考试</span>2.且该考试已结束</b><p/>" data-html="true"><span
                                class="glyphicon glyphicon-list-alt" aria-hidden="true"
                                style="padding-right: 10px;"></span>汇总
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-pull-1 col-md-2">
            <div class="col-md-pull-4 col-md-12">
                <a class="btn btn-primary btn-md btn-block"><span
                        class='glyphicon glyphicon-upload' aria-hidden='true' style='padding-right: 8px;'></span>导入
                    <div style="filter:alpha(opacity=0);cursor: pointer; opacity: 0; position: absolute;  width: 110px;margin: -18px 0 0 -6px;height:20px;overflow: hidden; ">
                        <form id="updateForm" enctype="multipart/form-data">
                            <input type="file" id="txtFile" onchange="toUpload()" name="file"
                                   style="font-size: 200px;cursor: pointer;direction: rtl !important; float: right\9; "/>
                        </form>
                    </div>
                </a>
            </div>
        </div>

    </div>
    <table id="table" class="table table-striped table-hover table-condensed">
        <thead>
        <tr>
            <th style="text-align: center;">汇总编号</th>
            <th style="text-align: center" class="col-md-4">依据考试</th>
            <th style="text-align: center">方向</th>
            <th style="text-align: center">汇总时间</th>
            <th style="text-align: center">面试成绩</th>
            <th style="text-align: center">操作</th>
        </tr>
        </thead>
        <tbody id="items">

        </tbody>
        <tr id="temp" style="display: none">
            <th id="id" style="text-align: center; vertical-align: middle;"></th>
            <td id="exams" style="text-align: center; vertical-align: middle;"></td>
            <td id="dir" style="text-align: center; vertical-align: middle;"></td>
            <td id="time" style="text-align: center; vertical-align: middle;"></td>
            <td id="enter" style="text-align: center; vertical-align: middle;"></td>
            <td id="btn" style="text-align: center; vertical-align: middle;"></td>
            <td id="b" style="text-align: center; vertical-align: middle;"></td>
        </tr>
    </table>
    <div id="pagination" class="row" style="text-align: center">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>
</div>
</body>
</html>

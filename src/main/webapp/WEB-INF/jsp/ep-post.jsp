<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/ep-post.js"></script>

</head>
<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>岗位管理</b></li>
    </ol>
    <hr/>
    <div style="padding-left: 20px;">
        <div class="row" style="padding-bottom: 20px;">
            <button type='button' class='btn btn-success btn-sm' data-toggle='modal' data-target='#addClass'>
                <span class="glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 6px;"></span>添加岗位
            </button>
        </div>
        <div class="row">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                <th style="text-align: center">序号</th>
                <th style="text-align: center">岗位名</th>
                <th style="text-align: center">修改名字</th>
                <th style="text-align: center">操作</th>
                </thead>
                <tbody id="result">

                </tbody>
                <tr style="display: none" id="m_template">
                    <td class="c_num" style="text-align: center"></td>
                    <td class="c_name" style="text-align: center"></td>
                    <td class="c_update" style="text-align: center"></td>
                    <td class="c_op" style="text-align: center"></td>
                </tr>
            </table>
        </div>
        <div id="pagination" class="row" style="text-align: center">
            <ul class="pagination pagination-lg">
                <li class="head"><a href="#">首页</a></li>
                <li class="lastpage"><a href="#">&laquo;</a></li>
                <li class="disabled morehead"><a>...</a></li>
                <li class="page-4"><a></a></li>
                <li class="page-3"><a></a></li>
                <li class="page-2"><a></a></li>
                <li class="page-1"><a></a></li>
                <li class="currentpage active"><a>1</a></li>
                <li class="page_1"><a></a></li>
                <li class="page_2"><a></a></li>
                <li class="page_3"><a></a></li>
                <li class="page_4"><a></a></li>
                <li class="disabled moretail"><a>...</a></li>
                <li class="nextpage"><a href="#">&raquo;</a></li>
                <li class="tail"><a href="#">尾页</a></li>
            </ul>
        </div>

    </div>

    <%--修改岗位model--%>
    <div class="modal fade" id="changClass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clearUpdateModal()"><span
                            aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">修改岗位</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="updateForm">
                        <div class="form-group has-success has-feedback">
                            <label for="newClassName" class="col-md-3 control-label">新名字: </label>
                            <div class="col-md-6">
                                <input id="newClassName" name="newClassName" type="text" class="form-control"/>
                                <input type="hidden" id="hello"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearUpdateModal()">关闭</button>
                    <button type="button" class="btn btn-primary" id="update_class_btn">确认修改</button>
                </div>
            </div>
        </div>
    </div>
    <%--end修改岗位model--%>

    <%--添加岗位model--%>
    <div class="modal fade" id="addClass" tabindex="-1" role="dialog"
         aria-labelledby="addClassModalLabel">
        <div class="modal-dialog dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" onclick="clearAddModal()">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addClassModalLabel">添加岗位</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="addForm">
                        <div class="form-group has-success has-feedback">
                            <label for="className1" class="col-md-3 control-label">岗位名: </label>
                            <div class="col-md-6">
                                <input id="className1" name="className1" type="text" class="form-control"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearAddModal()">关闭</button>
                    <button type="button" class="btn btn-primary" id="add_class_btn">确认添加</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

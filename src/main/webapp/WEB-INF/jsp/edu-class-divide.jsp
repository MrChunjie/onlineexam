<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="font/css/font-awesome.css">
    <script src="js/jquery-2.0.3.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap-select.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/edu-class-divide.js"></script>

    <script type="text/javascript">
        var allStudentCount = 0;
        var width = 0;
        var stu_count = 0;

        $(function () {
            $(".s_curve_div").hide();
            $(".s_three_div").hide();
            $("#divide_class_btn").hide();

            getStudentCount();

            getDirectionId();
        });


        function getStudentCount() {
            $.ajax({
                url: '<%=basePath%>edu/group/get_count.do',
                type: 'GET',
                data: {
                    "groupId": "<c:out value='${groupId}'/>"
                },
                dataType: 'json',
                success: function (res) {
                    if (res.success) {
                        allStudentCount = res.data;
                        $("#stu_count").html(res.data);
                        stu_count = res.data;
                        $("#level_stu_count").html(res.data);
                        $("#three_level_stu").val(res.data);
                    }
                }
            })
        }

        function getDirectionId() {
            $.ajax({
                url: '<%=basePath%>edu/group/get_direction.do',
                type: 'GET',
                data: {
                    "groupId": "<c:out value='${groupId}'/>"
                },
                dataType: 'json',
                success: function (res) {
                    if (res.success) {
                        removeLoading();
                        getAllClassByDirection(res.data);
                    }
                }
            })
        }

        function divideClassBySCurve() {

            var className = $(".filter-option").html();
            var classId = $("#class_num").selectpicker("val");

            if (classId == null || classId == "") {
                bootbox.alert("请选择班级");
                return;
            }

            classId = classId.toString();

            bootbox.confirm({
                title: "确认分班?",
                message: "<b>班级</b>: " + className + "<br/><br/>",
                buttons: {
                    cancel: {
                        label: '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 取消'
                    },
                    confirm: {
                        label: '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 确认'
                    }
                },
                callback: function (result) {
                    if (classId == "") {
                        bootbox.alert('请至少选择一个班级');
                    } else {
                        if (result) {
                            var dialog = bootbox.dialog({
                                title: '请稍候',
                                message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 分班中...</p>'
                            });

                            $.ajax({
                                url: basePath() + '/edu/class/divide_class_scurve.do',
                                type: 'GET',
                                data: {
                                    "classIds": classId,
                                    "groupId": "<c:out value='${groupId}'/>"
                                },
                                dataType: 'json',
                                success: function (res) {
                                    if (res.success) {
                                        dialog.find('.modal-title').html('完成');
                                        dialog.find('.modal-body').html(res.msg + "，请下载分班结果，如果不进行下载，文件可能会丢失");
                                        dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\" onclick='getDivideResult()'>下载</button></div>");
                                    } else {
                                        dialog.find('.modal-title').html('错误');
                                        dialog.find('.modal-body').html(res.msg);
                                        dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\">OK</button></div>");
                                    }
                                }
                            });
                        }
                    }
                }
            });
        }

        function divideByThreeStage() {

            var one_level_class = $("#one_level .filter-option").html();
            var two_level_class = $("#two_level .filter-option").html();
            var three_level_class = $("#three_level .filter-option").html();

            var one_level_class_id = $("#one_level .selectpicker").selectpicker("val");
            var one_level_divide_type = $("#one_level_type :selected").val();
            var one_level_stu_num = $("#one_level_stu").val();

            var two_level_class_id = $("#two_level .selectpicker").selectpicker("val");
            var two_level_divide_type = $("#two_level_type :selected").val();
            var two_level_stu_num = $("#two_level_stu").val();

            var three_level_class_id = $("#three_level .selectpicker").selectpicker("val");
            var three_level_divide_type = $("#three_level_type :selected").val();
            var three_level_stu_num = $("#three_level_stu").val();

            if (one_level_stu_num == "" || two_level_stu_num == "" || three_level_stu_num == "") {
                bootbox.alert("请输入班级总人数");
                return;
            }

            if (one_level_stu_num < 0 || two_level_stu_num < 0 || three_level_stu_num < 0) {
                bootbox.alert("班级人数不能为负数");
                return;
            }

            if (isNaN(one_level_stu_num) || isNaN(two_level_stu_num) || isNaN(three_level_stu_num)) {
                bootbox.alert("班级人数应为纯数字");
                return;
            }

            if ((one_level_class_id == null && one_level_stu_num != 0)
                || (two_level_class_id == null && two_level_stu_num != 0)
                || (three_level_class_id == null && three_level_stu_num != 0)) {
                bootbox.alert("请选择班级");
                return;
            }

//            var one_level_class_array = [];
//            var two_level_class_array = [];
//            var three_level_class_array = [];
//
//
//            if (one_level_class_id != null) {
//                one_level_class_id = one_level_class_id.toString();
//                one_level_class_array = one_level_class_id.split(",");
//            }
//            if (two_level_class_id != null) {
//                two_level_class_id = two_level_class_id.toString();
//                two_level_class_array = two_level_class_id.split(",");
//            }
//            if (three_level_class_id != null) {
//                three_level_class_id = three_level_class_id.toString();
//                three_level_class_array = three_level_class_id.split(",");
//            }
//
//            console.info(one_level_class_array + ";" + two_level_class_array);
//
//            for (var i = 0; i < one_level_class_array.length; i++) {
//                for (var j = 0; j < two_level_class_array.length; j++) {
//                    for (var k = 0; k < three_level_class_array.length; k++) {
//                        if (one_level_class_array[i] == two_level_class_array[j] ||
//                            one_level_class_array[i] == three_level_class_array[k] ||
//                            two_level_class_array[j] == three_level_class_array[k]
//                        ) {
//                            bootbox.alert("每阶段班级不能重复");
//                            return;
//                        }
//                    }
//                }
//            }

            one_level_class_id = one_level_class_id == null? one_level_class_id : one_level_class_id.toString();
            two_level_class_id = two_level_class_id == null ? two_level_class_id :two_level_class_id.toString();
            three_level_class_id = three_level_class_id == null ? three_level_class_id :three_level_class_id.toString();

            bootbox.confirm({
                title: "确认分班?",
                message: "<b>大神班</b>: " + (one_level_class == '请选择班级' ? '未设置' : one_level_class)
                + "<br/><br/><b>提升班</b>:" + (two_level_class == '请选择班级' ? '未设置' : two_level_class)
                + "<br/><br/><b>普通班</b>:" + (three_level_class == '请选择班级' ? '未设置' : three_level_class),
                buttons: {
                    cancel: {
                        label: '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 取消'
                    },
                    confirm: {
                        label: '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 确认'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var dialog = bootbox.dialog({
                            title: '请稍候',
                            message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 分班中...</p>'
                        });
                        $.ajax({
                            url: basePath() + '/edu/class/divide_class_three.do',
                            type: 'GET',
                            data: {
                                "oneId": one_level_class_id,
                                "oneType": one_level_divide_type,
                                "oneStu": one_level_stu_num,
                                "twoId": two_level_class_id,
                                "twoType": two_level_divide_type,
                                "twoStu": two_level_stu_num,
                                "threeId": three_level_class_id,
                                "threeType": three_level_divide_type,
                                "threeStu": three_level_stu_num,
                                "groupId": "<c:out value='${groupId}'/>"
                            },
                            dataType: 'json',
                            success: function (res) {
                                if (res.success) {
                                    dialog.find('.modal-title').html('完成');
                                    dialog.find('.modal-body').html(res.msg + "，请下载分班结果，如果不进行下载，文件可能会丢失");
                                    dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\" onclick='getDivideResult()'>下载</button></div>");
                                } else {
                                    dialog.find('.modal-title').html('错误');
                                    dialog.find('.modal-body').html(res.msg);
                                    dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\">OK</button></div>");
                                }
                            }
                        });
                    }
                }
            });
        }

        function getDivideResult() {

            var dialog = bootbox.dialog({
                title: '请稍候',
                message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 导出中...</p>'
            });

            var url = basePath() + '/edu/group/output_divide_excel.do' + "?id=" + "<c:out value='${groupId}'/>";
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);        // 也可以使用POST方式，根据接口
            xhr.responseType = "blob";    // 返回类型blob
            // 定义请求完成的处理函数，请求前也可以增加加载框/禁用下载按钮逻辑
            xhr.onload = function () {
                // 请求完成
                if (this.status === 200) {
                    // 返回200
                    var time = this.getResponseHeader("Content-Disposition");
                    if (time != null || time != "") {
                        time = time.substring(time.indexOf("filename=")+9);
                    }
                    var blob = this.response;
                    var reader = new FileReader();
                    reader.readAsDataURL(blob);    // 转换为base64，可以直接放入a表情href
                    reader.onload = function (e) {
                        console.info(e);
                        // 转换完成，创建一个a标签用于下载
                        var a = document.createElement('a');
                        a.download = "学生分班表-" + time +'.xlsx';
                        a.href = e.target.result;
                        $("body").append(a);    // 修复firefox中无法触发click
                        a.click();
                        $(a).remove();
                        dialog.find('.modal-title').html('完成');
                        dialog.find('.modal-body').html("导出成功");
                        dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\">OK</button></div>");

                    }
                }
            };
            // 发送ajax请求
            xhr.send();
        }

        function cal() {
            var sum = 0;
            if ($('#two_level_stu').val() != "" && $('#one_level_stu').val() != "") {
                sum = parseInt($('#one_level_stu').val()) + parseInt($('#two_level_stu').val());
            } else if ($('#two_level_stu').val() != "") {
                sum = parseInt($('#two_level_stu').val());
            } else if ($('#one_level_stu').val() != "") {
                sum = parseInt($('#one_level_stu').val());
            }

            var result = stu_count - sum;

            if (result < 0) {
                bootbox.alert("超过学生总人数");
                $("#one_level_stu").val("");
                $("#two_level_stu").val("");
                $("#three_level_stu").val(stu_count);
                return;
            }

            $('#three_level_stu').val(stu_count - sum);
        }


    </script>
</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>分班管理</b></li>
        <li><b>分班</b></li>
    </ol>
    <hr/>
    <div class="well well-sm">
        <b>学生总人数为：</b><span id="stu_count" class="label label-success"></label>">0</span> | <b>分班剩余学生：</b><span
            id="level_stu_count" class="label label-success">0</span>
    </div>
    <div class="row">
        <form class="form-horizontal bv-form " id="divide_class_form" style="padding-bottom: 50px;">
            <div class="form-group has-success has-feedback">
                <label for="divide_class_select" class="col-md-2 control-label">选择分班方式: </label>
                <div class="col-md-3">
                    <select class="form-control" id="divide_class_select" onchange="getDetail()">
                        <option>选择分班方式</option>
                        <option value="s_curve">S曲线分班</option>
                        <option value="three_stage">三段式分班</option>
                    </select>
                </div>
            </div>
            <div class="form-group has-success has-feedback s_curve_div">
                <label for="class_num" class="col-md-2 control-label">选择班级</label>
                <div class="col-md-3">
                    <div class="input-group">
                        <select id="class_num" class="selectpicker form-control" multiple data-live-search="false">
                        </select>
                        <span class="input-group-addon select-all">
                            <a href="javascript: void(0);" onclick="selectAll()" class="select-btn">所有</a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group has-success has-feedback s_three_div">
                <label for="one_level_type" class="col-md-2 control-label">大神/卓越班</label>
                <div class="col-md-3">
                    <select class="form-control" id="one_level_type">
                        <option value="line">直线</option>
                        <option value="curve">S曲线</option>
                    </select>
                </div>
                <div class="col-md-3" id="one_level">
                    <select type="text" id="one_level_class" class="selectpicker form-control" multiple
                            data-live-search="false" onchange="selectCLass('one_level_class')">
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="text" id="one_level_stu" placeholder="班级总容量" class="form-control" onchange="cal()">
                </div>
            </div>
            <div class="form-group has-success has-feedback s_three_div">
                <label for="two_level_type" class="col-md-2 control-label">提升班</label>
                <div class="col-md-3">
                    <select class="form-control" id="two_level_type">
                        <option value="line">直线</option>
                        <option value="curve">S曲线</option>
                    </select>
                </div>
                <div class="col-md-3" id="two_level">
                    <select id="two_level_class" class="selectpicker form-control" multiple data-live-search="false"
                             onchange="selectCLass('two_level_class')">
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="text" id="two_level_stu" placeholder="班级总容量" class="form-control" onchange="cal()">
                </div>
            </div>
            <div class="form-group has-success has-feedback s_three_div">
                <label for="three_level_type" class="col-md-2 control-label">普通班</label>
                <div class="col-md-3">
                    <select class="form-control" id="three_level_type">
                        <option value="line">直线</option>
                        <option value="curve">S曲线</option>
                    </select>
                </div>
                <div class="col-md-3" id="three_level">
                    <select id="three_level_class" class="selectpicker form-control" multiple data-live-search="false"
                             onchange="selectCLass('three_level_class')">
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="text" id="three_level_stu" placeholder="班级总容量" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-4 col-md-4">
                    <button type="button" class="btn btn-primary btn-block" id="divide_class_btn"
                            onclick="divideBtnOnclick()" style="margin-top: 40px;">确认分班
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>

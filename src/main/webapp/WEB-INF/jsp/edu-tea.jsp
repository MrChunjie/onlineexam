<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.css">
    <link rel="stylesheet" href="css/content.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-datetimepicker.css">
    <script src="js/jquery-2.0.3.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/pagetool.js" type="text/javascript"></script>
    <script src="bootstrap/js/bootstrapValidator.js" type="text/javascript"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/edu-tea.js"></script>
</head>
<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>教师管理</b></li>
    </ol>
    <hr/>
    <div style="padding-left: 20px;">
        <div class="row" style="padding-bottom: 30px;">
            <button type='button' class='btn btn-primary btn-sm btn-success' data-toggle='modal'
                    data-target='#addTeacher'>
                <span class="glyphicon glyphicon-plus" aria-hidden="true" style="padding-right: 6px;"></span>添加教师
            </button>
        </div>
        <div class="row">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                <th style="text-align: center">序号</th>
                <th style="text-align: center" class="col-md-5">教师用户名</th>
                <th style="text-align: center">修改</th>
                <th style="text-align: center">删除</th>
                <th style="text-align: center">重置</th>

                </thead>
                <tbody id="result">

                </tbody>
                <tr style="display: none" id="m_template">
                    <td class="t_num" style="text-align: center"></td>
                    <td class="t_name" style="text-align: center"></td>
                    <td class="t_update" style="text-align: center"></td>
                    <td class="t_operate" style="text-align: center"></td>
                    <td class="t_reset" style="text-align: center"></td>
                </tr>
            </table>
        </div>
        <div class="row" style="text-align: center" id="pagination">
            <ul class="pagination pagination-lg">
                <li class="head"><a href="#">首页</a></li>
                <li class="lastpage"><a href="#">&laquo;</a></li>
                <li class="disabled morehead"><a>...</a></li>
                <li class="page-4"><a></a></li>
                <li class="page-3"><a></a></li>
                <li class="page-2"><a></a></li>
                <li class="page-1"><a></a></li>
                <li class="currentpage active"><a>1</a></li>
                <li class="page_1"><a></a></li>
                <li class="page_2"><a></a></li>
                <li class="page_3"><a></a></li>
                <li class="page_4"><a></a></li>
                <li class="disabled moretail"><a>...</a></li>
                <li class="nextpage"><a href="#">&raquo;</a></li>
                <li class="tail"><a href="#">尾页</a></li>
            </ul>
        </div>

    </div>

    <%--修改教师model--%>
    <div class="modal fade" id="changTeacher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" onclick="clearUpdateModel()">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">修改教师</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="updateForm">
                        <div class="form-group has-success has-feedback">
                            <label for="newTeacherName" class="col-md-3 control-label">新教师用户名: </label>
                            <div class="col-md-6">
                                <input id="newTeacherName" name="newTeacherName" type="text" class="form-control"/>
                                <input type="hidden" id="newTeacherNameId"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearUpdateModel()">关闭</button>
                    <button type="button" class="btn btn-primary" id="update_teacher_btn">确认修改</button>
                </div>
            </div>
        </div>
    </div>
    <%--end修改班级model--%>

    <%--添加教师model--%>
    <div class="modal fade" id="addTeacher" tabindex="-1" role="dialog"
         aria-labelledby="addClassModalLabel">
        <div class="modal-dialog dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" onclick="clearAddModel()">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addClassModalLabel">添加教师</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="addForm">
                        <div class="form-group has-success has-feedback">
                            <label for="teacherName1" class="col-md-3 control-label">教师用户名: </label>
                            <div class="col-md-6">
                                <input id="teacherName1" name="teacherName1" type="text" class="form-control"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="clearAddModel()">关闭</button>
                    <button type="button" class="btn btn-primary" id="add_teacher_btn">确认添加</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

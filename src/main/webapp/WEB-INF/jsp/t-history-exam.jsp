<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>
    <base href="<%=basePath%>">
    <meta name="viewport" charset="utf-8"/>
    <title>历史考试</title>
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/content.css"/>

    <script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootbox.min.js"></script>

    <script src="${pageContext.request.contextPath}/js/echarts.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/essos.js"></script>
    <script src="${pageContext.request.contextPath}/js/walden.js"></script>
    <script src="${pageContext.request.contextPath}/js/roma.js"></script>
    <script src="${pageContext.request.contextPath}/js/jsPdf.debug.js"></script>
    <script src="${pageContext.request.contextPath}/js/html2canvas.js"></script>
    <script src="${pageContext.request.contextPath}/js/echarts-wordcloud.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/t-history-exam.js"></script>
</head>

<body id="app-layout">
<div id="father" class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>历史考试</b></li>
    </ol>
    <hr id="hr"/>

    <!-- 内容-->
    <div class="container">
        <div class="row">
            <div id="info">
                <button id="download" class="btn btn-success"
                        onclick="download('${examinfo.name}')"><span
                        class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>&nbsp;&nbsp;试卷打印
                </button>
                <p>
                <h1 align="center">试卷：${examinfo.name}</h1></p>
                <p>
                <h3 align="center">考试课程：${course.name }</h3></p>
                <p><h4 align="center">考试班级：</h4>
                <h5 align="center">
                    <c:forEach items="${sclass}" var="sclass">
                        ${sclass.name }&nbsp;
                    </c:forEach>
                </h5>
                <p><h4 align="center">考试时间:<fmt:formatDate value="${examinfo.starttime}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate> </h4> </p>
                <p><h4 align="center"> 考试时长:${examinfo.testtime }分钟&nbsp;&nbsp;
                分值:${examinfo.scores }分</h4></p>
                <p><h4>一、单选题（共${queListSize }题，每题${examinfo.score/queListSize  }分）</h4></p>
                <c:forEach items="${queList }" var="q" varStatus="i">
                    <p><B>${i.index + 1}、${q.quetitle }&nbsp;</B></p>
                    A.<c:out value = "${q.choicea }" escapeXml="true"/> &nbsp;&nbsp;&nbsp;
                    B.<c:out value = "${q.choiceb }" escapeXml="true"/>&nbsp;&nbsp;&nbsp;
                    C.<c:out value = "${q.choicec }" escapeXml="true"/>&nbsp;&nbsp;&nbsp;
                    D.<c:out value = "${q.choiced }" escapeXml="true"/>&nbsp;
                    <br/>
                    <br/>
                </c:forEach>

                <c:if test="${queListSize1 != 0 }">
                    <p><h4>二、简答题（共${queListSize1 }题，每题${examinfo.score1/queListSize1 }分）</h4></p>
                </c:if>
                <c:forEach items="${queList1 }" var="q" varStatus="i">

                    <p><B>${i.index + 1}、${q.title }</B></p>
                    <div class="col-md-12" style="padding-bottom: 10px;"><textarea rows="10" style="width: 100%"></textarea></div>
                </c:forEach>
                <c:if test="${queListSize2 != 0}">
                    <p><h4>三、编程题（共${queListSize2 }题，每题${examinfo.score2/queListSize2 }分）</h4></p>
                </c:if>
                <c:forEach items="${queList2 }" var="q" varStatus="i">

                    <p><B>${i.index + 1}、${q.title }</B></p>

                    <div class="col-md-12"><textarea rows="10" style="width: 100%"></textarea></div>
                </c:forEach>
                <br>

            </div>

        </div>
    </div>

    <hr>
    <div class="col-md-12" style="padding-bottom: 20px;">
        <b>卷尾</b>
    </div>
    <!-- end -->
</div>
</body>
</html>

<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>未到考试时间</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>

    <script src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">
        var day = ${timeMap.d };  // 天
        var hou = ${timeMap.h };// 小时
        var min = ${timeMap.m };// 分钟
        var se = ${timeMap.s }; // 秒
        //alert(day + " " + hou + " " + min + " " + se);
        window.setTimeout(callback, 1000); // 1000毫秒=1秒
        function callback() {
            var show = document.getElementById("show");
            show.innerHTML = "考试在 " + day + "天  " + hou + "小时  " + min + "分钟  " + se + "秒  "
                + "之后进行！";
            // 判断，什么条件下执行跳出循环。 计数器=0。
            if (min >= 1 && se == 0) {// 如果分钟大于1并且，秒为0
                se = 59; // 秒重新开始计数
                min = min - 1; // 分钟减一
            }
            if (hou >= 1 && min == 0) {
                min = 59;
                hou = hou - 1;
            }
            if (day >= 1 && hou == 0) {
                hou = 23;
                day = day - 1;
                se = 59;
            }
            se--;
            if (day <= 0 && hou <= 0 && min <= 0 && se <= -1) {
                form1.submit();
                return;
            }
            window.setTimeout(callback, 1000);
        }
    </script>
</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>进行考试</b></li>
        <li><b>考试倒计时</b></li>
    </ol>
    <hr>
    <div class="row" style="padding-bottom: 40px; padding-left: 20px;">
        <h2>剩余时间:</h2>
        <form name="form1" action="<%=basePath%>student/exam/stu_exam.do" target="_parent" method="get">
            <input type="hidden" value="${examid}" name="examid">
        </form>
        <div id="show" style="font-size: 20px;"></div>
    </div>
</div>
</div>

<hr>
</div>
</body>
</html>

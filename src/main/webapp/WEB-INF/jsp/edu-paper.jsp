<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="font/css/font-awesome.css">
    <script src="js/jquery-2.0.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap-select.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/edu-paper.js"></script>
</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>试卷导出</b></li>
    </ol>
    <hr id="hr"/>

    <div class="content">
        <div class="row">
            <table class="table table-striped table-hover table-condensed">
                <thead style="display: none" id="exam">
                <th style="text-align: center; word-break: keep-all;">考试编号</th>
                <th style="text-align: center; word-break: keep-all;">考试名称</th>
                <th style="text-align: center; word-break: keep-all;">考试科目</th>
                <th style="text-align: center; word-break: keep-all;">开始时间</th>
                <th style="text-align: center; word-break: keep-all;">考试时长</th>
                <th style="text-align: center;" class="col-md-3">考试班级</th>
                <th style="text-align: center; word-break: keep-all;">总分值</th>
                <th style="text-align: center; word-break: keep-all;">详情</th>
                <th style="text-align: center; word-break: keep-all;">操作</th>
                </thead>
                <tbody id="exam_result">
                </tbody>
                <!--循环设置样式模板-->
                <tr style="display: none" id="exam_m_template">
                    <td class="exam_id" style="text-align: center; vertical-align: middle;"></td>
                    <td class="exam_name" style="text-align: center; vertical-align: middle;"></td>
                    <td class="exam_cid" style="text-align: center; vertical-align: middle;"></td>
                    <td class="exam_start_time" style="text-align: center; vertical-align: middle;"></td>
                    <td class="exam_time" style="text-align: center; vertical-align: middle;"></td>
                    <td class="exam_class" style="text-align: center; vertical-align: middle;"></td>
                    <td class="exam_score" style="text-align: center; vertical-align: middle;"></td>
                    <td class="center" id="choice" style="text-align:center; vertical-align: middle;">
                        <a class="btn btn-info btn-xs" id="exam_find_this" href="">
                            <i class="glyphicon glyphicon-eye-open"></i>
                            查看详情
                        </a>
                    </td>
                    <td style="text-align: center; vertical-align: middle;">
                        <button id="exportBtn" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-download"
                                                                                    aria-hidden="true"
                                                                                    style="padding-right: 6px;"></span>导出
                        </button>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row" style="text-align: center" id="page">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>

</div>
</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>首页</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>

    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/echarts.min.js"></script>
    <script src="js/roma.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/edu-class-details.js"></script>


</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>班级管理</b></li>
        <li><b>班级详情</b></li>
    </ol>
    <hr/>

    <div class="well well-sm">
        <b>班级人数：</b><span id="stu_num">0</span>
    </div>

    <div style="padding-bottom: 20px;">
        <form class="form-horizontal bv-form" id="search_form">
            <div class="col-md-3 col-lg-3">
                <div class="form-group has-success has-feedback">
                    <label for="stu_name" name="stu_name" class="col-md-3 col-lg-3 control-label" style="text-align: left">姓名: </label>
                    <div class="col-md-9 col-lg-9" style="padding-left: 0px;">
                        <input id="stu_name" name="stu_names" type="text" class="form-control"/>
                    </div>
                </div>
            </div>
            <input type = "hidden" id = "date" value="date">
            <button type="button" id="search_btn" class="col-md-1 btn btn-primary"><span
                    class="glyphicon glyphicon-search" aria-hidden="true" style="padding-right: 6px;"></span>查询
            </button>
        </form>
    </div>

    <div style="padding-left: 20px;">
        <div class="row">
            <table class="table table-striped table-hover table-condensed">
                <thead>
                <th style="text-align: center">序号</th>
                <th style="text-align: center">学号</th>
                <th style="text-align: center">学生姓名</th>
                <th style="text-align: center">入班时间</th>
                <th style="text-align: center">新班级</th>
                <input type="hidden" id="classId" value="${classId}">
                </thead>
                <tbody id="result">

                </tbody>
                <tr style="display: none" id="m_template">
                    <td class="c_num" style="text-align: center"></td>
                    <td class="c_snum" style="text-align: center"></td>
                    <td class="c_sname" style="text-align: center"></td>
                    <td class="c_historytime" style="text-align:center"></td>
                    <td class="c_classname" style="text-align:center"></td>
                </tr>
            </table>
        </div>
        <div class="row" style="text-align: center" id="pagination">
            <ul class="pagination pagination-lg">
                <li class="head"><a href="#">首页</a></li>
                <li class="lastpage"><a href="#">&laquo;</a></li>
                <li class="disabled morehead"><a>...</a></li>
                <li class="page-4"><a></a></li>
                <li class="page-3"><a></a></li>
                <li class="page-2"><a></a></li>
                <li class="page-1"><a></a></li>
                <li class="currentpage active"><a>1</a></li>
                <li class="page_1"><a></a></li>
                <li class="page_2"><a></a></li>
                <li class="page_3"><a></a></li>
                <li class="page_4"><a></a></li>
                <li class="disabled moretail"><a>...</a></li>
                <li class="nextpage"><a href="#">&raquo;</a></li>
                <li class="tail"><a href="#">尾页</a></li>
            </ul>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
                <div id="main" style="height: 400px;width:600px;padding-top: 10px;"></div>
                <script type="text/javascript">
                    var myChart = echarts.init(document.getElementById('main'), 'roma');

                    myChart.showLoading();

                    myChart.on("click",function (param) {
                        $("#date").val(param.name);
                        getAllStuVoByHistory(1);
                    })


                    myChart.setOption({
                        title: {
                            text: "班级人数变更图"
                        },
                        tooltip: {
                            show: true
                        },
                        legend: {
                            data: []
                        },
                        xAxis: {
                            type: 'category',
                            data: ['dates'],
                            axisLabel: {
                                rotate: 20,
                                interval: 0   //强制显示所有x轴名称
                            },
                        },
                        yAxis: {
                            type: 'value',
                            data: ['numbers'],
                            axisLabel: {
                                rotate: 45  //刻度旋转45度角
                            }
                        },
                        series: [{
                            type: 'line',
                            data: []
                        }]
                    });
                    $.get('<%=basePath%>edu/class/class_all_number.do?classid=${classId}').done(function (data) {

                        myChart.hideLoading();
                        myChart.setOption({
                            xAxis: {
                                data: data.dates,
                                splitLine: true,
                                axisTick: false,
                                axisLine: {
                                    show: true,
                                    lineStyle: {
                                        color: '#48b'
                                    }
                                }
                            },
                            yAxis: {
                                splitArea: true,
                                data: data.value
                            },
                            series: [{
                                name: 'value',
                                data: data.numbers
                            }]
                        })
                    });

                    function getAllStuVoByHistory(page) {
                        var classId = $("#classId").val();
                        var c_date = $("#date").val();
                        var url = basePath()+"/edu/class/get_class_history_id.do";
                        var args = {
                            "pageNum": page,
                            "pageSize": "10",
                            "classId": classId,
                            "date":c_date
                        };
                        $.get(url, args, function (data) {
                            if (data.status == 0) {
                                removeLoading();
                                var pageResult = data.data;
                                var item = data.data.list;
                                page_item_num = item.length;
                                page = setPage(pageResult.pageNum, pageResult.pages, "getAllStuVoByHistory");
                                $("#result").html("");
                                $("#stu_num").html(item.length);
                                for (var i = 0; i < item.length; i++) {
                                    var temp = $("#m_template").clone();
                                    temp.attr("id", item[i].id);
                                    temp.removeAttr("style");
                                    temp.find(".c_num").html(pageResult.pageSize * (pageResult.pageNum - 1) + 1 + i);
                                    var acontent = "<form method='GET' action='"+basePath()+"/edu/stu/detail.do'>" +
                                        "<input type='hidden' name='id' value=" + item[i].sid + "> " +
                                        "<button type='submit'  class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span><span style='margin: 0px 8px;'>"+item[i].sname+"</span></button>" +
                                        "</form>";
                                    temp.find(".c_sname").html(acontent);
                                    temp.find(".c_snum").html(item[i].sid);
                                    temp.find(".c_historytime").html(item[i].historytime);
                                    temp.find(".c_classname").html(item[i].classname);
                                    $("#result").append(temp);
                                }
                            } else {
                            }
                        }, "json");
                    }


                    function getAllStuVoByHistoryAndName(page) {

                        var bv = $("#search_form").data('bootstrapValidator');
                        bv.validate();
                        if (!bv.isValid()) {
                            return;
                        }

                        var classId = $("#classId").val();
                        var c_date = $("#date").val();
                        var stu_name = $("#stu_name").val();
                        var url = basePath()+"/edu/class/get_class_history_name.do";
                        var args = {
                            "pageNum": page,
                            "pageSize": "10",
                            "classId": classId,
                            "date":c_date,
                            "name":stu_name
                        };
                        $.get(url, args, function (data) {
                            if (data.status == 0) {
                                removeLoading();
                                var pageResult = data.data;
                                var item = data.data.list;
                                page_item_num = item.length;
                                page = setPage(pageResult.pageNum, pageResult.pages, "getAllStuVoByHistoryAndName");
                                $("#result").html("");
                                $("#stu_num").html(item.length);
                                for (var i = 0; i < item.length; i++) {
                                    var temp = $("#m_template").clone();
                                    temp.attr("id", item[i].id);
                                    temp.removeAttr("style");
                                    temp.find(".c_num").html(pageResult.pageSize * (pageResult.pageNum - 1) + 1 + i);
                                    var acontent = "<form method='GET' action='"+basePath()+"/edu/stu/detail.do'>" +
                                        "<input type='hidden' name='id' value=" + item[i].sid + "> " +
                                        "<button type='submit'  class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span><span style='margin: 0px 8px;'>"+item[i].sname+"</span></button>" +
                                        "</form>";
                                    temp.find(".c_sname").html(acontent);
                                    temp.find(".c_snum").html(item[i].sid);
                                    temp.find(".c_historytime").html(item[i].historytime);
                                    temp.find(".c_classname").html(item[i].classname);
                                    $("#result").append(temp);
                                }
                            } else {
                            }
                        }, "json");
                    }

                    $("#search_btn").click(function(){
                        getAllStuVoByHistoryAndName(1);
                    });

                    function validatorSearch() {
                        $('#search_form').bootstrapValidator({
                            message: 'is not valid',
                            feedbackIcons: {
                                valid: 'glyphicon glyphicon-ok',
                                invalid: 'glyphicon glyphicon-remove',
                                validating: 'glyphicon glyphicon-refresh'
                            },
                            fields: {
                                stu_names: {
                                    message: 'this is not valid',
                                    validators: {
                                        notEmpty: {
                                            message: '学生名不能为空'
                                        },
                                        stringLength: {
                                            min :1,
                                            max: 10,
                                            message: '学生名长度应为1-10'
                                        },
                                        regexp: {
                                            regexp: /^(?=[0-9a-zA-Z\u4e00-\u9fa5]+$)/,
                                            message: '学生名只能由中文，数字和英文字母组成'
                                        }
                                    }
                                }
                            }
                        });
                    }
                </script>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <div id="pie" style="height: 400px;width:400px;"></div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                validatorSearch();
                $.get('<%=basePath%>edu/class/class_all_stu_class.do?classid=${classId}').done(function (data) {
                    pie1(data);//初始化echarts图
                })
            })

            function pie1(data) {
                var myChart1 = echarts.init(document.getElementById('pie'));

                //    myChart1.showLoading();

                myChart1.setOption({
                    title: {
                        text: '班级学生来源',
                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: 'vertical',
                        x: 'left',
                        data: data.classname
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            mark: {show: true},
                            dataView: {show: true, readOnly: false},
                            magicType: {
                                show: true,
                                type: ['pie', 'funnel'],
                                option: {
                                    funnel: {
                                        x: '25%',
                                        width: '50%',
                                        funnelAlign: 'left',
                                        max: 1548
                                    }
                                }
                            },
                            restore: {show: true},
                            saveAsImage: {show: true}
                        }
                    },
                    calculable: true,
                    series: [
                        {
                            name: '班级学生来源',
                            type: 'pie',
                            radius: '55%',
                            center: ['50%', '60%'],
                            data: data.pievalue
                        }
                    ]
                })
            }
        </script>

    </div>
</div>
</body>
</html>
、
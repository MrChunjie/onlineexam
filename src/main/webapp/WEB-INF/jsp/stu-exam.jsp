<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="page/pagetool.js"></script>
    <script src="js/dateformat.js"></script>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/loading.js"></script>
    <script type="text/javascript" src="js/common/stu-exam.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

</head>
<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>进行考试</b></li>
    </ol>
    <hr/>

    <table class="table table-striped table-hover table-condensed">
        <thead>
        <tr>
            <th style="text-align: center">学生姓名</th>
            <th style="text-align: center">试卷名</th>
            <th style="text-align: center">考试时间</th>
            <th style="text-align: center">考试时长</th>
            <th style="text-align: center">总分值</th>
            <th style="text-align: center">课程名称</th>
            <th style="text-align: center">出题人姓名</th>
            <th style="text-align: center">班级名称</th>
            <th style="text-align: center">考试状态</th>
            <th style="text-align: center">操作</th>
        </tr>
        </thead>
        <tbody id="result"></tbody>
        <tr style="display: none" id="m_template">
            <td class="p_sname" style="text-align: center"></td>
            <td class="p_name" style="text-align: center"></td>
            <td class="p_date" style="text-align: center"></td>
            <td class="p_time" style="text-align: center"></td>
            <td class="p_score" style="text-align: center"></td>
            <td class="p_course" style="text-align: center"></td>
            <td class="p_tname" style="text-align: center"></td>
            <td class="p_cname" style="text-align: center"></td>
            <td class="p_state" style="text-align: center"></td>
            <td class="center" style="text-align: center">
                <form id="form">
                    <input type="hidden" id="input" name="examid"/>
                    <a class="btn btn-info btn-xs" id="enter" >
                        <span class="glyphicon glyphicon-edit" style="padding-right: 4px;"></span>
                        进入考试
                    </a>
                </form>
            </td>
        </tr>
    </table>
    <div class="row" style="text-align: center">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>
</div>
</body>
</html>

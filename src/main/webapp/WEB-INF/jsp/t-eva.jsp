<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>阅卷</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>

    <script src="page/pagetool.js"></script>
    <script src="js/dateformat.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/t-eva.js"></script>
</head>

<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>成绩评估</b></li>
    </ol>
    <hr/>
  <%--  <div class="content">
        <div class="row" style="padding-left: 0px; padding-bottom: 10px;">
            <button class="btn btn-warning btn-xs" type="button" data-toggle="collapse" data-target="#collapseDivide"
                    aria-expanded="false" aria-controls="collapseExample">
                班级及课程平均成绩说明
            </button>
            <div class="collapse" id="collapseDivide" style="padding-top: 5px;">
                <div class="well" style="font-family: 楷体,serif; font-size: 15px; font-weight: bolder;">
                    1.等待教务进行成绩汇总<br/>
                    2.之后得到班级平均成绩<br/>
                </div>
            </div>
        </div>
    </div>--%>
        <form class="form-horizontal">
        <div class="form-group">
            <div class="col-md-3">
                <select name="stuClassId" id="stuClassId" class="form-control">
                    <option value="-1">请选择班级</option>
                    <c:if test="${stuClassId !=null&& stuClassId !=-1}">
                        <option value=" ${stuClassId}" selected="selected"> ${stuClassName}</option>
                    </c:if>
                </select>
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-primary btn-sm" id="btn-user" onclick="getPapers()"><span class="glyphicon glyphicon-search" aria-hidden="true" style="padding-right: 8px;"></span>查询</button>
            </div>
        </div>
    </form>
    <table class="table table-striped">
        <thead>
                <tr>
                    <th style="text-align: center; word-break: keep-all;">编号</th>
                    <th style="text-align: center; word-break: keep-all;">班级名称</th>
                    <th style="text-align: center; word-break: keep-all;">班级备注</th>
                    <th style="text-align: center; word-break: keep-all;">教师</th>
                    <th style="text-align: center; word-break: keep-all;">课程</th>
                    <th style="text-align: center; word-break: keep-all;" class="col-md-4">涉及考试</th>
                    <th style="text-align: center; word-break: keep-all;">平均成绩</th>
                    <th style="text-align: center; word-break: keep-all;">详情</th>
                    <th style="text-align: center; word-break: keep-all;">操作</th>
                </tr>
        </thead>
        <tbody id="result"></tbody>
        <tr style="display: none" id="m_template">
            <td class="p_id" style="padding: 2px; text-align: center"></td>
            <td class="p_className" style="padding: 2px; text-align: center"></td>
            <td class="p_other" style="padding: 2px; text-align: center"></td>
            <td class="p_teacher" style="padding: 2px; text-align: center"></td>
            <td class="p_courseName" style="padding: 2px; text-align: center"></td>
            <td class="p_examName" style="padding: 2px; text-align: center"></td>
            <td class="p_avg" style="padding: 2px; text-align: center"></td>
            <td class="center" style="text-align: center">
                <a class="btn btn-info btn-xs" id="enter" href="">
                    <span class="glyphicon glyphicon-list-alt"></span>
                    进入详情
                </a>
            </td>
            <td class="center" style="text-align: center;">
                <a class="btn btn-primary btn-xs" id="download">
                    <span class="glyphicon glyphicon-download"></span>
                    导出
                </a>
            </td>
        </tr>
    </table>
    <div class="row" style="text-align: center">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>
</div>

</body>
</html>

<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<!DOCTYPE html>
<html>
<head>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>
    <base href="<%=basePath%>">
    <meta name="viewport" charset="utf-8"/>
    <title>${current_student.sName}考试进行中</title>
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/content.css"/>
    <link rel="stylesheet" href="font/css/font-awesome.css">
    <link rel="stylesheet" href="icheck/minimal.css">
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootbox.min.js"></script>
    <script type="text/javascript" src="icheck/icheck.min.js"></script>
    <script type="text/javascript" src="js/common/stu-examing.js"></script>


</head>

<body id="app-layout" ondragstart="return false" draggable="false"
      ondragenter="event.dataTransfer.dropEffect='none'; event.stopPropagation(); event.preventDefault();"
      ondragover="event.dataTransfer.dropEffect='none';event.stopPropagation(); event.preventDefault();"
      ondrop="event.dataTransfer.dropEffect='none';event.stopPropagation(); event.preventDefault();">
<div class="col-md-12 col-lg-12 father">

    <!-- 导航栏开始-->
    <nav class="navbar navbar-default" role="navigation"
         style="position:fixed; width:100%; z-index:3; border-radius: 10px;">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar-collapse-1">
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a><i class="fa fa-comments-o"></i>正在考试</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- end -->


    <!-- 内容-->
    <div class="container">
        <div class="row" style="margin-top:60px;">
            <div id="info">
                <form id="form1" name="form1">
                    <p>
                    <h1 align="center">试卷：${test.name}</h1></p>
                    <p>
                    <h3 align="center">考试课程：${course.name }</h3></p>
                    <p><h4 align="center">考试班级： ${stuClass.name }&nbsp;&nbsp;</h4></p>
                    <h4 align="center">考试时长${test.testtime }分钟&nbsp;&nbsp;分值${test.scores }分</h4>
                    <p><h4>一、单选题（共${queListSize }题，每题${test.score/queListSize  }分）</h4></p>
                    <%int ii = -1; %>
                    <c:forEach items="${queList }" var="q" varStatus="i">
                        <B>${i.index + 1}、${q.quetitle}</B>

                        <%

                        %>

                        <%-- <%String value =(String) session.getAttribute(${q.id}+"_choice");//out.print(value); %> --%>
                        <% ii++;
                            String value = (String) session.getAttribute(Integer.toString(ii)); //out.println(value);%>
                        <div onclick="saveinfo(${q.id}+'_choice',${i.index })">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="${q.id }_choice" id="${q.id }_choice" value="A"
                                           <%if(value!=null&&value.equals("A")){ %>checked="checked"<%} %>>
                                    A.${q.choicea }
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="${q.id }_choice" id="${q.id }_choice" value="B"
                                           <%if(value!=null&&value.equals("B")){ %>checked="checked"<%} %>>
                                    B.${q.choiceb }
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="${q.id }_choice" id="${q.id }_choice" value="C"
                                           <%if(value!=null&&value.equals("C")){ %>checked="checked"<%} %>>
                                    C.${q.choicec }
                                </label>
                            </div>
                            <div class="radio">
                                <label id="question">
                                    <input type="radio" name="${q.id }_choice" id="${q.id }_choice" value="D"
                                           <%if(value!=null&&value.equals("D")){ %>checked="checked"<%} %>>
                                    D.${q.choiced }
                                </label>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="${q.id }"/>
                    </c:forEach>
                    <c:if test="${queListSize1>0}">
                        <p><h4>二、简答题（共${queListSize1 }题，每题${test.score1/queListSize1 }分）</h4></p>
                        <%ii = 499; %>
                        <div style="padding-left: 20px;">
                            <c:forEach items="${queList1 }" var="q" varStatus="i">
                                <%
                                    ii++;
                                    String value = (String) session.getAttribute(Integer.toString(ii));
                                %>
                                <div class="row" style="padding-top: 8px;">
                                    <p><B id="question1">${i.index + 1}、${q.title}</B></p>
                                </div>
                                <div class="row">
                                    <div class="col-md-11">
                        <textarea spellcheck="false" autocapitalize="off" autocomplete="off"
                                  autocorrect="off" rows="10"
                                  class="form-control" style="width: 100%" name="${q.id }_area" id="${q.id }_area"
                                  onblur="savebiginfo(${q.id}+'_area',${i.index+500 })"><%
                            if (value != null && !value.equals("")) {
                                out.print(value);
                            }
                        %></textarea>
                                    </div>
                                    <input type="hidden" name="${q.id }_contentTitle" value="${q.title }"/>
                                    <input type="hidden" name="queId1" value="${q.id }"/>
                                </div>
                            </c:forEach>
                        </div>
                    </c:if>
                    <c:if test="${queListSize2>0}">
                        <p><h4>三、编程题或调查问卷（共${queListSize2 }题，每题${test.score2/queListSize2 }分）</h4></p>
                        <%ii = 999; %>
                        <div style="padding-left: 20px;">
                            <c:forEach items="${queList2 }" var="q" varStatus="i">
                                <%
                                    ii++;
                                    String value = (String) session.getAttribute(Integer.toString(ii));
                                %>
                                <div class="row" style="padding-top: 8px;">
                                    <p><B id="question2">${i.index + 1}、${q.title }</B></p>
                                </div>
                                <div class="row">
                                    <div class="col-md-11">
                                 <textarea spellcheck="false" autocapitalize="off" autocomplete="off" autocorrect="off"
                                           class="form-control" style="width: 100%" rows="10" name="${q.id }_area"
                                           id="${q.id }_area"
                                           onblur="savebiginfo(${q.id}+'_area',${i.index+1000 })"><%
                                     if (value != null && !value.equals("")) {
                                         out.print(value);
                                     }
                                 %></textarea>
                                    </div>

                                    <input type="hidden" name="${q.id }_contentTitle" value="${q.title }"/>
                                    <input type="hidden" name="queId2" value="${q.id }"/>
                                </div>
                            </c:forEach>
                        </div>
                    </c:if>
                    <br>
                    <p></p>
                    <p></p>
                    <p></p>
                    <hr>
                    <p></p>
                    <p></p>
                    <input type="hidden" name="token" value="<%=session.getAttribute("token")%>">
                    <input type="hidden" name="name" value="${test.name }"/>
                    <input type="hidden" name="testId" value="${test.id }"/>
                    <input type="hidden" name="courseId" value="${test.courseid}"/>
                    <input type="hidden" name="endDate" value="${test.starttime }"/>
                    <input type="hidden" name="classIds" value="${test.classids }"/>
                    <input type="hidden" name="testTime" value="${test.testtime }"/>
                    <input type="hidden" name="scores" value="${test.scores }"/>
                    <input type="hidden" name="score" value="${test.score }"/>
                    <input type="hidden" name="score1" value="${test.score1 }"/>
                    <input type="hidden" name="score2" value="${test.score2 }"/>
                    <input type="hidden" name="countTime" id="countTime"/>    <!-- 计数器，记录答卷时长 -->
                    <div class="col-md-12">
                        <div style="float: left;">
                            卷尾
                        </div>
                        <div style="float: right;">
                            <a target="main" type="button" id="submit" onclick="sub();" class="btn btn-primary"
                               style="width: 150px;">
                                <span class="glyphicon glyphicon-upload" aria-hidden="true"
                                      style="padding-right: 10px;"></span>提交
                            </a>
                        </div>
                    </div>
                </form>
            </div>
            <div id="dialog" title="提示" style="border-radius: 30px;">
                <label>学号:</label>
                <b style="color:#0005EE;">${newId}</b>
                <br/>
                <label>用户名:</label>
                <b style="color:#0005EE;">${stuName}</b>
                <br><br>
                <label for="single">剩余单选题个数：</label>
                <p id="single" style="color:red;">
                    <input type="text" value="" size="13" readonly="readonly" style="color:#FF0000;" disabled="disabled"
                           id="singselect"/>
                </p>

                <label for="Title">剩余时间:</label>
                <p id="Title" style="color:red;">
                    <input type="text" value="" size="13" readonly="readonly" disabled="disabled" id="time"
                           style="color:#FF0000;"/>
                </p>

            </div>

        </div>
        <!--  end row -->

        <script src="js/write.js"></script>
    </div>

    <script type="text/javascript">
        //触发模态框的同时调用此方法
        function Info() {
            $(function () {
                $('#infomodal').modal({
                    keyboard: false
                });
            });
            $('.modal').css("top", "55px");

        }
    </script>
    <script type="text/javascript" src="js/bootbox.min.js"></script>

    <script type="text/javascript">
        var hou = ${timeMap.h};
        var min = ${timeMap.m};// 分钟
        var se = ${timeMap.s}; // 秒
        // 不能使用alert提示。
        //alert("考试剩余：" + hou + "小时 " + min + "分钟 " + se + "秒");
        window.setTimeout(callback, 1000); // 1000毫秒=1秒
        function callback() {
            se--;
            // 显示 倒计时。在time控件中。
            var input = document.getElementById("time");
            input.value = "考试剩余:" + hou + ":" + min + ":" + se + "";

            // 判断，什么条件下执行跳出循环。 计数器=0。
            if (min >= 1 && se == 0) {// 如果分钟大于1并且，秒为0
                se = 59; // 秒重新开始计数
                min = min - 1; // 分钟减一
            }
            if (hou >= 1 && min == 0) {
                min = 59;
                hou = hou - 1;
            }
            if (hou == 0 && min == 9 && se == 59) {
                bootbox.alert('距离考试结束还有10分钟');
            }
            if (hou == 0 && min == 0 && se == 0) {
                $.ajax({
                    url: '<%=basePath%>student/exam/exam_submit.do',
                    type: 'post',
                    data: $('#form1').serialize(),
                    dataType: 'json',
                    success: function (res) {
                        if (res.success) {
                            bootbox.alert("提交成功", function () {
                                window.location.href = '<%=basePath%>stu_index.do';
                            });
                        }
                    }
                });
                return;
            }
            if (se <= -1) {
                return;
            }
            window.setTimeout(callback, 1000);
        }

        // 计数器
        var count = 0;
        window.setTimeout(countTime, 1000);

        function countTime() {
            count++;
            window.setTimeout(countTime, 1000);
            var input1 = document.getElementById("countTime");
            input1.value = count;
            countsingselect();

        }

        function sub() {
            bootbox.setDefaults('locale', 'zh_CN');
            bootbox.confirm('您确定提交试卷吗？',
                function (result) {
                    if (result) {
                        var dialog = bootbox.dialog({
                            title: '请稍候',
                            message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 提交中...</p>'
                        });
                        $.ajax({
                            url: '<%=basePath%>student/exam/exam_submit.do',
                            type: 'post',
                            data: $('#form1').serialize(),
                            dataType: 'json',
                            success: function (res) {
                                if (res.success) {
                                    dialog.modal('hide');
                                    bootbox.alert("提交成功", function () {
                                        window.location.href = '<%=basePath%>stu_index.do';
                                    });
                                } else {
                                    dialog.modal('hide');
                                    bootbox.alert(res.msg, function () {
                                        window.location.href = '<%=basePath%>stu_index.do';
                                    });
                                }
                            }
                        });
                    }
                });
        }

        // 统计未选单选题
        function countsingselect() {
            var singcount = 0;
            <c:forEach items="${queList }" var="q" varStatus="i">
            var name = "${q.id }_choice";
            var radio = $("input[name='" + name + "']:checked").val();
            if (radio == null || radio == 'undefined') {
                singcount++;
            }
            </c:forEach>
            $("#singselect").val(singcount);
        }

        //判断题目有没有全部加载完
        window.onload = function () {
            var que = document.getElementById("question");
            var que1 = document.getElementById("question1");
            var que2 = document.getElementById("question2");
            var queSize = ${queListSize };
            var que1Size = ${queListSize1 };
            var que2Size = ${queListSize2 };
            var flag = true;
            if (queSize >= 1 && que == null) {
                flag = false;
            }
            if (que1Size >= 1 && que1 == null) {
                flag = false;
            }
            if (que2Size >= 1 && que2 == null) {
                flag = false;
            }
            //如果有题目没有加载完全   给个提示
            if (!flag) {
                bootbox.confirm('试卷没有加载完全，是否要重新加载？',
                    function (result) {
                        if (result) {
                            //重新加载试卷
                            var testid = ${test.id};
                            window.self.location = "<%=basePath%>student/exam/stu_exam.do?examid=" + testid;
                        }
                    });
            }
        };

        var leaveCount = 0;


        <%--$(function () {--%>

        <%--// 鼠标移出检测--%>
        <%--$(document).mouseleave(function () {--%>
        <%--if (leaveCount == 0) {--%>
        <%--bootbox.alert("鼠标不能离开试卷界面哦, 警告一次");--%>
        <%--}else if (leaveCount == 1) {--%>
        <%--bootbox.alert("警告两次");--%>
        <%--}else if (leaveCount == 2) {--%>
        <%--bootbox.alert("警告三次");--%>
        <%--}else if (leaveCount == 3) {--%>
        <%--bootbox.alert("警告四次");--%>
        <%--}else if (leaveCount == 4) {--%>
        <%--bootbox.alert("最后一次警告, 下次将提交试卷");--%>
        <%--}else if (leaveCount == 5) {--%>
        <%--var dialog = bootbox.dialog({--%>
        <%--title: '请稍候',--%>
        <%--message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 提交中...</p>'--%>
        <%--});--%>
        <%--$.ajax({--%>
        <%--url: '<%=basePath%>student/exam/exam_submit.do',--%>
        <%--type: 'post',--%>
        <%--data: $('#form1').serialize(),--%>
        <%--dataType: 'json',--%>
        <%--success: function (res) {--%>
        <%--if (res.success) {--%>
        <%--dialog.modal('hide');--%>
        <%--bootbox.alert("提交成功", function () {--%>
        <%--window.location.href = '<%=basePath%>stu_index.do';--%>
        <%--});--%>
        <%--}--%>
        <%--}--%>
        <%--});--%>
        <%--}--%>

        <%--leaveCount++;--%>
        <%--});--%>
        <%--});--%>

    </script>
    <!-- footer -->
    <div class="footer">
    </div>
    <!-- end -->
</div>
</body>
</html>

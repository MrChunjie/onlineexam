<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>阅卷</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>

    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/dateformat.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/edu-score.js"></script>
</head>

<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>课程成绩评估</b></li>
        <div style="float: right;">
            <button class="btn btn-warning btn-xs" type="button" data-toggle="collapse" data-target="#collapseDivide"
                    aria-expanded="false" aria-controls="collapseExample">
                <span class="glyphicon glyphicon-file" aria-hidden="true" style="padding-right: 5px;"></span>汇总成绩流程说明
            </button>
        </div>
    </ol>
    <hr/>
    <%--<div class="collapse" id="collapseDivide" style="padding-top: 5px;">--%>
        <%--<div class="well" style="font-family: 楷体,serif; font-size: 15px; font-weight: bolder;">--%>
            <%--1.发布某班级开课课程的考试;<br/>--%>
            <%--2.考试结束后进行阅卷打分;<br/>--%>
            <%--3.选择想要汇总的班级以及课程，点击生成并汇总平均成绩；<br/>--%>
            <%--4.得到该班级这门课程的平均成绩后“教师课程关系模块”才出现平均成绩，否则为空--%>
        <%--</div>--%>
    <%--</div>--%>

    <%--<form class="form-horizontal">--%>
        <%--<div class="form-group">--%>
            <%--<div class="col-md-3">--%>
                <%--<select name="stuClassId" id="stuClassId" class="form-control" onchange="getThisCourse()">--%>
                    <%--<option value="-1">请选择班级</option>--%>
                <%--</select>--%>
            <%--</div>--%>
            <%--<div class="col-md-3">--%>
                <%--<select name="courseId" id="courseId" class="form-control">--%>
                <%--</select>--%>
            <%--</div>--%>
            <%--<div class="col-md-3">--%>
                <%--<button type="button" class="btn btn-primary btn-sm" id="btn-user" onclick="getAllAvg()"--%>
                        <%--title="汇总该班级的选择的课程的平均成绩，汇总之后才可查看详情"><span--%>
                        <%--class="glyphicon glyphicon-list-alt" aria-hidden="true" style="padding-right: 10px;"></span>生成并汇总平均成绩--%>
                <%--</button>--%>
            <%--</div>--%>
        <%--</div>--%>
    <%--</form>--%>
    <table class="table table-striped">
        <thead>
        <tr>
            <th style="text-align: center; word-break: keep-all;">编号</th>
            <th style="text-align: center; word-break: keep-all;">班级名称</th>
            <th style="text-align: center; word-break: keep-all;">该班开设课程详情</th>
        </tr>
        </thead>
        <tbody id="result"></tbody>
        <tr style="display: none" id="m_template">
            <td class="p_id" style="padding: 2px; text-align: center"></td>
            <td class="p_className" style="padding: 2px; text-align: center"></td>
            <td class="center" style="text-align: center">
                <a class="btn btn-info btn-xs" id="p_detail" href="">
                    <i class="glyphicon glyphicon-eye-open icon-white"></i>
                    查看详情
                </a>
            </td>
        </tr>
    </table>
    <div class="row" style="text-align: center">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>
</div>

</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/loading.js"></script>
    <script type="text/javascript">
        $(function () {

        });

        function yanzheng() {
            var score = $("#score").val();
            var re = /^([1-9]\d*|[0]{1,1})$/; //判断字符串是否为数字 //判断正整数 /^[1-9]+[0-9]*]*$/
            if (!re.test(score)) {
                bootbox.alert("请输入合法数字,正整数");
                return false;
            }
            if (score >= 0 && score <= 1000) {
                if (score > ${score}) {
                    bootbox.alert("分给超了");
                    return false;
                } else {
                    return true;
                }
            } else {
                bootbox.alert("请输入合法分数..");
                return false;
            }
        }
    </script>
</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>流水阅卷-打分</b></li>
    </ol>
    <hr/>

    <h4 id="myModalLabel">
        <b>题干:</b>
        <div style="padding-left: 20px;">
            <p><textarea rows="10" cols="10" class="form-control">${title}</textarea> </p>
        </div>
    </h4>
    <h4 id="myModalLabe3"><b>标准答案:</b>
        <div style="padding-left: 20px;">
            <div ><textarea rows="10" cols="10" class="form-control">${bzAns}</textarea> </div>
        </div>
    </h4>
    <h4 id="myModalLabe4"><b>本题满分:</b>
        <div style="padding-left: 20px;">
            <div style="background-color: rgba(255,255,255,1);border-radius: 8px; padding: 15px; border: 2px solid rgba(221, 221, 221, 0.6);">${score}</div>
        </div>
    </h4>
    <h4 id="myModalLabel2">
        <b>考生答案:</b>
        <div style="padding-left: 20px;">
            <div>
           <textarea rows="10" cols="10" class="form-control"> ${ans}</textarea>
            </div>
        </div>
    </h4>
    <form class="form-horizontal bv-form" id="updateScore" method="post"
          action="<%=basePath%>teacher/correct/ls_update.do" onsubmit="return yanzheng()" >
        <div class="form-group has-success has-feedback">
            <label for="score" class="col-md-3 control-label">打分: </label>
            <div class="col-md-6">
                <input id="score" name="score" type="text" class="form-control" value="0"/>
                <input type="hidden" name="eId" value="${examId}"/>
                <input type="hidden" name="sId" value="${sId}"/>
                <input type="hidden" name="type" value="${type}"/>
                <input type="hidden" name="direct" value="${direct}"/>
                <input type="hidden" name="zscore" value="${score}"/>
                <input type="hidden" name="bigQuestionId" value="${bigQuestionId}">
                        </div>
                    </div>
                <div style="padding-top:30px; padding-bottom: 100px;">
                    <div class="col-md-offset-4 col-md-4">
                        <button type="submit" class="btn btn-primary btn-block">提交并阅下一题</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>

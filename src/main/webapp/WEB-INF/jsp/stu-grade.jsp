<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <link rel="stylesheet" href="css/stu-grade.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/stu-grade.js">

    </script>

</head>
<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>完善技能</b></li>
    </ol>
    <hr/>

    <div class="col-md-6 col-lg-6" style="padding-bottom: 50px;">
        <h4>技能&nbsp;&nbsp;<span id="tec" class="badge badge-info">0</span></h4>
        <div id="tags" class="tagsinput" style="height: auto; border: none; background: none;">
            <span id="temp" class="tag" onclick="add(this)" style="display: none;">Clean</span>
        </div>
    </div>

    <div class="col-md-6 col-lg-6" style="padding-bottom: 20px;">
        <h4>操作&nbsp;&nbsp;<span id="ope" class="badge badge-success">0</span></h4>
        <div id="list" class="tagsinput" style="height: 400px;">
            <ul class="list-group">
                <li id="list_temp" class="list-group-item"
                    style="background-color: #1abc9c; color: #ffffff; border-radius: 15px; display: none;">
                    <form class="form-horizontal" style="margin-bottom: 0px;">
                        <div class="form-group" style="margin-bottom: 0px;">
                            <label class="col-md-2 control-label" style="padding-right: 10px;">名称: </label>
                            <label id="item_name" class="col-md-3 control-label" style="text-align: left"></label>
                            <label class="col-md-2 control-label" style="padding-left: 0; padding-right: 0; text-align: center;">熟练度: </label>
                            <div class="col-md-3">
                                <select id="level" class="form-control input-sm">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="close" onclick="remove(this)"><span
                                        aria-hidden="true">×</span></button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        <div class="col-md-offset-2 col-md-8" style="padding-top: 20px;">
            <button id="sub" class="btn btn-success btn-block" style="text-align: center">提交</button>
        </div>
    </div>
</div>
</body>
</html>

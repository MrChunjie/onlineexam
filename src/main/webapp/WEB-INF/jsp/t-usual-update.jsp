<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>阅卷</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/dateformat.js"></script>
    <script src="js/common/t-usual-update.js"></script>
    <script src="js/jquery.form.js"></script>
    <script>
        function display() {
            $('#show').removeAttr("style");
        }

        function toUpload() {
            /*var dialog = bootbox.dialog({
             title: '请稍候',
             message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 导入中...</p>'
             });*/
            $('#uploadForm').ajaxSubmit({
                url: '<%=basePath%>teacher/correct/load_in_usual.do',
                type: 'post',
                async: true,
                cache: false,
                contentType: false,
                processData: false,
                data: "",
                dataType: 'json',
                success: function (data) {
                    bootbox.alert(data.msg, function () {
                    });
                    var item = data.data;
                    $("#length").val(item.length);
                    $("#testName").val(item[0].testName);
                    $("#result").html("");
                    for (var i = 0; i < item.length; i++) {
                        var temp = $("#m_template").clone();
                        temp.attr("id", i + 1);
                        temp.removeAttr("style");
                        temp.find(".p_id").html(i + 1);
                        temp.find(".p_testName").html(item[i].testName);
                        temp.find(".p_courseName").html(item[i].courseName);
                        temp.find(".p_classId").html(item[i].className);
                        temp.find(".p_stuName").html(item[i].stuName);
                        temp.find("#scoreM").val(item[i].scoreM);
                        $("#result").append(temp);
                    }
                }/*,
                error: function () {
                    bootbox.alert("导入失败, 请检查格式稍后重试", function () {
                    });
                }*/
            });
        }

        function submit() {
            //执行验证
            for (var i = 1; i <= $("#length").val(); i++) {
                var input = $("#" + i).children();
                var a = input.find("#scoreM").val();
                if (a >= 0 && a <= 100) {
                    continue;
                } else{
                    input.find("#scoreM").val(0)
                    alert("请按规范输入！");
                    return false;
                }
            }
            //全部正确之后，执行提交
            var str = "";
            for (var i = 1; i <= $("#length").val(); i++) {
                var input = $("#" + i).children();
                var a = input.find("#scoreM").val();
                str += a + ",";
            }
            $.ajax({
                url: '<%=basePath%>teacher/correct/update_usual.do',
                type: 'post',
                data: {
                    "str": str,
                    "testName": $("#testName").val()
                },
                dataType: 'json',
                success: function (res) {
                    bootbox.alert(res.msg, function () {
                        window.location.href = "<%=basePath%>t-usual.do";
                    });
                }
            });
        }
    </script>
</head>

<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>平时成绩</b></li>
        <li><b>开始打分</b></li>
        <div style="float: right">
            <button class="btn btn-warning btn-xs" type="button" data-toggle="collapse" data-target="#collapseDivide"
                    aria-expanded="false" aria-controls="collapseExample">
                <span class="glyphicon glyphicon-file" aria-hidden="true" style="padding-right: 5px;"></span>平时成绩流程说明
            </button>
        </div>
    </ol>
    <hr/>
    <div class="content">
        <div class="row" style="padding-left: 0px;">
            <div class="collapse" id="collapseDivide" style="padding-top: 5px;">
                <div class="well" style="font-family: 楷体,serif; font-size: 15px; font-weight: bolder;">
                    1.导出该课程该班的试卷;<br/>
                    2.下载得到的excel文件中，无需在平时成绩前面加入英文单引号<br/>
                    3.在EXCEL中填写学生平时成绩;<br/>
                    4.批量导入该班 参与该课程考试学生的平时成绩<br/>
                    5.每位同学后面文本框为导入的成绩，并且提供一次修改成绩的机会;<br/>
                    6.检查无误，提交平时成绩;<br/>
                </div>
            </div>
        </div>

        <div class="row" style="padding-left: 0px; padding-bottom: 10px;">
            <div class="col-md-1" style="padding-left: 0px;">
                <a class="btn btn-success btn-block btn-sm" id="download"
                   href="<%=basePath%>teacher/correct/usual_output_excel.do" onclick="display()"><span
                        class="glyphicon glyphicon-download"></span>
                    导出
                </a>
            </div>
            <div class="col-md-2" style="display: none" id="show">
                <%--<div class="col-md-1" id="show">--%>
                <a class="btn btn-success btn-sm" style="width:120px;"><span
                        class='glyphicon glyphicon-upload' aria-hidden='true' style="padding-right: 6px;"></span>导入平时成绩
                    <div style="filter:alpha(opacity=0);cursor: pointer; opacity: 0; position: absolute;  width: 110px;margin: -18px 0 0 -6px;height:20px;overflow: hidden; ">
                        <form id="uploadForm" enctype="multipart/form-data">
                            <input type="file" id="txtFile" onchange="toUpload()" name="file"
                                   style="font-size: 200px;cursor: pointer;direction: rtl !important; float: right\9; "/>
                        </form>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th style="text-align: center; word-break: keep-all;">编号</th>
            <th style="text-align: center; word-break: keep-all;">班级名称</th>
            <th style="text-align: center; word-break: keep-all;">学生姓名</th>
            <th style="text-align: center; word-break: keep-all;">课程名称</th>
            <th style="text-align: center; word-break: keep-all;">试卷名</th>
            <th style="text-align: center; word-break: keep-all;">得分</th>
        </tr>
        </thead>
        <tbody id="result"></tbody>
        <tr style="display: none" id="m_template">
            <td class="p_id" style="padding: 2px; text-align: center"></td>
            <td class="p_classId" style="padding: 2px; text-align: center"></td>
            <td class="p_stuName" style="padding: 2px; text-align: center"></td>
            <td class="p_courseName" style="padding: 2px; text-align: center"></td>
            <td class="p_testName" style="padding: 2px; text-align: center"></td>
            <td style="padding: 2px; text-align: center">
                <input type="text" name="scoreM" id="scoreM" value="0">
            </td>
        </tr>
        <input type="hidden" id="length" value="">
        <input type="hidden" id="testName" value="">
        <%--<input type="hidden" id="stuClassId" value="">--%>
    </table>
    <div class="form-group">
        <div class="col-md-offset-4 col-md-4" style="padding-bottom: 50px; padding-top: 60px;">
            <button type="button" class="btn btn-primary btn-block" onclick="submit()">
                <span class="glyphicon glyphicon-pencil"></span> 提交平时成绩
            </button>
        </div>
    </div>
    <%--  <form class="form-horizontal">
          <div class="form-group" style="padding-left: 15px;">
              <p><b></b></p>
              <div class="row">
                  <label class="control-label col-md-1" style="text-align: left;">分值: 100 </label>
                  <label class="control-label col-md-1" style="text-align: left;">得分: </label>
                  <div class="col-md-2">
                      <input type="text" value="0" class="form-control"/>
                  </div>
              </div>
          </div>
          <div class="form-group">
              <div class="col-md-offset-4 col-md-4" style="padding-bottom: 50px; padding-top: 60px;">
                  <button type="button" class="btn btn-primary btn-block" onclick="update()">
                      <span class="glyphicon glyphicon-pencil"></span> 提交并开始下一个打分
                  </button>
              </div>
          </div>
      </form>--%>

</div>
</body>
</html>

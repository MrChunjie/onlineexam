<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap-datetimepicker.css"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="font/css/font-awesome.css">

    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="page/pagetool.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="bootstrap/js/bootstrap-select.min.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/t-exam.js"></script>

</head>
<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>考试组题</b></li>
    </ol>
    <hr/>
    <div id="commit_exam">
        <div class="col-md-1" style="padding-bottom: 20px;">
            <button type='button' class='btn btn-sm btn-primary' id="addexam" onclick="showModal()">
                <span class="glyphicon glyphicon-align-left" aria-hidden="true" style="margin-right: 6px;"></span><span
                    id="biglist_span">发布考试</span>
            </button>
        </div>
        <div class="col-md-1" style="padding-bottom: 20px;padding-left: 50px;">
            <button type='button' class='btn btn-sm btn-info' id="raddexam" onclick="showResetExamModal()">
                <span class="glyphicon glyphicon-align-right" aria-hidden="true" style="margin-right: 6px;"></span><span
                    id="biglist_span2">发布补考考试</span>
            </button>
        </div>

        <div class="row" style="padding-left: 20px;">
            <table class="table table-striped table-hover table-condensed">
                <thead style="display: none" id="exam">
                <th style="text-align: center; word-break: keep-all;">考试编号</th>
                <th style="text-align: center">考试名称</th>
                <th style="text-align: center">考试科目</th>
                <th style="text-align: center; word-break: keep-all;">开始时间</th>
                <th style="text-align: center; word-break: keep-all;">考试时长</th>
                <th style="text-align: center" class="col-md-4">考试班级</th>
                <th style="text-align: center; word-break: keep-all;">总分值</th>
                <th style="text-align: center; word-break: keep-all;">操作</th>
                </thead>
                <tbody id="exam_result">
                </tbody>
                <!--循环设置样式模板-->
                <tr style="display: none" id="exam_m_template">
                    <td class="exam_id" style="text-align: center"></td>
                    <td class="exam_name" style="text-align: center"></td>
                    <td class="exam_cid" style="text-align: center"></td>
                    <td class="exam_start_time" style="text-align: center"></td>
                    <td class="exam_time" style="text-align: center"></td>
                    <td class="exam_class" style="text-align: center"></td>
                    <td class="exam_score" style="text-align: center"></td>
                    <td class="center" id="choice">
                        <a class="btn btn-info btn-xs" id="exam_find_this" href="">
                            <i class="glyphicon glyphicon-eye-open"></i>
                            查看详情
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- 发布考试-->
    <form id="subForm">
        <div class="col-md-12">
            <div style="text-align: center">
                <h1 id="commit_exam_name"></h1>
                <h3 id="commit_exam_course"></h3>
                <h4 id="commit_exam_className"></h4>
                <h4 id="commit_exam_testtime"></h4>

            </div>
        </div>
        <h4 id="commit_exam_questions"></h4>
        <div id="commit_exam_questions_template" style="display : none">
            <p><B id="questions_title"></B></p>
            <input type="radio" name="questions_chose"/>
            <span id="questions_a"></span>
            <input type="radio" name="questions_chose"/>
            <span id="questions_b"></span>
            <input type="radio" name="questions_chose"/>
            <span id="questions_c"></span>
            <input type="radio" name="questions_chose"/>
            <span id="questions_d"></span>
            <!--手动加value-->
            <input type="hidden" name="questionid" id="questionisd"/>
            <div class="q_replace" style="text-align: center"></div>
        </div>
        <div id="questions_result">

        </div>
        <%--</c:forEach>--%>


        <p><h4 id="big_questiont1">
    </h4></p>
        <div id="big_questiont1_template" style="display: none">
            <div class="row content">
                <p><B id="big_questiont1_title"></B></p>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <textarea rows="10" style="width: 100%" name="area" class="form-control"></textarea>
                </div>
                <input type="hidden" name="bigquestiontype1id" id="bigquestiont1"/>
                <div class="col-md-2">
                    <div class="bqt1_replace" style="text-align: center"></div>
                </div>
            </div>
        </div>
        <div id="big_question_result">
        </div>

        <p><h4 id="big_questiont2">
    </h4></p>

        <div id="big_questiont2_template" style="display: none">
            <div class="row content">
                <p><B id="big_questiont2_title"></B></p>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <textarea rows="10" style="width:100%;" name="_area" class="form-control"></textarea>
                </div>
                <input type="hidden" name="bigquestiontype2id" id="bigquestiontype2id"/>
                <div class="col-md-2">
                    <div class="bqt2_replace" style="text-align: center"></div>
                </div>
            </div>
        </div>
        <div id="big_questiont2_result">

        </div>
        <input type="hidden" name="name" id="name2"/>
        <input type="hidden" name="courseid" id="courseId2"/>
        <input type="hidden" name="starttime" id="starttime2"/>
        <input type="hidden" name="classids" id="classids2"/>
        <input type="hidden" name="testtime" id="testtime2"/>
        <input type="hidden" name="score" id="score02"/>
        <input type="hidden" name="score1" id="score12"/>
        <input type="hidden" name="score2" id="score22"/>
        <input type="hidden" name="examId" id="rexamId">
        <input type="hidden" name="stuId" id="rstuId">
        <div id="submit" style="display: none" class="col-md-offset-4 col-md-4">
            <div style="padding: 20px 0;">
                <button type="button" class="btn btn-success btn-block" onclick="sub()">
                    <span class="glyphicon glyphicon-upload" aria-hidden="true" style="padding-right: 6px;"></span>确认提交
                </button>
            </div>
        </div>
    </form>

    <%--<!--试卷信息-->--%>

    <div class="modal fade" id="addExamInFo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         style="padding-bottom: 50px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close btn-default" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" onclick="clearModal()">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">发布考试</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="addExamForm">
                        <div class="form-group has-success has-feedback">
                            <!-- for是什么 -->
                            <label for="name" class="col-md-3 control-label">考试名称: </label>
                            <div class="col-md-6">
                                <input id="name" name="name" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="courseid" class="col-md-3 control-label">考试科目: </label>
                            <div class="col-md-6">
                                <select id="courseid" name="courseid" type="text" class="form-control"></select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="starttime" class="col-md-3 control-label">开始时间: </label>
                            <div class="col-md-6">
                                <%--<input id="starttime" name="starttime" type="text" class="form-control"/>--%>
                                <div id="starttime" class="input-group date form_datetime"
                                     data-date-format="dd-MM-yyyy HH:mm:ss"
                                     data-link-field="dtp_input1">
                                    <input id="time" class="form-control" size="16" type="text" name="time"
                                           value="" readonly="" data-bv-field="time">
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                </div>
                                <small id="timeSmall" class="help-block" data-bv-validator="notEmpty"
                                       data-bv-for="newEnterpriseName" data-bv-result="NOT_VALIDATED"
                                       style="display: none; color: #a94442">开始时间不能为空
                                </small>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="testtime" class="col-md-3 control-label">考试时间: </label>
                            <div class="col-md-6 addtwo">
                                <div class="input-group">
                                    <input id="testtime" name="testtime" type="text" class="form-control"/>
                                    <span class="input-group-addon">
                                         <span>分钟</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="classidsdiv" class="col-md-3 control-label">面向方向: </label>
                            <div class="col-md-6" id="directionDiv">
                                <select class="form-control" id="direction" onchange="getSClass()">

                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="classidsdiv" class="col-md-3 control-label">考试班级: </label>
                            <div class="col-md-6" id="classidsdiv">
                                <div class="input-group">
                                    <select id="classIds" class="selectpicker form-control" multiple
                                            data-live-search="false">

                                    </select>
                                    <span class="input-group-addon" style="border: none;">
                                        <a href="javascript:void(0);" class="select-btn" onclick="selectAll()">所有</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="questionNum" class="col-md-3 control-label">单选题数目: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="questionNum" name="questionNum" type="text" class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>道</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="score" class="col-md-3 control-label">单选总分: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="score" name="score" type="text" class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>分</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="bigQuestionType1Num" class="col-md-3 control-label">简答题数目: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="bigQuestionType1Num" name="bigQuestionType1Num" type="text"
                                           class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>道</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="score1" class="col-md-3 control-label">简答题总分: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="score1" name="score1" type="text" class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>分</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="bigQuestionType2Num" class="col-md-3 control-label">编程题数目: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="bigQuestionType2Num" name="bigQuestionType2Num" type="text"
                                           class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>道</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="score2" class="col-md-3 control-label">编程题总分: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="score2" name="score2" type="text" class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>分</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" onclick="clearModal()">关闭</button>
                            <button type="button" class="btn btn-primary" onclick="addExam()">提交</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- 补考信息 -->
    <div class="modal fade" id="addResetExamInFo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         style="padding-bottom: 50px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close btn-default" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true" onclick="clearUModal()">&times;</span>
                    </button>
                    <h4 class="modal-title" id="rmyModalLabel">发布补考</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal bv-form" id="raddExamForm">
                        <div class="form-group has-success has-feedback">
                            <!-- for是什么 -->
                            <label for="rname" class="col-md-3 control-label">补考试卷: </label>
                            <div class="col-md-6">
                                <select id="rname" name="rname" type="text" class="form-control"
                                        onchange="getResetOtherInfo()">
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="rstarttime" class="col-md-3 control-label">开始时间: </label>
                            <div class="col-md-6">
                                <%--<input id="starttime" name="starttime" type="text" class="form-control"/>--%>
                                <div id="rstarttime" class="input-group date form_datetime"
                                     data-date-format="dd-MM-yyyy HH:mm:ss"
                                     data-link-field="dtp_input1">
                                    <input id="rtime" class="form-control" size="16" type="text" name="time"
                                           value="" readonly="" data-bv-field="time">
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-remove"></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                </div>
                                <small id="rtimeSmall" class="help-block" data-bv-validator="notEmpty"
                                       data-bv-for="newEnterpriseName" data-bv-result="NOT_VALIDATED"
                                       style="display: none; color: #a94442">开始时间不能为空
                                </small>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="rtesttime" class="col-md-3 control-label">考试时间: </label>
                            <div class="col-md-6 addtwo">
                                <div class="input-group">
                                    <input id="rtesttime" name="rtesttime" type="text" class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>分钟</span>
                                    </span>
                                </div>

                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="rquestionNum" class="col-md-3 control-label">单选题数目: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="rquestionNum" name="rquestionNum" type="text" class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>道</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="rscore" class="col-md-3 control-label">单选总分: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="rscore" name="rscore" type="text" class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>分</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="rbigQuestionType1Num" class="col-md-3 control-label">简答题数目: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="rbigQuestionType1Num" name="rbigQuestionType1Num" type="text"
                                           class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>道</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="rscore1" class="col-md-3 control-label">简答题总分: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="rscore1" name="rscore1" type="text" class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>分</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="rbigQuestionType2Num" class="col-md-3 control-label">编程题数目: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="rbigQuestionType2Num" name="rbigQuestionType2Num" type="text"
                                           class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>道</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-success has-feedback">
                            <label for="rscore2" class="col-md-3 control-label">编程题总分: </label>
                            <div class="col-md-6 addone">
                                <div class="input-group">
                                    <input id="rscore2" name="rscore2" type="text" class="form-control"/>
                                    <span class="input-group-addon">
                                        <span>分</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" onclick="clearUModal()">关闭</button>
                            <button type="button" class="btn btn-primary" onclick="addResetExam()">提交</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="row" style="text-align: center" id="fenye">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>
</div>
</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>阅卷</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>

    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="font/css/font-awesome.css">
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.js"></script>
    <script src="bootstrap/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/dateformat.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/edu-stu.js"></script>
    <script type="text/javascript">

        loading();

        var page_now = 1;

        function getPapers(page_num) {
            var stuName = $("#s_name").val();
            var classOther = $("#s_class").val();
            page_now = page_num;
            // Ajax异步请求,平均成绩
            $.ajax({
                url: '<%=basePath%>edu/score/stu_detail.do',
                type: 'POST',
                async: 'true',
                data: {
                    "pageNum": page_num,
                    "pageSize": "15",
                    "stuName": stuName,
                    "classOther": classOther
                },
                dataType: 'json',
                success: function (data) {
                    removeLoading();
                    var page = data.data;
                    setPage(page.pageNum, page.pages, "getPapers");
                    var item = data.data.list;
                    $("#result").html("");
                    for (var i = 0; i < item.length; i++) {
                        var temp = $("#m_template").clone();
                        temp.attr("id", page.pageSize * (page.pageNum - 1) + 1 + i);
                        temp.removeAttr("style");
                        temp.find(".p_id").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                        temp.find(".p_testName").html(item[i].examName);
                        temp.find(".p_courseName").html(item[i].courseName);
                        temp.find(".p_className").html(item[i].className);
                        temp.find(".p_stuName").html(item[i].stuName);
                        temp.find(".p_other").html(item[i].other);
                        temp.find(".p_scoreB").html(Math.round(item[i].score_b));
                        temp.find(".p_scoreM").html(Math.round(item[i].score_m));
                        temp.find(".p_scoreAll").html(Math.round(item[i].score_all));
                        $("#result").append(temp);
                    }/*else {
                        $("#result").html("");
                        bootbox.alert(data.msg);
                    }*/
                }
            });
        }
        $(function () {
            getPapers(1);
            //查询
            $("#search_btn").click(function () {
                getPapers(1);
            });
        });
    </script>
</head>

<body>
<div class="col-md-12 col-lg-12 father">
    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>学生成绩管理</b></li>
    </ol>
    <hr/>
    <button class='btn btn-info btn-sm' onclick="getExcel();"><span class="glyphicon glyphicon-download"
                                                                    aria-hidden="true"
                                                                    style="padding-right: 6px;"></span>下载学生信息及成绩
    </button>
    <div class="row" style="padding: 10px 0px 10px 10px;">
        <form class="form-horizontal bv-form" id="search_form">
            <div class="col-md-11 col-lg-11">
                <div class="form-group has-success has-feedback">
                    <label for="s_name" class="col-md-1 control-label" style="text-align: left;">姓名: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <input type="text" id="s_name" name="s_name" class="form-control">
                        </input>
                    </div>
                    <label for="s_class" class="col-md-1 control-label" style="padding-left: 0px;">班级备注: </label>
                    <div class="col-md-2" style="padding-left: 0px;">
                        <input type="text" id="s_class" class="form-control">
                        </input>
                    </div>
                    <div class="col-md-2">
                        <button type="button" id="search_btn" class="btn btn-primary btn-sm"><span
                                class="glyphicon glyphicon-search" aria-hidden="true"
                                style="padding-right: 8px;"></span><span style="padding-right: 6px;">查询</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th  style="text-align: center; word-break: keep-all;">编号</th>
            <th style="text-align: center; word-break: keep-all;">学生姓名</th>
            <th style="text-align: center; word-break: keep-all;">班级名称</th>
            <th style="text-align: center; word-break: keep-all;">班级备注</th>
            <th style="text-align: center; word-break: keep-all;">课程名称</th>
            <th style="text-align: center; word-break: keep-all;">试卷名称</th>
            <th style="text-align: center; word-break: keep-all;">笔试成绩</th>
            <th style="text-align: center; word-break: keep-all;">平时成绩</th>
            <th style="text-align: center; word-break: keep-all;">总成绩</th>
        </tr>
        </thead>
        <tbody id="result"></tbody>
        <tr style="display: none" id="m_template">
            <td class="p_id" style="padding: 2px; text-align: center"></td>
            <td class="p_stuName" style="padding: 2px; text-align: center"></td>
            <td class="p_className" style="padding: 2px; text-align: center"></td>
            <td class="p_other" style="padding: 2px; text-align: center"></td>
            <td class="p_courseName" style="padding: 2px; text-align: center"></td>
            <td class="p_testName" style="padding: 2px; text-align: center"></td>
            <td class="p_scoreB" style="padding: 2px; text-align: center"></td>
            <td class="p_scoreM" style="padding: 2px; text-align: center"></td>
            <td class="p_scoreAll" style="padding: 2px; text-align: center"></td>
        </tr>
    </table>
    <div class="row" style="text-align: center">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>
</div>

<div class="modal fade" id="getStudentExcel" tabindex="-1" role="dialog"
     aria-labelledby="Excel">
    <div class="modal-dialog dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="Excel">选择时间范围</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal bv-form" id="outForm">
                    <div class="form-group has-success has-feedback">
                        <label for="studentBeginTime" class="col-md-3 control-label">起始时间：</label>
                        <div class="col-md-8">
                            <div id="studentBeginTime" class="input-group date form_datetime"
                                 data-date-format="dd-MM-yyyy HH:mm:ss "
                                 data-link-field="dtp_input1">
                                <input id="newstime" class="form-control" size="16" type="text" name="time"
                                       value="" data-bv-field="time" readonly>
                                <span class="input-group-addon"><span
                                        class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                            </div>
                            <small id="newSTimeSmall" class="help-block" data-bv-validator="notEmpty"
                                   data-bv-for="studentBeginTime" data-bv-result="NOT_VALIDATED"
                                   style="display: none; color: #a94442">起始时间不能为空
                            </small>
                        </div>
                    </div>
                    <div class="form-group has-success has-feedback">
                        <label for="studentEndTime" class="col-md-3 control-label">终止时间：</label>
                        <div class="col-md-8">
                            <div id="studentEndTime" class="input-group date form_datetime"
                                 data-date-format="dd-MM-yyyy HH:mm:ss"
                                 data-link-field="dtp_input1">
                                <input id="newetime" class="form-control" size="16" type="text" name="time"
                                       value="" readonly="" data-bv-field="time">
                                <span class="input-group-addon"><span
                                        class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                            </div>
                            <small id="newETimeSmall" class="help-block" data-bv-validator="notEmpty"
                                   data-bv-for="studentEndTime" data-bv-result="NOT_VALIDATED"
                                   style="display: none; color: #a94442">终止时间不能为空
                            </small>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="clearTimeModal()">关闭</button>
                <button ype="button" class="btn btn-primary" onclick="downloadExcel()">下载</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>

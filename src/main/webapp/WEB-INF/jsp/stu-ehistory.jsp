<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>成绩分析系统</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/dateformat.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="page/pagetool.js"></script>
    <script src="js/loading.js"></script>
    <script src="js/common/stu-ehistory.js"></script>
</head>

<body>
<div class="col-md-12 col-lg-12 father">

    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>历史成绩</b></li>
    </ol>
    <hr/>
    <div class="content">

        <div class="row">
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="key" class="col-md-1 control-label" style="padding-left: 0px;">课程名称:</label>
                    <div class="col-md-2">
                        <input id="key" name="name" class="form-control"/>
                    </div>
                    <button type="button" class="btn btn-primary btn-sm" id="btn-user" onclick="getExamList()"><span class="glyphicon glyphicon-search" aria-hidden="true" style="padding-right: 6px;"></span>查询</button>
                   <%-- <button class="btn btn-success btn-sm" onclick="getExamList()">查询</button>--%>
                </div>
            </form>
        </div>
        <div class="row">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th style="text-align: center; word-break: keep-all;">编号</th>
                    <th style="text-align: center; word-break: keep-all;">姓名</th>
                    <th style="text-align: center; word-break: keep-all;">课程</th>
                    <th style="text-align: center; width: 100px;">试卷名</th>
                    <th style="text-align: center; word-break: keep-all;">分值</th>
                    <th style="text-align: center; word-break: keep-all;">单选</th>
                    <th style="text-align: center; word-break: keep-all;">简答</th>
                    <th style="text-align: center; word-break: keep-all;">编程</th>
                    <th style="text-align: center; word-break: keep-all;">总成绩</th>
                    <th style="text-align: center; word-break: keep-all;">考试时长</th>
                    <th style="text-align: center; word-break: keep-all;">开始时间</th>
                    <th style="text-align: center; word-break: keep-all;">交卷时间</th>
                    <th style="text-align: center; word-break: keep-all;">用时</th>
                    <th style="text-align: center; word-break: keep-all;">详情</th>
                </tr>
                </thead>
                <tbody id="result">

                </tbody>

                <tr style="display: none" id="p_template">
                    <td class="p_num" style="text-align: center"></td>
                    <td id="realName"  style="word-break: keep-all;"></td>
                    <td class="p_course" style="text-align: center"></td>
                    <td class="p_testName" style="text-align: center">
                    </td>
                    <td class="p_scores" style="text-align: center"></td>
                    <td class="p_score_1 " style="text-align: center"></td>
                    <td class="p_score_2" style="text-align: center"></td>
                    <td class="p_score_3" style="text-align: center"></td>
                    <td class="p_sum" style="text-align: center"></td>
                    <td class="p_testtime" style="text-align: center"></td>
                    <td class="p_starttime" style="text-align: center"></td>
                    <td class="p_createdate" style="text-align: center"></td>
                    <td class="p_time" style="text-align: center"></td>
                    <td class="center">
                        <button class="btn btn-info btn-xs" id="enter">
                            <span class="glyphicon glyphicon-eye-open icon-white" style="padding-right: 6px;"></span>
                            查看详情
                        </button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row" style="text-align: center">
        <ul class="pagination pagination-lg">
            <li class="head"><a href="#">首页</a></li>
            <li class="lastpage"><a href="#">&laquo;</a></li>
            <li class="disabled morehead"><a>...</a></li>
            <li class="page-4"><a></a></li>
            <li class="page-3"><a></a></li>
            <li class="page-2"><a></a></li>
            <li class="page-1"><a></a></li>
            <li class="currentpage active"><a>1</a></li>
            <li class="page_1"><a></a></li>
            <li class="page_2"><a></a></li>
            <li class="page_3"><a></a></li>
            <li class="page_4"><a></a></li>
            <li class="disabled moretail"><a>...</a></li>
            <li class="nextpage"><a href="#">&raquo;</a></li>
            <li class="tail"><a href="#">尾页</a></li>
        </ul>
    </div>

</div>
</body>
</html>

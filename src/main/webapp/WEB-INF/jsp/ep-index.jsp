<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>首页</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="css/index.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/user/common.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrapValidator.min.js"></script>

    <script src="js/common/ep-index.js"></script>
</head>
<body>

<%--标题栏--%>
<div class="row">
    <nav class="navbar navbar-default" style="border-color: #00ccff; margin-bottom: 10px;">
        <div class="container-fluid" style="background-color: #0099FF;">
            <div class="navbar-header">
                <div style="margin-left: 20px;">
                    <img src="imgs/sign.png" height="48px" width="90px" style="float: left; padding-top: 4px;">
                    <a class="navbar-text" href="javascript: window.location.reload();">慧与济宁-实训中心</a>
                </div>
            </div>
            <div class="navbar-right">
                <div style="margin-right: 20px;">
                    <button class="btn btn-sm myBtn navbar-btn" href="" data-toggle="modal" data-target="#changPwd">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true" style="padding-right: 6px;"></span><b>修改密码</b>
                    </button>
                    <div class="btn-group" style="padding-left: 6px;">
                        <button type="button" class="btn btn-sm dropdown-toggle navbar-btn myBtn"
                                data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"
                                                                                 aria-hidden="true"
                                                                                 style="padding-right: 6px;"></span><b
                                id="name"
                                style="padding-right: 8px;">${current_admin.name}</b><span
                                class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="<c:url value="/logout.do"/>">退出</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
<%--左侧导航栏--%>
<div class="row">
    <div class="col-md-2 col-lg-2" style="padding-right: 8px;">
        <div class="sidebar-nav menu">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation"><a target="main" href="<c:url value="/ep-post.do"/>"><span
                        class="glyphicon glyphicon-th-list" aria-hidden="true"
                        style="padding-right: 6px;"></span>岗位管理</a></li>
                <li role="presentation"><a target="main" href="<c:url value="/ep-addep.do"/>"><span
                        class="glyphicon glyphicon-folder-open" aria-hidden="true"
                        style="padding-right: 6px;"></span>企业管理</a>
                </li>
                <%--<li role="presentation"><a target="main" href="<c:url value="/ep-input.do"/>"><span--%>
                <%--class="glyphicon glyphicon-new-window" aria-hidden="true"--%>
                <%--style="padding-right: 6px;"></span>上传名单</a>--%>
                </li>
                <li role="presentation"><a target="main" href="<c:url value="/ep-out.do"/>"><span
                        class="glyphicon glyphicon-log-out" aria-hidden="true"
                        style="padding-right: 6px;"></span>导出名单</a></li>
                <li role="presentation"><a target="main" href="<c:url value="/ep-setting.do"/>"><span
                        class="glyphicon glyphicon-tasks" aria-hidden="true" style="padding-right: 8px;"></span>全局设置</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-md-10 col-lg-10" style="box-shadow: 0 0 20px #BDBDBD; border-radius: 10px;">
        <div style="margin-bottom: 40px;">
            <iframe name="main" frameborder="0" width="100%" height="100%">

            </iframe>
        </div>
    </div>
</div>

<%--<div>--%>
<%--<hr/>--%>
<%--<div style="background-color: rgba(0, 0, 0, 0.7); width: 100%; height: 40px;">--%>

<%--</div>--%>
<%--</div>--%>

<%--修改密码model--%>
<div class="modal fade" id="changPwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true" onclick="clearModal()">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">修改密码</h4>
            </div>
            <div class="modal-body">
                <form id="form" class="form-horizontal bv-form">
                    <div class="form-group has-success has-feedback">
                        <label for="newPassword" class="col-md-3 control-label">新密码: </label>
                        <div class="col-md-6">
                            <input id="newPassword" name="newPassword" type="password" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group has-success has-feedback" style="padding-top: 10px">
                        <label for="confirmPassword" class="col-md-3 control-label">确认密码: </label>
                        <div class="col-md-6">
                            <input id="confirmPassword" name="confirmPassword" type="password"
                                   class="form-control"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="clearModal()">关闭</button>
                <button type="button" class="btn btn-primary" onclick="change_pwd()">确认修改</button>
            </div>
        </div>
    </div>
</div>
<%--end修改密码model--%>

</body>
</html>

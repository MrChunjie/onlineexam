<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <link rel="stylesheet" href="css/stu-career.css"/>

    <script src="${pageContext.request.contextPath}/js/echarts.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/essos.js"></script>
    <script src="${pageContext.request.contextPath}/js/walden.js"></script>
    <script src="${pageContext.request.contextPath}/js/roma.js"></script>
    <script src="${pageContext.request.contextPath}/js/jsPdf.debug.js"></script>
    <script src="${pageContext.request.contextPath}/js/html2canvas.js"></script>
    <script src="${pageContext.request.contextPath}/js/echarts-wordcloud.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="js/loading.js"></script>
    <script type="text/javascript" src="js/common/stu-career.js"></script>

</head>
<body>
<div id="father" class="col-md-12 col-lg-12 father">


    <div style="height: 20px"></div>
    <ol class="breadcrumb">
        <li><b>首页</b></li>
        <li><b>查看生涯</b></li>
    </ol>
    <hr id="hr"/>

    <div style="height: 20px;"></div>
    <div class="content">
        <div class="row">
            <div class="col-md-12 title-div">
                <div class="col-md-4">
                    <div style="padding-top: 10px; padding-left: 30px;">
                        <img src="imgs/sign.png" height="60px">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="title">
                        个&nbsp;人&nbsp;生&nbsp;涯&nbsp;报&nbsp;告
                        <div class="title-btn">
                            <button id="download" class="btn btn-success btn-xs" onclick="download()"><span
                                    class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>&nbsp;&nbsp;个人生涯下载
                            </button>
                        </div>
                    </div>
                    <div class="title-small">
                        <p>Personal&nbsp;&nbsp; Career&nbsp;&nbsp; Report</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-2">
                    <img id="img" src="imgs/man.png" height="150">
                </div>
                <div class="col-md-10">
                    <div style="padding-left: 25px;">
                        <div class="row name">
                            <label class="control-label" id="name"></label>
                        </div>
                        <div class="row" style="padding-top: 10px;">
                            <div class="col-md-4" style="padding-left: 0">
                                <label class="control-label">性别: &nbsp;<span id="sex"></span></label>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">电话: &nbsp;<span id="tel"></span></label>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">民族: &nbsp;<span id="nation"></span></label>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 10px;">
                            <div class="col-md-4" style="padding-left: 0">
                                <label class="control-label">学校: &nbsp;<span id="school"></span></label>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">学院: &nbsp;<span id="college"></span></label>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">专业: &nbsp;<span id="major"></span></label>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 10px;">
                            <div class="col-md-4" style="padding-left: 0">
                                <label class="control-label">方向: &nbsp;<span id="direction"></span></label>
                            </div>
                            <div class="col-md-8">
                                <label class="control-label">家庭住址: &nbsp;<span id="home" style="word-break:break-all;"></span></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="heading">
            <span>
                <span class="glyphicon glyphicon-time" aria-hidden="true" style="padding-right: 6px;"></span>
                01.班级历程<small style="padding-left: 20px; color: #bbbbbb">CLASS COURSE</small>
            </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 div-bg">
                <div class="col-md-6" id="classLists" style="font-size: 16px;">
                    <div style="padding-top: 10px; font-size: 18px; padding-bottom: 15px;">
                        <label>班级变更历史</label>
                    </div>
                    <p class="showClass" style="display: none; font-family: 楷体; font-size: 18px;">
                        <span id="classIndex" style="font-weight: bold;"></span>、
                        在 <span id="classTime" style="font-weight: bold;"></span> 加入了
                        <span id="className" style="font-weight: bold;"></span>，由
                        <span id="classTeacher" style="font-weight: bold;"></span>
                        老师教授 <span id="classCourse" style="font-weight: bold;"></span> 课程。
                    </p>
                </div>
                <div class="col-md-6">
                    <div id="main" style="height: 300px;width:500px;padding-top: 10px;"></div>
                    <script type="text/javascript">
                        var myChart = echarts.init(document.getElementById('main'), 'roma');

                        myChart.showLoading();

                        myChart.setOption({
                            title: {
                                text: "班级变更图"
                            },
                            tooltip: {
                                show: true
                            },
                            legend: {
                                data: ['时间']
                            },
                            xAxis: {
                                type: 'category',
                                data: ['dates'],
                                axisLabel: {
                                    rotate: 15,
                                    interval: 0   //强制显示所有x轴名称
                                }
                            },
                            yAxis: {
                                type: 'value',
                                data: ['classId'],
                                axisLabel: {
                                    rotate: 45,  //刻度旋转45度角
                                    formatter: '{value} 人'
                                }

                            },
                            series: [{
                                name: '时间',
                                type: 'line',
                                data: []
                            }]
                        });
                        $.get('<%=basePath%>student/class/stu_class.do').done(function (data) {

                            myChart.hideLoading();
                            myChart.setOption({
                                xAxis: {
                                    data: data.dates,
                                    splitLine: true,
                                    axisTick: false,
                                    axisLine: {
                                        show: true,
                                        lineStyle: {
                                            color: '#48b'
                                        }
                                    }
                                },
                                yAxis: {
                                    splitArea: true,
                                    data: data.classname
                                },
                                series: [{
                                    name: 'value',
                                    data: data.classId
                                }]
                            })
                        })
                    </script>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="heading">
            <span>
                <span class="glyphicon glyphicon-time" aria-hidden="true" style="padding-right: 6px;"></span>
                02.技能展览<small style="padding-left: 20px; color: #bbbbbb">SKILLS EXHIBITION</small>
            </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 div-bg">
                <div class="col-md-6">
                    <div id="word" style="height: 300px;width: 500px;padding-top: 10px;"></div>
                </div>
                <div class="col-md-6" style="font-size: 20px;">
                    <div class="row" style="padding-top: 30px;">
                        <label class="control-label"><span style="font-family: 楷体; font-size: 30px;">技能数量: </span><span
                                id="grade_num" style="color: #c9302c; font-size: 60px;"></span></label>
                    </div>
                    <div class="row" style="padding-top: 20px;">
                        <div class="col-md-6">
                            <label class="control-label">最熟练技能: <span id="grade_name"></span></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">熟练度: <span id="grade_value"></span></label>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 30px;">
                        <div class="col-md-6">
                            <label class="control-label">技能平均等级: <span id="avg_grade"></span></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">技能平均熟练度: <span id="avg_value"></span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $.get('<%=basePath%>student/career/word_cloud.do').done(function (data) {
                    echartsCloud(data);//初始化echarts图
                })
            })

            function echartsCloud(data) {
                var myChart1 = echarts.init(document.getElementById('word'));

//    myChart1.showLoading();

                myChart1.setOption({
                    title: {
                        text: '词云图',
                        backgroundColor: '#a2ddff'
                    },
                    tooltip: {
                        show: true
                    },
                    series: {
                        name: '词云图',
                        type: 'wordCloud',
                        textStyle: {
                            normal: {
                                color: function () {
                                    return 'rgb(' + [
                                        Math.round(Math.random() * 160),
                                        Math.round(Math.random() * 160),
                                        Math.round(Math.random() * 160)
                                    ].join(',') + ')'
                                }
                            }
                        },
                        textRotation: [0, 90, -45, 45],
                        autoSize: true,
                        data: data.dataCloud
                    }
                })
            }
        </script>

        <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <span>
                        <span class="glyphicon glyphicon-time" aria-hidden="true" style="padding-right: 6px;"></span>
                        03.综合评估
                        <small style="padding-left: 20px; color: #bbbbbb">COMPREHENSIVE EVALUATION</small>
                     </span>
                </div>
            </div>
        </div>


        <div class="row" style="padding-bottom: 10px;">
            <div class="col-md-12 div-bg">
                <div class="col-md-6 radar">
                    <div class="row">
                        <label class="control-label">1、沟通能力: <span id="radar_communication_num"></span></label>
                        <br/>
                        <span id="radar_communication" ></span>
                    </div>
                    <div class="row">
                        <label class="control-label">2、学习能力: <span id="radar_study_num"></span></label>
                        <br/>
                        <span id="radar_study"></span>
                    </div>
                    <div class="row">
                        <label class="control-label">3、实践能力: <span id="radar_practice_num"></span></label>
                        <br/>
                        <span id="radar_practice"></span>
                    </div>
                    <div class="row">
                        <label class="control-label">4、成长幅度: <span id="radar_grow_num"></span></label>
                        <br/>
                        <span id="radar_grow"></span>
                    </div>
                    <div class="row">
                        <label class="control-label">5、技能丰富度: <span id="radar_skill_num"></span></label>
                        <br/>
                        <span id="radar_skill"></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="radar" style="height: 400px;width: 500px;padding-top: 10px;padding-bottom: 10px;"></div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $.get('<%=basePath%>student/career/get_radar.do').done(function (data) {
                    newradar(data);//初始化echarts图
                })
            })

            function newradar(data) {


                var myChart1 = echarts.init(document.getElementById('radar'));

                //    myChart1.showLoading();
                myChart1.setOption({
                    title: {
                        text: '雷达图',
                        backgroundColor: '#a2ddff'
                    },
                    tooltip: {
                        show: true
                    },
                    legend: {
                        orient: 'vertical',
                        x: 'center',
                        y: 'top',
                        data: ['个人特长']
                    },
                    polar: [
                        {
                            indicator: [
                                {text: '沟通', max: 100},
                                {text: '学习', max: 100},
                                {text: '实践', max: 100},
                                {text: '成长', max: 100},
                                {text: '技能', max: 100}
                            ]
                        }
                    ],
                    calculable: true,
                    series: [
                        {
                            type: 'radar',
                            itemStyle: {
                                normal: {
                                    areaStyle: {
                                        type: 'default'
                                    }
                                }
                            },
                            data: [
                                {
                                    value: data.radarvalue,
                                    name: '个人特长'
                                }
                            ]
                        }
                    ]
                })
            }
        </script>

    </div>
</div>
</body>
</html>


function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}


loading();

$(function () {

    $.ajax({
        url: basePath() + '/student/common/get_name.do',
        type: 'GET',
        async: false,
        dateType: 'JSON',
        success: function (res) {
            $('#realName').html(res);
        }
    });
    getExamList(1);
});
var page_now = 1;

function getExamList(page_num) {
    page_now = page_num;
    var courseName = $("#key").val();
    var realName= $('#realName').html();
    $.ajax({
        url: basePath()+'/student/history/exam_history.do',
        type: 'GET',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "5",
            "courseName": courseName
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            var page = data.data;
            var item = data.data.list;

            if (item.length == 0) {
                $('.pagination').hide();
                $('.table').hide();
                $('#alertMsg').remove();
                $('.form-horizontal').after("<div id='alertMsg' class='alert alert-warning alert-dismissible' role='alert'>\n" +
                    "  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>\n" +
                    "  <strong style='padding-left: 30px;'>暂时没有任何成绩</strong>" +
                    "</div>");
                return;
            }else {
                $('#alertMsg').hide();
                $('.table').show();
                $('.pagination').show();
            }

            setPage(page.pageNum, page.pages, "getExamList");
            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#p_template").clone();
                temp.removeAttr("style");
                temp.find(".p_num").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.find("#realName").html(realName);
                temp.find(".p_course").html(item[i].courseName);
                temp.find(".p_testName").html(item[i].testName);
                temp.find(".p_scores").html(item[i].scores);
                temp.find(".p_score_1").html(Math.round(item[i].score_1));
                temp.find(".p_score_2").html(item[i].score_2);
                temp.find(".p_score_3").html(item[i].score_3);
                temp.find(".p_sum").html(Math.round(item[i].score_b));
                temp.find(".p_testtime").html(item[i].testtime);
                temp.find(".p_starttime").html(new Date(item[i].starttime).format("yyyy-MM-dd hh:mm:ss"));
                temp.find(".p_createdate").html(new Date(item[i].createdate).format("yyyy-MM-dd hh:mm:ss"));
                temp.find(".p_time").html(item[i].time);
                temp.find("#enter").attr("onclick", "showDetail("+ item[i].s_id +"," + item[i].exam_id +")");
                $("#result").append(temp);
            }
        }
    })
}


function showDetail(sId, examId) {

    $.ajax({
        url: basePath()+"/student/history/exam_end.do",
        type: 'post',
        data: {"examId":examId},
        dataType: 'json',
        success: function (res) {
            if(res.success) {
                window.location.href = basePath()+"/stu-ehistory-detail.do?stuId=" + sId+"&examId="+examId;
            }else {
                bootbox.alert(res.msg);
            }
        }
    });
}

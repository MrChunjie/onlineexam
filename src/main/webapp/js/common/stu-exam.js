function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}


loading();

var page_now = 1;

function getPapers(page_num) {
    page_now = page_num;
    // Ajax异步请求,待考试卷
    $.ajax({
        url: basePath()+'/student/exam/show_papers.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "99",
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            var page = data.data;
            setPage(page.pageNum, page.pages, "getPapers");
            var item = data.data.list;


            if (item.length == 0) {
                $('.pagination').hide();
                $('.table').hide();
                $('hr').after("<div class='alert alert-warning alert-dismissible fade in' role='alert'>\n" +
                    "  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>\n" +
                    "  <strong style='padding-left: 30px;'>暂时没有任何考试</strong>" +
                    "</div>");
                return;
            }else {
                $('.table').show();
                $('.pagination').show();
            }

            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", item[i].id);
                temp.removeAttr("style");
                temp.find(".p_sname").html(item[i].stuName);
                temp.find(".p_name").html(item[i].testName);
                temp.find(".p_date").html(item[i].starttime);
                temp.find(".p_time").html(item[i].testTime);
                temp.find(".p_score").html(item[i].scores);
                temp.find(".p_course").html(item[i].courseName);
                temp.find(".p_tname").html(item[i].teacherName);
                temp.find(".p_cname").html(item[i].className);
                temp.find(".p_state").html("Todo");
                temp.find("#form").attr('id', 'form'+item[i].id);
                temp.find('#enter').attr('onclick', "doSubmit('"+ item[i].id +"')");
                $("#result").append(temp);
            }
        }
    });
}

function doSubmit(id) {

    $.ajax({
        url: basePath()+ '/student/exam/exam_left_time.do',
        type: 'get',
        data: {'examId':id},
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                window.parent.frames.location.href= basePath() + "/student/exam/stu_exam.do?examid="+id;
            }else {
                window.parent.main.location.href= basePath() + "/student/exam/stu_exam.do?examid="+id;
            }
        }
    });
}

$(function () {
    getPapers(1);
});
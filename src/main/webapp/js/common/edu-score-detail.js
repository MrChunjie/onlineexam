function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}

loading();

var page_now = 1;

function getAvgs(page_num) {
    page_now = page_num;
    // Ajax异步请求,平均成绩
    $.ajax({
        url: basePath()+'/edu/score/show_avg_detail.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "4"
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            var page = data.data;
            setPage(page.pageNum, page.pages, "getAvgs");
            var item = data.data.list;
            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.removeAttr("style");
                temp.find(".p_id").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.find(".p_courseName").html(item[i].courseName);
                temp.find(".p_examNames").html(item[i].examNames);
                temp.find(".p_courseAvg").html(Math.round(item[i].avg));
                $("#result").append(temp);
            }
        }
    });
}

$(function () {
    getAvgs(1);
});
function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}
$(function () {
    var warn = $("#warn").val();
    if(warn==1){
        bootbox.alert("试卷批阅完毕!", function () {
            window.location.href=basePath()+"/t-correct.do";
        });
    }
});
function checkScore1() {
    var score1 = $("#score1").val();
    var num1=$("#num1").val();
    for(var i=0;i<num1;i++){
        var update="score1Update"+i;
        var score1Update = $("#"+update).val();
        if(score1Update==""){
            bootbox.alert("分值不能为空!");
            $("#" + update).val("0");
        }
        score1Update=score1Update%100;
        var re = /^([1-9]\d*|[0]{1,1})$/; //判断字符串是否为数字 //判断正整数 /^[1-9]+[0-9]*]*$/
        if (!re.test(score1Update)) {
            bootbox.alert("请输入合法数字,正整数");
            $("#" + update).val("0");
        }else if (score1Update >= 0 && score1Update <= 100) {
            if (score1 < score1Update) {
                bootbox.alert("请不要大于该题目分值");
                $("#" + update).val("0");
            }
        }
    }
}
function checkScore2() {
    var score2 = $("#score2").val();
    var num2=$("#num2").val();
    for(var i=0;i<num2;i++){
        var update="score2Update"+i;
        var score2Update = $("#"+update).val();
        if(score2Update==""){
            bootbox.alert("分值不能为空!");
            $("#" + update).val("0");
        }
        score2Update=score2Update%100;
        var re = /^([1-9]\d*|[0]{1,1})$/; //判断字符串是否为数字 //判断正整数 /^[1-9]+[0-9]*]*$/
        if (!re.test(score2Update)) {
            bootbox.alert("请输入合法数字,正整数");
            $("#"+update).val("0");
        }else if (score2Update >= 0 && score2Update <= 100) {
            if (score2 < score2Update) {
                bootbox.alert("请不要大于该题目分值");
                $("#" + update).val("0");
            }
        }
    }
}
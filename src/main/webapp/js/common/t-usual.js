function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}
loading();

var page_now = 1;

function getPapers(page_num) {
    page_now = page_num;
    var stuClassId = $("#stuClassId").val();
    // Ajax异步请求,待批阅平时成绩
    $.ajax({
        url: basePath()+'/teacher/correct/show_usual.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "10",
            "stuClassId": stuClassId
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            var page = data.data;
            setPage(page.pageNum, page.pages, "getPapers");
            var item = data.data.list;
            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.removeAttr("style");
                temp.find(".p_id").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.find(".p_classId").html(item[i].className);
                /* temp.find(".p_stuName").html(item[i].stuName);*/
                temp.find(".p_courseName").html(item[i].courseName);
                temp.find(".p_testName").html(item[i].testName);
                /* temp.find(".p_scoreM").html(item[i].scoreM);*/
                /* temp.find(".p_sum").html(item[i].scores);*/
                if (item[i].status == 2||item[i].status == 3) {
                    temp.find("#choice").html("<span class='label label-success'>完成打分</span>");
                }else  {
                    temp.find("#enter").attr("href", basePath()+"/t-usual-update.do?sId="+item[i].sId+"&examId="+item[i].examId+"&classId="+item[i].classId);
                }
                $("#result").append(temp);
            }
        }
    });
}

function getClasses() {
    // Ajax异步请求,班级列表
    var stuClassId = $("#stuClassId").val();
    $.ajax({
        url: basePath()+'/teacher/correct/class_list.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": 1,
            "pageSize": "99"
        },
        dataType: 'json',
        success: function (data) {
            var item = data.data.list;
            for (var i = 0; i < item.length; i++) {
                if(item[i].id==stuClassId){
                    continue;
                }else {
                    $("#stuClassId").append("<option value='" + item[i].id + "'>" + item[i].name + "</option>");
                }
            }
        }
    });
}

$(function () {
    getClasses();
    getPapers(1);
});

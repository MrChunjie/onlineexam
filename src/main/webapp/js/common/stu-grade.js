function basePath() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPath + projectName);
}


loading();

var tec_count = 0;
var ope_count = 0;

var color = [
    '#FFFFCC',
    '#FFCCCC',
    '#CCCCFF',
    '#FF9966',
    '#FF6666',
    '#FFCCCC',
    '#CCFF99',
    '#66CC66',
    '#66CC99',
    '#1abc9c',
    '#FF0033',
    '#99CC66',
    '#99CC33',
    '#6699CC'
];

$remind = $("<h4 style='width: 90%; text-align: center'>暂无</h4>");

$(function () {
    getAllGrade();
    getAllOpe();
    $('#sub').click(function () {
        sub();
    });


});

function sub() {

    var items = [];
    $('ul > li').each(function () {
        if ($(this).css('display') == 'block') {
            var item = {};
            item.gradeId = $(this).attr('id');
            item.value = $(this).find("#level").val();
            items.push(item);
        }
    });

    if (items.length == 0) {
        bootbox.alert("没有需要提交的技能");
        return;
    }

    $.ajax({
        url: basePath() + '/student/grade/add_grades.do',
        type: 'post',
        data: JSON.stringify(items),
        contentType: 'application/json',
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                bootbox.alert(res.msg);
            }
        }
    });

}


//左侧往右侧添加
function add(html) {
    //隐藏此标签
    $(html).hide();
    tec_count--;
    ope_count++;
    if (tec_count <= 0) {
        $('#tags').append($remind);
    }
    var item = $('#list_temp').clone();
    item.attr('id', $(html).attr('id'));
    item.attr('name', $(html).attr('id'));
    item.attr('style', $(html).attr('style'));
    item.css('display', 'block');
    item.css('color', '#ffffff');
    item.find('#item_name').html($(html).html());
    $('.list-group').append(item);
    //技能数目显示
    $('#tec').html(tec_count);
    //操作数目显示
    $('#ope').html(ope_count);
}

function remove(html) {
    var id = $(html).parent().parent().parent().parent().attr('id');
    //左侧回现
    $('#' + id).css('display', '');

    tec_count++;
    ope_count--;
    if (tec_count > 0) {
        $remind.remove();
    }

    //右侧消失
    $("li[name='" + id + "']").remove();

    //技能数目显示
    $('#tec').html(tec_count);
    //操作数目显示
    $('#ope').html(ope_count);

    //对数据库删除的操作
    $.ajax({
        url: basePath() + '/student/grade/delete_sGrade.do',
        data: {'gradeId': id},
        dataType: 'json',
        success: function () {
        }
    });

}

function getAllGrade() {
    $.ajax({
        url: basePath() + '/student/grade/get_all_grade.do',
        async: false,
        type: 'get',
        dataType: 'json',
        success: function (data) {

            if (data.success) {
                $.each(data.data, function (index, item) {
                    var temp = $('#temp').clone();
                    temp.removeAttr('style');
                    temp.attr('id', item.id);
                    temp.attr('style', 'background-color:' + getRandomColor());
                    temp.html(item.name);
                    $('#tags').append(temp);
                    tec_count++;
                })
            }
            //技能数目显示
            $('#tec').html(tec_count);
        }
    });

}

function getAllOpe() {
    $.ajax({
        url: basePath() + '/student/grade/get_sGrades.do',
        async: false,
        dataType: 'json',
        success: function (res) {
            removeLoading();
            var count = 0;
            if (res.success) {
                $.each(res.data, function (index, item) {
                    count++;
                    //模拟点击，完成回显的功能
                   add($('#tags').find("#" + item.gradeId));
                    //熟练度回显
                    $("li[name='" + item.gradeId + "']").find('#level').val(item.value);
                });
                //对于不成功回显的偶现bug修改
                if ($('#ope').html() == '0' && count != 0) {
                    window.location.reload();
                }

            }
        }
    });
}

var getRandomColor = function () {
    return color[Math.round(Math.random() * (color.length - 2) + 1)];
}

function LightenDarkenColor(col, amt) {

    var usePound = false;

    if (col[0] == "#") {
        col = col.slice(1);
        usePound = true;
    }

    var num = parseInt(col, 16);

    var r = (num >> 16) + amt;

    if (r > 255) r = 255;
    else if (r < 0) r = 0;

    var b = ((num >> 8) & 0x00FF) + amt;

    if (b > 255) b = 255;
    else if (b < 0) b = 0;

    var g = (num & 0x0000FF) + amt;

    if (g > 255) g = 255;
    else if (g < 0) g = 0;

    return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);

}
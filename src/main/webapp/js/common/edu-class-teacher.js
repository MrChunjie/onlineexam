function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}


loading();

var page_now = -1;

$(function () {

    allClass();

    allTeacher();

    allCourse();

    getAll(1);

    //查询
    $("#search_btn").click(function () {
        searchByCondition(1);
    });


    validator();

});


function validator() {
    $('#addForm').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            m_hours: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '课时不能为空'
                    },
                    stringLength: {
                        min: 1,
                        max: 4,
                        message: '输入长度必须在1位到4位之间'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '课时只能为纯数字'
                    }
                }
            }
        }
    });
}

//获得课程
function allCourse() {
    $.ajax({
        url: basePath()+"/edu/course/get_all_course.do",
        type: 'GET',
        success: function (res) {
            if (res.success) {
                $("#q_course option").remove();
                var selected_content = "<option value='' selected='selected'>所有课程</option>";
                $("#q_course").append(selected_content);
                if (res.data.length == 0) {
                    var content = "<option>暂无课程数据</option>";
                    $("#q_course").append(content);
                } else {
                    for (var i = 0, a = res.data.length; i < a; i++) {
                        var content = "<option value='" + res.data[i].id + "'>" + res.data[i].name + "</option>";
                        $("#q_course").append(content);
                    }
                }

                //modal 数据加载
                $("#m_course option").remove();
                if (res.data.length == 0) {
                    var content = "<option>暂无课程数据</option>";
                    $("#m_course").append(content);
                } else {
                    for (var i = 0, a = res.data.length; i < a; i++) {
                        var content = "<option value='" + res.data[i].id + "'>" + res.data[i].name + "</option>";
                        $("#m_course").append(content);
                    }
                }

            }
        }
    });
}

//获得老师
function allTeacher() {
    $.ajax({
        url: basePath()+"/edu/tea/get_all_teacher.do",
        type: 'GET',
        success: function (res) {
            if (res.success) {
                $("#q_teacher option").remove();
                var selected_content = "<option value='' selected='selected'>所有教师</option>";
                $("#q_teacher").append(selected_content);
                if (res.data.length == 0) {
                    var content = "<option>暂无教师数据</option>"
                    $("#q_teacher").append(content);
                } else {
                    for (var i = 0, a = res.data.length; i < a; i++) {
                        var content = "<option value='" + res.data[i].id + "'>" + res.data[i].name + "</option>";
                        $("#q_teacher").append(content);
                    }
                }

                //modal 数据加载
                $("#m_teacher option").remove();
                if (res.data.length == 0) {
                    var content = "<option>暂无教师数据</option>"
                    $("#m_teacher").append(content);
                } else {
                    for (var i = 0, a = res.data.length; i < a; i++) {
                        var content = "<option value='" + res.data[i].id + "'>" + res.data[i].name + "</option>";
                        $("#m_teacher").append(content);
                    }
                }
            }
        }
    });
}

//获得班级
function allClass() {
    $.ajax({
        url: basePath()+"/edu/class/all_class.do",
        type: 'GET',
        success: function (res) {
            if (res.success) {
                $("#q_class option").remove();
                var selected_content = "<option value='' selected='selected'>所有班级</option>";
                $("#q_class").append(selected_content);
                if (res.data.length == 0) {
                    var content = "<option>暂无班级数据</option>";
                    $("#q_class").append(content);
                } else {
                    for (var i = 0, a = res.data.length; i < a; i++) {
                        var content = "<option value='" + res.data[i].id + "'>" + res.data[i].name + "</option>";
                        $("#q_class").append(content);
                    }
                }

                //modal 数据加载
                $("#m_class option").remove();
                if (res.data.length == 0) {
                    var content = "<option>暂无班级数据</option>";
                    $("#m_class").append(content);
                } else {
                    for (var i = 0, a = res.data.length; i < a; i++) {
                        var content = "<option value='" + res.data[i].id + "'>" + res.data[i].name + "</option>";
                        $("#m_class").append(content);
                    }
                }

            }
        }
    })
}


//分页获得所有数据
function getAll(page_num) {
    page_now = page_num;
    $.ajax({
        url: basePath()+'/edu/class_teacher/get_all_class_teacher.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "8"
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            if (data.success) {
                var page = data.data;
                var item = data.data.list;
                setPage(page.pageNum, page.pages, "getAll");
                $("#result").html("");
                for (var i = 0; i < item.length; i++) {
                    var temp = $("#m_template").clone();
                    temp.removeAttr("style");
                    temp.find(".s_num").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                    temp.find(".s_class").html(item[i].className);
                    temp.find(".s_teacher").html(item[i].teacher);
                    temp.find(".s_course").html(item[i].course);
                    temp.find(".s_avg").html(item[i].avg);
                    temp.find(".s_hours").html(item[i].hours);
                    temp.find(".s_satisfaction").html(item[i].satisfaction);
                    temp.find(".s_classId").html(item[i].classId);
                    temp.find(".s_tId").html(item[i].tId);
                    temp.find(".s_courseId").html(item[i].courseId);
                    //删除按钮组装
                    var dcontent = "<button type='button' class='btn btn-danger btn-xs' onclick='doDelete("+ item[i].classId +","+ item[i].tId +","+ item[i].courseId+" )'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span><span style='margin: 0px 8px;'>取消</span></button>"
                    temp.find(".s_op").html(dcontent);
                    $("#result").append(temp);
                }
            } else {
                bootbox.alert(data.msg);
            }
        }
    })
}

//分页查询数据
function searchByCondition(page_num) {
    var courseId = $("#q_course").val();
    var teacherId = $("#q_teacher").val();
    var classId = $("#q_class").val();
    $.ajax({
        url: basePath()+'/edu/class_teacher/search_by_condition.do',
        type: 'POST',
        data: {
            "pageNum": page_num,
            "pageSize": "8",
            "courseId": courseId,
            "teacherId": teacherId,
            "classId": classId
        },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                var page = data.data;
                var item = data.data.list;
                setPage(page.pageNum, page.pages, "searchByCondition");
                $("#result").html("");
                for (var i = 0; i < item.length; i++) {
                    var temp = $("#m_template").clone();
                    temp.removeAttr("style");
                    temp.find(".s_num").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                    temp.find(".s_class").html(item[i].className);
                    temp.find(".s_teacher").html(item[i].teacher);
                    temp.find(".s_course").html(item[i].course);
                    temp.find(".s_avg").html(item[i].avg);
                    temp.find(".s_hours").html(item[i].hours);
                    temp.find(".s_satisfaction").html(item[i].satisfaction);
                    //删除按钮组装
                    var dcontent = "<button type='button' class='btn btn-danger btn-xs' onclick='doDelete(" + item[i].classId + "," + item[i].tId + "," + item[i].courseId + ")'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span><span style='margin: 0px 8px;'>取消</span></button>"
                    temp.find(".s_op").html(dcontent);
                    $("#result").append(temp);
                }
            } else {
                $("#result").html("");
                bootbox.alert(data.msg);
            }
        }
    })
}


//添加数据
function add() {

    // 提交前先主动验证表单
    var bv = $("#addForm").data('bootstrapValidator');
    bv.validate();
    if (!bv.isValid()) {
        return;
    }

    var courseId = $('#m_course').val();
    var teacherId = $('#m_teacher').val();
    var classId = $('#m_class').val();
    var hours = $('#m_hours').val();

    $.ajax({
        url: basePath()+'/edu/class_teacher/add_class_teacher.do',
        type: 'post',
        data: {"courseId": courseId, "tId": teacherId, "classId": classId, "hours": hours},
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                bootbox.alert(res.msg);
                clearAddModal();
                getAll(page_now);
            } else {
                bootbox.alert(res.msg);
            }
        }
    });

}


function doDelete(classId, teacherId, courseId) {
    bootbox.confirm({
        message: "确认取消吗?",
        buttons: {
            confirm: {
                label: '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 是',
                className: 'btn-danger'
            },
            cancel: {
                label: '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 否',
                className: 'btn-default'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: basePath()+'/edu/class_teacher/delete_by_pk.do',
                    type: 'post',
                    data: {"courseId": courseId, "tId": teacherId, "classId": classId},
                    dataType: 'json',
                    success: function (res) {
                        if (res.success) {
                            bootbox.alert(res.msg);
                            getAll(page_now);
                        } else {
                            bootbox.alert(res.msg);
                        }
                    }
                });
            }
        }
    });
}


function clearAddModal() {
    $('#addModal').modal('hide');
    $('#addForm select option').removeAttr('selected');

    $('#addForm').data('bootstrapValidator').destroy();
    $('#addForm').data('bootstrapValidator', null);
    $("form[id='addForm'] > div").addClass('has-success');
    validator();
}

function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}

loading();

var page_now = 1;

function getPapers(page_num) {
    page_now = page_num;
    var stuClassId = $("#stuClassId").val();
    // Ajax异步请求,待批阅试卷
    $.ajax({
        url: basePath()+'/teacher/correct/show_read_over.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "5",
            "stuClassId": stuClassId
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            var page = data.data;
            setPage(page.pageNum, page.pages, "getPapers");
            var item = data.data.list;
            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.removeAttr("style");
                temp.find(".p_examId").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.find(".p_classId").html(item[i].className);
                temp.find(".p_courseName").html(item[i].courseName);
                temp.find(".p_testName").html(item[i].testName);
                temp.find(".p_scores").html(item[i].scores);
                temp.find(".p_score1").html(item[i].score1.toFixed(1));
                temp.find(".p_score2").html(item[i].score2.toFixed(1));
                temp.find(".p_score3").html(item[i].score3.toFixed(1));
                temp.find(".p_sum").html(item[i].total.toFixed(1));
                temp.find(".p_testtime").html(item[i].testtime);
                temp.find(".p_startDate").html(new Date(item[i].startDate).format("yyyy-MM-dd hh:mm:ss"));
                temp.find(".p_createdate").html(new Date(item[i].createdate).format("yyyy-MM-dd hh:mm:ss"));
                temp.find(".p_time").html(item[i].time);
                if (item[i].status == 1||item[i].status == 3) {
                    temp.find("#choice").html("<span class='label label-success'>完成阅卷</span>");
                }else {
                    temp.find("#enter").attr("href", basePath()+"/teacher/correct/open_papers.do?examId=" + item[i].examId +
                        "&sId=" + item[i].sId + "&stuClassId=" + stuClassId);
                }
                $("#result").append(temp);
            }
        }
    });
}

function getClasses() {
    // Ajax异步请求,班级列表
    var stuClassId = $("#stuClassId").val();
    $.ajax({
        url: basePath()+'/teacher/correct/class_list.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": 1,
            "pageSize": "99"
        },
        dataType: 'json',
        success: function (data) {
            var item = data.data.list;
            for (var i = 0; i < item.length; i++) {
                if(item[i].id==stuClassId){
                    continue;
                }else {
                    $("#stuClassId").append("<option value='" + item[i].id + "'>" + item[i].name + "</option>");
                }
            }
        }
    });
}

$(function () {
    getClasses();
    getPapers(1);
});
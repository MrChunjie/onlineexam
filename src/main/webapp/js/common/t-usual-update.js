function basePath() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPath + projectName);
}
function getPapers() {
    var stuClassId = $("#stuClassId").val();
    // Ajax异步请求,待批阅平时成绩
    $.ajax({
        url: basePath() + '/teacher/correct/open_usual.do',
        type: 'GET',
        async: 'true',
        data: {
            "stuClassId": stuClassId
        },
        dataType: 'json',
        success: function (data) {
            var page = data.data;
            //setPage(page.pageNum, page.pages, "getPapers");
            var item = data.data;
            $("#result").html("");
            /* if (page.total == 0) {
             bootbox.alert("试卷批阅完毕!", function () {
             window.location.href = basePath()+"/t-usual.do";
             });
             }*/
            $("#length").val(item.length);
            $("#testName").val(item[0].testName);
            for (var i = 0; i < item.length; i++) {
                /*var temp = $("#m_template").clone();
                 temp.attr("id", page.pageSize * (page.pageNum - 1) + 1 + i);
                 temp.removeAttr("style");
                 temp.find(".p_id").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                 temp.find(".p_classId").html(item[i].className);
                 temp.find(".p_stuName").html(item[i].stuName);
                 temp.find(".p_courseName").html(item[i].courseName);
                 temp.find(".p_testName").html(item[i].testName);
                 $("#result").append(temp);
                 $("#examId").val(item[i].examId);
                 $("#stuClassId").val(item[i].classId);*/
                var temp = $("#m_template").clone();
                temp.attr("id", i + 1);
                temp.removeAttr("style");
                temp.find(".p_id").html(i + 1);
                temp.find(".p_testName").html(item[i].testName);
                temp.find(".p_courseName").html(item[i].courseName);
                temp.find(".p_classId").html(item[i].className);
                temp.find(".p_stuName").html(item[i].stuName);
                temp.find("#scoreM").val(0);
                $("#result").append(temp);
            }
        }
    });
}

function update() {
    var scoreM = $("#scoreM").val();
    if (scoreM > 100) {
        alert("请不要大于该题目分值");
        return false;
    } else if (scoreM >= 0 && scoreM <= 100) {
        var examId = $("#examId").val();
        // Ajax异步请求,待批阅平时成绩
        $.ajax({
            url: basePath() + '/teacher/correct/update_usual.do',
            type: 'POST',
            async: 'true',
            data: {
                "scoreM": scoreM,
                "examId": examId
            },
            dataType: 'json',
            success: function (data) {
                $('#scoreM').val("0");
                getPapers();
            }
        });
    } else {
        alert("请按规范输入！");
        return false;
    }
}

$(function () {
    getPapers();
});
function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}
loading();

var page = 1;

$(function () {
    getGrades(page);
    validatorU();
    validatorA();
    $("#add_grade_btn").click(function () {
        addGrade();
    });
    $("#update_grade_btn").click(function () {
        updateGrade();
    });

});

function validatorU() {
    $('#changGrade').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            newGradeName: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '技能名不能为空'
                    },
                    stringLength:{
                        min : 1,
                        max : 10,
                        message : '技能名长度应为1-10'
                    }
                }
            }
        }
    });
}

function validatorA() {
    $('#addGrade').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            gradeName1: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '技能名不能为空'
                    },
                    stringLength:{
                        min : 1,
                        max : 10,
                        message : '技能名长度应为1-10'
                    }
                }
            },

        }
    });
}

function getGrade() {

    var url = basePath()+"/grade/toupdate_grade.do";
    var data = {"gradeId": gradeId};
    $.get(url, data, function (data) {
        if (data.status == 0) {
            $("#gradename1").val(data.data.name);
        } else {
            bootbox.alert(data.msg);
        }
    }, "JSON");
}

function getGrades(pageNum) {
    $.ajax({
        url: basePath()+'/teacher/grade/all_grade.do',
        type: 'GET',
        async: 'true',
        data: {
            "pageNum": pageNum,
            "pageSize": "10"
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            var pageResult = data.data;
            var item = data.data.list;
            page = setPage(pageResult.pageNum, pageResult.pages, "getGrades");
            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", item[i].id);
                temp.removeAttr("style");
                temp.find(".t_num").html(pageResult.pageSize * (pageResult.pageNum - 1) + 1 + i);
                temp.find(".t_name").html(item[i].name);
                temp.find(".t_grade").html(item[i].grade);
                //修改按钮组装
                var ucontent = "<button type='button'  class='btn btn-primary btn-xs btn-warning' style='margin-right: 10px;' onclick='toUpdate(" + item[i].id + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>修改</span></button>"
                var dcontent = "<button type='button'  class='btn btn-primary btn-xs btn-danger' onclick='deleteGrade(" + item[i].id + ");' ><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></span><span style='margin: 0px 8px;'>删除</span></button>"
                temp.find(".t_update").html(ucontent);
                //删除按钮组装
                temp.find(".t_operate").html(dcontent);
                $("#result").append(temp);
            }
        }
    })
};

function deleteGrade(gradeId) {
    bootbox.setDefaults('locale', 'zh_CN');
    bootbox.confirm('确认要删除选择的数据吗?', function (res) {
        if (res) {
            var url = basePath()+"/teacher/grade/delete_grade.do";
            var data = {"gradeId": gradeId};
            $.get(url, data, function (data) {
                if (data.status == 0) {
                    bootbox.alert("删除成功");
                    getGrades(page);
                } else {
                    bootbox.alert(data.msg);
                }
            }, "JSON");
        }
    });
};

function toUpdate(gradeId) {
    var url = basePath()+"/teacher/grade/toupdate_grade.do";
    var data = {"gradeId": gradeId};
    $.get(url, data, function (data) {
        if (data.status == 0) {
            $("#newGradeName").val(data.data.name);
            $("#newGradeNameId").val(gradeId);
            $("#newGrade option[value=" + data.data.grade + "]").attr("selected", "selected");
            $("#changGrade").modal("show");
        } else {
            bootbox.alert(data.msg);
        }
    }, "JSON");

}

function updateGrade() {
    var url = basePath()+"/teacher/grade/update_grade.do";
    var grade = $("#newGrade").val();
    var name = $("#newGradeName").val();
    var id = $("#newGradeNameId").val();
    var data = {"id": id, "name": name, "grade": grade};
    $.post(url, data, function (data) {
        if (data.status == 0) {
            $("#changGrade").modal("hide");
            getGrades(page);
            bootbox.alert(data.msg);
        } else {
            bootbox.alert(data.msg);
        }
    }, "JSON");

};

function addGrade() {

    // 提交前先主动验证表单
    var bv = $("#addGrade").data('bootstrapValidator');
    bv.validate();
    if (!bv.isValid()) {
        return;
    }

    $.ajax({
        url: basePath()+"/teacher/grade/add_grade.do",
        type: "post",
        async: "true",
        data: {"name": $('#gradeName1').val(), "grade": $('#grade1').val()},
        dataType: "json",
        success: function (data) {
            if (data.status == 0) {
                clearAddModel();
                bootbox.alert("添加成功");
                getGrades(page);
            }
            else {
                bootbox.alert(data.msg);
            }
        }
    });
}

function clearUpdateModel() {
    $('#changGrade').modal('hide');
    $('#newGradeName').val("");
    $('#changGrade').data('bootstrapValidator').destroy();
    $('#changGrade').data('bootstrapValidator', null);
    $("form[id='addForm'] > div").addClass('has-success');

    validatorU();
}

function clearAddModel() {
    $('#addGrade').modal('hide');
    $('#gradeName1').val("");
    $('#addGrade').data('bootstrapValidator').destroy();
    $('#addGrade').data('bootstrapValidator', null);
    $("form[id='addForm'] > div").addClass('has-success');
    validatorA();
}
function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}


loading();

$(function () {
    getPostList(1);
    validatorU();
    validatorA();
    $("#add_class_btn").click(function () {
        add();
    });
    $("#update_class_btn").click(function () {
        update();
    });

})

function validatorU() {
    $('#updateForm').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            newClassName: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '岗位名不能为空'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5]+$/,
                        message: '岗位名必须为汉字'
                    },
                    stringLength: {
                        min: 2,
                        max: 5,
                        message: '长度必须在2位到5位之间'
                    }
                }
            }
        }
    });
}

function validatorA() {
    $('#addForm').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            className1: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '岗位名不能为空'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5]+$/,
                        message: '岗位名必须为汉字'
                    },
                    stringLength: {
                        min: 2,
                        max: 5,
                        message: '长度必须在2位到5位之间'
                    }
                }
            }
        }
    });
}

var page_now = 1;
var this_page_num = 0;
var item_num = 10;

function getPostList(page_num) {
    page_now = page_num;
    $.ajax({
        url: basePath()+'/ep/post/post_list.do',
        type: 'GET',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "8"
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            var page = data.data;
            var item = data.data.list
            this_page_num = item.length;
            setPage(page.pageNum, page.pages, "getPostList");
            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", item[i].id);
                temp.removeAttr("style");
                temp.find(".c_num").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.find(".c_name").html(item[i].name);
                temp.find(".c_content").html(item[i].other);
                //修改按钮组装
                var up_btn = "<button type='button'  class='btn btn-primary btn-xs btn-warning' onclick='toModal(this);' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>修改</span></button>"
                temp.find(".c_update").html(up_btn);
                var op_btn = "<button type='button'  class='btn btn-primary btn-xs btn-danger' onclick='toChange(this);' ><span class='glyphicon glyphicon-remove' aria-hidden='true'></span><span style='margin: 0px 8px;'>删除</span></button>"
                temp.find(".c_op").html(op_btn);
                $("#result").append(temp);
            }
        }
    })
}

function add() {

    // 提交前先主动验证表单
    var bv = $("#addForm").data('bootstrapValidator');
    bv.validate();
    if (!bv.isValid()) {
        return;
    }

    var name = $("#className1").val();
    $.ajax({
        url: basePath()+'/ep/post/add_post.do',
        type: 'POST',
        async: 'true',
        data: {
            "name": name,
        },
        dataType: 'json',
        success: function (res) {
            if (res.success) {

                bootbox.alert(res.msg);
                clearAddModal();
                getPostList(1);

            } else {
                bootbox.alert(res.msg);
                getPostList(1);
            }

        }
    })
}

function clearAddModal() {
    $('#addClass').modal('hide');
    $('#className1').val("");

    $('#addForm').data('bootstrapValidator').destroy();
    $('#addForm').data('bootstrapValidator', null);
    $("form[id='addForm'] > div").addClass('has-success');
    validatorA();
}

//更新备注
function update() {

    // 提交前先主动验证表单
    var bv = $("#updateForm").data('bootstrapValidator');
    bv.validate();
    if (!bv.isValid()) {
        return;
    }

    var new_content = $("#newClassName").val();
    var id = $("#hello").val();
    $.ajax({
        url: basePath()+'/ep/post/update_post.do',
        type: 'POST',
        async: 'true',
        data: {
            "name": new_content,
            "id": id
        },
        dataType: 'json',
        success: function (res) {
            if (!res.success) {
                clearUpdateModal();
                bootbox.alert(res.msg);
                getPostList(1);
            } else {
                bootbox.alert(res.msg);
                $('body').css('padding-right', '0px');
                clearUpdateModal();
                getPostList(1);
            }
        }
    })
}

function clearUpdateModal() {
    $('#changClass').modal('hide');
    $('#newClassName').val("");

    $('#updateForm').data('bootstrapValidator').destroy();
    $('#updateForm').data('bootstrapValidator', null);
    $("form[id='updateForm'] > div").addClass('has-success');
    validatorU();
}

function toModal(what) {
    var c_id = $(what).parent().parent().attr("id");
    console.log(c_id);
    $("#hello").val(c_id);
    $("#changClass").modal("show");
}

//注销班级
function toChange(what) {
    bootbox.setDefaults("locale","zh_CN");
    bootbox.confirm("确认删除?", function (res) {
        if (res) {
            var c_id = $(what).parent().parent().attr("id");
            $.ajax({
                url: basePath()+'/ep/post/delete_post.do',
                method: 'POST',
                async: 'true',
                data: {
                    "id": c_id
                },
                dataType: 'json',
                success: function (res) {
                    bootbox.alert(res.msg);
                    getPostList(1);
                }
            })
        }
    });
}
function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}
loading();

$(function () {
    getAllDirections();
    getAllScore(1);
});

function getAllDirections() {
    $.ajax({
        url: basePath()+'/teacher/exam/get_all_directions.do',
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                $.each(res.data, function (index, item) {
                    var item = "<option value='" + item.id + "'>" + item.name + "</option>";
                    $('#deptName').append(item);
                });
            }
        }
    });
}

function select(page_num) {
    page_now = page_num;
    var queType = $("#queType").val();
    var direct = $("#deptName").val();
    $.ajax({
        url: basePath()+'/teacher/correct/ls_show_all.do',
        type: 'GET',
        async: true,
        data: {
            "pageNum": page_num,
            "pageSize": "1",
            "deptName": direct,
            "queType": queType
        },
        dataType: 'JSON',
        success: function (data) {
            removeLoading();
            var page = data.data;
            var item = data.data.list;
            setPage(page.pageNum, page.pages, "select");
            $("#score_result").html("");
            for (var i = 0; i < item.length; i++) {
                var title = $("#ls_correct");
                title.removeAttr("style");
                var temp = $("#m_template").clone();
                temp.attr("id", i);
                temp.removeAttr("style");
                temp.find(".e_name").html("<xmp>"+item[i].examName+"</xmp>");
                temp.find(".s_direction").html(item[i].directionName);
                temp.find(".q_contentitle").html("<xmp>"+item[i].smallTitle+"</xmp>");
                var queType;
                if (item[i].queType == 1) {
                    queType = '简答题'
                } else {
                    queType = '编程题'
                }
                temp.find((".q_type")).html(queType);

                temp.find(".s_score1").html(item[i].score1);
                temp.find(".s_score2").html(item[i].score2);
                temp.find(".s_score3").html(item[i].score3);
                temp.find(".s_scoreb").html(item[i].scoreb);
                temp.find(".testtime").html(item[i].testtime);
                if (item[i].status == 0) {
                    temp.find("#q_update").attr("href", basePath()+"/teacher/correct/ls_show_update.do?eId=" + item[i].eId +
                        "&sId=" + item[i].sId +
                        "&queType=" + item[i].queType +
                        "&direction=" + item[i].direction+
                        "&bqScore="+item[i].bqScore+
                        "&bigQuestionId="+item[i].bigQuestionId);
                } else if (item[i].status == 1) {
                    temp.find("#choice").html("<span class='label label-success'>完成阅卷</span>");
                }
                //插入结果
                $("#score_result").append(temp);
            }

        }
    })


}

function updateScore(eId, sId, title, ans, type, direct) {
    $.ajax({
        url: basePath()+'/teacher/correct/ls_show_update.do',
        type: 'POST',
        async: true,
        data: {
            "eId": eId,
            "sId": sId,
            "title": title,
            "ans": ans,
            "type": type,
            "direct": direct
        },
        dataType: 'JSON',
        success: function (data) {
            $("#tigan").val(data.data.title);
            $("#daan").val(data.data.ans);
            $("#eId").val(data.data.examId);
            $("#sId").val(data.data.sId);
            $("#title").val(data.data.title);
            $("#type").val(data.data.type);
            $("#ans").val(data.data.ans);
            $("#direct").val(data.data.direct);
            $("#changScore").modal("show");
        }
    })
}

function getAllScore(page_num) {
    page_now = page_num;
    $.ajax({
        url: basePath() + '/teacher/correct/ls_show_all.do',
        type: 'GET',
        async: true,
        data: {
            "pageNum": page_num,
            "pageSize": "1"
        },
        dataType: 'JSON',
        success: function (data) {
            removeLoading();
            var page = data.data;
            var item = data.data.list;
            setPage(page.pageNum, page.pages, "getAllScore");
            $("#score_result").html("");
            for (var i = 0; i < item.length; i++) {
                var title = $("#ls_correct");
                title.removeAttr("style");
                var temp = $("#m_template").clone();
                temp.attr("id", i);
                temp.removeAttr("style");
                temp.find(".e_name").html(item[i].examName);
                temp.find(".s_direction").html(item[i].directionName);
                temp.find(".q_contentitle").html(item[i].smallTitle);
                var queType;
                if (item[i].queType == 1) {
                    queType = '简答题'
                } else {
                    queType = '编程题'
                }
                temp.find((".q_type")).html(queType);

                temp.find(".s_score1").html(item[i].score1);
                temp.find(".s_score2").html(item[i].score2);
                temp.find(".s_score3").html(item[i].score3);
                temp.find(".s_scoreb").html(item[i].scoreb);
                temp.find(".testtime").html(item[i].testtime);
                if (item[i].status == 0) {
                    temp.find("#q_update").attr("href", basePath() + "/teacher/correct/ls_show_update.do?eId=" + item[i].eId +
                        "&sId=" + item[i].sId +
                        "&queType=" + item[i].queType +
                        "&direction=" + item[i].direction +
                        "&bqScore=" + item[i].bqScore +
                        "&bigQuestionId=" + item[i].bigQuestionId);
                } else if (item[i].status == 1) {
                    temp.find("#choice").html("<span class='label label-success'>完成阅卷</span>");
                }
                $("#score_result").append(temp);
            }

        }
    })
}
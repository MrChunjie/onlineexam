function basePath() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPath + projectName);
}

loading();
var width = 0;

function getDetail() {
    var t_val = $("#divide_class_select").find("option:selected").val();
    if (t_val == 'three_stage') {
        $(".s_three_div").show();
        $(".s_curve_div").hide();
        $("#divide_class_btn").show();
        $('.dropdown-toggle').attr('style', '');
    } else if (t_val == 's_curve') {
        $(".s_curve_div").show();
        $(".s_three_div").hide();
        $("#divide_class_btn").show();
        if (width != 0) {
            $('.dropdown-toggle').attr('style', 'text-overflow: ellipils; overflow: hidden; width: ' + width + 'px;');
        }
    } else {
        $(".s_curve_div").hide();
        $(".s_three_div").hide();
        $("#divide_class_btn").hide();
    }
}

var options = [];

function getAllClassByDirection(direction) {
    $.ajax({
        url: basePath() + '/edu/class/list_by_type.do',
        type: 'GET',
        data: {
            'direction': direction
        },
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                $(".selectpicker").selectpicker({
                    noneSelectedText: '请选择班级',
                    style: 'btn-default'
                });
                $('.selectpicker').empty();
                $.each(res.data, function (index, item) {
                    var op = "<option value='" + item.id + "'>" + item.name + "(" + item.other  + ")</option>";
                    $('.selectpicker').append(op);
                    options.push(item.id);
                });
                $('.selectpicker').selectpicker('refresh');
            } else {
                $('.selectpicker').empty();
                $(".selectpicker").selectpicker({
                    noneSelectedText: res.msg
                });
                $('.selectpicker').selectpicker('refresh');
            }
        }
    })
}



function selectCLass(id) {

    //级联 移除
    var classes = ['one_level_class', 'two_level_class', 'three_level_class'];
    var ids = $('#' + id).selectpicker('val');

    if (ids != null) {
        ids = ids.toString().split(",");
        for (var j = 0; j < classes.length; j++) {
            if (classes[j] == id) {
                continue;
            } else {
                for (var i = 0; i < ids.length; i++) {
                    $('#' + classes[j]).find("[value='" + ids[i] + "']").attr('style', 'display: none;');
                    $('#' + classes[j]).selectpicker('refresh');
                }

            }
        }
    }


    var choose = [];
    var nochoose = [];

    // 获取所有选中的班级id
    for (var i = 0; i < classes.length; i++) {
        var classIds = $('#' + classes[i]).selectpicker('val');
        if (classIds != null) {
            classIds = classIds.toString().split(",");
            for (var j = 0; j < classIds.length; j++) {
                choose.push(classIds[j]);
            }
        }
    }

    // 所有未被选择的班级 id
    for (var i = 0; i < options.length; i++) {
        var isExist = false;
        for (var j = 0; j < choose.length; j++) {
            if (options[i] == choose[j]) {
                isExist = true;
                break;
            }
        }
        if (isExist == false) {
            nochoose.push(options[i]);
        }
    }

    //级联 检测取消选中项 恢复的操作
    for (var i = 0; i < nochoose.length; i++) {
        for (var j = 0; j < classes.length; j++) {
            $('#' + classes[j]).find("[value='" + nochoose[i] + "']").attr('style', 'display: block;');
            $('#' + classes[j]).selectpicker('refresh');
        }
    }

}

function divideBtnOnclick() {
    cal();
    $.ajax({
        url: basePath() + '/edu/group/get_divide_result.do',
        type: 'GET',
        success: function (res) {
            if (res.success) {
                if (res.data) {
                    if (!confirm("您已经进行过分班，是否重新进行分班")) {
                        return false;
                    }
                }
                divideClass();
            }
        }
    })
}

function divideClass() {
    var t_val = $("#divide_class_select").find("option:selected").val();
    if (t_val == "three_stage") {
        divideByThreeStage();
    } else if (t_val == "s_curve") {
        divideClassBySCurve();
    } else {
        bootbox.alert("请选择分班方式");
    }
}

function selectAll() {
    width = $('.bootstrap-select').width();
    $('.dropdown-toggle').attr('style', 'text-overflow: ellipils; overflow: hidden; width: ' + width + 'px;');
    $('#class_num').selectpicker('selectAll');
}

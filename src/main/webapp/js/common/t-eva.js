function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}
loading();

var page_now = 1;

function getPapers(page_num) {
    page_now = page_num;
    var stuClassId = $("#stuClassId").val();
    // Ajax异步请求,平均成绩
    $.ajax({
        url: basePath()+'/teacher/eva/show_avg.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "10",
            "stuClassId": stuClassId
        },
        dataType: 'json',
        success: function (data) {

            removeLoading();

            var page = data.data;
            setPage(page.pageNum, page.pages, "getPapers");
            var item = data.data.list;
            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.removeAttr("style");
                temp.find(".p_id").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.find(".p_className").html(item[i].className);
                temp.find(".p_other").html(item[i].other);
                temp.find(".p_teacher").html(item[i].teacher);
                temp.find(".p_courseName").html(item[i].course);
                temp.find(".p_examName").html(item[i].examNames);
                temp.find(".p_avg").html(item[i].avg);
                temp.find("#enter").attr("href", basePath()+"/t-eva-detail.do?classId=" + item[i].classId+"&examId="+item[i].examId);
                temp.find("#download").attr("onclick", "doDownload("+item[i].classId+","+item[i].examId+")");
                $("#result").append(temp);
            }
        }
    });
}

function doDownload(classId, examId) {

    var dialog = bootbox.dialog({
        title: '请稍候',
        message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 导出中...</p>'
    });

    var tr = $('#result').find("a[onclick='doDownload("+ classId +","+examId +")']").parent().parent();
    var other = tr.find('.p_other').html();
    var examName = tr.find('.p_examName').html();
    var url = basePath()+'/teacher/eva/avg_output_excel.do' + "?classId=" + classId + "&examId=" + examId;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);        // 也可以使用POST方式，根据接口
    xhr.responseType = "blob";    // 返回类型blob
    // 定义请求完成的处理函数，请求前也可以增加加载框/禁用下载按钮逻辑
    xhr.onload = function () {
        // 请求完成
        if (this.status === 200) {
            // 返回200
            var blob = this.response;
            var reader = new FileReader();
            reader.readAsDataURL(blob);    // 转换为base64，可以直接放入a表情href
            reader.onload = function (e) {
                // 转换完成，创建一个a标签用于下载
                var a = document.createElement('a');
                a.download = "成绩评估-" + other + "-"+ examName + ".xlsx";
                a.href = e.target.result;
                $("body").append(a);    // 修复firefox中无法触发click
                a.click();
                $(a).remove();
                dialog.find('.modal-title').html('完成');
                dialog.find('.modal-body').html("导出成功");
                dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\">OK</button></div>");

            }
        }
    };
    // 发送ajax请求
    xhr.send();

}

function getClasses() {
    // Ajax异步请求,班级列表
    var stuClassId = $("#stuClassId").val();
    $.ajax({
        url: basePath()+'/teacher/correct/class_list.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": 1,
            "pageSize": "99"
        },
        dataType: 'json',
        success: function (data) {
            var item = data.data.list;
            for (var i = 0; i < item.length; i++) {
                if(item[i].id==stuClassId){
                    continue;
                }else {
                    $("#stuClassId").append("<option value='" + item[i].id + "'>" + item[i].name + "</option>");
                }
            }
        }
    });
}
$(function () {
    getClasses();
    getPapers(1);
});
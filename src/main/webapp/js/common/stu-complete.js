function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}


loading();

var initial = "";
var edited = 0;
var direction = "";

var directionMap = {}

$(function () {

    $("#restudytime").datetimepicker({
        language: 'zh',
        format: 'yyyy-mm-dd',
        minView: 'month',
        language: 'zh-CN',
        autoclose: true,//选中自动关闭
        startDate: '1900-01-01',
        todayBtn: true,//显示今日按钮
        pickerPosition: 'top-right'
    });

    validator();

    $.ajax({
        url: basePath()+'/student/common/all_direction_class.do',
        dataType: 'json',
        success: function (res) {
            $('#direction').empty();
            if (res.success) {
                $.each(res.data, function (index, item) {
                    directionMap[item.name] = item.id;
                    $('#direction').append("<option value='" + item.id+ "'>" + item.name + "</option>");
                });
            }else {
                $('#direction').append("<option>" + res.msg + "</option>");
            }
        }
    });

    getAllProvince();

    $.ajax({
        url: basePath()+'/student/common/get_studentInfo.do',
        type: 'get',
        async: true,
        dataType: 'json',
        success: function (res) {
            removeLoading();
            initial = res.direction;

            if (initial != null) {
                $('#direction').attr('disabled', 'disabled');
                edited = 1;
            }
            // $("#direction option[value='"+ initial + "']").prop('selected', true);

            if (initial != null) {
                $.ajax({
                    url: basePath() + '/student/common/get_direction_name.do',
                    data: {'id': initial},
                    async: true,
                    dataType: 'json',
                    success: function (res) {
                        if (res.success) {
                            $('#direction').append("<option value='" + initial + "' selected>" + res.data + "</option>");
                        }
                    }
                });
            }

            $('#dorm').val(res.dorm);
            if (res.parent != null) {
                var parent = res.parent.split(/;|:/);
                $('#fatherName').val(parent[0]);
                $('#fatherPhone').val(parent[1]);
                $('#matherName').val(parent[2]);
                $('#matherPhone').val(parent[3]);
            }
            if (res.home != null) {
                var home = res.home.split(";");
                $("#province option").removeAttr("selected");
                $("#province option[value = '" + home[0] + "'] ").prop("selected", true);
                getCity(home[0]);
                $("#city option:contains('" + home[1] + "') ").prop("selected", true);
                $('#detail').val(home[2]);
            }
            $('#job').val(res.job == "无" ? "" : res.job);
            if (res.counselor != null) {
                var conuselor = res.counselor.split(":");
                $('#counselorName').val(conuselor[0]);
                $('#counselorPhone').val(conuselor[1]);
            }
            $('#english').val(res.english);

            if (res.skill != null) {
                var skill = res.skill.split(";");
                $('#skill').val(skill[0]);
                $('#award').val(skill[1]);
            }
            $('#restudyname').val(res.restudyname == "无" ? "" : res.restudyname);

            $('#restudynum').val(res.restudynum == "0" ? "" : res.restudynum);

            $('#time').val(res.restudytime == "2000-01-01" ? "" : res.restudytime);

        }
    });



});

function validator() {
    $("#form").bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            /*验证*/
            dorm: {
                message: 'The dorm is not valid',
                validators: {
                    notEmpty: {
                        message: '宿舍号不能为空'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: "宿舍号不合法，请重新输入"
                    },
                    stringLength: {
                        max: 10,
                        message: '宿舍号最长为10位'
                    }
                }
            },
            fatherName: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: '父亲姓名不能为空'
                    },
                    stringLength: {
                        min: 2,
                        max: 5,
                        message: '姓名必须在2-5位之间'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5]+$/,
                        message: "姓名不合法，请重新输入"
                    }
                }
            },
            fatherPhone: {
                message: 'The phone is not valid',
                validators: {
                    notEmpty: {
                        message: '父亲电话不能为空'
                    },
                    regexp: {
                        regexp: '^1(3|4|5|7|8)\\d{9}$',
                        message: '手机号不合法, 请重新输入'
                    },
                    stringLength: {
                        min: 11,
                        max: 11,
                        message: '电话必须为11位'
                    }

                }
            },
            matherName: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: '母亲姓名不能为空'
                    },
                    stringLength: {
                        min: 2,
                        max: 5,
                        message: '姓名必须在2-5位之间'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5]+$/,
                        message: "姓名不合法，请重新输入"
                    }
                }
            },
            matherPhone: {
                message: 'The phone is not valid',
                validators: {
                    notEmpty: {
                        message: '母亲电话不能为空'
                    },
                    regexp: {
                        regexp: '^1(3|4|5|7|8)\\d{9}$',
                        message: '手机号不合法, 请重新输入'
                    },
                    stringLength: {
                        min: 11,
                        max: 11,
                        message: '电话必须为11位'
                    }

                }
            },
            detail: {
                message: 'The home is not valid',
                validators: {
                    notEmpty: {
                        message: '家庭详细住址不能为空'
                    },
                    stringLength: {
                        min: 6,
                        message: '家庭详细住址不能少于六位'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5a-zA-Z0-9#]+$/,
                        message: "家庭住址不合法，请重新输入"
                    }
                }
            },
            counselorName: {
                message: 'This name is not valid',
                validators: {
                    notEmpty: {
                        message: '导员姓名不能为空'
                    },
                    stringLength: {
                        min: 2,
                        max: 5,
                        message: '姓名必须在2-5位之间'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5]+$/,
                        message: "姓名不合法，请重新输入"
                    }
                }
            },
            counselorPhone: {
                message: 'The phone is not valid',
                validators: {
                    notEmpty: {
                        message: '导员电话不能为空'
                    },
                    regexp: {
                        regexp: '^1(3|4|5|7|8)\\d{9}$',
                        message: '手机号不合法, 请重新输入'
                    },
                    stringLength: {
                        min: 11,
                        max: 11,
                        message: '电话必须为11位'
                    }
                }
            },
            english: {
                message: 'the english is not valid',
                validators: {
                    notEmpty: {
                        message: '外语水平至少选择一个'
                    }
                }
            },
            restudynum: {
                message: 'this is not valid',
                validators: {
                    regexp: {
                        regexp:  /^[0-9]*$/,
                        message: '重修课数量不合法'
                    },
                    stringLength: {
                        max: 2,
                        message: '数量必须在2位以内'
                    }
                }
            },
            job: {
                message: '',
                validators: {
                    stringLength: {
                        max: 10,
                        message: '职务长度必须在10位以内'
                    }
                }
            }
        }
    });
}

function complete() {

    // 提交前先主动验证表单
    var bv = $("#form").data('bootstrapValidator');
    bv.validate();
    if (!bv.isValid()) {
        bootbox.alert("请查看红色字段");
        return;
    }

    direction = $('#direction').val();

    var diectionName = $('#direction').find("option[value='"+ direction +"']").html();

    if (directionMap[diectionName] != direction) {
        bootbox.alert("方向错误, 请重新填写", function () {
            window.location.reload();
        });
        return;
    }

    if (edited == 1 && initial != direction) {
        bootbox.alert("方向已经修改过, 不能再次修改");
        return;
    }

    if (direction == '未设置方向') {
        direction = undefined;
    }

    var dorm = $('#dorm').val();
    var parent = $('#fatherName').val() + ":" + $('#fatherPhone').val() + ";" + $('#matherName').val() + ":" + $('#matherPhone').val();
    var home = $('#province').val() + ";" + $('#city :selected').html() + ";" + $('#detail').val();
    var job = $('#job').val();
    job = job == "" ? "无" : job;
    var counselor = $('#counselorName').val() + ":" + $('#counselorPhone').val();
    var english = $('#english').val();
    var skill = $('#skill').val() + ";" + $('#award').val();
    skill = skill == ";" ? "无" : skill;
    var restudyname = $('#restudyname').val();
    restudyname = restudyname == "" ? "无" : restudyname;
    var restudynum = $('#restudynum').val();
    restudynum = restudynum == "" ? "0" : restudynum;
    var restudytime = $('#time').val();
    restudytime = restudytime == "" ? "2000-01-01" : restudytime;

    var position = $('#city').val();

    $.ajax({
        url: basePath()+'/student/common/update_info.do',
        type: 'POST',
        data: {
            'direction': direction, 'dorm': dorm, 'parent': parent, 'home': home, 'job': job,
            'counselor': counselor, 'english': english, 'skill': skill,
            'restudyname': restudyname, 'restudynum': restudynum, 'restudytime': restudytime,
            'position': position
        },
        dataType: 'JSON',
        success: function (res) {
            if (res.success) {
                bootbox.alert(res.msg);
                $('#submit').removeAttr('disabled');
                if ($('#direction').val() != "未设置方向") {
                    $('#direction').attr('disabled', 'disabled');
                }
                edited = 1;
                initial = direction;
            } else {
                bootbox.alert(res.msg);
            }
        }
    });
}

function getAllProvince() {
    $.ajax({
        url: basePath()+'/student/common/get_all_province.do',
        dataType: 'json',
        async: false,
        success: function (res) {
            if (res.success) {
                $.each(res.data, function (index, item) {
                    var op = "<option value='" + item + "'>" + item + "</option>";
                    $('#province').append(op);
                });
            }
        }
    });
}

function getCity1() {
    var province = $('#province').val();

    $.ajax({
        url: basePath()+'/student/common/get_city.do',
        type: 'post',
        async: false,
        data: {'province': province},
        dataType: 'json',
        success: function (res) {

            //移除原有的数据
            $('#city').empty();

            if (res.success) {
                $.each(res.data, function (index, item) {
                    var op = "<option value='" + item.id + "'>" + item.city + "</option>";
                    $('#city').append(op);
                });
            } else {

            }
        }

    });
}

function getCity(province) {

    $.ajax({
        url: basePath()+'/student/common/get_city.do',
        type: 'post',
        async: false,
        data: {'province': province},
        dataType: 'json',
        success: function (res) {

            //移除原有的数据
            $('#city').empty();

            if (res.success) {
                $.each(res.data, function (index, item) {
                    var op = "<option value='" + item.id + "'>" + item.city + "</option>";
                    $('#city').append(op);
                });
            } else {

            }
        }

    });
}

function reminding() {
    bootbox.alert("此方向只能修改一次，提交后则不能修改");
}

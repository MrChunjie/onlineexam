function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}


if(self.location!=top.location){
    top.location=self.location;
}

$(function () {

    var height = document.documentElement.clientHeight;
    $('#loginDiv').attr('style', 'padding-top:' + (height/3.5) + "px;");

    $("#form").bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            /*输入框不同状态，显示图片的样式*/
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            /*验证*/
            username: {
                /*键名username和input name值对应*/
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: '用户名不能为空'
                    },
                    stringLength: {
                        /*长度提示*/
                        min: 4,
                        max: 12,
                        message: '用户名长度必须在4到12位之间'
                    },
                    regexp:{
                        regexp: /^(?=[0-9a-zA-Z\u4e00-\u9fa5]+$)/,
                        message: '用户名只能由中文，数字和英文字母组成'
                    }
                }
            },
            password: {
                message: '密码无效',
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    },
                    stringLength: {
                        min: 2,
                        max: 30,
                        message: '密码长度必须在2到30之间'
                    },
                    regexp:{
                        regexp: /^[^ ]+$/,
                        message: '密码不能有空格'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e) {
        e.preventDefault();
        login();
    });
});

function login() {
    var identity = $("#identity").val();
    var username = $("#username").val();
    var password = SHA($("#password").val());
    if (identity === "admin") {
        m_ajax(username, password, "admin/login.do", "/admin_index.do");
    } else if (identity === "teacher") {
        m_ajax(username, password, "teacher/common/login.do", "/teacher_index.do");
    } else {
        m_ajax(username, password, "student/common/login.do", "/stu_index.do");
    }

}

function m_ajax(username, password, url, result_url) {
    $.ajax({
        url: url,
        method: 'POST',
        data: {
            'username': username,
            'password': password
        },
        success: function (res) {
            if (res.success) {
                window.location.href = basePath() + result_url;
            } else {
                alert(res.msg);
                window.location.href = window.location.href;
            }
        }
    })
}


function register() {
    $.ajax({
        url:basePath()+'/student/common/register_open_or_not.do',
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                window.location.href = basePath()+"/register.do";
            } else {
                alert(res.msg);
            }
        }
    });
}


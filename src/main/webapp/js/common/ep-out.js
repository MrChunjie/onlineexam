function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}


var page = 1;
var page_item_num = 0;
var item_num = 10;

loading();

$(function () {
    search(1);

    $("#signStime").datetimepicker({
        language: 'zh',
        format: 'yyyy-mm-dd hh:ii',
        language: 'zh-CN',
        autoclose: true,//选中自动关闭
        initialDate: new Date(),
        startDate: "2014-01-01 00:00",
        todayBtn: true,//显示今日按钮
        pickerPosition: 'bottom-right'
    });

});

function search(pageNum) {
    var date = $("#stime").val();
    if(date.trim() == ""){
        date = "2014-01-01 00:00";
    }

    $.ajax({
        url:  basePath()+'/ep/manager/get_stime_enterprises.do',
        type: 'POST',
        async: 'true',
        data: {
            "sTime":date,
            "pageNum": pageNum,
            "pageSize": item_num
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            //显示列表div
            $("#displayList").css("display","");
            var pageResult = data.data;
            var item = data.data.list;
            page_item_num = item.length;
            max_page_num = pageResult.pages;
            page = setPage(pageResult.pageNum, pageResult.pages, "search");
            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", item[i].id);
                temp.removeAttr("style");
                temp.find(".ep_num").html(pageResult.pageSize * (pageResult.pageNum - 1) + 1 + i);
                temp.find(".ep_name").html(item[i].name);
                temp.find(".ep_stime").html(item[i].sTime);
                temp.find(".ep_etime").html(item[i].eTime);
                if (item[i].type == 0) {
                    temp.find(".ep_type").html("全部");
                }
                if (item[i].type == 1) {
                    temp.find(".ep_type").html("开发");
                }
                if (item[i].type == 2) {
                    temp.find(".ep_type").html("测试");
                }

                if (item[i].category == 0) {
                    temp.find(".ep_category").html("招聘");
                }
                if (item[i].category == 1) {
                    temp.find(".ep_category").html("定制");
                }

                var info = "<h3>正文</h3>" + item[i].text + "<br><h3>岗位需求</h3><br>";
                for (var j = 0; j < item[i].posts.length; j++) {
                    info += item[i].posts[j].name + "<br>";
                }

                temp.find(".ep_category").before("<input type='hidden' value='" + info + "'/>");
                //正文按钮组装
                var enterpriseInfo = "<button type='button'  class='btn btn-primary btn-xs' onclick='toInfo(this);' ><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span><span style='margin: 0px 8px;'>查看</span></button>"
                temp.find(".ep_info").append(enterpriseInfo);
                $("#result").append(temp);
            }
        }
    })
};

function toInfo(temp) {
    var text = $(temp).parent().parent().children(":input").val();
    $("#enterpriseTextInfo").html(text);
    $("#enterpriseInfo").modal("show");
}

function print() {
    var date = $("#stime").val();
    if(date.trim() == ""){
        date = "2014-01-01 00:00";
    }
    var url = basePath()+"/ep/out/output_enterprise.do?sTime=" + date;
    window.location.href = url;
}



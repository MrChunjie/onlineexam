function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}


loading();

$(function () {
    getPostList(1);
})
var page_now = 1;
var this_page_num = 0;
var item_num = 10;
function getPostList(page_num) {
    page_now = page_num;
    $.ajax({
        url: basePath()+'/student/class/class_history.do',
        type: 'GET',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "8"
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            var page = data.data;
            var item = data.data.list
            this_page_num = item.length;
            setPage(page.pageNum, page.pages, "getPostList");
            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", item[i].id);
                temp.removeAttr("style");
                temp.find(".c_num").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.find(".c_name").html(item[i].sName);
                temp.find(".c_content").html(getclass(item[i].classId));
                temp.find(".c_history").html(item[i].historyTime);
                $("#result").append(temp);
            }
        }
    })
}

function getclass(id) {
    var class_name = "";
    $.ajax({
        url:basePath()+'/student/class/get_class.do',
        async : false,
        data:{
            id:id
        },
        dataType:'json',
        success:function (data) {
            class_name=data.data.name;
        }
    })
    return class_name;
}
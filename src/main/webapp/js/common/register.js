function basePath() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPath + projectName);
}


$(function () {

    $.ajax({
        url: basePath() + '/student/common/register_open_or_not.do',
        dataType: 'json',
        success: function (res) {
            if (res.success) {

            } else {
                bootbox.alert(res.msg, function () {
                    window.location.href = basePath() + "/index.do";
                });
            }
        }
    });

    $("#form").bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

            name: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: '姓名名不能为空'
                    },
                    stringLength: {
                        min: 2,
                        max: 6,
                        message: '姓名长度必须在2到6之间'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5·]+$/,
                        message: "姓名不合法，请重新输入"
                    }
                }
            },
            phone: {
                message: 'The phone is not valid',
                validators: {
                    notEmpty: {
                        message: '电话不能为空'
                    },
                    regexp: {
                        regexp: '^1(3|4|5|7|8|9)\\d{9}$',
                        message: '手机号不合法, 请重新输入'
                    },
                    stringLength: {
                        min: 11,
                        max: 11,
                        message: '电话必须为11位'
                    }

                }
            },
            nation: {
                message: 'The nation is not valid',
                validators: {
                    notEmpty: {
                        message: '民族不能为空'
                    },
                    stringLength: {
                        min: 2,
                        max: 5,
                        message: '民族长度必须在2位到5位之间'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5]+$/,
                        message: "民族不合法，请重新输入"
                    }
                }
            },
            idCard: {
                message: 'The idCard is not valid',
                validators: {
                    notEmpty: {
                        message: '身份证号不能为空'
                    },
                    regexp: {
                        regexp: /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/,
                        message: '身份证号不合法, 请重新输入'
                    }
                }
            },
            school: {
                message: 'This school is not valid',
                validators: {
                    notEmpty: {
                        message: '学校不能为空'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5]+$/,
                        message: "学校不合法，请重新输入"
                    },
                    stringLength: {
                        max: 20,
                        message: '学校名称最长20位'
                    }
                }
            },
            college: {
                message: 'The college is not valid',
                validators: {
                    notEmpty: {
                        message: '院系不能为空'
                    },
                    stringLength: {
                        max: 20,
                        message: '院系最长为20位'
                    }
                }
            },
            major: {
                message: 'the major is not valid',
                validators: {
                    notEmpty: {
                        message: '专业不能为空'
                    },
                    stringLength: {
                        max: 20,
                        message: '专业最长为20位'
                    }
                }
            },
            s_class: {
                message: 'The class is not valid',
                validators: {
                    notEmpty: {
                        message: '班级不能为空'
                    },
                    stringLength: {
                        max: 20,
                        message: '原班级最长为20位'
                    }
                }
            },
            id: {
                message: 'The id is not valid',
                validators: {
                    notEmpty: {
                        message: '原学号不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]*$/,
                        message: '学号不合法, 请重新输入'
                    },
                    stringLength: {
                        max:20,
                        message: '学号最长20位'
                    }
                }
            }
        }
    });
});

//注册功能
function doRegister() {

    // 提交前先主动验证表单
    var bv = $("#form").data('bootstrapValidator');
    bv.validate();
    if (!bv.isValid()) {
        bootbox.alert("请查看红色字段");
        return;
    }

    var r_name = $('#name').val();
    var r_sex = $('#sex').val();
    var r_phone = $('#phone').val();
    var r_nation = $('#nation').val();
    var r_id_card = $('#idCard').val();
    var r_school = $('#school').val();
    var r_college = $('#college').val();
    var r_major = $('#major').val();
    var r_s_class = $('#s_class').val();
    var r_id = $('#id').val();
    var r_intention = $('#intention').val();
    var r_pwd = r_id_card.substring(12, 18);
    var pwd = SHA(r_pwd);

    $.ajax({
        url: basePath() + '/student/common/register.do',
        type: 'POST',
        data: {
            "name": r_name,
            "sex": r_sex,
            "tel": r_phone,
            "nation": r_nation,
            "number": r_id_card,
            "school": r_school,
            "college": r_college,
            "major": r_major,
            "schoolClass": r_s_class,
            "schoolId": r_id,
            "stuOrder": r_intention,
            "pwd": pwd
        },
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                bootbox.dialog({
                    title: "注册成功",
                    message: "<b>用户名: </b>身份证后12位<br/><br/><b>密码: </b>身份证后6位",
                    buttons: {
                        ok: {
                            label: '确认',
                            callback: function () {
                                window.location.href = basePath() + "/index.do";
                            }
                        }
                    }
                });
            } else {
                bootbox.alert(res.msg);
            }

        }
    })
};

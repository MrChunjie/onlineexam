function basePath() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPath + projectName);
}

loading();

var startTime_V = true;
var rStartTime_V = true;

$(function () {
    $(".selectpicker").selectpicker({
        noneSelectedText: '没有选项',
        style: 'btn-default'
    });

    showAllExam(1);

    getAllDirections();


    $("#starttime").datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
        language: 'zh-CN',
        autoclose: true,//选中自动关闭
        initialDate: new Date(),
        startDate: new Date(),
        todayBtn: true,//显示今日按钮
        minView: 0,
        minuteStep: 5
    }).change(function () {
        validatorTime("#time", "#starttime", "#timeSmall", startTime_V);
    });

    $("#rstarttime").datetimepicker({
        format: 'yyyy-mm-dd hh:ii:ss',
        language: 'zh-CN',
        autoclose: true,//选中自动关闭
        initialDate: new Date(),
        startDate: new Date(),
        todayBtn: true,//显示今日按钮
        minView: 0,
        minuteStep: 5
    }).change(function () {
        validatorTime("#rtime", "#rstarttime", "#rtimeSmall", rStartTime_V);
    });

    validitor();
    validitorU();

});


function validatorTime(id1, id2, id3, name) {
    if ($(id1).val() == "") {
        var group = $(id2).parent().parent();
        group.removeClass("has-success");
        group.addClass("has-error");
        $(id3).css('display', '');
        name = false;
    } else {
        var group = $(id2).parent().parent();
        group.removeClass("has-error");
        group.addClass("has-success");
        $(id3).css('display', 'none');
        name = true;
    }
}

function validitor() {
    $('#addExamForm').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '考试名称不能为空'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5a-zA-Z0-9]+$/,
                        message: '考试名称只能输入汉字、英文和数字'
                    }
                }
            },
            testtime: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '考试时间不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '考试时间只能输入数字'
                    }
                }
            },
            classIds: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '考试班级不能为空'
                    }
                }
            },
            questionNum: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '单选数目不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '单选数目只能输入数字'
                    },
                    positive: {
                        message: '单选题目必须大于0'
                    }
                }
            },
            score: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '单选总分不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '单选总分只能输入数字'
                    },
                    positive: {
                        message: '单选总分必须大于0'
                    }
                }
            },
            bigQuestionType1Num: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '简答题数目不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '简答题数目只能输入数字'
                    }
                }
            },
            score1: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '简答题总分不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '简答题总分只能输入数字'
                    }
                }
            },
            bigQuestionType2Num: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '编程题数目不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '编程题数目只能输入数字'
                    }
                }
            },
            score2: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '编程题总分不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '编程题总分只能输入数字'
                    }
                }
            }
        }
    });
}

function validitorU() {
    $('#raddExamForm').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '考试名称不能为空'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5a-zA-Z0-9]+$/,
                        message: '考试名称只能输入汉字、英文和数字'
                    }
                }
            },
            rtesttime: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '考试时间不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '考试时间只能输入数字'
                    }
                }
            },
            rquestionNum: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '单选数目不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '单选数目只能输入数字'
                    }
                }
            },
            rscore: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '单选总分不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '单选总分只能输入数字'
                    }
                }
            },
            rbigQuestionType1Num: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '简答题数目不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '简答题数目只能输入数字'
                    }
                }
            },
            rscore1: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '简答题总分不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '简答题总分只能输入数字'
                    }
                }
            },
            rbigQuestionType2Num: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '编程题数目不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '编程题数目只能输入数字'
                    }
                }
            },
            rscore2: {
                messageL: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '编程题总分不能为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '编程题总分只能输入数字'
                    }
                }
            }
        }
    });
}

function getAllDirections() {
    $.ajax({
        url: basePath() + '/teacher/exam/get_all_directions.do',
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                $.each(res.data, function (index, item) {
                    var item = "<option value='" + item.id + "'>" + item.name + "</option>";
                    $('#direction').append(item);
                });
            }
        }
    });
}


function showAllExam(page_num) {
    $.ajax({
        url: basePath() + '/teacher/exam/show_my_commit_exam.do',
        method: 'GET',
        async: true,
        dataType: 'JSON',
        data: {
            "pageNum": page_num,
            "pageSize": "10"
        },
        success: function (data) {
            removeLoading();
            //serverResponse里放pageinfo 在这直接获取data就是整个data
            var page = data.data;
            //获取放入pageinfo的List 用到分页的一定是list List是默认的属性名
            var item = data.data.list;
            //根据总页数和当前页数进行分页  第一个参数是当前页数  第二个是总页数  都是 pageinfo给的
            setPage(page.pageNum, page.pages, "showAllExam");
            //把获取的数据放入这个id的区域
            $("#exam_result").html("");
            for (var i = 0; i < item.length; i++) {
                //克隆这个模板 然后在填充数据 jquery 方法
                var title = $("#exam");
                title.removeAttr("style");
                var temp = $("#exam_m_template").clone();
                //给那个模板进行id的改变 让他不重复
                temp.attr("id", item[i].id);
                //开始是隐藏样式 移除这个样式
                temp.removeAttr("style");
                //设置序号  可以不一样
                temp.find(".exam_id").html(item[i].id);
                //find方法就是把值给他根据属性名
                temp.find(".exam_name").html(item[i].name);
                temp.find(".exam_cid").html(item[i].coursename);
                temp.find(".exam_start_time").html(item[i].starttime);
                temp.find(".exam_time").html(item[i].testtime);
                temp.find(".exam_class").html(item[i].classes);
                temp.find(".exam_score").html(item[i].scores);
                temp.find("#exam_find_this").attr("href", basePath() + "/teacher/exam/find_this_exam.do?examId=" + item[i].id);
                temp.find("#exam_find_this").attr("id", "exam_find_this" + item[i].id);

                //插入结果
                $("#exam_result").append(temp);
            }
        }
    })
}

function replaceQuestion(queType, queId) {
    var name = $('#name2').val();
    var courseId = $('#courseId2').val();
    var startTime = $('#starttime2').val();
    var testTime = $('#testtime2').val();
    var classIds = $('#classids2').val().toString();
    var score = $('#score02').val();
    var score1 = $('#score12').val();
    var score2 = $('#score22').val();
    $.ajax({
        url: basePath() + '/teacher/exam/replace_question.do',
        async: true,
        dataType: 'JSON',
        type: 'post',
        data: {
            "name": name,
            "courseid": courseId,
            "starttime": startTime,
            "testtime": testTime,
            "classids": classIds,
            "questionNum": 1,
            "score": score,
            "bigQuestionType1Num": 1,
            "score1": score1,
            "bigQuestionType2Num": 1,
            "score2": score2,
            "queType": queType
        },
        success: function (res) {
            if (res.success) {
                var i = 0;
                if (queType == 0) {
                    var div = $("#question" + queId).parent();
                    div.find("#questions_title").html(queId + 1 + "." + res.data.question[i].quetitle);
                    div.find("#questions_a").html("a." + res.data.question[i].choicea);
                    div.find("#questions_b").html("b." + res.data.question[i].choiceb);
                    div.find("#questions_c").html("c." + res.data.question[i].choicec);
                    div.find("#questions_d").html("d." + res.data.question[i].choiced);
                    //重新给id  val是给他赋值  html是让他看的值  after和 html一样作用,但是用于元素节点没有后标签 让他显示什么
                    div.find("#questionisd").attr("id", "question" + queId);
                    //向隐藏域添加值
                    div.find("#" + "question" + queId).val(res.data.question[i].id);
                    var up_btn = "<button type='button'  class='btn btn-primary btn-xs btn-warning' onclick='replaceQuestion(0," + queId + ")' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>替换</span></button>"
                    div.find(".q_replace").html(up_btn);
                } else if (queType == 1) {
                    var div = $("#bqt1" + queId).parent().parent();
                    div.find("#big_questiont1_title").html(res.data.bigt1Question[i].title);
                    div.find("#bigquestiont1").attr("id", "bqt1" + queId);
                    div.find("#" + "bqt1" + queId).val(res.data.bigt1Question[i].id);
                    var up_btn = "<button type='button'  class='btn btn-primary btn-xs btn-warning' onclick='replaceQuestion(1," + queId + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>替换</span></button>"
                    div.find(".bqt1_replace").html(up_btn);
                } else if (queType == 2) {
                    var div = $("#bqt2" + queId).parent().parent();
                    div.find("#big_questiont2_title").html(res.data.bigt2Question[i].title);
                    div.find("#bigquestiontype2id").attr("id", "bqt2" + queId);
                    div.find("#" + "bqt2" + queId).val(res.data.bigt2Question[i].id);
                    var up_btn = "<button type='button'  class='btn btn-primary btn-xs btn-warning' onclick='replaceQuestion(2," + queId + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>替换</span></button>"
                    div.find(".bqt2_replace").html(up_btn);
                }
            }
            else {
                bootbox.alert(res.msg);
            }
        }
    });
}

function showModal() {
    $.ajax({
        url: basePath() + '/teacher/exam/get_course.do',
        async: true,
        dataType: 'JSON',
        success: function (res) {
            if (res.success) {
                //清空
                $('#courseid').empty();

                $.each(res.data, function (index, item) {
                    var option = "<option value = " + item.id + ">" + item.name + "</option>"
                    $("#courseid").append(option);
                });
            }
        }
    });

    getSClass();

    $(".selectpicker").selectpicker({
        noneSelectedText: '请选择',
        style: 'btn-default'
    });

    $('#addExamInFo').modal('show');

}

function getSClass() {

    $.ajax({
        url: basePath() + '/teacher/exam/get_class_by_type.do',
        type: 'post',
        data: {'type': $('#direction').val()},
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                $('#classIds').empty();
//                        $('#classIds').addClass('selectpicker');
                $.each(res.data, function (index, item) {
                    var option = "<option value='" + item.id + "'>" + item.name + "</option>";
                    $('#classIds').append(option);
                });
                $('#classIds').selectpicker('refresh');

            } else {
                $('#classIds').removeClass('selectpicker');

                $('#classIds').empty();

                var option = "<option value='0'>" + res.msg + "</option>";
                $('#classIds').append(option);

            }
        }
    });
}

function addExam() {

    // 提交前先主动验证表单
    var bv = $("#addExamForm").data('bootstrapValidator');
    bv.validate();
    if (!bv.isValid()) {
        return;
    }

    var classIds = $('#classIds').val();
    if (classIds == null) {
        bootbox.alert("考试班级至少选择一个");
        $('#addExamInFo').css('overflow-x', 'hidden');
        $('#addExamInFo').css('overflow-y', 'auto');
        return;
    }

    classIds = classIds.toString();


    var name = $('#name').val();
    var courseId = $('#courseid').val();
    var startTime = $('#time').val();

    if (startTime == "") {
        validatorTime("#time", "#starttime", "#timeSmall", startTime_V);
        return;
    } else {
        validatorTime("#time", "#starttime", "#timeSmall", startTime_V);
    }

    if (startTime_V == false) {
        return;
    }

    var testTime = $('#testtime').val();
    var classIds = $('#classIds').val().toString();
    var questionNum = $('#questionNum').val();
    if (questionNum <= 0) {
        bootbox.alert("单选数目必须大于0");
        $('#addExamInFo').css('overflow-x', 'hidden');
        $('#addExamInFo').css('overflow-y', 'auto');
        return;
    }
    var score = $('#score').val();
    if (score <= 0) {
        bootbox.alert("单选分数必须大于0");
        $('#addExamInFo').css('overflow-x', 'hidden');
        $('#addExamInFo').css('overflow-y', 'auto');
        return;
    }
    var bigQuestuin1Num = $('#bigQuestionType1Num').val();
    var score1 = $('#score1').val();
    var bigQuestuin2Num = $('#bigQuestionType2Num').val();
    var score2 = $('#score2').val();
//            console.info(JSON.stringify({
//                'name': name,
//                'courseid': courseId,
//                'starttime': startTime,
//                'testtime': testTime,
//                'classids': classIds,
//                'questionNum': questionNum,
//                'score': score,
//                'bigQuestionType1Num': bigQuestuin1Num,
//                'score1': score1,
//                'bigQuestionType2Num': bigQuestuin2Num,
//                'score2': score2
//            }));

    $.ajax({
        url: basePath() + '/teacher/exam/ready_add_exam.do',
        type: 'post',
        data: {
            "name": name,
            "courseid": courseId,
            "starttime": startTime,
            "testtime": testTime,
            "classids": classIds,
            "questionNum": questionNum,
            "score": score,
            "bigQuestionType1Num": bigQuestuin1Num,
            "score1": score1,
            "bigQuestionType2Num": bigQuestuin2Num,
            "score2": score2
        },
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                bootbox.alert("组题成功!");
                clearModal();
                $("#submit").removeAttr("style");
                var commit_exam = document.getElementById('commit_exam');
                commit_exam.style = "display:none;";
                var fenye = document.getElementById('fenye');
                fenye.style = "display:none;";
                $("#commit_exam_name").html("试卷名:" + res.data.exam.name);
                $("#commit_exam_course").html("考试科目:" + res.data.course.name);
                $("#commit_exam_className").html("考试班级" + res.data.className);
                $("#commit_exam_testtime").html("考试时长" + res.data.exam.testtime + "分钟,卷面总分:" + res.data.exam.scores + "分");

                if (res.data.question.length != 0) {
                    $("#commit_exam_questions").html("一、单选题（共" + res.data.exam.questionNum + "题，每题" + res.data.exam.score / res.data.exam.questionNum + " 分）");
                }
                for (var i = 0; i < res.data.question.length; i++) {
                    //克隆选择题模板
                    var questions_template = $("#commit_exam_questions_template").clone();
                    questions_template.find("#questions_title").html(i + 1 + "." + res.data.question[i].quetitle);
                    questions_template.find("#questions_a").html("a." + res.data.question[i].choicea);
                    questions_template.find("#questions_b").html("b." + res.data.question[i].choiceb);
                    questions_template.find("#questions_c").html("c." + res.data.question[i].choicec);
                    questions_template.find("#questions_d").html("d." + res.data.question[i].choiced);
                    //向隐藏域添加值
                    questions_template.find("#questionisd").val(res.data.question[i].id);
                    //重新给id  val是给他赋值  html是让他看的值  after和 html一样作用,但是用于元素节点没有后标签 让他显示什么
                    questions_template.find("#questionisd").attr("id", "question" + i);
                    var up_btn = "<button type='button'  class='btn btn-primary btn-xs btn-warning' onclick='replaceQuestion(0," + i + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>替换</span></button>"
                    questions_template.find(".q_replace").html(up_btn);
                    questions_template.removeAttr("style");
                    $("#questions_result").append(questions_template);
                }
                if (res.data.bigt1Question.length != 0) {
                    $("#big_questiont1").html("二、简答题（共" + res.data.exam.bigQuestionType1Num + "题，每题" + res.data.exam.score1 / res.data.exam.bigQuestionType1Num + " 分）");
                }
                for (var i = 0; i < res.data.bigt1Question.length; i++) {
                    var bigquestiont1_template = $("#big_questiont1_template").clone();
                    bigquestiont1_template.find("#big_questiont1_title").html(i + 1 + "、" + res.data.bigt1Question[i].title);
                    bigquestiont1_template.find("#bigquestiont1").val(res.data.bigt1Question[i].id);
                    bigquestiont1_template.find("#bigquestiont1").attr("id", "bqt1" + i);
                    var up_btn = "<button type='button'  class='btn btn-primary btn-xs btn-warning' onclick='replaceQuestion(1," + i + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>替换</span></button>"
                    bigquestiont1_template.find(".bqt1_replace").html(up_btn);
                    bigquestiont1_template.removeAttr("style");
                    $("#big_question_result").append(bigquestiont1_template);
                }
                if (res.data.bigt2Question.length != 0) {
                    $("#big_questiont2").html("三、程序题（共" + res.data.exam.bigQuestionType2Num + "题，每题" + res.data.exam.score2 / res.data.exam.bigQuestionType2Num + "分)");
                }
                for (var i = 0; i < res.data.bigt2Question.length; i++) {
                    var bigquestiont2_template = $("#big_questiont2_template").clone();
                    bigquestiont2_template.find("#big_questiont2_title").html(i + 1 + "、" + res.data.bigt2Question[i].title);
                    bigquestiont2_template.find("#bigquestiontype2id").val(res.data.bigt2Question[i].id);
                    bigquestiont2_template.find("#bigquestiontype2id").attr("id", "bqt2" + i);
                    var up_btn = "<button type='button'  class='btn btn-primary btn-xs btn-warning' onclick='replaceQuestion(2," + i + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>替换</span></button>"
                    bigquestiont2_template.find(".bqt2_replace").html(up_btn);
                    bigquestiont2_template.removeAttr("style");
                    $("#big_questiont2_result").append(bigquestiont2_template);
                }
                $("#name2").val(res.data.exam.name);
                $("#courseId2").val(res.data.course.id);
                $("#starttime2").val(res.data.startTime);
                $("#classids2").val(res.data.classid);
                $("#testtime2").val(res.data.exam.testtime);
                $("#score02").val(res.data.exam.score);
                $("#score12").val(res.data.exam.score1);
                $("#score22").val(res.data.exam.score2);
            } else {
                bootbox.alert(res.msg, function () {
                    $('#addExamInFo').css('overflow-x', 'hidden');
                    $('#addExamInFo').css('overflow-y', 'auto');
                });
            }
        },
        error: function (res) {
            console.info("failed");
            console.info(JSON.stringify(res));
        }
    });
}


function clearModal() {
    $('#addExamInFo').modal('hide');
    $("#addResetExamInFo").modal('hide');
    $(":input", "#addExamInFo").not(":button", ":reset", "hidden", "submit")
        .val("")
        .removeAttr("checked")
        .removeAttr("selected")
        .removeAttr("style");
    $('#classIds').selectpicker('val', null);
    $('#addExamForm').data('bootstrapValidator').destroy();
    $('#addExamForm').data('bootstrapValidator', null);
    $("form[id='addExamForm'] > div").addClass('has-success');
    validitor();
}

function clearUModal() {
    $("#addResetExamInFo").modal('hide');
    $(":input", "#raddExamForm").not(":button", ":reset", "hidden", "submit")
        .val("")
        .removeAttr("checked")
        .removeAttr("selected")
        .removeAttr("style");
    $('#classId').selectpicker('val', null);
    $('#raddExamForm').data('bootstrapValidator').destroy();
    $('#raddExamForm').data('bootstrapValidator', null);
    $("form[id='raddExamForm'] > div").addClass('has-success');
    validitorU();
}


function selectAll() {
    var width = $('.bootstrap-select').width();
    $('.dropdown-toggle').attr('style', 'text-overflow: ellipils; overflow: hidden; width: ' + width + 'px;');
    $('#classIds').selectpicker('selectAll');
}


function selectAll() {
    var width = $('.bootstrap-select').width();
    $('.dropdown-toggle').attr('style', 'text-overflow: ellipils; overflow: hidden; width: ' + width + 'px;');
    $('#classIds').selectpicker('selectAll');
}

function sub() {
    var dialog = bootbox.dialog({
        title: '请稍候',
        message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 提交中...</p>'
    });
    $.ajax({
        url: basePath() + '/teacher/exam/add_exam.do',
        type: 'post',
        data: $('#subForm').serialize(),
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                dialog.modal('hide');
                bootbox.alert("提交成功", function () {
                    window.location.href = basePath() + "/t-exam.do";
                });
            }
        }
    });
}

function showResetExamModal() {
    $.ajax({
        url: basePath() + '/teacher/exam/get_reset_examinfo.do',
        async: true,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
            var item = data.data;
            $("#rname").empty();
            for (var i = 0; i < item.length; i++) {
                var option = "<option id ='" + (0 - item[i].examId) + "' stuId='" + item[i].sId + "' courseId='" + item[i].courseId + "' classId='" + item[i].classId + "'  value='" + (0 - item[i].examId) + "' >" + item[i].examName + "</option>";
                $('#rname').append(option);
            }
            $("#addResetExamInFo").modal('show');
        }
    })
}

function addResetExam() {

    // 提交前先主动验证表单
    var bv = $("#raddExamForm").data('bootstrapValidator');
    bv.validate();
    if (!bv.isValid()) {
        return;
    }

    if (startTime_V == false) {
        return;
    }

    var examId = $("#rname").val();
    var examName = $("#rname").find("option:selected").text() + "补考";
    var option = $("#" + examId);
    var classId = option.attr('classId');
    var courseId = option.attr("courseId");
    var stuId = option.attr("stuId");
    var statrTime = $("#rtime").val();

    if (statrTime == "") {
        validatorTime("#rtime", "#rstarttime", "#rtimeSmall", rStartTime_V);
        return;
    } else {
        validatorTime("#rtime", "#rstarttime", "#rtimeSmall", rStartTime_V);

    }


    var testTime = $("#rtesttime").val();
    var questionnum = $("#rquestionNum").val();
    if (questionnum <= 0) {
        bootbox.alert("单选题目必须大于0");
        $('#addResetExamInFo').css('overflow-x', 'hidden');
        $('#addResetExamInFo').css('overflow-y', 'auto');
        return;
    }
    var bqt1num = $("#rbigQuestionType1Num").val();
    var bqt2num = $("#rbigQuestionType2Num").val();
    var score1 = $("#rscore").val();
    if (score1 <= 0) {
        bootbox.alert("单选分数必须大于0");
        $('#addResetExamInFo').css('overflow-x', 'hidden');
        $('#addResetExamInFo').css('overflow-y', 'auto');
        return;
    }
    var score2 = $("#rscore1").val();
    var score3 = $("#rscore2").val();
    $.ajax({
        url: basePath() + '/teacher/exam/add_reset_exam.do',
        type: 'POST',
        data: {
            "examId": examId,
            "examName": examName,
            "courseId": courseId,
            "stuId": stuId,
            "classId": classId,
            "statrTime": statrTime,
            "testTime": testTime,
            "questionnum": questionnum,
            "bqt1num": bqt1num,
            "bqt2num": bqt2num,
            "score1": score1,
            "score2": score2,
            "score3": score3
        },
        dataType: 'JSON',
        success: function (res) {
            if (res.status == 0) {
                bootbox.alert(res.msg);
                clearUModal();
                $("#submit").removeAttr("style");
                var commit_exam = document.getElementById('commit_exam');
                commit_exam.style = "display:none;";
                var fenye = document.getElementById('fenye');
                fenye.style = "display:none;";
                $("#commit_exam_name").html("试卷名:" + res.data.exam.examName);
                $("#commit_exam_course").html("考试科目:" + res.data.course.name);
                $("#commit_exam_testtime").html("考试时长" + res.data.exam.testTime + "分钟,卷面总分:" + res.data.exam.scores + "分");
                $("#commit_exam_questions").html("一、单选题（共" + res.data.exam.questionnum + "题，每题" + res.data.exam.score1 / res.data.exam.questionnum + " 分）");

                for (var i = 0; i < res.data.question.length; i++) {
                    //克隆选择题模板
                    var questions_template = $("#commit_exam_questions_template").clone();
                    questions_template.find("#questions_title").html(i + 1 + "." + res.data.question[i].quetitle);
                    questions_template.find("#questions_a").html("a." + res.data.question[i].choicea);
                    questions_template.find("#questions_b").html("b." + res.data.question[i].choiceb);
                    questions_template.find("#questions_c").html("c." + res.data.question[i].choicec);
                    questions_template.find("#questions_d").html("d." + res.data.question[i].choiced);
                    //向隐藏域添加值
                    questions_template.find("#questionisd").val(res.data.question[i].id);
                    //重新给id  val是给他赋值  html是让他看的值  after和 html一样作用,但是用于元素节点没有后标签 让他显示什么
                    questions_template.find("#questionisd").attr("id", "question" + i);
                    var up_btn = "<button type='button'  class='btn btn-primary btn-xs btn-warning' onclick='replaceQuestion(0," + i + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>替换</span></button>"
                    questions_template.find(".q_replace").html(up_btn);
                    questions_template.removeAttr("style");
                    $("#questions_result").append(questions_template);
                }
                if(res.data.bigt1Question.length!=0){
                    $("#big_questiont1").html("二、简答题（共" + res.data.exam.bqt1num + "题，每题" + res.data.exam.score2 / res.data.exam.bqt1num + " 分）");
                }
                for (var i = 0; i < res.data.bigt1Question.length; i++) {
                    var bigquestiont1_template = $("#big_questiont1_template").clone();
                    bigquestiont1_template.find("#big_questiont1_title").html(res.data.bigt1Question[i].title);
                    bigquestiont1_template.find("#bigquestiont1").val(res.data.bigt1Question[i].id);
                    bigquestiont1_template.find("#bigquestiont1").attr("id", "bqt1" + i);
                    var up_btn = "<button type='button'  class='btn btn-primary btn-xs btn-warning' onclick='replaceQuestion(1," + i + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>替换</span></button>"
                    bigquestiont1_template.find(".bqt1_replace").html(up_btn);
                    bigquestiont1_template.removeAttr("style");
                    $("#big_question_result").append(bigquestiont1_template);
                }
                if(res.data.bigt2Question.length!=0){
                    $("#big_questiont2").html("三、程序题（共" + res.data.exam.bqt2num + "题，每题" + res.data.exam.score3 / res.data.exam.bqt2num + "分)");
                }
                for (var i = 0; i < res.data.bigt2Question.length; i++) {
                    var bigquestiont2_template = $("#big_questiont2_template").clone();
                    bigquestiont2_template.find("#big_questiont2_title").html(res.data.bigt2Question[i].title);
                    bigquestiont2_template.find("#bigquestiontype2id").val(res.data.bigt2Question[i].id);
                    bigquestiont2_template.find("#bigquestiontype2id").attr("id", "bqt2" + i);
                    var up_btn = "<button type='button'  class='btn btn-primary btn-xs btn-warning' onclick='replaceQuestion(2," + i + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>替换</span></button>"
                    bigquestiont2_template.find(".bqt2_replace").html(up_btn);
                    bigquestiont2_template.removeAttr("style");
                    $("#big_questiont2_result").append(bigquestiont2_template);
                }
                $("#name2").val(res.data.exam.examName);
                $("#courseId2").val(res.data.course.id);
                $("#starttime2").val(res.data.startTime);
                $("#classids2").val(res.data.classid);
                $("#testtime2").val(res.data.exam.testTime);
                $("#score02").val(res.data.exam.score1);
                $("#score12").val(res.data.exam.score2);
                $("#score22").val(res.data.exam.score3);
                $("#rexamId").val(res.data.exam.examId);
                $("#rstuId").val(res.data.exam.stuId);
            } else {
                bootbox.alert(res.msg, function () {
                    $('#addResetExamInFo').css('overflow-x', 'hidden');
                    $('#addResetExamInFo').css('overflow-y', 'auto');
                });
            }
        },
        error: function (res) {
            console.log(res);
        }
    })
}
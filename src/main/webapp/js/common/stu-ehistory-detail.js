function basePath() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPath + projectName);
}

loading();

$(function () {
    $.ajax({
        url: basePath() + '/student/history/exam_history_detail.do',
        type: 'GET',
        async: 'true',
        data: "",
        dataType: 'json',
        success: function (data) {

            if (data.msg == '查询成功') {
                var item = data.data;
                var choiceQu = item.queList;
                $("#result0").html("");
                var k = 0;
                if (choiceQu != null) {
                    for (var i = 0; i < choiceQu.length; i++) {
                        var temp = $("#p_template0").clone();
                        temp.attr("id", i);
                        temp.removeAttr("style");
                        temp.find("#p_title0").html(i + 1 + "、" + choiceQu[i].quetitle + "<br/>  ——你的答案：" + choiceQu[i].ans);
                        if (item.wrongqueid[k] == choiceQu[i].id) {
                            if (item.wrongans[k] == "null" || item.wrongans[k] == " null") {
                                item.wrongans[k] = "未作答";
                            }
                            temp.find("#p_title0").html(i + 1 + "、" + choiceQu[i].quetitle + "<br/> ——你的答案：" + item.wrongans[k]);
                            k = k + 1;
                        }

                        var answer = choiceQu[i].ans.toLowerCase();
                        if (answer == 'a') {
                            temp.find("#correctA").html("正确答案");
                        } else if (answer == 'b') {
                            temp.find("#correctB").html("正确答案");
                        } else if (answer == 'c') {
                            temp.find("#correctC").html("正确答案");
                        } else if (answer == 'd') {
                            temp.find("#correctD").html("正确答案");
                        }
                        temp.find("#p_choiceA").html("A." + choiceQu[i].choicea);
                        temp.find("#p_choiceB").html("B." + choiceQu[i].choiceb);
                        temp.find("#p_choiceC").html("C." + choiceQu[i].choicec);
                        temp.find("#p_choiceD").html("D." + choiceQu[i].choiced);
                        $("#result0").append(temp);
                    }
                }
                $("#result1").html("");

                if (item.titles1 != null&&item.titles1.length > 0 && item.titles1[0] != "") {
                    $("#result1").append("<h4>二、简答题</h4>");
                    for (var i = 0; i < item.titles1.length; i++) {
                        var temp = $("#p_template1").clone();
                        temp.attr("id", i);
                        temp.removeAttr("style");
                        temp.find("#p_title1").html(i + 1 + "、" + item.titles1[i]);
                        temp.find("#p_ans1").html(item.ans1[i]);
                        temp.find("#p_correct_ans1").html(item.que1Ans[i]);
                        $("#result1").append(temp);
                    }
                }
                $("#result2").html("");
                if (item.titles2 != null) {
                    $("#result2").append("<h4>三、编程题</h4>");
                    for (var i = 0; i < item.titles2.length; i++) {
                        var temp = $("#p_template2").clone();
                        temp.attr("id", i);
                        temp.removeAttr("style");
                        temp.find("#p_title2").html(i + 1 + "、" + item.titles2[i]);
                        temp.find("#p_ans2").html(item.ans2[i]);
                        temp.find("#p_correct_ans2").html(item.que2Ans[i]);
                        $("#result2").append(temp);
                    }
                }
            } else {
                bootbox.alert(data.msg);
                window.location.href = basePath() + "/stu-ehistory.do";
            }
            removeLoading();
        }
    });
});
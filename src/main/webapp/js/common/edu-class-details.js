function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}

loading();

var page = 1;
var page_item_num = 0;
var item_num = 10;

$(function () {
    getAllStuVo(page);
});

function getAllStuVo(page) {
    var classId = $("#classId").val();
    var url = basePath()+"/edu/class/get_class_students.do";
    var args = {
        "pageNum": page,
        "pageSize": item_num,
        "classId": classId
    };
    $.get(url, args, function (data) {
        if (data.status == 0) {
            removeLoading();
            var pageResult = data.data;
            var item = data.data.list;
            page_item_num = item.length;
            page = setPage(pageResult.pageNum, pageResult.pages, "getAllStuVo");
            $("#result").html("");
            $("#stu_num").html(item.length);
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", item[i].id);
                temp.removeAttr("style");
                temp.find(".c_num").html(pageResult.pageSize * (pageResult.pageNum - 1) + 1 + i);
                var acontent = "<form method='GET' action='"+basePath()+"/edu/stu/detail.do'>" +
                    "<input type='hidden' name='id' value=" + item[i].sid + "> " +
                    "<button type='submit'  class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span><span style='margin: 0px 8px;'>"+item[i].sname+"</span></button>" +
                    "</form>";
                temp.find(".c_sname").html(acontent);
                temp.find(".c_snum").html(item[i].sid);
                temp.find(".c_historytime").html(item[i].historytime);
                temp.find(".c_classname").html(item[i].classname);
                $("#result").append(temp);
            }
        } else {
        }


    }, "json");
}
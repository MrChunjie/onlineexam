function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}

loading();

$(function () {
    validatorU();
    getSetting();
});


function validatorU() {
    $('#settingForm').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            signNum: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '最大报名数不能为空'
                    },
                    stringLength: {
                        max: 3,
                        message: '最大报名数在3位以内'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '报名数只允许为数字'
                    }
                }
            }
        }
    });
}

function getSetting() {

    var url = basePath()+"/ep/setting/get_setting.do";
    var args = {};
    $.get(url, args, function (data) {
        removeLoading();
        if (data.status == 0) {
            $("#signNum").val(data.data.maxSign);
        } else {

        }
    }, "json");
}

function update() {
    var signNum = $("#signNum").val();
    var url = basePath()+"/ep/setting/update_setting.do";
    var args = {"maxSign": signNum};
    $.post(url, args, function (data) {
        bootbox.alert(data.msg);
    }, "json");
}

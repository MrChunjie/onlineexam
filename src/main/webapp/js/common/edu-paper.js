function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}

loading();

$(function () {
    showAllExam(1);
});

function showAllExam(page_num) {
    $.ajax({
        url: basePath()+'/edu/paper/get_all_exam.do',
        data: {
            "pageNum": page_num,
            "pageSize": "10"
        },
        dataType: 'JSON',
        success: function (data) {
            removeLoading();
            if (data.data == null) {
                $('#page').hide();
                $('#hr').after("<div class='alert alert-warning alert-dismissible' role='alert'>\n" +
                    "  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>\n" +
                    "  <strong style='padding-left: 30px;'>暂时没有任何考试</strong>" +
                    "</div>");
                return;
            }else {
                $('#page').show();
            }

            var page = data.data;
            var item = data.data.list;

            setPage(page.pageNum, page.pages, "showAllExam");
            $("#exam_result").html("");
            for (var i = 0; i < item.length; i++) {
                var title = $("#exam");
                title.removeAttr("style");
                var temp = $("#exam_m_template").clone();
                temp.attr("id", item[i].id);
                temp.removeAttr("style");
                temp.find(".exam_id").html(item[i].id);
                temp.find(".exam_name").html(item[i].name);
                temp.find(".exam_cid").html(item[i].coursename);
                temp.find(".exam_start_time").html(item[i].starttime);
                temp.find(".exam_time").html(item[i].testtime);
                temp.find(".exam_class").html(item[i].classes);
                temp.find(".exam_score").html(item[i].scores);
                temp.find("#exam_find_this").attr("href", basePath()+"/edu/paper/find_this_exam.do?examId=" + item[i].id);
                temp.find("#exam_find_this").attr("id", "exam_find_this" + item[i].id);
                temp.find('#exportBtn').attr('onclick', 'out(' + item[i].id + ')');
                $("#exam_result").append(temp);
            }
        }
    })
}


function out(examId) {

    var dialog = bootbox.dialog({
        title: '请稍候',
        message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 导出中...</p>'
    });

    var url = basePath()+"/edu/paper/export_pdf.do?examId=" + examId;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);        // 也可以使用POST方式，根据接口
    xhr.responseType = "blob";    // 返回类型blob
    // 定义请求完成的处理函数，请求前也可以增加加载框/禁用下载按钮逻辑
    xhr.onload = function () {
        // 请求完成
        if (this.status === 200) {
            // 返回200
            var blob = this.response;
            var reader = new FileReader();
            reader.readAsDataURL(blob);    // 转换为base64，可以直接放入a表情href
            reader.onload = function (e) {
                // 转换完成，创建一个a标签用于下载
                var a = document.createElement('a');
                var fileName = $("#"+examId).find('.exam_name').html();
                a.download = fileName+'.zip';
                a.href = e.target.result;
                $("body").append(a);    // 修复firefox中无法触发click
                a.click();
                $(a).remove();
                dialog.find('.modal-title').html('完成');
                dialog.find('.modal-body').html("导出成功");
                dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\">OK</button></div>");

            }
        }
    };
    // 发送ajax请求
    xhr.send();

}
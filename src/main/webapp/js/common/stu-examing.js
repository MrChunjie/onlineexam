function basePath() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPath + projectName);
}


$(function () {

    if (window.history && window.history.pushState) {
        $(window).on('popstate', function () {
            window.history.pushState('forward', null, 'stu_index.do');
            window.history.forward(1);
        });
    }
    window.history.pushState('forward', null, 'stu_index.do'); //在IE中必须得有这两行
    window.history.forward(1);


    $('input').iCheck({
        labelHover: true,
        cursor: true,
        radioClass: 'iradio_minimal',
        increaseArea: '20%'
    });

    $("textarea").on(
        'keydown',
        function (e) {
            if (e.keyCode == 9) {
                e.preventDefault();
                var indent = '    ';
                var start = this.selectionStart;
                var end = this.selectionEnd;
                var selected = window.getSelection().toString();
                selected = indent + selected.replace(/\n/g, '\n' + indent);
                this.value = this.value.substring(0, start) + selected
                    + this.value.substring(end);
                this.setSelectionRange(start + indent.length, start
                    + selected.length);
            }
        })
});

function saveinfo(id, index) {
    var value = $('input:radio[name=' + id + ']:checked').val();
    //alert(id+"---"+value+"---"+index);
    if (value != null) {
        $.ajax({
            type: 'GET',
            url: basePath() + '/student/exam/save_papers.do',
            data: {index: index, value: value},
            async: 'true',
            dataType: 'text',
            success: function (data) {
                /*  alert("success");*/
            }
        });
    }
}


function savebiginfo(id, index) {
    var value = $('#' + id).val();
    //alert(id+"--"+value+"--"+index);
    if (value != null) {
        $.ajax({
            type: 'GET',
            url: basePath() + '/student/exam/save_papers.do',
            data: {index: index, value: value},
            async: 'true',
            dataType: 'text',
            success: function (data) {
                /* alert("success");*/
            }
        });
    }
}

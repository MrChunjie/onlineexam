function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}

loading();

$(function () {
    getRegisterState();
});





function getRegisterState() {
    $.ajax({
        url: basePath()+'/edu/setting/get_register_state.do',
        dataType: 'json',
        success: function (res) {
            removeLoading();
            var btn = $('#registerState');
            if (res.data == "1") {
                btn.parent().removeClass("btn-success");
                btn.parent().addClass("btn-warning");
                btn.attr('value', res.data);
                btn.html("关闭注册功能");
                $('#registerIcon').removeClass('glyphicon-ok-circle');
                $('#registerIcon').addClass("glyphicon-ban-circle");
            } else {
                btn.parent().removeClass("btn-warning");
                btn.parent().addClass("btn-success");
                btn.attr('value', res.data);
                btn.html("开启注册功能");
                $('#registerIcon').removeClass('glyphicon-ban-circle');
                $('#registerIcon').addClass("glyphicon-ok-circle");

            }
        }
    });
}

function updateRegisterState() {
    var registerState = $('#registerState').attr('value');
    if (registerState == 1) {
        registerState = 0;
    }else {
        registerState = 1;
    }
    $.ajax({
        url: basePath()+'/edu/setting/update_register_state.do',
        type: 'post',
        data: {"registerState":registerState},
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                bootbox.alert(res.msg);
                getRegisterState();
            }
        }
    });
}

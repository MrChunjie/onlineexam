function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}
$(function () {
    menu();
    validator();

});

//给点击的li添加active属性。
function menu() {
    $("li[role='presentation']").click(function () {
        $(this).addClass('active').siblings('li').removeClass('active');
        $(this).attr("style", "margin-left: 10px;").siblings('li').removeAttr("style");
    });
}

function validator() {
    $('#form').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            newPassword: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '新密码不能为空'
                    },
                    stringLength: {
                        min: 2,
                        max: 30,
                        message: '密码长度必须在2到30之间'
                    },
                    regexp:{
                        regexp: /^[^ ]+$/,
                        message: '密码不能有空格'
                    },
                    identical: {
                        field: 'confirmPassword',
                        message: '两次密码必须一致'
                    }
                }
            },
            confirmPassword: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '确认密码不能为空'
                    },
                    regexp:{
                        regexp: /^[^ ]+$/,
                        message: '密码不能有空格'
                    },
                    identical: {
                        field: 'newPassword',
                        message: '两次密码必须一致'
                    }
                }
            }
        }
    });
}

function change_pwd() {
    // 提交前先主动验证表单
    var bv = $("#form").data('bootstrapValidator');
    bv.validate();
    if (!bv.isValid()) {
        return;
    }
    var newPassword = SHA($('#newPassword').val());
    $.ajax({
        url: 'admin/change_pwd.do',
        type: 'POST',
        data: {'pwd': newPassword},
        dataType: 'JSON',
        success: function (res) {
            if (res.success) {
                bootbox.alert(res.msg);
                clearModal();
            } else {
                bootbox.alert(res.msg);
            }
        }
    });
}

function clearModal() {
    $('#changPwd').modal('hide');
    $('#newPassword').val("");
    $('#confirmPassword').val("");
    //重置表单验证
    $('#form').data('bootstrapValidator').destroy();
    $('#form').data('bootstrapValidator', null);
    $("form[id='form'] > div").addClass("has-success");
    validator();
}

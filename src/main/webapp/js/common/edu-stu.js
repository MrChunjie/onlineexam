function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}

loading();

var page_now = 0;

var start_time = true;
var end_time = true;

var s_class_id = "";

var view_s_type = 0;

$(function () {
    validatorSearch();

    validatorUpdate();

    getStudentList(1);

    Direction();

    allClass();

    $("#stu_update_btn").click(function () {
        updateStuInfomation();
    });


    //查询
    $("#search_btn").click(function () {
        searchStudent(1);
    });

    $("#get_stu_list_btn").click(function () {
        var value = $("#get_stu_list").html();
        if (value == '查看非在读学生') {
            getNotOnlineStudentList(1);
            $("#get_stu_list").html("查看在读学生");
            $('#get_stu_list').parent().removeClass('btn-primary');
            $('#get_stu_list').parent().addClass('btn-info');
        } else {
            getStudentList(1);
            $("#get_stu_list").html("查看非在读学生");
            $('#get_stu_list').parent().removeClass('btn-info');
            $('#get_stu_list').parent().addClass('btn-primary');
        }
    });

    $("#studentBeginTime").datetimepicker({
        language: 'zh',
        format: 'yyyy-mm-dd hh:ii:ss',
        language: 'zh-CN',
        autoclose: true,//选中自动关闭
        startDate: '2016-01-01',
        todayBtn: true,//显示今日按钮
        pickerPosition: 'bottom-right'
    }).change(function () {
        validatorTime('#newstime', '#studentBeginTime', '#newSTimeSmall', start_time)
    });

    $("#studentEndTime").datetimepicker({
        language: 'zh',
        format: 'yyyy-mm-dd hh:ii:ss',
        language: 'zh-CN',
        autoclose: true,//选中自动关闭
        startDate:'2016-01-01',
        todayBtn: true,//显示今日按钮
        pickerPosition: 'bottom-right'
    }).change(function () {
        validatorTime('#newetime', '#studentEndTime', '#newETimeSmall', end_time)
    });
});

function Direction() {
    $.ajax({
        url: basePath()+"/edu/stu/get_all_direction.do",
        type: 'GET',
        success: function (res) {
            if (res.success) {
                $("#hp_direction option").remove();
                $("#stu_direction option").remove();
                var selected_content = "<option value='-1' selected='selected'>所有方向</option>";
                $("#stu_direction").append(selected_content);
                if (res.data.length == 0) {
                    var content = "<option>暂无数据</option>";
                    $("#hp_direction").append(content);
                    $("#stu_direction").append(content);
                } else {
                    for (var i = 0, a = res.data.length; i < a; i++) {
                        var content = "<option value='" + res.data[i].id + "'>" + res.data[i].name + "</option>";
                        $("#hp_direction").append(content);
                        $("#stu_direction").append(content);
                    }
                    $("#hp_direction option[value=0]").prop("selected",true);
                }
            }
        }
    })
}

function allClass(value) {
    var s_type = $("#hp_direction :selected").val();
    if(s_type == "" || s_type == null || s_type == undefined){
        s_type = 0;
    }
    if(value != "" &&  value != null && value != undefined){
        s_type = value;
    }


    var s_class;

    if (s_class_id != "") {
        $.ajax({
            url: basePath() + "/edu/class/get_class.do",
            async: false,
            data: {'id':s_class_id},
            dataType: 'json',
            success: function (res) {
                if (res.success) {
                    s_class = res.data;
                }
            }
        });
    }

    $.ajax({
        url: basePath()+"/edu/class/get_all_class.do",
        type: 'GET',
        data : {
            "type" : s_type
        },
        dataType : "json",
        success: function (res) {
            if (res.success) {
                $("#hp_class option").remove();

                if (s_class != undefined && s_class.state == 2) {
                    var op = "<option value='"+ s_class.id +"' selected>"+ s_class.name +"(已过期)</option>";
                    $('#hp_class').append(op);
                }

                var selected_content = "<option value='' selected='selected'>所有班级</option>";
                $("#stu_class").append(selected_content);
                if (res.data.length == 0) {
                    var content = "<option>暂无班级数据</option>";
                    $("#hp_class").append(content);
                } else {
                    for (var i = 0, a = res.data.length; i < a; i++) {
                        var content = "<option value='" + res.data[i].id + "'>" + res.data[i].name + "</option>";
                        $("#hp_class").append(content);
                    }
                }
            }
        }
    });
    return true;
}

function allClassBySearch(value) {
    var s_type = $("#stu_direction :selected").val();
    if(s_type == "" || s_type == null || s_type == undefined){
        s_type = 0;
    }
    if(value != "" &&  value != null && value != undefined){
        s_type = value;
    }


    if(s_type == '-1'){
        $("#stu_class option").remove();
        var selected_content = "<option value='' selected='selected'>所有班级</option>";
        $("#stu_class").append(selected_content);
        return ;
    }

    $.ajax({
        url: basePath()+"/edu/class/get_all_class.do",
        type: 'GET',
        data : {
            "type" : s_type
        },
        dataType : "json",
        success: function (res) {
            if (res.success) {
                $("#stu_class option").remove();
                if (res.data.length == 0) {
                    var content = "<option>暂无班级数据</option>"
                    $("#stu_class").append(content);
                } else {
                    for (var i = 0, a = res.data.length; i < a; i++) {
                        var content = "<option value='" + res.data[i].id + "'>" + res.data[i].name + "</option>";
                        $("#stu_class").append(content);
                    }
                    var selected_content = "<option value=''>所有班级</option>";
                    $("#stu_class").append(selected_content);
                }
            }
        }
    })
}

//更新慧与学号
function updateStuInfomation() {

    var bv = $("#update_form_id").data('bootstrapValidator');
    bv.validate();
    if (!bv.isValid()) {
        return;
    }

    var id = $("#this_id").val();
    var hp_id = $("#hp_id").val();
    var hp_direction = $("#hp_direction").val();
    var hp_state = $("#hp_state").val();
    var hp_class = $("#hp_class").val();

    if (hp_class == '暂无班级数据'){
        bootbox.alert("不能修改");
        return;
    }

    $.ajax({
        url: basePath()+'/edu/stu/update_part.do',
        type: 'POST',
        data: {
            "id": id,
            "newId": hp_id,
            "state": hp_state,
            "direction": hp_direction,
            "spare": hp_class
        },
        dataType: 'json',
        success: function (res) {
            $("#updateStudent").modal('hide');
            bootbox.alert(res.msg);
            if (view_s_type == 0) {
                getStudentList(page_now);
            }else {
                getNotOnlineStudentList(page_now);
            }
        }
    })
}


//分页获得所有学生
function getStudentList(page_num) {
    view_s_type = 0;
    page_now = page_num;
    $.ajax({
        url: basePath()+'/edu/stu/list.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "8"
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();

            if (data.success) {
                var page = data.data;
                var item = data.data.list;
                setPage(page.pageNum, page.pages, "getStudentList");
                $("#result").html("");
                for (var i = 0; i < item.length; i++) {
                    var temp = $("#m_template").clone();
                    temp.attr("id", item[i].id);
                    temp.removeAttr("style");
                    temp.find(".s_num").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                    temp.find(".s_name").html(item[i].name);
                    temp.find(".s_new_id").html(item[i].newId);
                    temp.find(".s_spare").html(item[i].className);
                    temp.find(".s_tel").html(item[i].tel);
                    temp.find(".s_sex").html(item[i].sex);
                    temp.find(".s_school").html(item[i].school);
                    temp.find(".s_state").html(getState(item[i].state));
                    temp.find(".s_direction").html(item[i].directionName);
                    //修改按钮组装
                    var ucontent = "<button type='button'  class='btn btn-warning btn-xs' onclick='updateStudent(" + item[i].id + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>修改</span></button>"
                    temp.find(".s_op").html(ucontent);
                    var acontent = "<form method='GET' action='"+basePath()+"/edu/stu/detail.do'>" +
                        "<input type='hidden' name='id' value=" + item[i].id + "> " +
                        "<button type='submit'  class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span><span style='margin: 0px 8px;'>查看全部</span></button>" +
                        "</form>";
                    temp.find(".s_detail").html(acontent);
                    var resetBtn = "<button type='button' class='btn btn-primary btn-xs btn-danger' onclick='resetPassword(" + item[i].id + ")'><span class='glyphicon glyphicon-refresh' aria-hidden='true' style='padding-right: 1px;'></span><span style='margin: 0px 2px;'>重置密码</span></button>";
                    temp.find('.s_reset').html(resetBtn);
                    $("#result").append(temp);
                }
            } else {
                bootbox.alert(data.msg);
            }
        }
    })
}

function resetPassword(id) {

    bootbox.confirm({
        message: "确认重置密码吗?",
        buttons: {
            confirm: {
                label: '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 是',
                className: 'btn-danger'
            },
            cancel: {
                label: '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 否',
                className: 'btn-default'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: basePath()+'/edu/stu/reset_pwd.do',
                    type: 'post',
                    data: {'id': id},
                    dataType: 'json',
                    success: function (res) {
                        if (res.success) {
                            bootbox.alert(res.msg);
                        } else {
                            bootbox.alert(res.msg);
                        }
                    }
                });
            }
        }
    });


}

function getNotOnlineStudentList(page_num) {
    view_s_type = 1;
    page_now = page_num;
    $.ajax({
        url: basePath()+'/edu/stu/level_list.do',
        type: 'GET',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "8"
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();

            if (data.success) {
                var page = data.data;
                var item = data.data.list;
                setPage(page.pageNum, page.pages, "getNotOnlineStudentList");
                $("#result").html("");
                for (var i = 0; i < item.length; i++) {
                    var temp = $("#m_template").clone();
                    temp.attr("id", item[i].id);
                    temp.removeAttr("style");
                    temp.find(".s_num").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                    temp.find(".s_name").html(item[i].name);
                    temp.find(".s_new_id").html(item[i].newId);
                    temp.find(".s_spare").html(item[i].className);
                    temp.find(".s_tel").html(item[i].tel);
                    temp.find(".s_sex").html(item[i].sex);
                    temp.find(".s_school").html(item[i].school);
                    temp.find(".s_state").html(getState(item[i].state));
                    temp.find(".s_direction").html(item[i].directionName);
                    //修改按钮组装
                    var ucontent = "<button type='button'  class='btn btn-warning btn-xs' onclick='updateStudent(" + item[i].id + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>修改</span></button>"
                    temp.find(".s_op").html(ucontent);
                    var acontent = "<form method='GET' action='"+basePath()+"/edu/stu/detail.do'>" +
                        "<input type='hidden' name='id' value=" + item[i].id + "> " +
                        "<button type='submit'  class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span><span style='margin: 0px 8px;'>查看全部</span></button>" +
                        "</form>";
                    temp.find(".s_detail").html(acontent);
                    var resetBtn = "<button type='button' class='btn btn-primary btn-xs btn-danger' onclick='resetPassword(" + item[i].id + ")'><span class='glyphicon glyphicon-refresh' aria-hidden='true' style='padding-right: 1px;'></span><span style='margin: 0px 2px;'>重置密码</span></button>";
                    temp.find('.s_reset').html(resetBtn);
                    $("#result").append(temp);
                }
            } else {
                bootbox.alert(data.msg);
            }
        }
    })
}
//分页查询学生
function searchStudent(page_num) {
    var bv = $("#search_form").data('bootstrapValidator');
    bv.validate();
    if (!bv.isValid()) {
        return;
    }

    var name = $("#stu_name").val();
    var school = $("#stu_school").val();
    var direction = $("#stu_direction").val();
    var s_class = $("#stu_class").val();
    $.ajax({
        url: basePath()+'/edu/stu/search.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "8",
            "name": name,
            "school": school,
            "spare": s_class,
            "direction": direction
        },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                var page = data.data;
                var item = data.data.list;
                setPage(page.pageNum, page.pages, "searchStudent");
                $("#result").html("");
                for (var i = 0; i < item.length; i++) {
                    var temp = $("#m_template").clone();
                    temp.attr("id", item[i].id);
                    temp.removeAttr("style");
                    temp.find(".s_num").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                    temp.find(".s_name").html(item[i].name);
                    temp.find(".s_new_id").html(item[i].newId);
                    temp.find(".s_spare").html(item[i].className);
                    temp.find(".s_tel").html(item[i].tel);
                    temp.find(".s_sex").html(item[i].sex);
                    temp.find(".s_school").html(item[i].school);
                    temp.find(".s_state").html(getState(item[i].state));
                    temp.find(".s_direction").html(item[i].directionName);
                    //修改按钮组装
                    var ucontent = "<button type='button'  class='btn btn-warning btn-xs' onclick='updateStudent(" + item[i].id + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>修改</span></button>"
                    temp.find(".s_op").html(ucontent);
                    var acontent = "<form method='GET' action='"+basePath()+"/edu/stu/detail.do'>" +
                        "<input type='hidden' name='id' value=" + item[i].id + "> " +
                        "<button type='submit'  class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span><span style='margin: 0px 8px;'>查看全部</span></button>" +
                        "</form>";
                    temp.find(".s_detail").html(acontent);
                    var resetBtn = "<button type='button' class='btn btn-primary btn-xs btn-danger' onclick='resetPassword(" + item[i].id + ")'><span class='glyphicon glyphicon-refresh' aria-hidden='true' style='padding-right: 1px;'></span><span style='margin: 0px 2px;'>重置密码</span></button>";
                    temp.find('.s_reset').html(resetBtn);
                    $("#result").append(temp);
                }
            } else {
                bootbox.alert(data.msg);
            }
        }
    })
}




//打开模态框显示的数据加载
function updateStudent(s_id) {
    $.ajax({
        url: basePath()+'/edu/stu/part.do',
        type: 'GET',
        async: 'false',
        data: {
            "id": s_id
        },
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $("#this_id").val(data.data.id);
                $("#hp_id").val(data.data.newId);
                $("#hp_direction option").removeAttr("selected");
                $("#hp_direction option[value = '" + data.data.direction + "']").prop("selected", true);
                s_class_id = data.data.spare;
                if(allClass(null)) {
                    $("#hp_class option").removeAttr("selected");
                    $("#hp_class option[value = '" + data.data.spare + "']").prop("selected", true);
                    $("#hp_state option").removeAttr("selected");
                    $("#hp_state option[value = '" + data.data.state + "']").prop("selected", true);
                    $("#updateStudent").modal('show');
                }else{
                    $("#hp_class option").removeAttr("selected");
                    $("#hp_class option[value = '" + data.data.spare + "']").prop("selected", true);
                    $("#hp_state option").removeAttr("selected");
                    $("#hp_state option[value = '" + data.data.state + "']").prop("selected", true);
                    $("#updateStudent").modal('show');
                }
            } else {
                bootbox.alert(data.msg);
            }
        }
    });

}


//当前毕业生毕业
function graduation() {
    bootbox.confirm({
        title: "确认",
        message: "确认毕业当前学生?",
        buttons: {
            cancel: {
                className: 'btn-default',
                label: '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 取消'
            },
            confirm: {
                className: 'btn-danger',
                label: '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 确认'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: 'edu/stu/graduation_all.do',
                    type: 'GET',
                    dataType: 'json',
                    success: function (res) {
                        if (res.success) {
                            bootbox.alert(res.msg);
                            getStudentList(1);
                        } else {
                            bootbox.alert(res.msg);
                        }
                    }
                });
            }
        }
    });

}

//获得状态的值
function getState(value) {
    var result;
    if (value == 0) {
        result = '未报到';
    } else if (value == 1) {
        result = '在读';
    } else if (value == 2) {
        result = '毕业'
    } else if (value == 3) {
        result = '退学';
    } else {
        result = '其他';
    }
    return result;
}

function validatorSearch() {
    $('#search_form').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            stu_names: {
                message: 'this is not valid',
                validators: {
                    stringLength: {
                        max: 10,
                        message: '学生名长度不能过长'
                    },
                    regexp: {
                        regexp: /^(?=[0-9a-zA-Z\u4e00-\u9fa5]+$)/,
                        message: '学生名只能由中文，数字和英文字母组成'
                    }
                }
            },
            stu_school: {
                message: 'this is not valid',
                validators: {
                    stringLength: {
                        max: 12,
                        message: '学校名长度不能过长'
                    },
                    regexp: {
                        regexp: /^(?=[0-9a-zA-Z\u4e00-\u9fa5]+$)/,
                        message: '学校名只能由中文，数字和英文字母组成'
                    }
                }
            }
        }
    });
}

function validatorUpdate() {
    $('#update_form_id').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            hp_id: {
                message: 'this is not valid',
                validators: {
                    stringLength: {
                        max: 10,
                        message: '学号长度不能过长'
                    },
                    regexp: {
                        regexp: /^[0-9]*$/,
                        message: '学号只能由数字组成'
                    }
                }
            }
        }
    });
}

function clearModal() {
    $('#updateStudent').modal('hide');

    $('#update_form_id').data('bootstrapValidator').destroy();
    $('#update_form_id').data('bootstrapValidator', null);
    $("form[id='update_form_id'] > div").addClass('has-success');
    validatorUpdate();
}

function getExcel() {
    $("#getStudentExcel").modal("show");
}

function clearTimeModal(){
    $("#getStudentExcel").modal("hide");
}

function validatorTime(id1, id2, id3, name) {
    if ($(id1).val() == "") {
        var group = $(id2).parent().parent();
        group.removeClass("has-success");
        group.addClass("has-error");
        $(id3).css('display', '');
        name = false;
    } else {
        var group = $(id2).parent().parent();
        group.removeClass("has-error");
        group.addClass("has-success");
        $(id3).css('display', 'none');
        name = true;
    }
}

function downloadExcel(){

    if (start_time == false || end_time == false) {
        return;
    }

    var beginTime = $("#newstime").val();
    console.info(beginTime);
    if (beginTime == "") {
        validatorTime('#newstime', '#studentBeginTime', '#newSTimeSmall', start_time);
        return;
    }else {
        validatorTime('#newstime', '#studentBeginTime', '#newSTimeSmall', start_time);
    }

    var endTime = $("#newetime").val();
    if (endTime == "") {
        validatorTime('#newetime', '#studentEndTime', '#newETimeSmall', end_time);
        return;
    }else {
        validatorTime('#newetime', '#studentEndTime', '#newETimeSmall', end_time);
    }

    $.ajax({
        url: basePath() + '/edu/stu/get_score_count.do',
        type: 'post',
        data: {'begin': beginTime, 'end':endTime},
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                // window.location.href = basePath()+"/edu/stu/get_excel.do?begin="+beginTime+"&end="+endTime;
                clearTimeModal();

                var dialog = bootbox.dialog({
                    title: '请稍候',
                    message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 导出中...</p>'
                });

                var url = basePath()+"/edu/stu/get_excel.do?begin="+beginTime+"&end="+endTime;
                var xhr = new XMLHttpRequest();
                xhr.open('GET', url, true);        // 也可以使用POST方式，根据接口
                xhr.responseType = "blob";    // 返回类型blob
                // 定义请求完成的处理函数，请求前也可以增加加载框/禁用下载按钮逻辑
                xhr.onload = function () {
                    // 请求完成
                    if (this.status === 200) {
                        // 返回200
                        var blob = this.response;
                        var reader = new FileReader();
                        reader.readAsDataURL(blob);    // 转换为base64，可以直接放入a表情href
                        reader.onload = function (e) {
                            // 转换完成，创建一个a标签用于下载
                            var a = document.createElement('a');
                            a.download = "学生信息表.xlsx";
                            a.href = e.target.result;
                            $("body").append(a);    // 修复firefox中无法触发click
                            a.click();
                            $(a).remove();
                            dialog.find('.modal-title').html('完成');
                            dialog.find('.modal-body').html("导出成功");
                            dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\">OK</button></div>");

                        }
                    }
                };
                // 发送ajax请求
                xhr.send();

            }else {
                bootbox.alert("此阶段考试数量为0");
            }
        }

    });


}

function getStudentInfoExcel(){
                var dialog = bootbox.dialog({
                    title: '请稍候',
                    message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 导出中...</p>'
                });

                var url = basePath()+"/edu/stu/get_info_excel_title.do?";
                var xhr = new XMLHttpRequest();
                xhr.open('GET', url, true);
                xhr.responseType = "blob";
                xhr.onload = function () {
                    // 请求完成
                    if (this.status === 200) {
                        // 返回200
                        var blob = this.response;
                        var reader = new FileReader();
                        reader.readAsDataURL(blob);
                        reader.onload = function (e) {
                            var a = document.createElement('a');
                            a.download = "学生注册表.xlsx";
                            a.href = e.target.result;
                            $("body").append(a);
                            a.click();
                            $(a).remove();
                            dialog.find('.modal-title').html('完成');
                            dialog.find('.modal-body').html("导出成功");
                            dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\">OK</button></div>");

                        }
                    }
                };
                xhr.send();
}
function toUploadStudentInfoExcel() {
    var dialog = bootbox.dialog({
        title: '请稍候',
        message: '<i class="icon-spinner icon-spin icon-2x pull-left"></i> 导入中...</p>'
    });

    $('#updateForm').ajaxSubmit({
        url: basePath()+'/edu/stu/upload_student_info_excel.do',
        type: 'post',
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                dialog.find('.modal-title').html('完成');
                dialog.find('.modal-body').html(res.msg);
                dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\">OK</button></div>");
                getStudentList(1);
            } else {
                dialog.find('.modal-title').html('错误');
                dialog.find('.modal-body').html(res.msg);
                dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\">OK</button></div>");
            }
        },
        error: function () {
            dialog.find('.modal-title').html('失败');
            dialog.find('.modal-body').html("导入失败, 请稍后重试");
            dialog.find('.modal-body').after("<div class=\"modal-footer\"><button data-bb-handler=\"ok\" type=\"button\" class=\"btn btn-primary\">OK</button></div>");
        }

    });
}
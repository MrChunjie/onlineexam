function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}
loading();
var page = 1;
var page_item_num = 0;
var item_num = 8;
var max_page_num = 0;

$(function () {
    getSignResult(page);
    getAllProvince()
    validator();
    $("#complete_sign_btn").click(function () {
        completeInfo();
    });
});

function validator() {
    $('#completeForm').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            money: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '薪资范围不能为空'
                    },
                }
            }
        }
    });
}

function getSignResult(pageNum) {
    $.ajax({
        url: basePath()+'/student/sign/sign_result.do',
        type: 'GET',
        async: 'true',
        data: {
            "pageNum": pageNum,
            "pageSize": item_num
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            var pageResult = data.data;
            var items = data.data.list;

            if (items.length == 0) {
                $('.pagination').hide();
                $('.table').hide();
                $('hr').after("<div class='alert alert-warning alert-dismissible' role='alert'>\n" +
                    "  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>\n" +
                    "  <strong style='padding-left: 30px;'>暂时没有任何报名</strong>" +
                    "</div>");
                return;
            }else {
                $('.table').show();
                $('.pagination').show();
            }

            page_item_num = items.length;
            max_page_num = pageResult.pages;
            page = setPage(pageResult.pageNum, pageResult.pages, "getSignResult");
            $("#result").html("");
            for (var i = 0; i < items.length; i++) {

                var temp = $("#m_template").clone();
                temp.attr("id", items[i].enterprise.id);
                temp.removeAttr("style");
                temp.find(".ep_num").html(pageResult.pageSize * (pageResult.pageNum - 1) + 1 + i);
                temp.find(".ep_name").html(items[i].enterprise.name);
                temp.find(".ep_etime").html(items[i].enterprise.eTime);
                if (items[i].enterprise.type == 0) {
                    temp.find(".ep_type").html("全部");
                }
                if (items[i].enterprise.type == 1) {
                    temp.find(".ep_type").html("开发");
                }
                if (items[i].enterprise.type == 2) {
                    temp.find(".ep_type").html("测试");
                }

                if (items[i].enterprise.category == 0) {
                    temp.find(".ep_category").html("招聘");
                }
                if (items[i].enterprise.category == 1) {
                    temp.find(".ep_category").html("定制");
                }

                var info;

                if (items[i].state == -1) {
                    temp.find(".ep_state").html("<span class='label label-info'>等待录取</span>");
                    info = "<span class='label label-info' ><span style='margin: 0px 8px;'>等待完善</span></span>"
                }else if (items[i].state == 0) {
                    temp.find(".ep_state").html("<span class='label label-warning'>未录取</span>");
                    info = "<span class='label label-info' ><span style='margin: 0px 8px;'>不能完善</span></span>"
                }else if (items[i].state == 1) {
                    temp.find(".ep_state").html("<span class='label label-success'>已录取</span>");
                    if (items[i].position != null) {
                        info = "<button type='button'  class='btn btn-success btn-xs' style='width: 80px' onclick='toInfo(" + items[i].position + ")' ><span style='margin: 0px 8px;'><span class='glyphicon glyphicon-ok-circle' aria-hidden='true' style='padding-right: 4px;'></span>已完善</span></button>"
                        $(textInfo).after("<input id='money" + items[i].position + "' type='hidden' value='" + items[i].money + "'>")
                    } else {
                        info = "<button type='button'  class='btn btn-primary btn-xs' style='width: 90px' onclick='tocompleteInfo(" + items[i].enterprise.id + ")' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>完善信息</span></button>"
                    }
                }

                temp.find(".ep_category").before("<input type='hidden' value='" + items[i].enterprise.text + "'/>");

                temp.find(".ep_post").html(items[i].choice);

                //组装按钮
                temp.find(".ep_info").html(info);

                $("#result").append(temp);
            }
        }
    })
};

function toInfo(positionId) {
    $.ajax({
        url: basePath()+"/student/common/get_position.do",
        type: 'post',
        async: false,
        data: {"positionId": positionId},
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $("#textInfo").html("薪资：" + $("#money" + positionId).val() + "<br>城市：" + data.data.province + data.data.city);
                $("#alreadyCompleteInfo").modal("show");
            }
        }
    });
}

function tocompleteInfo(enterpriseId) {
    $("#enterpriseId").val(enterpriseId);
    $("#completeInfo").modal("show");

}

function completeInfo() {
    var enterpriseId = $("#enterpriseId").val();
    var money = $("#money").val();
    var city = $("#city").val();
    var url = basePath()+"/student/sign/sign_complete.do";
    var args = {"enterId": enterpriseId, "money": money, "position": city};
    $.post(url, args, function (data) {
        if (data.status == 0) {
            clearCompleteModel()
            getSignResult(page);
            bootbox.alert(data.msg);
        } else {
            bootbox.alert(data.msg);
        }
    }, "json");
}

function clearCompleteModel() {
    $('#completeInfo').modal('hide');
    // $("#money").val("");
    // $("#city").val("");
}


function getAllProvince() {
    $.ajax({
        url: basePath()+'/student/common/get_all_province.do',
        dataType: 'json',
        async: false,
        success: function (res) {
            if (res.success) {
                $.each(res.data, function (index, item) {
                    var op = "<option value='" + item + "'>" + item + "</option>";
                    $('#province').append(op);
                });
            }
        }
    });
}

function getCity() {
    var province = $('#province').val();
    $.ajax({
        url: basePath()+'/student/common/get_city.do',
        type: 'post',
        async: false,
        data: {'province': province},
        dataType: 'json',
        success: function (res) {

            //移除原有的数据
            $('#city').empty();

            if (res.success) {
                $.each(res.data, function (index, item) {
                    var op = "<option value='" + item.id + "'>" + item.city + "</option>";
                    $('#city').append(op);
                });
            } else {

            }
        }
    });
}
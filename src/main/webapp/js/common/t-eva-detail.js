function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}
var page_now = 1;

function getPapers(page_num) {
    page_now = page_num;
    // Ajax异步请求,平均成绩
    $.ajax({
        url: basePath()+'/teacher/eva/show_avg_detail.do',
        type: 'POST',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "15"
        },
        dataType: 'json',
        success: function (data) {
            var page = data.data;
            setPage(page.pageNum, page.pages, "getPapers");
            var item = data.data.list;
            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.removeAttr("style");
                temp.find(".p_id").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.find(".p_testName").html(item[i].examName);
                temp.find(".p_courseName").html(item[i].courseName);
                temp.find(".p_className").html(item[i].sClassName);
                temp.find(".p_stuName").html(item[i].stuName);
                temp.find(".p_score1").html(Math.round(item[i].score_1));
                temp.find(".p_score2").html(Math.round(item[i].score_2));
                temp.find(".p_score3").html(Math.round(item[i].score_3));
                temp.find(".p_scoreB").html(Math.round(item[i].score_b));
                temp.find(".p_scoreM").html(Math.round(item[i].score_m));
                temp.find(".p_scoreAll").html(Math.round(item[i].score_all));
                $("#result").append(temp);
            }
        }
    });
}
$(function () {
    getPapers(1);
});
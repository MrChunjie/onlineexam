function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}
loading();
var page = 1;
var page_item_num = 0;
var item_num = 8;
var max_page_num = 0;

$(function () {
    getEnterprises(page);
    validatorU();
    validatorA();
});

function validatorU() {
    $('#updateForm').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            newEnterpriseName: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '企业名不能为空'
                    },
                    stringLength: {
                        message: '企业名已存在，请重新输入'
                    }
                }
            }
        }
    });
}

function validatorA() {
    $('#addForm').bootstrapValidator({
        message: 'is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            enterpriseName1: {
                message: 'this is not valid',
                validators: {
                    notEmpty: {
                        message: '企业名不能为空'
                    },
                    stringLength: {
                        message: '企业名已存在，请重新输入'
                    }
                }
            },

        }
    });
}

function getEnterprises(pageNum) {
    $.ajax({
        url: basePath()+'/student/sign/all_enterprise.do',
        type: 'GET',
        async: 'true',
        data: {
            "pageNum": pageNum,
            "pageSize": item_num
        },
        dataType: 'json',
        success: function (data) {
            if(data.status == 0){
                removeLoading();

                if (data.data.enterprises == null || data.data.enterprises.list == '') {
                    $('.pagination').hide();
                    $('.table').hide();
                    $('hr').after("<div class='alert alert-warning alert-dismissible' role='alert'>\n" +
                        "  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>\n" +
                        "  <strong style='padding-left: 30px;'>暂时没有任何企业</strong>" +
                        "</div>");
                    return;
                }else {
                    $('.table').show();
                    $('.pagination').show();
                }
                var pageResult = data.data.enterprises;
                var enterpriseItem = data.data.enterprises.list;
                var signInfoItem = data.data.signInfos;
                page_item_num = enterpriseItem.length;
                max_page_num = pageResult.pages;
                page = setPage(pageResult.pageNum, pageResult.pages, "getEnterprises");
                $("#result").html("");
                for (var i = 0; i < enterpriseItem.length; i++) {
                    var temp = $("#m_template").clone();
                    temp.attr("id", enterpriseItem[i].id);
                    temp.removeAttr("style");
                    temp.find(".ep_num").html(pageResult.pageSize * (pageResult.pageNum - 1) + 1 + i);
                    temp.find(".ep_name").html(enterpriseItem[i].name);
                    temp.find(".ep_stime").html(enterpriseItem[i].sTime);
                    temp.find(".ep_etime").html(enterpriseItem[i].eTime);
                    if (enterpriseItem[i].type == 0) {
                        temp.find(".ep_type").html("全部");
                    }
                    if (enterpriseItem[i].type == 1) {
                        temp.find(".ep_type").html("开发");
                    }
                    if (enterpriseItem[i].type == 2) {
                        temp.find(".ep_type").html("测试");
                    }

                    if (enterpriseItem[i].category == 0) {
                        temp.find(".ep_category").html("招聘");
                    }
                    if (enterpriseItem[i].category == 1) {
                        temp.find(".ep_category").html("定制");
                    }
                    temp.find(".ep_category").before("<input type='hidden' value='" + enterpriseItem[i].text + "'/>");
                    //正文按钮组装
                    var enterpriseInfo = "<button type='button'  class='btn btn-info btn-xs' onclick='toInfo(this);' ><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span><span style='margin: 0px 8px;'>查看</span></button>"
                    temp.find(".ep_info").append(enterpriseInfo);
                    //报名按钮组装
                    var flag = false;
                    if(signInfoItem != null ){
                        for (var k = 0; k < signInfoItem.length; k++) {
                            if (signInfoItem[k].enterprise.id == enterpriseItem[i].id) {
                                flag = true;
                                temp.find(".ep_post").html(signInfoItem[k].choice);
                                var sign = "<label id='signButton" + enterpriseItem[i].id + "'  class='label label-success' style='padding: 2px 15px;'>已报名</label>"
                                temp.find(".ep_sign").append(sign);
                                break;
                            }
                        }
                    }

                    if (!flag) {
                        var option = "";
                        for (var j = 0; j < enterpriseItem[i].posts.length; j++) {
                            option += "<option value='" + enterpriseItem[i].posts[j].id + "'>" + enterpriseItem[i].posts[j].name + "</option>";
                        }
                        temp.find(".ep_post").html("<select style='width: 90px;height: 25px;padding-top:0px;margin-left: 30%' class='form-control' name='signPost' id='signPost" + enterpriseItem[i].id + "'>" + option + "</select>");
                        var sign = "<button type='button' id='signButton" + enterpriseItem[i].id + "'  class='btn btn-primary btn-xs' onclick='sign(" + enterpriseItem[i].id + ");' ><span class='glyphicon glyphicon-edit' aria-hidden='true'></span><span style='margin: 0px 8px;'>报名</span></button>"
                        temp.find(".ep_sign").append(sign);
                    }
                    $("#result").append(temp);
                }
            }else{
                bootbox(data.msg);
            }

        }
    })
};


function sign(enterpriseId) {
    var postId = $("#signPost" + enterpriseId).val();
    bootbox.confirm({
        title: "确认报名?",
        message: "<b>公司</b>: " + $("#signPost" + enterpriseId).parent().parent().find('.ep_name').html() +"<br/><br/><b>方向</b>: " + $("#signPost" + enterpriseId).find("option[value='"+postId+"']").html(),
        buttons: {
            cancel: {
                label: '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 取消'
            },
            confirm: {
                label: '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 确认'
            }
        },
        callback: function (result) {
            if (result) {
                var url = basePath()+"/student/sign/sign_enterprise.do";
                var data = {"enterpriseId": enterpriseId, "postId": postId};
                $.post(url, data, function (data) {
                    if (data.status == 0) {
                        bootbox.alert(data.msg);
                        getEnterprises(page);
                    } else {
                        bootbox.alert(data.msg);
                    }

                }, "json");
            }
        }
    });

}

function toInfo(temp) {
    var text = $(temp).parent().parent().children(":input").val();
    if (text == "") {
        text = "无介绍";
    }
    $("#enterpriseTextInfo").html(text);
    $("#enterpriseInfo").modal("show");
}
function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}

loading();
var page_now = 1;

function getAvgs(page_num) {
    page_now = page_num;
    // Ajax异步请求,平均成绩
    $.ajax({
        url: basePath()+'/edu/class/class_list.do',
        type: 'GET',
        async: 'true',
        data: {
            "pageNum": page_num,
            "pageSize": "8"
        },
        dataType: 'json',
        success: function (data) {
            removeLoading();
            var page = data.data;
            setPage(page.pageNum, page.pages, "getAvgs");
            var item = data.data.list;
            $("#result").html("");
            for (var i = 0; i < item.length; i++) {
                var temp = $("#m_template").clone();
                temp.attr("id", page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.removeAttr("style");
                temp.find(".p_id").html(page.pageSize * (page.pageNum - 1) + 1 + i);
                temp.find(".p_className").html( item[i].name);
                temp.find("#p_detail").attr("href", basePath()+"/edu-score-forward.do?classId="+ item[i].id );
                $("#result").append(temp);
            }
        }
    });
}

function getClasses() {
    // Ajax异步请求,班级列表
    var stuClassId = $("#stuClassId").val();
    $.ajax({
        url: basePath()+'/edu/class/class_list.do',
        type: 'GET',
        async: 'true',
        data: {
            "pageNum": 1,
            "pageSize": "99"
        },
        dataType: 'json',
        success: function (data) {
            var item = data.data.list;
            $("#stuClassId").append("<option value='-3' >当前阶段所有班级</option>");
            for (var i = 0; i < item.length; i++) {
                $("#stuClassId").append("<option value='" + item[i].id + "'>" + item[i].name + "</option>");
            }
        }
    });
}
//获得课程
function getCourses() {
    $.ajax({
        url:basePath()+"/edu/course/get_all_course.do",
        type:'GET',
        success:function (res) {
            if (res.success) {
                $("#courseId option").remove();
                var selected_content = "<option value='-3' selected='selected'>该班级所有课程</option>";
                $("#courseId").append(selected_content);
                if (res.data.length == 0) {
                    var content = "<option>暂无课程数据</option>"
                    $("#courseId").append(content);
                } else {
                    for (var i = 0, a = res.data.length; i < a; i++) {
                        var content = "<option value='" + res.data[i].id + "'>" + res.data[i].name + "</option>";
                        $("#courseId").append(content);
                    }
                }
            }
        }
    })
}
$(function () {
    getClasses();
    getCourses();
    getAvgs(1);
});
function getThisCourse() {
    var classId = $("#stuClassId").val();
    if (classId == -3) {
        $("#courseId option").remove();
        var option = "<option value='-3'>所有班级所有课程</option>"
        $("#courseId").append(option);
    } else{
        $.ajax({
            url: basePath() + '/edu/score/get_course.do',
            type: 'POST',
            data: {
                "classId": classId
            },
            dataType: 'JSON',
            success: function (data) {
                $("#courseId option").remove();
                if (data.data.length == 0) {
                    var option = "<option>暂无课程数据</option>"
                    $("#courseId").append(option);
                }
                else {
                    var option = "<option value='-1'>该班所有课程</option>";
                    $("#courseId").append(option);
                    for (var a = 0; a < data.data.length; a++) {
                        var option = "<option value='" + data.data[a].id + "'>" + data.data[a].name + "</option>";
                        $("#courseId").append(option);
                    }
                }
            }
        })
}
}
function getAllAvg(){
    var classId = $("#stuClassId").val();
    var courseId = $("#courseId").val();
    console.info(classId);
    if (classId == "-1"){
        bootbox.alert("请选择班级");
        return;
    }
    $.ajax({
        url : basePath()+'/edu/score/get_class_avg_by_course.do',
        method :'POST',
        async : true,
        data:{
            "courseId" :courseId,
            "classId" :classId
        },
        success:function(data){
            bootbox.alert(data.data);
        }
    })
}
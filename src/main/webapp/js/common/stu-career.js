function basePath(){
    var curWwwPath = window.document.location.href;
    var pathName =  window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPath = curWwwPath.substring(0,pos);
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);
    return (localhostPath + projectName);
}

loading();

$(function () {
    document.body.style.overflow="hidden";
    getStuInfo();
    getClassCareer();
    getWordCloud();
    getRadar();
});

function getStuInfo() {
    $.ajax({
        url: basePath()+'/student/career/get_student_info.do',
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                if (res.data.sex == "男") {
                    $('#img').attr('src', 'imgs/man.png');
                } else {
                    $('#img').attr('src', 'imgs/women.png');
                }
                $('#sex').html(res.data.sex);
                $.ajax({
                    url: basePath()+'/student/career/get_one_direction.do',
                    type: 'post',
                    async: false,
                    data: {'id': res.data.direction},
                    dataType: 'json',
                    success: function (res) {
                        if (res.success) {
                            $('#direction').html(res.data.name);
                        }
                    }
                });
                $('#name').html(res.data.name);
                $('#tel').html(res.data.tel);
                $('#nation').html(res.data.nation);
                $('#school').html(res.data.school);
                $('#college').html(res.data.college);
                $('#major').html(res.data.major);

                if (res.data.home != null) {
                    $('#home').html(res.data.home.replace(new RegExp(';', 'g'), ''));
                }


            }
        }
    });
}

function getClassCareer() {
    $.ajax({
        url: basePath()+'/student/career/get_all_class_career.do',
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                $.each(res.data, function (index, item) {
                    var temp = $('.showClass').clone();
                    temp.css('display', 'block');
                    temp.removeAttr('class');
                    temp.find('#classIndex').html(index + 1);
                    temp.find('#classTime').html(item.changetime);
                    temp.find('#className').html(item.classname);
                    temp.find('#classTeacher').html(item.teachername);
                    temp.find('#classCourse').html(item.coursename);
                    $('#classLists').append(temp);
                });
            }
        }
    });
}


function getWordCloud() {
    $.ajax({
        url: basePath()+'/student/career/get_word_cloud_career.do',
        dataType: 'json',
        success: function (res) {
            if (res.success) {
                $('#grade_num').html(res.data.gradenum);
                $('#grade_name').html(res.data.gradename);
                $('#grade_value').html(res.data.value);
                $('#avg_value').html(res.data.avgvalue);
                $('#avg_grade').html(res.data.avggrade);
            }
        }
    });
}

function getRadar() {
    $.ajax({
        url: basePath()+'/student/career/get_radar.do',
        dataType: 'json',
        success: function (res) {
            //#ac2925  #D58515  #449d44
            var value = res.radarvalue;
            var description = res.description;
            var num_id = ['#radar_communication_num', '#radar_study_num', '#radar_practice_num',
                '#radar_grow_num', '#radar_skill_num'];
            var value_id = ['#radar_communication', '#radar_study', '#radar_practice',
                '#radar_grow', '#radar_skill'];

            for (var i = 0; i < num_id.length; i++) {
                $(num_id[i]).html(value[i]);
                switch (value[i]/10){
                    case 10:
                        $(num_id[i]).css('color', '#449d44');
                        break;
                    case 9:
                        $(num_id[i]).css('color', '#449d44');
                        break;
                    case 8:
                        $(num_id[i]).css('color', '#449d44');
                        break;
                    case 7:
                        $(num_id[i]).css('color', '#D58515');
                        break;
                    case 6:
                        $(num_id[i]).css('color', '#ac2925');
                        break;
                }
                $(value_id[i]).html(description[i]);
            }

//                    $('#radar_communication_num').html(value[0]);
//                    $('#radar_communication').html(description[0]);
//                    $('#radar_study_num').html(value[1]);
//                    $('#radar_study').html(description[1]);
//                    $('#radar_practice_num').html(value[2]);
//                    $('#radar_practice').html(description[2]);
//                    $('#radar_grow_num').html(value[3]);
//                    $('#radar_grow').html(description[3]);
//                    $('#radar_skill_num').html(value[4]);
//                    $('#radar_skill').html(description[4]);
            removeLoading();
            document.body.style.overflow="scroll";
        }
    });
}


function download() {
    $('#download').hide();
    $('.father').removeClass('father');
    $('#father').attr('style', 'background-color: #ffffff;');
    $('.breadcrumb').hide();
    $('#hr').hide();
    html2canvas(document.body, {
        onrendered: function (canvas) {

            var contentWidth = canvas.width;
            var contentHeight = canvas.height;

            //一页pdf显示html页面生成的canvas高度;
            var pageHeight = contentWidth / 592.28 * 841.89;
            //未生成pdf的html页面高度
            var leftHeight = contentHeight;
            //页面偏移
            var position = 0;
            //a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
            var imgWidth = 595.28;
            var imgHeight = 592.28 / contentWidth * contentHeight;

            var pageData = canvas.toDataURL('image/jpeg', 1.0);

            var pdf = new jsPDF('', 'pt', 'a4');

            //有两个高度需要区分，一个是html页面的实际高度，和生成pdf的页面高度(841.89)
            //当内容未超过pdf一页显示的范围，无需分页
            if (leftHeight < pageHeight) {
                pdf.addImage(pageData, 'JPEG', 0, 0, imgWidth, imgHeight);
            } else {
                while (leftHeight > 0) {
                    pdf.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight)
                    leftHeight -= pageHeight;
                    position -= 841.89;
                    //避免添加空白页
                    if (leftHeight > 0) {
                        pdf.addPage();
                    }
                }
            }

            pdf.save('个人生涯.pdf');

        }
    });

    $("#download").show();
    $('#father').addClass('father');
    $('#father').removeAttr('style');
    $('.breadcrumb').show();
    $('#hr').show();

}

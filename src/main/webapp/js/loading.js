//进度条加载
var progress = 1;
var totalWidth;
var initstr;


function change(id) {
    var a = $('#div11').html();
    $("#div11").html(a);
    $('#' + id).css({
        width: progress
    });
    if (progress >= totalWidth) {
        clearInterval(initstr);
    }
    progress = progress + 50;
}


function loading() {
    /*  设置加载页面*/
    //获取浏览器页面可见高度和宽度
    var _PageHeight = document.documentElement.clientHeight, _PageWidth = document.documentElement.clientWidth;
    //计算loading框距离顶部和左部的距离（loading框的宽度为215px，高度为61px）
    var _LoadingTop = _PageHeight > 61 ? (_PageHeight - 61) / 2 : 0,
        _LoadingLeft = _PageWidth > 215 ? (_PageWidth - 215) / 2 : 0;


    //在页面未加载完毕之前显示的loading Html自定义内容
    var _LoadingHtml = '<div id="loadingDiv" style="position:absolute;left:0;width:100%;height:'
        + _PageHeight
        + 'px;top:0;background: rgba(255,255,255,0.96);z-index:10000; border-radius: 15px;"><div style="position: absolute; cursor1: wait; left: '
        + (_LoadingLeft - 150)
        + 'px; top:'
        + _LoadingTop
        + 'px; width: auto; height: 57px; line-height: 57px; padding-left: 50px; padding-right: 5px; color: #696969; font-family:\'Microsoft YaHei\';">' +
        "<div id='div11' class='progress' style='width: 400px;'>" +
        "<div id='div1' class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar'" +
        "aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width: 0%; z-index: 10;'>" +
        "</div>" +
        "</div>"
        + '</div></div>';

    //呈现loading效果
    document.write(_LoadingHtml);


    totalWidth = 400;
    initstr = setInterval("change('div1')", 1);
}


function removeLoading() {


    setTimeout(function () {
        $('#div1').css('width', '1000');
        var loadingMask = document.getElementById('loadingDiv');
        if (loadingMask != null) {
            loadingMask.parentNode.removeChild(loadingMask);
        }
    }, 100);
}

function waiting() {
    /*  设置加载页面*/
    //获取浏览器页面可见高度和宽度
    var _PageHeight = document.documentElement.clientHeight, _PageWidth = document.documentElement.clientWidth;
    //计算loading框距离顶部和左部的距离（loading框的宽度为215px，高度为61px）
    var _LoadingTop = _PageHeight > 61 ? (_PageHeight - 61) / 2 : 0,
        _LoadingLeft = _PageWidth > 215 ? (_PageWidth - 215) / 2 : 0;


    //在页面未加载完毕之前显示的loading Html自定义内容
    var _LoadingHtml = '<div id="loadingDiv" style="position:absolute;left:0;width:100%;height:'
        + _PageHeight
        + 'px;top:0;background: rgba(255,255,255,0.6);z-index:10000; border-radius: 15px;"><div style="position: absolute; cursor1: wait; left: '
        + (_LoadingLeft - 150)
        + 'px; top:'
        + _LoadingTop
        + 'px; width: auto; height: 57px; line-height: 57px; padding-left: 50px; padding-right: 5px; color: #696969; font-family:\'Microsoft YaHei\';">' +
        "<div id='div11' class='progress' style='width: 400px;'>" +
        "<div id='div1' class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar'" +
        "aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width: 0%; z-index: 10;'>" +
        "</div>" +
        "</div>"
        + '</div></div>';


    $(".father").append(_LoadingHtml);

    totalWidth = 400;
    initstr = setInterval("change('div1')", 1);

}
﻿//js未加密备份
var flag = true; //初始时间判断
//信息对话框
$('#dialog').dialog({
    height: 350,
    width: 120,
    position: {
        my: "right top",
        at: "right-20px top+60px",
        of: window
    },
    open: function (event, ui) {
        $(".ui-dialog-titlebar-close", $(this).parent()).hide();
    }
});


//获取当前聚焦元素，当聚焦元素>640时， 滚动条自动拉取
var count = 1; //当前滚动条初始化，满足成倍条件时自动滚动
function focus() {
    var focus = $(":focus");
    if (focus.length == 1) {
        var height = ($(window).height() - 300) * count;
        if (focus.offset().top >= height) {
            count++;
            $("body").animate({scrollTop: focus.offset().top - 100}, 1500);
        }
    }
};
setInterval("focus()", 1000 * 30);

var count = 0;

//禁用用户不良操作, Ctrl-C, Ctrl-V, 鼠标右键
function key(e) {
    var keynum;
    if (window.event) {
        keynum = e.keyCode;
    } else if (e.which) {
        keynum = e.which;
    }
    if (e.ctrlKey && (keynum == 67 || keynum == 86)) {//禁用赋值粘贴组合键

        if (count > 3) {
            bootbox.alert("别想着复制粘贴了，安心做吧!");
        } else {
            bootbox.alert("你是想搞事情啊!");
        }
        count++;
        return false;
    }
    if (e.ctrlKey && keynum == 83) { //禁止保存
        bootbox.alert("你是想搞事情啊!");
        return false;
    }
    if (keynum == 123) { //F12
        bootbox.alert("你是想搞事情啊!");
        return false;
    }

    //禁止F5刷新以及Ctrl+R
    if (keynum == 116 || (e.ctrlKey && keynum == 82 )) {
        bootbox.alert("你是想搞事情啊!");
        return false;
    }


}

$(document).bind("contextmenu", function () {//禁用鼠标右键
    return false;
});
$(document).bind("selectstart", function () {//禁用鼠标选择
    return false;
});
$(document).keydown(function () {
    return key(arguments[0])
});
$('#text0').bind("cut copy paste", function () {
    return false;
});

$('#typing0').removeAttr("disabled");
$('#typing0').focus(); //初始聚焦于第一行
$("#typing0").bind("input propertychange", function () {
    //当键盘开始输入时，进行计时
    if (($(this).val().length) == 1 && flag) {
        flag = false;
        timedCount();
    }
});

//对比输入
var tstr;
var str;

function CheckInput(currid) {
    tstr = $('#typing' + currid).val();
    str = $('#show' + currid).val();
    var len = tstr.length;
    var elem = $('#text' + currid);
    var textlen = elem.html().length;
    var dis = str.length;
    var dataset = new Array();
    var mystr = new Array();
    if (len <= dis) {
        for (var i = 0; i < len; i++) {
            if (tstr[i] != str[i]) {
                dataset[i] = 1;
            } else {
                dataset[i] = 0;
            }
        }
        for (var i = 0; i < str.length; i++) {
            mystr[i] = str[i];
        }
        elem.html(SetDIV(dataset, mystr));
    } else if (tstr[len - 1] == " " && (len - 1 == dis)) {
        var pos = currid + 1;
        //input attr disable
        $('#typing' + currid).attr("disabled", true);
        $('#text' + pos).bind("cut copy paste", function () {
            return false;
        });
        $('#typing' + pos).removeAttr("disabled");
        $("#typing" + pos).focus();
    }
}

//设置输入字符属性

function SetDIV(DataSet, MyStr) {

    var HTStr = "";
    for (var i = 0; i < DataSet.length; i++) {
        if (DataSet[i] == 1)
            MyStr[i] = '<span class="red">' + MyStr[i] + '</span>';
        if (DataSet[i] == 0)
            MyStr[i] = '<span  class="green">' + MyStr[i] + '</span>';
    }
    for (var i = 0; i < MyStr.length; i++) {
        HTStr += MyStr[i];
    }
    return HTStr;
}

var c = 0;
var t;
var r;

function timedCount() {
    if (c >= 60) {
        var mins = parseInt(c / 60);
        var secd = c - 60 * mins;
        $('#testTime').val(mins + "分" + secd + '秒');
        $('#testTimeShow').text(mins + "分" + secd + '秒');
    } else {
        $('#testTime').val(c + '秒');
        $('#testTimeShow').text(c + '秒');
    }
    c = c + 1;
    t = setTimeout("timedCount()", 1000);
    r = setTimeout("spedandrate()", 1000);
}

var greennum = 0;
var rednum = 0;
var num = 0;
var kpm = 0;
var correctrate = 0;

function spedandrate() {
    greennum = $('.green').length;
    rednum = $('.red').length;
    num = greennum + rednum;
    kpm = (greennum / c * 60).toFixed(2);
    $('#wordNumShow').text(num);
    $('#WrongWordNumShow').text(rednum);
    $('#speedShow').text(kpm);
    correctrate = 0;
    if (greennum + rednum == 0) {
        $('#accuracyShow').text("0%");
    } else {
        correctrate = (greennum / (greennum + rednum) * 100).toFixed(2);
        $('#accuracyShow').text(correctrate + "%");
    }
}

//填写表单
function set() {
    $('#wordNum').val(num);
    $('#WrongWordNum').val(rednum);
    $('#speed').val(kpm);
    $('#accuracy').val(correctrate);
    clear();
}

//清除定时器
function clear() {
    clearTimeout(t);
}

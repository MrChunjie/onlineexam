<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <base href="<%=basePath%>"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/content.css"/>
    <title>404</title>
</head>
<body>
<div class="col-md-12 col-lg-12 father">
    <div class="row">
        <div class="col-md-offset-2 col-md-4" style="padding-top: 50px">
            <img src="imgs/error1.png">
        </div>
        <div class="col-md-6">
            <div style="padding-top: 60px;">
                <span style="font-size:200px; font-family: 'AR CENA'">404</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-5">
            <h1 style="font-family: 'AR CENA'">页面找不到了</h1>
        </div>
    </div>
    <div class="row" style="padding-bottom: 80px;">
        <div class="col-md-offset-8" style="padding-top: 80px; font-family: 方正舒体">
            <a href="javascript: parent.location.reload();" style="font-size: 45px;color: #c9302c; text-decoration: none;">回到首页</a>
        </div>
    </div>
</div>
</body>
</html>

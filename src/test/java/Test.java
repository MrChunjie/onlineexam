import com.hpe.dao.*;
import com.hpe.pojo.Admin;
import com.hpe.pojo.Post;
import com.hpe.pojo.SClass;
import com.hpe.pojo.SGrade;
import com.hpe.service.PostService;
import com.hpe.vo.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Map;

/**
 * Created by chunjie on 2017/9/21.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class Test {

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private PostService postService;

    @Autowired
    private SGradeMapper sGradeMapper;

    @Autowired
    private ClassTeacherMapper classTeacherMapper;

    @Autowired
    private SClassMapper sClassMapper;

    @Autowired
    private GroupMapper groupMapper;

    @Autowired
    private ExamMapper examMapper;

    @Autowired
    private ClassHistoryMapper classHistoryMapper;

    @org.junit.Test
    public void test(){
        Admin admin = adminMapper.selectByPrimaryKey(1);
        assert admin == null;
    }

    @org.junit.Test
    public void test1(){
        List<SGrade> sGrades=sGradeMapper.selectByStudentId(1);
        System.out.println(sGrades.size());
    }

    @org.junit.Test
    public void test2(){
        List<Post> posts=postService.getAllPost();
        for(Post post:posts){
            System.out.println(post.toString());
        }
    }

    @org.junit.Test
    public void test3(){
        List<ClassTeacherVo> classTeacherVos=classTeacherMapper.findAll();
        for(ClassTeacherVo classTeacherVo:classTeacherVos){
            System.out.println(classTeacherVo.toString());
        }
    }

    @org.junit.Test
    public void test4(){
        List<SClass> sClasses=sClassMapper.getClassListByType(1);
        for(SClass sClass:sClasses){
            System.out.println(sClass.toString());
        }
    }

    @org.junit.Test
    public void test5(){
        List<GroupStuVo> groupStuVos=groupMapper.selectGroupStuVo(1);
        for(GroupStuVo groupStuVo:groupStuVos){
            System.out.println(groupStuVo.toString());
        }
        GroupVo groupVo=groupMapper.findGroupVo(1);
        System.out.println(groupVo.toString());
    }


    @org.junit.Test
    public void test6(){
        List<ExamVo> classExamVos=examMapper.findExamVoByTeacherId(1);
        for(ExamVo classExamVo:classExamVos){
            System.out.println(classExamVo.toString());
        }
    }

    @org.junit.Test
    public void test7(){
        List<StudentCareerClassVo> studentCareerClassVos=classHistoryMapper.getStudentClassCareer(64);
        for(StudentCareerClassVo studentCareerClassVo:studentCareerClassVos){
            System.out.println(studentCareerClassVo.toString());
        }
    }

    @org.junit.Test
    public void test8(){
        WordCloudVo wordCloudVo=sGradeMapper.getWordCloudCareer(67);
        System.out.println(wordCloudVo.toString());
    }

    @org.junit.Test
    public void test9(){
        List<StudentInfoAndGradeExcelVo> maps = groupMapper.getStudentInfoExcel("2014-11-01","2018-01-01");
        System.out.println(maps.size());
        for(StudentInfoAndGradeExcelVo studentInfoAndGradeExcelVo : maps){
            System.out.println(studentInfoAndGradeExcelVo);
        }
    }
    @org.junit.Test
    public void test11(){
        String str = ",";
        String[] split = str.split(",");
        System.out.println(split);
    }
}
